﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CsQuery.ExtensionMethods;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Extensions;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Common.Lang;
using MinecraftCubed.Minecraft.Models;
using MinecraftCubed.Static;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MinecraftCubed.Minecraft
{
    public static class MinecraftUtils
    {
        public static Task<AuthorizationResponse> AuthorizeAsync(string username, string password)
        {
            return Task.Run(() => Authorize(username, password));
        }

        public static AuthorizationResponse Authorize(string username, string password)
        {
            // try legacy method first.  see if we can get a proper case username
            /*
            for (var i = 0; i < 3; i++)
            {
                try
                {
                    using (var client = new WebClient())
                    {
                        var response = client.UploadValues("https://login.minecraft.net", new NameValueCollection
                        {
                            {
                                "user", username
                            },
                            {
                                "password", password
                            },
                            {
                                "version", "13"
                            }
                        });

                        var s = client.Encoding.GetString(response);

                        var split = s.Split(':');

                        if (split.Length >= 3)
                        {
                            username = split[2];
                        }
                    }
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                }
            }*/

            // try new method

            var http = (HttpWebRequest)WebRequest.Create(new Uri("https://authserver.mojang.com/authenticate"));

            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "POST";

            var parsedContent = JsonConvert.SerializeObject(new
            {
                clientToken = Guid.NewGuid().ToString(),
                username,
                password,
                agent = new
                {
                    name = "Minecraft",
                    version = 1
                }
            });

            var encoding = new ASCIIEncoding();
            var bytes = encoding.GetBytes(parsedContent);

            var newStream = http.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();

            try
            {
                var response = http.GetResponse();
                var stream = response.GetResponseStream();

                if (stream != null)
                {
                    var sr = new StreamReader(stream);
                    var content = sr.ReadToEnd();

                    var o = JObject.Parse(content);

                    if (o["selectedProfile"] != null)
                    {
                        var uuid = o["selectedProfile"]["id"].Value<string>();
                        username = o["selectedProfile"]["name"].Value<string>();

                        return new AuthorizationResponse
                        {
                            Success = true,
                            Username = username,
                            UUID = uuid
                        };
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    return new AuthorizationResponse
                    {
                        Success = false,
                        Message = Messages.MCBadLogin
                    };
                }
            }

            return new AuthorizationResponse
            {
                Success = false,
                Message = Messages.MCError
            };
        }

        public static string[] GetRandomHeadNames()
        {
            return new[]
            {
                "Konseince",
                "Korblox1134",
                "kornfarmer",
                "Korvic",
                "kotasuperhero",
                "KrayzieJ",
                "KrispyKslave",
                "Krusher548",
                "ktish",
                "ktran253",
                "kumilightbulb",
                "Kumpass_dodo",
                "Kumpass_Skater",
                "kylearmy",
                "L4YT0N",
                "Lachie1020",
                "Lady_Scarlet",
                "Latnem",
                "Lava_soldier",
                "lbvermillion",
                "LDSFlame",
                "Leafreo",
                "Leetskillz",
                "lekin680810",
                "lennon8467",
                "LeoFerari",
                "LexiBooHoo",
                "Lightbikemaster",
                "lilbuddhaman",
                "lilprplebnny",
                "lilymunroe",
                "Lil_Ch00b",
                "limeyman7",
                "lindsayboulanger",
                "LittleFirestar",
                "LittleMidget17",
                "llcyanidell",
                "Lleonidass",
                "Lockewiggen",
                "lodada1",
                "Lokthar328i",
                "LOLeannie",
                "Loliotaku",
                "LoneTonberry",
                "LootieLoo",
                "lora_snelson",
                "LordOTheSigns",
                "LoveGunner",
                "LOVETHEWHITE",
                "Lowen_Brau",
                "lucisiac",
                "LuckyChams",
                "M0nstrosity",
                "macmorris92",
                "madon47",
                "MadxRad",
                "mageg",
                "magicman112233",
                "MagmaDragon12",
                "Magyckmage25",
                "mag_wraith",
                "majam",
                "Major_Mayjor",
                "manjiggler",
                "MarcoTheItalian",
                "MarieC90",
                "martinez9",
                "masacardi",
                "masterw3",
                "matharama",
                "matt321123",
                "mattheus02",
                "Maverick_Beast",
                "maxrage",
                "maxxxafterhours",
                "mcarchitect22",
                "McGruber78",
                "McMichael96",
                "mcotterman",
                "mdc0923",
                "MELICENT",
                "meltedcup",
                "mercury199",
                "metalpants2",
                "MetroRoadWarrior",
                "Mewlver82",
                "Mezua",
                "MGBroadcast",
                "mhayes3",
                "midnightchan123",
                "mikey14",
                "Mimmy99",
                "MineMyles",
                "miner49burr",
                "minerman183",
                "Miner_Nick_M",
                "Mingpow321",
                "Misterz_Noodles",
                "Mitchsmom",
                "MjCbse",
                "mlp94",
                "mmitchell9",
                "Mobsterguy",
                "ModernDragoness",
                "monkeybidness",
                "MooMooutters",
                "moosehunter123",
                "morgan2041",
                "MortalWombat88",
                "MosesT",
                "motown2003",
                "mowat40",
                "mrbaconbitts",
                "MrBlahblah",
                "MrDragonBreath",
                "MrPumpkinMuffins",
                "MrSquirrelMan",
                "Mr_Boomer337",
                "MR_RANDOM999",
                "Mr_SneakyFace",
                "Mr_Sterling",
                "msbrun02",
                "mtndrew1",
                "Muddha",
                "MuffinOfFun",
                "MuffinSpankz",
                "murderrr",
                "murdock99",
                "myers121",
                "mylimo7",
                "mysticmock",
                "Mystronghold",
                "M_Boogie_C",
                "nacho30",
                "nanderson17",
                "Narcisism",
                "Navia",
                "nerdnut",
                "nerraj",
                "nevermines",
                "newmatt003",
                "Nexun",
                "ngennaro",
                "NICFREAK5577",
                "nicilbar",
                "Nick_O_Shlas",
                "Nighthawk354",
                "NightmareV",
                "Nikkisweety",
                "ninjaboy6728",
                "NinjaElite71",
                "nleroy",
                "nobrain98",
                "NoglasticNinja",
                "noisymanray",
                "nolarboot",
                "Noproblembrah",
                "Noresha",
                "nosnarbo",
                "novak189",
                "nr88hg",
                "nubkia",
                "Nyntoku",
                "Nzgbjunior",
                "obfalcons",
                "ObiWanHoshizaki",
                "ogot101",
                "ojpnobeast",
                "oK9power",
                "Old_Ninja",
                "OllyWilliams",
                "oMasterCole",
                "omegasnab1",
                "Omega_Haxors",
                "oOoCodeName2",
                "ooveNoIVIooJR",
                "owning254",
                "ozzyoverlord",
                "Palegreenpants",
                "Pantupino",
                "passiontiger74",
                "Pastabob",
                "patrick60316",
                "patsteirer",
                "payton18",
                "paznos6",
                "pbuelo",
                "PenguinKnight303",
                "permit101",
                "pfcwelch",
                "PhalseFire",
                "Phifft",
                "phil235",
                "PhreshMatt",
                "Pie2608",
                "pieman35333",
                "pikmin259",
                "pilot23456",
                "PinballWizardxX",
                "Pishe",
                "pjt0620",
                "plaunier",
                "plgxdark",
                "Poedew",
                "Pog_",
                "pomi44",
                "ponderius25",
                "poopypoopman",
                "porky9441",
                "posidanvzeus",
                "possesor1",
                "potassium0",
                "potassiumxthree",
                "POTATO_TOT",
                "Pozzumgee",
                "pres_rufus",
                "Preven",
                "Primordium",
                "prodigy901",
                "prophet19",
                "psychicmuffinpop",
                "pugsy",
                "PunisheR2404",
                "pwn78",
                "qentropy",
                "quinnstrong",
                "Quixoticus",
                "qzr31",
                "R0nBurgandy",
                "Rackshatta",
                "radioactivkitten",
                "Raid4Kill",
                "rakonei",
                "Random_NPC",
                "raphfireball99",
                "Rappin_Cubian",
                "Razgriz_Legend",
                "rcfreek13",
                "rctamura",
                "Reapergirl2",
                "RecycledVomit",
                "reddgirl27",
                "remsat10",
                "RhosID",
                "Rickbox",
                "riskwarlord",
                "ritzfizz",
                "rmckinstry_601",
                "robberbandit",
                "robertsar",
                "RobMayor",
                "Rob_25",
                "roetheverett",
                "rolf108",
                "roscoepig",
                "Rosdower",
                "Rubber_Duckies",
                "rube99",
                "Ryan6338",
                "ryancup",
                "Ryanorocks",
                "r_ninja14",
                "S4INT",
                "sac3nt3r",
                "SacredAssassin",
                "Sadderday",
                "saggyclamburger",
                "SaintsGamingPUB",
                "samlen",
                "Samrith",
                "Samuri_Bake_Pie",
                "sandieman100",
                "SannamWOOT",
                "saromon50",
                "Sasuke7677",
                "sasxxsnip3rxx",
                "Sayomie555",
                "Scarlet_wizard",
                "sccrfreak9",
                "scoobysnakcers",
                "SCORPION222",
                "scottbmx17",
                "scottish1900",
                "Scythe_001",
                "sdog180",
                "seanpr",
                "seaofnarum",
                "sebastianse3",
                "SecureUmbra",
                "Selastrovious",
                "seniorl",
                "SergeantCarew",
                "SergeMonster",
                "SergioXSigala",
                "seth6815",
                "sethallery",
                "Seventhcircle72",
                "sexymamegoma",
                "shadowtype",
                "shadowwarrior567",
                "shaerobbo",
                "Shake_NN_Blake",
                "shane2k3",
                "Sharitoncr18",
                "shoogax12",
                "shooterbooth",
                "Shotsfired",
                "shujaatali",
                "Sicknos",
                "Sidain",
                "Silentcricket",
                "SilverShadeFox",
                "SimplePancake",
                "SimplyGabriele",
                "simsiam",
                "sinisterevil",
                "Sir_Wyvernos",
                "sithlord28",
                "skatefallen",
                "skaterdjdude",
                "sKiLLxSn1p3z",
                "SkronyDog",
                "SkyXI",
                "SLanshe88",
                "Slic3man",
                "slingray120",
                "slong85",
                "Slurth",
                "Smartness",
                "SmDFrylock",
                "smileyface0",
                "Smiley_Riley12",
                "smokingoldenace",
                "SmplstcBllistic",
                "sneakysockpuppet",
                "snipermn12",
                "SnowBirds",
                "soar851",
                "SoDScoper",
                "soldger",
                "Sollomon666",
                "SolomonDeLugo",
                "Soonica",
                "Sostratus",
                "Sourful",
                "SOZman794",
                "Spacewolf78",
                "spalo",
                "SpazmaticSniper",
                "SpecOps298",
                "spider333",
                "Spinningbullet",
                "SpottedSnail",
                "sprite8182",
                "spwkiller",
                "spydermonkeyx",
                "SrgWallopy",
                "srs13",
                "Srsbizns",
                "stacey0811",
                "StalkerExecutor",
                "stanglemeir",
                "STaSHZILLA420",
                "SteveTech",
                "Steviewonder3",
                "stiason",
                "stokesinman",
                "Striker211",
                "Sultan_Mogroka",
                "Sunew",
                "sungetsu",
                "Sunshine3_14",
                "Sup3rtaco",
                "superdelux",
                "Superdoug",
                "supermedinita",
                "supersizefries"
            };
        }
    }
}
