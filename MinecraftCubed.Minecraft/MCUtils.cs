﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MinecraftCubed.Minecraft
{
    public static class MCUtils
    {
        public static String ReadString(Stream stream)
        {
            var buffer = new List<Byte>();
            do
            {
                var current = stream.ReadByte();
                if (current == -1 || current == 0)
                    break;
                buffer.Add(Convert.ToByte(current));
            }
            while (true);

            return new String(Encoding.ASCII.GetChars(buffer.ToArray()));
        }

        public static List<string> ReadDelimitedList(Stream stream)
        {
            var buffer = new List<Byte>();
            var list = new List<string>();
            int current = 0;
            do
            {
                if (current == -1)
                    break;

                do
                {
                    current = stream.ReadByte();
                    if (current == 0 || current == -1)
                        break;
                    buffer.Add(Convert.ToByte(current));
                }
                while (true);

                var str = new String(Encoding.ASCII.GetChars(buffer.ToArray()));

                if (str.Length > 0)
                {
                    list.Add(str);
                    buffer = new List<Byte>();
                }
            }
            while (true);
            return list;
        }

        public static List<string> ReadDoubleDelimited(Stream stream)
        {
            var buffer = new List<Byte>();
            var list = new List<string>();

            int current = 0;
            do
            {
                if (current == -1)
                    break;

                do
                {
                    current = stream.ReadByte();

                    if (current == -1)
                    {
                        break;
                    }

                    if (current == 0)
                    {
                        current = stream.ReadByte();

                        if (current == 0)
                        {
                            break;
                        }
                    }

                    buffer.Add(Convert.ToByte(current));
                }
                while (true);

                var str = new String(Encoding.ASCII.GetChars(buffer.ToArray()));

                if (str.Length > 0)
                {
                    list.Add(str);
                    buffer = new List<Byte>();
                }
            }
            while (true);
            return list;
        }

        public static bool ArrayEquals(ICollection<byte> first, IList<byte> second)
        {
            if (first.Count != second.Count)
                return false;

            return !first.Where((t, i) => t != second[i]).Any();
        }

        public static HashSet<string> ParsePlugins(string p)
        {
            var plugins = new HashSet<string>();

            if (p == null || p.Length < 5)
            {
                return plugins;
            }

            var start = p.IndexOf(":", StringComparison.Ordinal);

            p = p.Substring(start + 2);

            var split = p.Split(';');

            foreach (var plug in split)
            {
                var parts = plug.Trim().Split(' ');

                if (parts.Length == 0)
                {
                    continue;
                }

                // if it splits in two, discard the version

                if (parts.Length <= 2)
                {
                    plugins.Add(parts[0]);
                    continue;
                }

                // otherwise test to see if the middle parts are version or part of the name

                var good = new HashSet<string>();

                for (var i = parts.Length - 2; i > 0; i--)
                {
                    var part = parts[i].Trim();

                    if (!part.Any(char.IsDigit) &&
                        part != "-" &&
                        !part.Contains("<") &&
                        !part.Contains("(") &&
                        !part.Contains("["))
                    {
                        good.Add(part);
                    }
                }

                good.Add(parts[0]);
                plugins.Add(String.Join(" ", good.Reverse()));
            }

            return plugins;
        }
    }
}
