﻿namespace MinecraftCubed.Minecraft.Models
{
    public class AuthorizationResponse
    {
        public bool Success { get; set; }
        public string Username { get; set; }
        public string Message { get; set; }
        public string UUID { get; set; }
    }
}
