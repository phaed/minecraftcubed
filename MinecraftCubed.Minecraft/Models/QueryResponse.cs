﻿using System.Collections.Generic;

namespace MinecraftCubed.Minecraft.Models
{
    public class QueryResponse
    {
        public QueryResponse()
        {
            Players = new List<string>();
            Plugins = new HashSet<string>();
        }

        public int ServerId { get; set; }
        public string Version { get; set; }
        public int PlayersOnline { get; set; }
        public int MaxPlayers { get; set; }
        public List<string> Players { get; set; }
        public HashSet<string> Plugins { get; set; }
        public bool HasQueryDisabled { get; set; }
        public bool Success { get; set; }
    }
}
