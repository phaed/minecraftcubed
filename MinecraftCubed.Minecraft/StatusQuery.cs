﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Static;
using MinecraftCubed.Minecraft.Models;

namespace MinecraftCubed.Minecraft
{
    public static class StatusQuery
    {
        public static async Task<QueryResponse> PingAsync(string remoteHostName, int remotePort, Timeout timeout = Timeout.FifteenSeconds)
        {
            return await Task.Run(() => Ping(remoteHostName, remotePort, timeout));
        }

        public static async Task<QueryResponse> QueryAsync(string remoteHostName, int remotePort, Timeout timeout = Timeout.ThirtySeconds)
        {
            return await Task.Run(() => Query(remoteHostName, remotePort, timeout));
        }

        public static QueryResponse Query(string remoteHostName, int remotePort, Timeout timeout = Timeout.ThirtySeconds)
        {
            try
            {
                // connect to client

                using (var client = new UdpClient(remoteHostName, remotePort)
                {
                    Client =
                    {
                        SendTimeout = (int)timeout,
                        ReceiveTimeout = (int)timeout,
                    }
                })
                {

                    var hostEntry = Dns.GetHostEntry(remoteHostName);
                    var address = hostEntry.AddressList[0].GetAddressBytes();
                    var ipAddress = new IPAddress(address);

                    var remoteEndPoint = new IPEndPoint(ipAddress, remotePort);

                    // build handshake packet

                    var handshakeSend = new byte[]
                    {
                        0xFE, 0xFD,
                        0x09,
                        0x00, 0x00, 0x00, 0x00
                    };

                    // generate random session id

                    var rand = new byte[4];
                    var session = new byte[4];

                    var random = new Random();
                    random.NextBytes(rand);

                    // clear the high bits

                    for (var b = 0; b < 4; b++)
                    {
                        session[b] = (byte)(rand[b] & 0xF);
                    }

                    Array.Copy(session, 0, handshakeSend, 3, session.Length);

                    // send handshake

                    var bytesSent = client.Send(handshakeSend, handshakeSend.Length);

                    // receive payload

                    var handshakeResponse = client.Receive(ref remoteEndPoint);
                    var challengeTokenReader = new MemoryStream(handshakeResponse);

                    // check first byte

                    var challengeType = Convert.ToByte(challengeTokenReader.ReadByte());
                    // read the session id, should match

                    var responseId = new byte[4];
                    challengeTokenReader.Read(responseId, 0, responseId.Length);

                    if (!MCUtils.ArrayEquals(session, responseId))
                    {
                        Debug.WriteLine("uh-oh, the IDs have changed somehow. " +
                                        Encoding.Default.GetString(session) + " vs " +
                                        Encoding.Default.GetString(responseId));
                    }

                    // read the challange token and pack to int

                    var value = MCUtils.ReadString(challengeTokenReader);
                    var challengeToken = Convert.ToInt32(value);
                    Debug.WriteLine("Challenge Token: " + challengeToken);

                    // build request packet

                    var dataSendList = new List<byte>
                    {
                        0xFE,
                        0xFD,
                        0x00
                    };

                    // add in session id

                    dataSendList.AddRange(session);

                    Debug.WriteLine("Is Little Endian?" + BitConverter.IsLittleEndian);

                    var challengeBytes = BitConverter.GetBytes(challengeToken);

                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(challengeBytes);
                    }

                    // add in challange

                    dataSendList.AddRange(challengeBytes);

                    // add padding

                    dataSendList.AddRange(new List<byte>
                    {
                        0x00,
                        0x00,
                        0x00,
                        0x00
                    });

                    // send request

                    var getDataSend = dataSendList.ToArray();
                    var bytesSent2 = client.Send(getDataSend, getDataSend.Length);

                    // receive response

                    var statusResponse = client.Receive(ref remoteEndPoint);
                    var reader = new MemoryStream(statusResponse, false)
                    {
                        Position = 0
                    };

                    // Status type

                    var statusType = reader.ReadByte();

                    // Session id

                    var idStatusResponse = new byte[4];
                    reader.Read(idStatusResponse, 0, idStatusResponse.Length);

                    // garbage

                    var garbage = new byte[11];
                    reader.Read(garbage, 0, garbage.Length);

                    var hash = new Dictionary<string, string>();

                    // key values

                    while (hash.Count < 10)
                    {
                        hash.Add(MCUtils.ReadString(reader), MCUtils.ReadString(reader));
                    }

                    // garbage

                    MCUtils.ReadString(reader);
                    MCUtils.ReadString(reader);
                    MCUtils.ReadString(reader);

                    // collect players

                    var players = MCUtils.ReadDelimitedList(reader);

                    // parse plugins

                    var plugins = MCUtils.ParsePlugins(hash["plugins"]);

                    // return data

                    return new QueryResponse
                    {
                        Version = GeneralHelper.FormatVersion(hash["version"]),
                        Plugins = plugins,
                        PlayersOnline = Convert.ToInt32(hash["numplayers"]),
                        MaxPlayers = Convert.ToInt32(hash["maxplayers"]),
                        Players = players,
                        Success = true
                    };
                }
            }
            catch (Exception)
            {
                // some error
            }

            return null;
        }

        public static QueryResponse Ping(string remoteHostName, int remotePort, Timeout timeout = Timeout.FifteenSeconds)
        {
            try
            {
                // assemble payload

                var payload = new List<byte>
                {
                    0xFE,
                    0x01,
                    0xFA,
                    0x00,
                    0x0B
                };

                var pingHost = Encoding.BigEndianUnicode.GetBytes("MC|PingHost");

                var host = Encoding.BigEndianUnicode.GetBytes(remoteHostName);

                var port = BitConverter.GetBytes(remotePort);

                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(port);
                }

                var hostLength = BitConverter.GetBytes((short)(remoteHostName.Length));

                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(hostLength);
                }

                var dataLength = BitConverter.GetBytes((short)(1 + hostLength.Length + host.Length + port.Length));

                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(dataLength);
                }

                payload.AddRange(pingHost);
                payload.AddRange(dataLength);
                payload.Add(0x4A);
                payload.AddRange(hostLength);
                payload.AddRange(host);
                payload.AddRange(port);

                // connect to client

                using (var client = new TcpClient
                {
                    Client =
                    {
                        SendTimeout = (int)timeout,
                        ReceiveTimeout = (int)timeout,
                    }
                })
                {
                    // send payload

                    client.Connect(remoteHostName, remotePort);

                    var stream = client.GetStream();

                    stream.Write(payload.ToArray(), 0, payload.Count);

                    var hex = BitConverter.ToString(payload.ToArray());
                    Debug.Write(hex.Replace("-", " ") + " ");

                    // receive response

                    var kick = stream.ReadByte();

                    if (kick != 0xFF)
                    {
                        return null;
                    }

                    // garbage

                    stream.ReadByte();
                    stream.ReadByte();

                    stream.ReadByte();
                    stream.ReadByte();
                    stream.ReadByte();

                    var list = MCUtils.ReadDoubleDelimited(stream);

                    return new QueryResponse
                    {
                        Version = list[2],
                        PlayersOnline = Convert.ToInt32(list[4]),
                        MaxPlayers = Convert.ToInt32(list[5]),
                        Success = true
                    };
                }
            }
            catch (Exception)
            {
                // some error
            }

            return null;
        }
    }
}
