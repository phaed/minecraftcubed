﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;

namespace MinecraftCubed.Minecraft
{
    public class VotifierQuery
    {
        public static async Task<bool> VoteAsync(string type, string remoteHostName, int remotePort, string username, string publicKey)
        {
            return await Task.Run(() => Vote(type, remoteHostName, remotePort, username, publicKey));
        }

        public static byte[] EncryptRSABlock(string vote, string key)
        {
            var decoratedKey = "-----BEGIN PUBLIC KEY-----\n" + key + "\n-----END PUBLIC KEY-----";

            var bytes = new byte[245];
            Encoding.UTF8.GetBytes(vote, 0, vote.Length, bytes, 0);

            var pu = new PemReader(new StringReader(decoratedKey));
            var rsaKeyParameters = (RsaKeyParameters)pu.ReadObject();
            
            var cipher = CipherUtilities.GetCipher("RSA/ECB/PKCS1Padding");
            cipher.Init(true, rsaKeyParameters);
            return cipher.DoFinal(bytes);
        }

        public static bool Vote(string type, string remoteHostName, int remotePort, string username, string publicKey)
        {
            try
            {
                // connect to client
                var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                var hostEntry = Dns.GetHostEntry(remoteHostName);
                var address = hostEntry.AddressList[0].GetAddressBytes();
                var ipAddress = new IPAddress(address);
                var remoteEndPoint = new IPEndPoint(ipAddress, remotePort);

                socket.Connect(remoteEndPoint);

                // send payload

                var bytes = new byte[1024];
                var size = socket.Receive(bytes);
                var message = Encoding.ASCII.GetString(bytes, 0, size);

                if (message.Substring(0, 8) != "VOTIFIER")
                {
                    return false;
                }

                var vote = "VOTE\nminecraftcubed.net|"+type+"\n" + username + "\n" + ipAddress + "\n" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");

                var data = EncryptRSABlock(vote, publicKey);

                socket.Send(data);
                return true;
            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }

            return false;
        }
    }
}
