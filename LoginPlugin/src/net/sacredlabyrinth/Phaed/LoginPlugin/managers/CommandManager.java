package net.sacredlabyrinth.Phaed.LoginPlugin.managers;

import net.sacredlabyrinth.Phaed.LoginPlugin.CallTask;
import net.sacredlabyrinth.Phaed.LoginPlugin.LoginPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandManager implements CommandExecutor
{
    private final LoginPlugin plugin;

    public CommandManager(LoginPlugin plugin)
    {
        this.plugin = plugin;
    }


    /**
     * @param sender
     * @param command
     * @param label
     * @param args
     * @return
     */
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        if (command.getName().equals("verify"))
        {
            verify((Player) sender);
            return true;
        }

        return false;
    }

    public void verify(Player player)
    {
        player.sendRawMessage(ChatColor.AQUA + " Verifying...");

        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new CallTask(plugin, player), 0L);
    }
}
