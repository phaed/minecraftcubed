package net.sacredlabyrinth.Phaed.LoginPlugin;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.logging.Level;

public class CallTask implements Runnable
{
    private Plugin plugin;
    private Player player;

    public CallTask(Plugin plugin, Player player)
    {
        this.plugin = plugin;
        this.player = player;
    }

    public void run()
    {
        try
        {
            String response = postUrl();

            if (response.contains("True"))
            {
                player.sendRawMessage(ChatColor.AQUA + "Your MinecraftCubed account has been verified.");
                player.kickPlayer("Your MinecraftCubed account has been verified.");
            }
            plugin.getLogger().log(Level.INFO, "Response... " + response);
        }
        catch (Exception e)
        {
            plugin.getLogger().log(Level.SEVERE, "[" + player.getName() + " | " + player.getUniqueId() + "] error while verifying: " + e.getMessage());
        }
    }

    private String postUrl() throws Exception
    {
        String key = "e2dsfghjh3r234f34f34f4817a62c30ca97e0dsadasda";
        String body = "verify";
        String url = "https://minecraftcubed.net/auth/verifyendpoint/" + URLEncoder.encode(key, "UTF-8") + "/" + URLEncoder.encode(player.getUniqueId().toString(), "UTF-8") + "/" + URLEncoder.encode(player.getName(), "UTF-8");

        plugin.getLogger().log(Level.INFO, "Posting: " + url);
        plugin.getLogger().log(Level.INFO, "Body: " + body);

        URL oracle = new URL(url);
        URLConnection conn = oracle.openConnection();
        ((HttpURLConnection)conn).setRequestMethod("POST");

        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setUseCaches(false);
        conn.setRequestProperty("charset", "utf-8");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(body.length()));

        // Create I/O streams
        DataOutputStream  outStream = new DataOutputStream(conn.getOutputStream());

        // Send request
        outStream.writeBytes(body);
        outStream.flush();
        outStream.close();

        BufferedReader inStream = new BufferedReader(new InputStreamReader(conn.getInputStream()));

        // Get Response
        String inputLine;
        String result = "";
        while ((inputLine = inStream.readLine()) != null)
        {
            result += inputLine;
        }

        // Close I/O streams
        inStream.close();
        outStream.close();

        return result;
    }
}