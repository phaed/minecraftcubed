package net.sacredlabyrinth.Phaed.LoginPlugin.listeners;


import net.sacredlabyrinth.Phaed.LoginPlugin.LoginPlugin;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class LoginPluginEventListener implements Listener
{
    private final LoginPlugin plugin;

    /**
     * @param plugin
     */
    public LoginPluginEventListener(LoginPlugin plugin)
    {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerLogin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();
        player.sendRawMessage(ChatColor.AQUA + " Welcome to the MinecraftCubed account verification server.");
        player.sendRawMessage(ChatColor.AQUA + " Type " + ChatColor.WHITE + "/verify" + ChatColor.AQUA + " to confirm that you own the " + player.getName() + " account.");
    }

    @EventHandler
    public void onRemainingAirChange(Player player, int old)
    {
        player.setRemainingAir(9001);
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event)
    {
        if ((event.getEntity() instanceof Player))
        {
            Player player = (Player) event.getEntity();
            player.setHealth(20);
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event)
    {
        if (((event.getEntity() instanceof Player)))
        {
            event.setCancelled(true);
        }
    }
}
