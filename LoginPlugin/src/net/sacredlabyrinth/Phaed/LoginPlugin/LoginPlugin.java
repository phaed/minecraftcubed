package net.sacredlabyrinth.Phaed.LoginPlugin;

import net.sacredlabyrinth.Phaed.LoginPlugin.listeners.LoginPluginEventListener;
import net.sacredlabyrinth.Phaed.LoginPlugin.managers.CommandManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

/**
 * LoginPlugin for Bukkit
 *
 * @author Phaed
 */
public class LoginPlugin extends JavaPlugin
{
    public static Logger log;

    @Override
    public void onEnable()
    {
        log = Logger.getLogger("Minecraft");

        CommandManager cm = new CommandManager(this);
        getCommand("verify").setExecutor(cm);

        getServer().getPluginManager().registerEvents(new LoginPluginEventListener(this), this);

        log.info("[" + this.getDescription().getName() + "] version [" + this.getDescription().getVersion() + "] loaded");
    }
}
