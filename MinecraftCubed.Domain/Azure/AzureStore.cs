﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using MinecraftCubed.Common;
using StructureMap;

namespace MinecraftCubed.Domain.Azure
{
    public class AzureStore : IAzureStore
    {
        private async Task<CloudBlobContainer> GetContainer(string container)
        {
            var blobClient = CloudStorageAccount.Parse(Settings.AzureConnectionString).CreateCloudBlobClient();
            var containerReference = blobClient.GetContainerReference(container);

            if (!await containerReference.ExistsAsync().ConfigureAwait(false))
            {
                await containerReference.CreateAsync().ConfigureAwait(false);
                await containerReference.SetPermissionsAsync(new BlobContainerPermissions
                {
                    PublicAccess = BlobContainerPublicAccessType.Blob
                }).ConfigureAwait(false);
            }

            return containerReference;
        }

        public static IAzureStore Current
        {
            get
            {
                return ObjectFactory.GetInstance<IAzureStore>();
            }
        }

        public async Task<bool> Store(string container, string filename, Stream stream)
        {
            if (Settings.InDevelopment)
            {
                return true;
            }

            try
            {
                // reset stream in case its pointing at end
                stream.Position = 0;

                var cont = await GetContainer(container).ConfigureAwait(false);

                var blockBlob = cont.GetBlockBlobReference(filename);
                await blockBlob.UploadFromStreamAsync(stream).ConfigureAwait(false);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> StoreIfNotExists(string container, string filename, Stream stream)
        {
            if (Settings.InDevelopment)
            {
                return true;
            }

            try
            {
                // reset stream in case its pointing at end
                stream.Position = 0;

                var cont = await GetContainer(container).ConfigureAwait(false);

                var blockBlob = cont.GetBlockBlobReference(filename);

                if (!(await blockBlob.ExistsAsync().ConfigureAwait(false)))
                {
                    await blockBlob.UploadFromStreamAsync(stream).ConfigureAwait(false);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<byte[]> GetBytes(string container, string filename)
        {
            if (Settings.InDevelopment)
            {
                return null;
            }

            try
            {
                var cont = await GetContainer(container).ConfigureAwait(false);

                var blockBlob = cont.GetBlockBlobReference(filename);

                blockBlob.FetchAttributes();
                var fileByteLength = blockBlob.Properties.Length;
                var fileContent = new byte[fileByteLength];
                for (var i = 0; i < fileByteLength; i++)
                {
                    fileContent[i] = 0x20;
                }
                await blockBlob.DownloadToByteArrayAsync(fileContent, 0);
                return fileContent;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<bool> Delete(string container, string filename)
        {
            if (Settings.InDevelopment)
            {
                return true;
            }

            try
            {
                var cont = await GetContainer(container).ConfigureAwait(false);

                var blockBlob = cont.GetBlockBlobReference(filename);

                if (await blockBlob.DeleteIfExistsAsync())
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }
    }
}
