﻿using System.IO;
using System.Threading.Tasks;

namespace MinecraftCubed.Domain.Azure
{
    public interface IAzureStore
    {
        Task<bool> Store(string container, string filename, Stream stream);
        Task<bool> StoreIfNotExists(string container, string filename, Stream stream);
        Task<byte[]> GetBytes(string container, string filename);
        Task<bool> Delete(string container, string filename);
    }
}
