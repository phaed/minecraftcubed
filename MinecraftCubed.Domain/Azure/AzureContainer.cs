﻿
namespace MinecraftCubed.Domain.Azure
{
    public class AzureContainer
    {
        public static string MinecraftHeads
        {
            get { return "minecraftheads"; }
        }
         
        public static string MinecraftServerLogos
        {
            get { return "minecraftserverlogos"; }
        }

        public static string DefaultBanners
        {
            get { return "defaultbanners"; }
        }

        public static string CategoryBanners
        {
            get { return "categorybanners"; }
        }

        public static string MinecraftBanners
        {
            get { return "minecraftbanners"; }
        }

        public static string MinecraftMiniGames
        {
            get { return "minecraftminigames"; }
        }

        public static string MinecraftEventBanners
        {
            get { return  "minecrafteventbanners"; }
        }

        public static string MinecraftFeaturedBanners
        {
            get { return "minecraftfeaturedbanners"; }
        }

        public static string MinecraftHeroes
        {
            get { return "minecraftheroes"; }
        }

        public static string MinecraftPlugins
        {
            get { return "minecraftplugins"; }
        }

        public static string MinecraftModpacks
        {
            get { return "minecraftmodpacks"; }
        }
    }
}
