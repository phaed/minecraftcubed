﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftCubed.Domain.Models
{
    public class ServerConnectInfo
    {
        public int ServerId { get; set; }
        public string Address { get; set; }
        public int Port{ get; set; }
        public string QueryInfoAddress { get; set; }
        public int QueryInfoPort{ get; set; }
    }
}
