﻿namespace MinecraftCubed.Domain.Models.List
{
    public class ListServer
    {
        public int Id { get; set; }
        public string I { get; set; }
        public string N { get; set; }
        public string Ve { get; set; }
        public bool OFF { get; set; }
        public int A { get; set; }
        public int O { get; set; }
        public int[] PD { get; set; }
        public int P { get; set; }
        public string[] TS { get; set; }
        public int[] UD { get; set; }
        public string CA { get; set; }
        public string CN { get; set; }
        public string LA { get; set; }
        public string B { get; set; }
        public string FB { get; set; }
        public string IP { get; set; }
        public string DL { get; set; }
        public string DU { get; set; }
        public bool HD { get; set; }
        public bool HW { get; set; }
        public string WU { get; set; }
        public int BV { get; set; }
    }
}