﻿
using System.Collections.Generic;

namespace MinecraftCubed.Domain.Models.List
{
    public class ListResult
    {
        public ICollection<ListServer> OrganicList { get; set; }
        public int TotalCount { get; set; }
        public string RequestId { get; set; }
        public bool EndOfList { get; set; }
    }
}
