﻿using System.Collections.Generic;
using System.Linq;

namespace MinecraftCubed.Domain.Models.List
{
    public class ListState
    {
        public ICollection<string> Types { get; set; }
        public ICollection<string> Gameplays { get; set; }
        public ICollection<string> MiniGames { get; set; }
        public ICollection<string> Plugins { get; set; }
        public ICollection<string> Modpacks { get; set; }
        public ICollection<string> Names { get; set; }
        public ICollection<string> Descriptions { get; set; }
        public string Version { get; set; }
        public string Country { get; set; }
        public string Language { get; set; }
        public string Sort { get; set; }  // omited from serialization below on purpose (so caching can match regardless or sort)

        public override string ToString()
        {
            var str = string.Empty;

            if (Types != null)
            {
                str = ":T:" + Types.OrderBy(x => x).Aggregate((x, y) => x + "," + y);
            }
            if (Gameplays != null)
            {
                str = ":G:" + Gameplays.OrderBy(x => x).Aggregate(str, (x, y) => x + "," + y);
            }
            if (MiniGames != null)
            {
                str = ":MG:" + MiniGames.OrderBy(x => x).Aggregate(str, (x, y) => x + "," + y);
            }
            if (Plugins != null)
            {
                str = ":P:" + Plugins.OrderBy(x => x).Aggregate(str, (x, y) => x + "," + y);
            }
            if (Modpacks != null)
            {
                str = ":MP:" + Modpacks.OrderBy(x => x).Aggregate(str, (x, y) => x + "," + y);
            }
            if (Names != null)
            {
                str = ":N:" + Names.OrderBy(x => x).Aggregate(str, (x, y) => x + "," + y);
            }
            if (Descriptions != null)
            {
                str = ":D:" + Descriptions.OrderBy(x => x).Aggregate(str, (x, y) => x + "," + y);
            }

            if (!string.IsNullOrEmpty(Version))
            {
                str += ":V:" + Version;
            }
            if (!string.IsNullOrEmpty(Country))
            {
                str += ":C:" + Country;
            }
            if (!string.IsNullOrEmpty(Language))
            {
                str += ":L:" + Language;
            }

            if (str.Length > 0)
            {
                return str.Substring(1);
            }
            return str;
        }
    }
}
