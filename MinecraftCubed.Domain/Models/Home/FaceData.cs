﻿
using System;

namespace MinecraftCubed.Domain.Models.Home
{
    public class FaceData
    {
        public string Username { get; set; }
        public string Url { get; set; }
        public byte[] Bytes { get; set; }
    }
}