﻿using System.Collections.Generic;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Models.Page
{
    public class PageServer
    {
        public ICollection<SubServerData> SubsServers { get; set; }
        public string Identifier { get; set; }
        public string Name { get; set; }
        public string DarkHeaderClass { get; set; }
        public string DarkMiddleClass { get; set; }
        public string DarkBackgroundClass { get; set; }
        public string LogoUrl { get; set; }
        public bool ShuffleImages { get; set; }
        public string LogoTransparentBottomClass { get; set; }
        public bool VideosFirst { get; set; }
        public string DescriptionText { get; set; }
        public string CountryAbbreviation { get; set; }
        public string CountryName { get; set; }
        public string LanguageName { get; set; }
        public string PlayerCountsString { get; set; }
        public string UptimeDataString { get; set; }
        public string BusiestTime { get; set; }
        public int OnlinePlayerCount { get; set; }
        public int Activity { get; set; }
        public int PeakPlayerCount { get; set; }
        public bool ShowPlugins { get; set; }
        public bool HasLinks { get; set; }
        public Website Website { get; set; }
        public Color Color { get; set; }
        public ICollection<Hero> Heroes { get; set; }
        public ICollection<Video> Videos { get; set; }
    }
}
