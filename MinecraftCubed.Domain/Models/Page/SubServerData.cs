﻿using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Models.Page
{
    public class SubServerData
    {
        public string Name { get; set; }
        public string Ip { get; set; }
        public bool HasDownload { get; set; }
        public string DownloadLabel { get; set; }
        public string DownloadUrl { get; set; }
        public bool HasWhitelist { get; set; }
        public string WhitelistUrl { get; set; }
    }
}
