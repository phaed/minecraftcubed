﻿namespace MinecraftCubed.Domain.Models.Page.Datas
{
    public class ModpackData
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Website { get; set; }
        public string LogoUrl { get; set; }
    }
}
