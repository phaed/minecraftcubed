﻿namespace MinecraftCubed.Domain.Models.Page.Datas
{
    public class NameData
    {
        public string Name { get; set; }
        public int Group { get; set; }
    }
}
