﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftCubed.Domain.Models.Page.Datas
{
    public class StaffData
    {
        public string Username { get; set; }
        public string Rank { get; set; }
        public string Quote { get; set; }
        public string Color { get; set; }
        public string FaceUrl { get; set; }
    }
}
