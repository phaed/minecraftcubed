﻿namespace MinecraftCubed.Domain.Models.Page.Datas
{
    public class MiniGameData
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Website { get; set; }

        public string LogoUrl { get; set; }
        public string EmbedUrl { get; set; }
        public bool HasVideo { get; set; }
    }
}
