﻿
using System.Collections.Generic;
using MinecraftCubed.Domain.Models.Backend;
using MinecraftCubed.Domain.Models.Page.Datas;

namespace MinecraftCubed.Domain.Models.Page
{
    public class PageData
    {
        public int ServerId { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsManager { get; set; }
        public bool IsStaff { get; set; }
        public string Color { get; set; }
        public string LogoUrl { get; set; }
        public string Identifier { get; set; }
        public string Version { get; set; }
        public string IP { get; set; }
        public int Port { get; set; }
        public IEnumerable<NameData> ServerTypes { get; set; }
        public IEnumerable<NameData> Gameplay { get; set; }
        public IEnumerable<PluginData> Plugins { get; set; }
        public IEnumerable<MiniGameData> MiniGames { get; set; }
        public IEnumerable<ModpackData> Modpacks { get; set; }
        public IEnumerable<StaffData> Staff { get; set; }
        public bool Logged { get; set; }
        public object TagData { get; set; }
        public bool Verified { get; set; }
        public List<List<string>> InitialChat { get; set; }
        public string Username { get; set; }
    }
}
