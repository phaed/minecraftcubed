﻿namespace MinecraftCubed.Domain.Models.Backend
{
    public class ServerStaffData
    {
        public string Username { get; set; }
        public string FaceUrl { get; set; }
        public string Rank { get; set; }
        public string Color { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsManager { get; set; }
        public bool IsStaff { get; set; }
        public string Quote { get; set; }
    }
}