﻿using System.ComponentModel.DataAnnotations;

namespace MinecraftCubed.Domain.Models.Backend
{
    public class ServerPluginData
    {
        public int ServerPluginId { get; set; }
        public bool Enabled { get; set; }
        public bool IsAuthor { get; set; }
        public string PluginIdentifier { get; set; }
        public string PluginName { get; set; }
        [MaxLength(350)]
        public string PluginDescription { get; set; }
        [MaxLength(255)]
        public string PluginWebsite { get; set; }
        [MaxLength(255)]
        public string PluginLogoUrl { get; set; }
    }
}
