﻿using System.ComponentModel.DataAnnotations;

namespace MinecraftCubed.Domain.Models.Backend
{
    public class ServerConnectionInfoData
    {
        public int ServerConnectionInfoId { get; set; }
        public string ConnectionInfoIdentifier { get; set; }
        public int ConnectionInfoConnectionInfoId { get; set; }
        [Required]
        public string ConnectionInfoName { get; set; }
        [Required]
        public string ConnectionInfoAddress { get; set; }
        public int ConnectionInfoPort { get; set; }
        public bool ConnectionInfoPlayReqHasDownload { get; set; }
        public string ConnectionInfoPlayReqDownloadLabel { get; set; }
        [Url]
        public string ConnectionInfoPlayReqDownloadUrl { get; set; }

        public bool ConnectionInfoPlayReqHasWhitelist { get; set; }
        [Url]
        public string ConnectionInfoPlayReqWhitelistUrl { get; set; }
        public string GetFriendlyIp()
        {
            var ip = ConnectionInfoAddress;

            if (ConnectionInfoPort != 25565)
            {
                ip += ":" + ConnectionInfoPort;
            }

            return ip;
        }
    }
}
