﻿using System.ComponentModel.DataAnnotations;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Static;

namespace MinecraftCubed.Domain.Models.Backend
{
    public class ServerModpackData
    {
        public int ServerModpackId { get; set; }
        public bool Enabled { get; set; }
        public bool Delete { get; set; }
        public bool IsAuthor { get; set; }

        /**
         *  [Required] removed due to modpacks that already exist need to be able to post empty
         */

        public string ModpackIdentifier { get; set; }
        public string ModpackName { get; set; }
        [MaxLength(1024)]
        public string ModpackDescription { get; set; }
        [MaxLength(1024)]
        public string ModpackWebsite { get; set; }
        [MaxLength(255)]
        public string ModpackLogoUrl { get; set; }
        public string GetAbsoluteUrl(bool breakCache)
        {
            if (string.IsNullOrEmpty(ModpackLogoUrl))
            {
                return string.Empty;
            } 
            
            var cache = "";

            if (breakCache)
            {
                cache = "?c=" + EncryptionHelper.GenerateConfirmationCode(8);
            }
            
            return Constant.AzureStorageUrl + Bucket.MinecraftModpacks + "/" + ModpackIdentifier + ".png" + cache;
        }
    }
}
