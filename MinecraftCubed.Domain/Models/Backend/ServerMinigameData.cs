﻿using System.ComponentModel.DataAnnotations;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Static;

namespace MinecraftCubed.Domain.Models.Backend
{
    public class ServerMiniGameData
    {
        public int ServerMiniGameId { get; set; }
        public bool Delete { get; set; }
        public string MiniGameIdentifier { get; set; }
        public string ServerIdentifier { get; set; }
        [Required]
        public string MiniGameName { get; set; }
        [MaxLength(1024)]
        [Required]
        public string Description { get; set; }
        [MaxLength(1024)]
        public string Website { get; set; }
        [MaxLength(255)]
        [Required]
        public string LogoUrl { get; set; }
        public string EmbedUrl { get; set; }
        public bool HasVideo { get; set; }

        public string GetAbsoluteUrl(bool breakCache)
        {
            var cache = "";

            if (breakCache)
            {
                cache = "?c=" + EncryptionHelper.GenerateConfirmationCode(8);
            }

            return Constant.AzureStorageUrl + Bucket.MinecraftMiniGames + "/" + ServerIdentifier + "-" + MiniGameIdentifier + ".png" + cache;
        }
    }
}
