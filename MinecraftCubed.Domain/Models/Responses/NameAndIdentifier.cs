﻿
namespace MinecraftCubed.Domain.Models.Responses
{
    public class NameAndIdentifier
    {
        public string Name { get; set; }
        public string Identifier { get; set; }
    }
}
