﻿
namespace MinecraftCubed.Domain.Models.Responses
{
    public class ValueIdModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Extra { get; set; }
    }
}
