﻿
using System.Collections.Generic;

namespace MinecraftCubed.Domain.Models.Events
{
    public class EventResult
    {
        public ICollection<ListEvent> List { get; set; }
        public int TotalCount { get; set; }
        public string RequestId { get; set; }
        public bool EndOfList { get; set; }
    }
}
