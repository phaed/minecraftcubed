﻿
using System.Linq;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Models.Events
{
    public class ListEvent
    {
        public int Id { get; set; } // id
        public string N { get; set; } // name
        public string AD { get; set; } // address
        public int P { get; set; } // port
        public string D { get; set; } // description
        public string B { get; set; } // banner url
        public string SN { get; set; } // server name
        public string EI { get; set; } // event identifier
        public string SI { get; set; } // server identifier
        public string SD { get; set; } // start date
        public string ED { get; set; } // end date
        public int L { get; set; } // length in hours
        public string A { get; set; } // attendees
        public int AC { get; set; } // attendee count
        public bool F { get; set; } // featured
        public bool HD { get; set; } // HasDownload
        public string DL { get; set; } // DownloadLabel
        public string DU { get; set; } // DownloadUrl
        public bool HW { get; set; } // HasWhitelist
        public string WU { get; set; } // WhitelistUrl
        public string CA { get; set; } // CountryAbbreviation
        public string CN { get; set; } // CountryName
        public string LA { get; set; } // Language
        public string V { get; set; } // Version
        public bool HR { get; set; } // HasRules
        public string RU { get; set; } // RulesUrl
        public bool HAW { get; set; } // HAsAttendeeWhitelist
        public int BV { get; set; } // Banner version
    }
}