﻿using System.Collections.Generic;
using System.Linq;

namespace MinecraftCubed.Domain.Models.Events
{
    public class EventsState
    {
        public ICollection<string> EventGameplays { get; set; }
        public string Country { get; set; }
        public string Language { get; set; }
        public string Sort { get; set; } // omited from serialization on purpose (so caching can match regardless or sort)

        public override string ToString()
        {
            var str = string.Empty;

            if (EventGameplays != null)
            {
                str = ":EG:" + EventGameplays.OrderBy(x => x).Aggregate(str, (x, y) => x + "," + y);
            }

            if (!string.IsNullOrEmpty(Country))
            {
                str += ":C:" + Country;
            }
            if (!string.IsNullOrEmpty(Language))
            {
                str += ":L:" + Language;
            }

            if (str.Length > 0)
            {
                return str.Substring(1);
            }
            return str;
        }
    }
}
