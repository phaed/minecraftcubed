﻿using System.Collections.Generic;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Models.Stub
{
    public class UserStubs
    {
        public User User { get; set; }
        public List<Context.Stub> Stubs { get; set; }
        public List<Event> Events { get; set; }
    }
}