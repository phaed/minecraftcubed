﻿namespace MinecraftCubed.Domain.Models
{
    public class UserSettings
    {
        public bool ReceiveEventNotifications { get; set; }
    }
}
