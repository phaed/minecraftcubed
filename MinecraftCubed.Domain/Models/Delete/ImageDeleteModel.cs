﻿
namespace MinecraftCubed.Domain.Models.Delete
{
    public class ImageDeleteModel
    {
        public int Id { get; set; }
        public string Identifier { get; set; }
        public string Key { get; set; }
    }
}
