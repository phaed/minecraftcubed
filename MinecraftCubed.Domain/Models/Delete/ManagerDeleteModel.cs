﻿
namespace MinecraftCubed.Domain.Models.Delete
{
    public class ManagerDeleteModel
    {
        public string Username { get; set; }
        public string Identifier { get; set; }
    }
}
