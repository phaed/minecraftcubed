﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftCubed.Domain.Models.Delete
{
    public class StubDeleteModel
    {
        public int Id { get; set; }
        public string Key { get; set; }
    }
}
