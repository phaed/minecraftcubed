﻿namespace MinecraftCubed.Domain.Models.Tags
{
    public class GeneralTagData
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
