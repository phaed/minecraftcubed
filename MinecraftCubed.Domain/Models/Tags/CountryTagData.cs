﻿namespace MinecraftCubed.Domain.Models.Tags
{
    public class CountryTagData
    {
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public int Count { get; set; }
    }
}
