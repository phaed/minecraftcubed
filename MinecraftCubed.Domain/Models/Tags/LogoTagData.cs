﻿namespace MinecraftCubed.Domain.Models.Tags
{
    public class LogoTagData
    {
        public string Name { get; set; }
        public string LogoUrl { get; set; }
        public string Description { get; set; }
        public int Count { get; set; }
    }
}
