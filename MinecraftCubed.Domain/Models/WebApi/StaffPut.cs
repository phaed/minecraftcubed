﻿
namespace MinecraftCubed.Domain.Models.WebApi
{
    public class StaffPut
    {
        public string ApiKey { get; set; }
        public string Username { get; set; }
        public string Rank { get; set; }
        public string Color { get; set; }
        public string Quote { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsManager { get; set; }
    }
}
