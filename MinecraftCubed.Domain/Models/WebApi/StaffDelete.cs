﻿
namespace MinecraftCubed.Domain.Models.WebApi
{
    public class StaffDelete
    {
        public string ApiKey { get; set; }
        public string Username { get; set; }
    }
}
