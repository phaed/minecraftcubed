﻿using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Redis.Interfaces;
using MinecraftCubed.Static;

namespace MinecraftCubed.Domain.Redis.Extensions
{
    public static class BatchingExtensions
    {
        public static void ClearServerCaches(this IRedisStore source, string identifier)
        {
            source.Remove(Constant.Cache.BuiltServer + identifier, RedisDatabase.BuiltObjects);
            source.Remove(Constant.Cache.PageData + identifier, RedisDatabase.BuiltObjects);
            source.Remove(Constant.Cache.PageServer + identifier, RedisDatabase.BuiltObjects);
        }
        
        public static void ClearEventCaches(this IRedisStore source, Event eve)
        {
            foreach (var date in eve.EventDates)
            {
                source.Remove(Constant.Cache.EventDate + date.EventDateId, RedisDatabase.BuiltObjects);
            }
        }
    }
}
