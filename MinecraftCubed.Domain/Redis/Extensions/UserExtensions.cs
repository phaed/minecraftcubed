﻿using System;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Domain.Redis.Interfaces;

namespace MinecraftCubed.Domain.Redis.Extensions
{
    public static class UserExtensions
    {
        public static T UserGetOrStore<T>(this IRedisStore source, string key, Func<T> fetch, TimeSpan expiredIn)
        {
            return source.GetOrStore(key + "_" + UserHelper.GetUserIdentity(), fetch, expiredIn, RedisDatabase.User);
        }

        public static void UserRemove(this IRedisStore source, string key)
        {
            source.Remove(key + "_" + UserHelper.GetUserIdentity(), RedisDatabase.User);
        }
    }
}
