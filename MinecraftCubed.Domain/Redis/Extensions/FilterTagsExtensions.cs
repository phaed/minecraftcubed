﻿using System;
using MinecraftCubed.Domain.Redis.Interfaces;
using MinecraftCubed.Static;

namespace MinecraftCubed.Domain.Redis.Extensions
{
    public static class FilterTagsExtensions
    {
        public static T FilterTagHashGetOrStore<T>(this IRedisStore source, string key, string state, Func<T> fetch)
        {
            return source.HashGetOrStore(key, state, fetch, RedisDatabase.FilterTags);
        }

        public static void FilterTagsRemove(this IRedisStore source, string key)
        {
            source.Remove(key, RedisDatabase.FilterTags);
        }

        public static void ClearFilterCaches(this IRedisStore source)
        {
            source.FilterTagsRemove(Constant.Cache.Filters.Countries);
            source.FilterTagsRemove(Constant.Cache.Filters.Languages);
            source.FilterTagsRemove(Constant.Cache.Filters.ServerTypes);
            source.FilterTagsRemove(Constant.Cache.Filters.Gameplay);
            source.FilterTagsRemove(Constant.Cache.Filters.MiniGames);
            source.FilterTagsRemove(Constant.Cache.Filters.Modpacks);
            source.FilterTagsRemove(Constant.Cache.Filters.Plugins);
            source.FilterTagsRemove(Constant.Cache.Filters.Versions);
        }
    }
}