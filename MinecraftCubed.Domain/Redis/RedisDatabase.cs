﻿
namespace MinecraftCubed.Domain.Redis
{
    public enum RedisDatabase
    {
        User = 1,
        BuiltObjects = 2,
        ChatUsers = 3,
        FilterTags = 4,
        Heads = 5,
        General = 10,
        SessionState = 15,
    }
}
