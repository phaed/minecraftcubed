﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MinecraftCubed.Domain.Redis.Interfaces
{
    public interface IRedisStore
    {
        void Expire(string key, TimeSpan expireIn, RedisDatabase db = RedisDatabase.General);
        bool Exists(string key, RedisDatabase db = RedisDatabase.General);
        T Get<T>(string key, RedisDatabase db = RedisDatabase.General);
        Task<T> GetAsync<T>(string key, RedisDatabase db = RedisDatabase.General);
        void Set<T>(string key, T value, TimeSpan? expiredIn, RedisDatabase db = RedisDatabase.General);
        Task SetAsync<T>(string key, T value, TimeSpan? expiredIn, RedisDatabase db = RedisDatabase.General);
        bool Remove(string key, RedisDatabase db = RedisDatabase.General);
        T GetOrStore<T>(string key, Func<T> fetch, TimeSpan? expiredIn, RedisDatabase db = RedisDatabase.General);

        Task<T> GetOrStoreAsync<T>(string key, Func<Task<T>> fetch, TimeSpan? expiredIn, RedisDatabase db = RedisDatabase.General);

        Task<bool> HashDeleteAsync(string key, RedisDatabase db = RedisDatabase.General);
        long HashLength(string key, RedisDatabase db = RedisDatabase.General);
        bool HashExistsName(string key, string name, RedisDatabase db = RedisDatabase.General);
        void HashSetItem<T>(string key, string name, T value, RedisDatabase db = RedisDatabase.General);
        bool HashRemoveItem(string key, string name, RedisDatabase db = RedisDatabase.General);
        T HashGetItem<T>(string key, string name, RedisDatabase db = RedisDatabase.General);
        List<string> HashGetNamesList(string key, RedisDatabase db = RedisDatabase.General);
        List<T> HashGetValuesList<T>(string key, RedisDatabase db = RedisDatabase.General);
        T HashGetOrStore<T>(string key, string name, Func<T> fetch, RedisDatabase db = RedisDatabase.General);

        void ListSetValue<T>(string key, string item, RedisDatabase db = RedisDatabase.General);
        long ListRemoveValue<T>(string key, T value, RedisDatabase db = RedisDatabase.General);
        List<T> ListGetAll<T>(string key, RedisDatabase db = RedisDatabase.General);
    }
}
