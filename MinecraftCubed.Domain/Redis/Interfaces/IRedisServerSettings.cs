﻿namespace MinecraftCubed.Domain.Redis.Interfaces
{
    public interface IRedisServerSettings
    {
        bool PreferSlaveForRead { get; }
        string ConnectionStringOrName { get; }
        int DefaultDb { get; }
    }
}
