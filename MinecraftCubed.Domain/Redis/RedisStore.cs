﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Redis.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using StackExchange.Redis;
using StructureMap;

namespace MinecraftCubed.Domain.Redis
{
    public class RedisStore : IRedisStore
    {
        private readonly CommandFlags _readFlag;
        private readonly JsonSerializerSettings _settings;
        private readonly IRedisConnectionWrapper _connection;

        public RedisStore(IRedisConnectionWrapper connectionWrapper, IRedisServerSettings settings)
        {
            _connection = connectionWrapper;

            _settings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Include,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                //ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            };

            _readFlag = settings.PreferSlaveForRead ? CommandFlags.PreferSlave : CommandFlags.PreferMaster;
        }

        public static IRedisStore Current
        {
            get
            {
                return ObjectFactory.GetInstance<IRedisStore>();
            }
        }

        public void Expire(string key, TimeSpan expireIn, RedisDatabase db = RedisDatabase.General)
        {
            _connection.Database((int)db).KeyExpire(key, expireIn);
        }

        public bool Exists(string key, RedisDatabase db = RedisDatabase.General)
        {
            return _connection.Database((int)db).KeyExists(key, _readFlag);
        }

        public T Get<T>(string key, RedisDatabase db = RedisDatabase.General)
        {
            var obj = _connection.Database((int)db).StringGet(key, CommandFlags.PreferSlave);

            if (obj.IsNull)
                return default(T);

            return JsonConvert.DeserializeObject<T>(obj, _settings);
        }

        public void Set<T>(string key, T value, TimeSpan? expiredIn, RedisDatabase db = RedisDatabase.General)
        {
            _connection.Database((int)db).StringSet(key, JsonConvert.SerializeObject(value, _settings), expiredIn);
        }

        public async Task<T> GetAsync<T>(string key, RedisDatabase db = RedisDatabase.General)
        {
            var obj = await _connection.Database((int)db).StringGetAsync(key, _readFlag).ConfigureAwait(false);

            if (obj.IsNull)
                return default(T);

            return JsonConvert.DeserializeObject<T>(obj, _settings);
        }
        
        public async Task SetAsync<T>(string key, T value, TimeSpan? expiredIn, RedisDatabase db = RedisDatabase.General)
        {
            await _connection.Database((int)db).StringSetAsync(key, JsonConvert.SerializeObject(value, _settings), expiredIn).ConfigureAwait(false);
        }

        public bool Remove(string key, RedisDatabase db = RedisDatabase.General)
        {
            return _connection.Database((int)db).KeyDelete(key);
        }

        public T GetOrStore<T>(string key, Func<T> fetch, TimeSpan? expiredIn, RedisDatabase db = RedisDatabase.General)
        {
            if (Settings.RedisCachingDisabled)
            {
                return fetch.Invoke();
            }

            var value = Get<T>(key, db);

            if (!EqualityComparer<T>.Default.Equals(value, default(T)))
                return value;

            value = fetch.Invoke();

            if (!EqualityComparer<T>.Default.Equals(value, default(T)))
                Set(key, value, expiredIn, db);

            return value;
        }

        public async Task<T> GetOrStoreAsync<T>(string key, Func<Task<T>> fetch, TimeSpan? expiredIn, RedisDatabase db = RedisDatabase.General)
        {
            if (Settings.RedisCachingDisabled)
            {
                return await fetch.Invoke().ConfigureAwait(false);
            }

            var value = await GetAsync<T>(key, db).ConfigureAwait(false);

            if (!EqualityComparer<T>.Default.Equals(value, default(T)))
                return value;

            value = await fetch.Invoke().ConfigureAwait(false);

            if (!EqualityComparer<T>.Default.Equals(value, default(T)))
                await SetAsync(key, value, expiredIn, db).ConfigureAwait(false);

            return value;
        }

        /**********************************************************************************************************************************************************************************/

        public async Task<bool> HashDeleteAsync(string key, RedisDatabase db = RedisDatabase.General)
        {
            return await _connection.Database((int)db).KeyDeleteAsync(key).ConfigureAwait(false);
        }

        public long HashLength(string key, RedisDatabase db = RedisDatabase.General)
        {
            return _connection.Database((int)db).HashLength(key, _readFlag);
        }

        public bool HashExistsName(string key, string name, RedisDatabase db = RedisDatabase.General)
        {
            return _connection.Database((int)db).HashExists(key, name, _readFlag);
        }

        public void HashSetItem<T>(string key, string name, T value, RedisDatabase db = RedisDatabase.General)
        {
            _connection.Database((int)db).HashSet(key, new[] { new HashEntry(name, JsonConvert.SerializeObject(value, _settings)) });
        }
        
        public bool HashRemoveItem(string key, string name, RedisDatabase db = RedisDatabase.General)
        {
            return _connection.Database((int)db).HashDelete(key, name);
        }

        public T HashGetItem<T>(string key, string name, RedisDatabase db = RedisDatabase.General)
        {
            var obj = _connection.Database((int)db).HashGet(key, name, _readFlag);

            if (obj.IsNull)
                return default(T);

            return JsonConvert.DeserializeObject<T>(obj, _settings);
        }

        public List<string> HashGetNamesList(string key, RedisDatabase db = RedisDatabase.General)
        {
            return _connection.Database((int)db).HashGetAll(key, _readFlag).Select(x => x.Name.ToString()).ToList();
        }

        public List<T> HashGetValuesList<T>(string key, RedisDatabase db = RedisDatabase.General)
        {
            return _connection.Database((int)db).HashGetAll(key, _readFlag).Select(x => JsonConvert.DeserializeObject<T>(x.Value, _settings)).ToList();
        }

        public T HashGetOrStore<T>(string key, string name, Func<T> fetch, RedisDatabase db = RedisDatabase.General)
        {
            if (Settings.RedisCachingDisabled)
            {
                return fetch.Invoke();
            }

            var value = HashGetItem<T>(key, name, db);

            if (!EqualityComparer<T>.Default.Equals(value, default(T)))
                return value;

            value = fetch.Invoke();

            if (!EqualityComparer<T>.Default.Equals(value, default(T)))
                HashSetItem(key, name, value, db);

            return value;
        }

        /**********************************************************************************************************************************************************************************/

        public void ListSetValue<T>(string key, string value, RedisDatabase db = RedisDatabase.General)
        {
            _connection.Database((int)db).ListLeftPush(key, JsonConvert.SerializeObject(value, _settings));
        }

        public long ListRemoveValue<T>(string key, T value, RedisDatabase db = RedisDatabase.General)
        {
            return _connection.Database((int)db).ListRemove(key, JsonConvert.SerializeObject(value, _settings));
        }

        public List<T> ListGetAll<T>(string key, RedisDatabase db = RedisDatabase.General)
        {
            return _connection.Database((int)db).ListRange(key).Select(x => JsonConvert.DeserializeObject<T>(x, _settings)).ToList();
        }
    }
}
