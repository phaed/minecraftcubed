﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Redis.Interfaces;
using StackExchange.Redis;

namespace MinecraftCubed.Domain.Redis
{
    public class RedisConnectionWrapper : IRedisConnectionWrapper
    {
        private readonly IRedisServerSettings _settings;
        private readonly Lazy<string> _connectionString;

        private volatile ConnectionMultiplexer _connection;
        private readonly object _lock = new object();

        public RedisConnectionWrapper(IRedisServerSettings settings)
        {
            _settings = settings;

            _connectionString = new Lazy<string>(GetConnectionString);
        }

        private string GetConnectionString()
        {
            var con = ConfigurationManager.ConnectionStrings[_settings.ConnectionStringOrName];

            return con == null ? _settings.ConnectionStringOrName : con.ConnectionString;
        }

        private ConnectionMultiplexer GetConnection()
        {
            if (_connection != null && _connection.IsConnected)
                return _connection;

            lock (_lock)
            {
                if (_connection != null && _connection.IsConnected)
                    return _connection;

                if (_connection != null)
                {
                    Trace.TraceInformation("Connection disconnected. Disposing connection...");
                    _connection.Close(false);
                    _connection.Dispose();
                    _connection = null;
                }

                Trace.TraceInformation("Creating new instance of Redis Connection");

                var options = ConfigurationOptions.Parse(_connectionString.Value);
                options.ClientName = "MinecraftCubed";
                options.AllowAdmin = true;
                _connection = ConnectionMultiplexer.Connect(options);
            }

            return _connection;
        }

        public IDatabase Database(int? db = null)
        {
            return GetConnection().GetDatabase(db ?? _settings.DefaultDb);
        }

        public IServer Server(EndPoint endPoint)
        {
            return GetConnection().GetServer(endPoint);
        }

        public EndPoint[] GetEndpoints()
        {
            return GetConnection().GetEndPoints();
        }

        public void FlushDb(int? db = null)
        {
            var endPoints = GetEndpoints();

            foreach (var endPoint in endPoints)
            {
                Server(endPoint).FlushDatabase(db ?? _settings.DefaultDb);
            }
        }

        public void Dispose()
        {
            if (_connection != null)
            {
                _connection.Dispose();
            }
        }
    }
}
