﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Repositories.Interfaces
{
    public interface IPluginRepository : IRepository
    {
        void AddServerPlugin(int serverId, ServerPlugin plugin);
        void DeleteServerPlugin(ServerPlugin plugin);
        bool ExistsPlugin(string name);
        Plugin GetPluginByIdentifier(string name);
        Plugin GetPluginById(int id);
        IQueryable<ServerPlugin> GetServerPlugins(int serverId);
        IQueryable<Plugin> GetPlugins();
    }
}