﻿using System.Collections.Generic;
using System.Linq;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Repositories.Interfaces
{
    public interface IHomeRepository : IRepository
    {
        IQueryable<User> GetLatestVisitingUsers(int count);
    }
}