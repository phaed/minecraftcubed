﻿using System.Collections.Generic;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models.Stub;

namespace MinecraftCubed.Domain.Repositories.Interfaces
{
    public interface IStubRepository : IRepository
    {
        Server GetServerFromStub(string accessCode);
        Stub GetStub(string accessCode);
        IEnumerable<UserStubs> GetStubEditors();
    }
}