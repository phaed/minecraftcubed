﻿
using System;
using System.Collections.Generic;
using System.Linq;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models;

namespace MinecraftCubed.Domain.Repositories.Interfaces
{
    public interface IServerRepository : IRepository
    {
        IQueryable<Server> GetAllServers();
        bool ExistsServer(string address, int port);
        bool ExistsServer(string identifier);
        bool ExistsIdentifier(string id);
        Server GetServerByIdentifier(string identifier);
        Server GetServerForPage(string identifier);
        int GetServerIdFromIdentifier(string identifier);
        string GetIdentifierFromServerId(int serverId);
        Server GetServerById(int id);
        IQueryable<ServerConnectInfo> GetAllServerConnectInfos();
        ServerConnectInfo GetServerConnectInfo(int serverId);
        Server GetServerForList(int serverId);
        IQueryable<Server> GetEnabledServersForPinger();
        IQueryable<int> GetMatchingEnabledServerIds(
            IEnumerable<string> types,
            IEnumerable<string> gameplays,
            IEnumerable<string> minigames,
            IEnumerable<string> plugins,
            IEnumerable<string> modpacks,
            string version,
            string country,
            string language);

        IQueryable<Server> GetServersWithType(IEnumerable<int> serverIds, ICollection<string> tags);
        IQueryable<Server> GetServersWithGameplay(IEnumerable<int> serverIds, ICollection<string> tags);
        IQueryable<Server> GetServersWithMiniGame(IEnumerable<int> serverIds, ICollection<string> tags);
        IQueryable<Server> GetServersWithPlugin(IEnumerable<int> serverIds, ICollection<string> tags);
        IQueryable<Server> GetServersWithMod(IEnumerable<int> serverIds, ICollection<string> tags);
        IQueryable<Server> GetServersWithVersion(IEnumerable<int> serverIds, string tag);
        IQueryable<Server> GetServersWithCountry(IEnumerable<int> serverIds, string tag);
        IQueryable<Server> GetServersWithLanguage(IEnumerable<int> serverIds, string tag);

        bool HasCreatedEvent(int serverId);
    }
}
