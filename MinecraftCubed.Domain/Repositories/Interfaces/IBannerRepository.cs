﻿using System.Linq;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Repositories.Interfaces
{
    public interface IBannerRepository : IRepository
    {
        void DeleteHero(int heroId);
        void DeleteVideo(int videoId);

        int GetHeroCount(int serverId);

        IQueryable<Hero> GetHeroes(int serverId);
        IQueryable<Video> GetVideos(int serverId);

        int GetNextHeroPosition(int serverId);
        int GetNextVideoPosition(int serverId);
    }
}