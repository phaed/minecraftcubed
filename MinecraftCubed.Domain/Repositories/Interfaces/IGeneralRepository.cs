﻿using System.Collections.Generic;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Repositories.Interfaces
{
    public interface IGeneralRepository : IRepository
    {
        void AddServerPing(ServerPing ping);
    }
}