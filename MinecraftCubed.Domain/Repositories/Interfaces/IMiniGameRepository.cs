﻿using System.Linq;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Repositories.Interfaces
{
    public interface IMiniGameRepository : IRepository
    {
        int GetNextMiniGamePosition(int serverId);
        void AddServerMiniGame(ServerMiniGame minigame);
        void DeleteUnusedMiniGames();
        MiniGame FindMiniGame(string identifier);
        IQueryable<MiniGame> GetMiniGames();
        IQueryable<ServerMiniGame> GetServerMiniGames();
        IQueryable<MiniGame> FindMiniGames(string name);

        ServerMiniGame FindServerMiniGame(int serverId, string identifier);
        IQueryable<ServerMiniGame> GetServerMiniGames(int serverId);
    }
}