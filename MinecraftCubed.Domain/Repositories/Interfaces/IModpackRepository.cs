﻿using System.Linq;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Repositories.Interfaces
{
    public interface IModpackRepository : IRepository
    {
        int GetNextModpackPosition(int serverId);
        void AddServerModpack(int serverId, ServerModpack modpack);
        void DeleteUnusedModpacks();
        Modpack FindModpack(string identifier);
        IQueryable<ServerModpack> GetServerModpacks(int serverId);
        IQueryable<Modpack> GetModpacks();
        IQueryable<ServerModpack> GetServerModpacks();
        IQueryable<Modpack> FindModpacks(string name);
        bool ExistsModpack(string name);
    }
}