﻿using System.Collections.Generic;
using System.Linq;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Repositories.Interfaces
{
    public interface ITagsRepository : IRepository
    {
        IQueryable<Country> GetAllCountries();
        IQueryable<Host> GetAllHosts();
        
        Type GetTypeByName(string name);
        EventTag GetEventTagByName(string name);
        Gameplay GetGameplayByName(string name);
        Version GetVersionByName(string name);
        Country GetCountryByName(string name);
        Language GetLanguageByName(string name);
        Host GetHostByName(string name);

        IQueryable<Type> GetTypes(int serverId);
        IQueryable<EventTag> GetEventTags(int eventId);
        IQueryable<Gameplay> GetGameplays(int serverId);
        
        bool DeleteUnusedTypes();
        bool DeleteUnusedEventTags();
        bool DeleteUnusedGameplays();
        bool DeleteUnusedVersions();
        bool DeleteUnusedLanguages();
        bool DeleteUnusedHosts();
        
        void DeleteServerTypes(int serverId);
        void DeleteEventEventTags(int eventId);
        void DeleteServerGameplays(int serverId);
    }
}