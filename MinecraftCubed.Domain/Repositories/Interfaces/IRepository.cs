﻿using System;
using System.Collections.Generic;

namespace MinecraftCubed.Domain.Repositories.Interfaces
{
    public interface IRepository
    {
        void DisableProxyCreation();
        void Add<TEntity>(TEntity entity) where TEntity : class;
        void Delete<TEntity>(TEntity entity) where TEntity : class;
        bool Delete<TEntity>(Func<TEntity, Boolean> where) where TEntity : class;
        bool Exists<TEntity>(Func<TEntity, Boolean> where) where TEntity : class;
        int Count<TEntity>(Func<TEntity, Boolean> where) where TEntity : class;
        TEntity GetById<TEntity>(int id) where TEntity : class;
        TEntity Get<TEntity>(Func<TEntity, Boolean> where) where TEntity : class;
        IEnumerable<TEntity> GetAll<TEntity>() where TEntity : class;
        IEnumerable<TEntity> GetWhere<TEntity>(Func<TEntity, bool> where) where TEntity : class;
        void DeleteRange<TEntity>(Func<TEntity, bool> where) where TEntity : class;
        void ExecuteSqlCommand(string sql);
    }
}