﻿using System.Collections.Generic;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Repositories.Interfaces
{
    public interface IChatRepository : IRepository
    {
        IEnumerable<ChatLine> GetChat(int serverId, int skip, int take);
    }
}