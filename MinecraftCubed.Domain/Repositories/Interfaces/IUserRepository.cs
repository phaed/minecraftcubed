﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Repositories.Interfaces
{
    public interface IUserRepository : IRepository
    {
        User GetUserByEmail(string email);
        User GetUserByUsername(string username);
        bool ExistsUserByEmail(string email);
        bool ExistsUserByUsername(string username);
        bool ExistsUsernameIgnoreUnverified(string username);
        User FindUnverifiedDupe(string username);
        bool ExistsUserByUUID(string uuid);
        Role GetRoleById(int id);
        IQueryable<User> FindUsers(string name);
        string GenerateAnonUsername();
    }
}