﻿using System.Linq;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Repositories.Interfaces
{
    public interface IConnectionInfoRepository : IRepository
    {
        int GetNextConnectionInfoPosition(int serverId);
        void AddServerConnectionInfo(ServerConnectionInfo info);
        void DeleteServerConnectionInfo(string identifier);

        ConnectionInfo FindConnectionInfo(string identifier);
        IQueryable<ConnectionInfo> GetConnectionInfos();
        bool ExistsConnectionInfo(string name, string address, int port);
        IQueryable<ServerConnectionInfo> GetServerConnectionInfos(int serverId);
        void DeleteConnectionInfo(int id);
    }
}