﻿using System.Collections.Generic;
using System.Linq;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Repositories.Interfaces
{
    public interface IStaffRepository : IRepository
    {
        void DeleteStaff(string identifier, string username);
        ServerStaff GetStaff(string identifier, string username);
        IQueryable<ServerStaff> GetStaff(string identifier);
        bool IsServerManager(string identifier, string username);
        bool IsServerAdmin(string identifier, string username);
        bool IsServerStaff(string identifier, string username);
        bool IsAnyServerManager(string username);
        bool IsAnyServerAdmin(string username);
        int GetAdminCount(string identifier);
        bool AdminsServer(int userId, string identifier);
        bool StaffsServer(int userId, string identifier);
        bool ManagesServer(int userId, string identifier);
        bool ManagesEvent(int userId, string identifier);
        Server GetManagedServerByIdentifier(int userId, string identifier);
        Event GetManagedEventByIdentifier(int userId, string identifier);
        Server GetFirstManagedServer(int userId);
        Event GetFirstManagedEvent(int userId);
        IQueryable<Server> GetManagedServers(int userId);
        IQueryable<Event> GetManagedEvents(int userId);
    }
}