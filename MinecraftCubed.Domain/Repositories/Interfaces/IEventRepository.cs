﻿
using System.Collections.Generic;
using System.Linq;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Repositories.Interfaces
{
    public interface IEventRepository : IRepository
    {
        bool ExistsEvent(string name);
        bool ExistsIdentifier(string id);
        Event GetEventFromId(int eventId);
        IQueryable<int> GetActiveEvenDatetIds(int take);
        IQueryable<int> GetLatestEventDateIds(int take);
        IQueryable<int> GetLatesWithDupestEventDateIds(int take);
        IQueryable<Event> GetAllEvents();

        void AddEventDate(EventDate ed);
        void DeleteEventDate(int eventDateId);
        bool IsPastEventDate(int eventDateId);

        void DeleteEvent(Event eve);

        IQueryable<int> GetMatchingEventDateIds(
            IEnumerable<string> gameplays,
            string country,
            string language);

        EventDate GetEventDate(int eventDateId);
        EventDate GetEventDateForList(int eventDateId);

        IQueryable<EventDate> GetLiveEventDatesWithEventGameplay(IEnumerable<int> eventIds, IEnumerable<string> tags);
        IQueryable<EventDate> GetLiveEventDatesWithCountry(IEnumerable<int> eventIds, string tag);
        IQueryable<EventDate> GetLiveEventDatesWithLanguage(IEnumerable<int> eventIds, string tag);

        Event GetEventByIdentifier(string eventIdentifier);
        int GetEventIdFromIdentifier(string eventIdentifier);
        string GetIdentifierFromEventId(int eventId);

        void DeleteEventAttendee(EventAttendee ea);

        ICollection<string> GetEventDateAttendees(int eventDateId);
    }
}
