﻿using System;
using System.Linq;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;

namespace MinecraftCubed.Domain.Repositories
{
    public class ConnectionInfoRepository : Repository, IConnectionInfoRepository
    {
        public ConnectionInfoRepository(IDatabaseFactory databaseFactory) : base(databaseFactory) { }

        public void AddServerConnectionInfo(ServerConnectionInfo minigame)
        {
            try
            {
                DataContext.ServerConnectionInfos.Add(minigame);
            }
            catch
            {
                // ignore unique rule on serverId+modId which may popup due to concurrency issues
            }
        }

        public void DeleteServerConnectionInfo(string identifier)
        {
            var info = DataContext.ServerConnectionInfos.FirstOrDefault(x => x.ConnectionInfo.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));
            
            if (info != null)
            {
                DataContext.ServerConnectionInfos.Remove(info);
            }
        }

        public ConnectionInfo FindConnectionInfo(string identifier)
        {
            return DataContext.ConnectionInfos.FirstOrDefault(x => x.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));
        }

        public IQueryable<ConnectionInfo> GetConnectionInfos()
        {
            return DataContext.ConnectionInfos.OrderBy(x => x.Name);
        }

        public bool ExistsConnectionInfo(string name, string address, int port)
        {
            return DataContext.ConnectionInfos.Any(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase) && x.Address.Equals(address, StringComparison.InvariantCultureIgnoreCase) && x.Port == port);
        }
        
        public IQueryable<ServerConnectionInfo> GetServerConnectionInfos(int serverId)
        {
            return DataContext.ServerConnectionInfos
                .Include("ConnectionInfo")
                .Where(x => x.ServerId == serverId)
                .OrderBy(x => x.Position);
        }

        public int GetNextConnectionInfoPosition(int serverId)
        {
            var lastItem = DataContext.ServerConnectionInfos.Where(x => x.ServerId == serverId).OrderByDescending(x => x.Position).FirstOrDefault();

            if (lastItem != null)
            {
                return lastItem.Position + 1;
            }

            return 1;
        }

        public void DeleteConnectionInfo(int id)
        {
            var info = DataContext.ConnectionInfos.Find(id);

            if (info != null)
            {
                var infos = info.ServerConnectionInfos.ToList();
                var req = info.PlayReq;

                foreach (var serverInfo in infos)
                {
                    DataContext.ServerConnectionInfos.Remove(serverInfo);
                }

                DataContext.ConnectionInfos.Remove(info);
                DataContext.PlayReqs.Remove(req);
            }
        }
    }
}
