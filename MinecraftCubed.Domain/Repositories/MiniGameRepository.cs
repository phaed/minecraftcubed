﻿using System;
using System.Linq;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Azure;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;

namespace MinecraftCubed.Domain.Repositories
{
    public class MiniGameRepository : Repository, IMiniGameRepository
    {
        public MiniGameRepository(IDatabaseFactory databaseFactory) : base(databaseFactory) { }

        public void AddServerMiniGame(ServerMiniGame minigame)
        {
            try
            {
                DataContext.ServerMiniGames.Add(minigame);
            }
            catch
            {
                // ignore unique rule on serverId+modId which may popup due to concurrency issues
            }
        }

        public void DeleteUnusedMiniGames()
        {
            var unused = DataContext.MiniGames.Where(x => !x.ServerMiniGames.Any());

            foreach (var minigame in unused)
            {
                DataContext.MiniGames.Remove(minigame);
            }
        }
        
        public MiniGame FindMiniGame(string identifier)
        {
            return DataContext.MiniGames.FirstOrDefault(x => x.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));
        }

        public IQueryable<MiniGame> GetMiniGames()
        {
            return DataContext.MiniGames.OrderBy(x => x.Name);
        }

        public IQueryable<ServerMiniGame> GetServerMiniGames()
        {
            return DataContext.ServerMiniGames
                .Include("Server")
                .Include("MiniGame");
        }

        public IQueryable<MiniGame> FindMiniGames(string name)
        {
            return DataContext.MiniGames
                .Where(x => x.Name.ToLower().Contains(name.ToLower().Trim()))
                .OrderBy(x => x.Name);
        }

        public ServerMiniGame FindServerMiniGame(int serverId, string identifier)
        {
            return DataContext.ServerMiniGames
                .Where(x => x.ServerId == serverId)
                .FirstOrDefault(x => x.MiniGame.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));
        }

        public IQueryable<ServerMiniGame> GetServerMiniGames(int serverId)
        {
            return DataContext.ServerMiniGames
                .Include("MiniGame")
                .Include("Server")
                .Where(x => x.ServerId == serverId)
                .OrderBy(x => x.Position);
        }

        public int GetNextMiniGamePosition(int serverId)
        {
            var lastItem = DataContext.ServerMiniGames.Where(x => x.ServerId == serverId).OrderByDescending(x => x.Position).FirstOrDefault();

            if (lastItem != null)
            {
                return lastItem.Position + 1;
            }

            return 1;
        }
    }
}
