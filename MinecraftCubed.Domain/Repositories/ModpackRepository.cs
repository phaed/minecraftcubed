﻿using System;
using System.Linq;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;

namespace MinecraftCubed.Domain.Repositories
{
    public class ModpackRepository : Repository, IModpackRepository
    {
        public ModpackRepository(IDatabaseFactory databaseFactory) : base(databaseFactory) { }

        public void AddServerModpack(int serverId, ServerModpack modpack)
        {
            try
            {
                DataContext.ServerModpacks.Add(modpack);
            }
            catch
            {
                // ignore unique rule on serverId+modId which may popup due to concurrency issues
            }
        }

        public void DeleteUnusedModpacks()
        {
            var unused = DataContext.Modpacks.Where(x => x.ServerModpacks.Count == 0);

            foreach (var host in unused)
            {
                DataContext.Modpacks.Remove(host);
            }
        }

        public Modpack FindModpack(string identifier)
        {
            return DataContext.Modpacks.FirstOrDefault(x => x.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));
        }

        public IQueryable<ServerModpack> GetServerModpacks(int serverId)
        {
            return DataContext.ServerModpacks
                .Include("Modpack")
                .Include("Server")
                .Where(x => x.ServerId == serverId)
                .OrderBy(x => x.Position);
        }

        public IQueryable<Modpack> GetModpacks()
        {
            return DataContext.Modpacks.OrderBy(x => x.Name);
        }

        public IQueryable<ServerModpack> GetServerModpacks()
        {
            return DataContext.ServerModpacks
                .Include("Server")
                .Include("Modpack");
        }

        public IQueryable<Modpack> FindModpacks(string name)
        {
            return DataContext.Modpacks
                .Where(x => x.Name.ToLower().Contains(name.ToLower().Trim()))
                .OrderBy(x => x.Name);
        }
        public int GetNextModpackPosition(int serverId)
        {
            var lastItem = DataContext.ServerModpacks.Where(x => x.ServerId == serverId).OrderByDescending(x => x.Position).FirstOrDefault();

            if (lastItem != null)
            {
                return lastItem.Position + 1;
            }

            return 1;
        }
        public bool ExistsModpack(string name)
        {
            return DataContext.Modpacks.Any(x => x.Name.ToLower().Equals(name.ToLower().Trim()));
        }
    }
}
