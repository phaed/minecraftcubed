﻿using System;
using System.Linq;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;

namespace MinecraftCubed.Domain.Repositories
{
    public class PluginRepository : Repository, IPluginRepository
    {
        public PluginRepository(IDatabaseFactory databaseFactory) : base(databaseFactory) { }

        public void AddServerPlugin(int serverId, ServerPlugin plugin)
        {
            try
            {
                DataContext.ServerPlugins.Add(plugin);
            }
            catch
            {
                // ignore unique rule on serverId+pluginId which may popup due to concurrency issues
            }
        }

        public void DeleteServerPlugin(ServerPlugin plugin)
        {
            DataContext.ServerPlugins.Remove(plugin);
        }
        
        public bool ExistsPlugin(string name)
        {
            return DataContext.Plugins.Any(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public Plugin GetPluginByIdentifier(string identifier)
        {
            return DataContext.Plugins.FirstOrDefault(x => x.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));
        }

        public Plugin GetPluginById(int id)
        {
            return DataContext.Plugins.Find(id);
        }

        public IQueryable<ServerPlugin> GetServerPlugins(int serverId)
        {
            return DataContext.ServerPlugins
                .Include("Plugin")
                .Where(x => x.ServerId == serverId)
                .OrderBy(x => x.Plugin.Name);
        }

        public IQueryable<Plugin> GetPlugins()
        {
            return DataContext.Plugins;
        }
    }
}
