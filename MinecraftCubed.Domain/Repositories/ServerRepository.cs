﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Static;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;

namespace MinecraftCubed.Domain.Repositories
{
    public class ServerRepository : Repository, IServerRepository
    {
        public ServerRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        public IQueryable<Server> GetAllServers()
        {
            return DataContext.Servers;
        }

        public bool ExistsServer(string address, int port)
        {
            return DataContext.Servers.Any(x => x.ServerConnectionInfos.OrderBy(y => y.ConnectionInfoId).FirstOrDefault().ConnectionInfo.Address.Equals(address, StringComparison.InvariantCultureIgnoreCase) && x.ServerConnectionInfos.OrderBy(y => y.ConnectionInfoId).FirstOrDefault().ConnectionInfo.Port == port);
        }

        public bool ExistsServer(string identifier)
        {
            return DataContext.Servers.Count(x => x.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase)) > 0;
        }

        public bool ExistsIdentifier(string id)
        {
            return DataContext.Servers.Any(x => x.Identifier.Equals(id, StringComparison.InvariantCultureIgnoreCase));
        }

        public Server GetServerByIdentifier(string identifier)
        {
            return DataContext.Servers.FirstOrDefault(x => x.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));
        }

        public Server GetServerForPage(string identifier)
        {
            return DataContext.Servers
                .Include(x => x.Color)
                .Include(x => x.Website)
                .Include(x => x.Country)
                .Include(x => x.Language)
                .Include(x => x.ServerSetting)
                .Include(x => x.Heroes)
                .Include(x => x.ServerStaffs)
                .Include(x => x.ServerPings)
                .Include(x => x.Videos)
                .Include(x => x.ServerConnectionInfos)
                .Include(x => x.ServerConnectionInfos.Select(z => z.ConnectionInfo))
                .Include(x => x.ServerConnectionInfos.Select(z => z.ConnectionInfo.PlayReq))
                .Include(x => x.Version)
                .Include(x => x.Description)
                .Include(x => x.ServerGameplays)
                .Include(x => x.ServerGameplays.Select(z => z.Gameplay))
                .Include(x => x.ServerMiniGames)
                .Include(x => x.ServerMiniGames.Select(z => z.MiniGame))
                .Include(x => x.ServerModpacks)
                .Include(x => x.ServerModpacks.Select(z => z.Modpack))
                .Include(x => x.ServerPlugins)
                .Include(x => x.ServerPlugins.Select(z => z.Plugin))
                .Include(x => x.ServerTypes)
                .Include(x => x.ServerTypes.Select(z => z.Type))
                .FirstOrDefault(x => x.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));
        }

        public int GetServerIdFromIdentifier(string identifier)
        {
            return RedisStore.Current.GetOrStore(Constant.Cache.ServerIdFromIdentifier + identifier.ToLower(), () =>
                DataContext.Database.SqlQuery<int>("SELECT ServerId from Servers where Identifier={0}", identifier).FirstOrDefault<int>(), TimeSpan.FromHours(24));
        }
        public string GetIdentifierFromServerId(int serverId)
        {
            return RedisStore.Current.GetOrStore(Constant.Cache.IdentifierFromServerId + serverId, () =>
                DataContext.Database.SqlQuery<string>("SELECT Identifier from Servers where ServerId={0}", serverId).FirstOrDefault<string>(), TimeSpan.FromHours(24));
        }

        public Server GetServerById(int id)
        {
            return DataContext.Servers.Find(id);
        }

        public IQueryable<ServerConnectInfo> GetAllServerConnectInfos()
        {
            return DataContext.Servers.Select(x => new ServerConnectInfo
            {
                Address = x.ServerConnectionInfos.OrderBy(y => y.Position).FirstOrDefault().ConnectionInfo.Address,
                Port = x.ServerConnectionInfos.OrderBy(y => y.Position).FirstOrDefault().ConnectionInfo.Port,
                QueryInfoAddress = x.QueryInfo.Address,
                QueryInfoPort = x.QueryInfo.Port,
                ServerId = x.ServerId
            });
        }

        public ServerConnectInfo GetServerConnectInfo(int serverId)
        {
            var x = DataContext.Servers.Find(serverId);

            if (x != null)
            {
                var info = new ServerConnectInfo
                {
                    QueryInfoAddress = x.QueryInfo.Address,
                    QueryInfoPort = x.QueryInfo.Port,
                    ServerId = x.ServerId
                };

                if (x.ServerConnectionInfos != null && x.ServerConnectionInfos.Any())
                {
                    info.Address = x.ServerConnectionInfos.OrderBy(y => y.Position).First().ConnectionInfo.Address;
                    info.Port = x.ServerConnectionInfos.OrderBy(y => y.Position).First().ConnectionInfo.Port;
                }
                else
                {
                    info.Address = "none";
                    info.Port = 25565;
                }
                return info;
            }

            return null;
        }

        /*********************************************************************************************************************/

        public Server GetServerForList(int serverId)
        {
            return DataContext.Servers
                .Include(x => x.ServerGameplays)
                .Include(x => x.ServerGameplays.Select(z => z.Gameplay))
                .Include(x => x.ServerMiniGames)
                .Include(x => x.ServerMiniGames.Select(z => z.MiniGame))
                .Include(x => x.ServerModpacks)
                .Include(x => x.ServerModpacks.Select(z => z.Modpack))
                .Include(x => x.ServerPlugins)
                .Include(x => x.ServerPlugins.Select(z => z.Plugin))
                .Include(x => x.ServerTypes)
                .Include(x => x.ServerTypes.Select(z => z.Type))
                .Include(x => x.Country)
                .Include(x => x.Language)
                .Include(x => x.ServerPings)
                .Include(x => x.Version)
                .Include(x => x.ServerSetting)
                .Include(x => x.ServerConnectionInfos)
                .Include(x => x.ServerConnectionInfos.Select(z => z.ConnectionInfo))
                .Include(x => x.ServerConnectionInfos.Select(z => z.ConnectionInfo.PlayReq))
                .FirstOrDefault(x => x.ServerId == serverId);
        }

        public IQueryable<Server> GetEnabledServersForPinger()
        {
            return DataContext.Servers
                .Include(x => x.ServerConnectionInfos)
                .Where(x => x.Enabled);
        }

        public IQueryable<int> GetMatchingEnabledServerIds(
            IEnumerable<string> types,
            IEnumerable<string> gameplays,
            IEnumerable<string> minigames,
            IEnumerable<string> plugins,
            IEnumerable<string> modpacks,
            string version,
            string country,
            string language)
        {
            var query = DataContext.Servers
                .Where(x => x.ServerPings.Count > 0 && x.ServerPings.OrderByDescending(z => z.CreationTime).FirstOrDefault().IsOnline)
                .Where(x => x.Enabled);

            if (types != null)
            {
                query = query.Where(x => !types.Except(x.ServerTypes.Select(z => z.Type.Name)).Any());
            }
            if (gameplays != null)
            {
                query = query.Where(x => !gameplays.Except(x.ServerGameplays.Select(z => z.Gameplay.Name)).Any());
            }
            if (minigames != null)
            {
                query = query.Where(x => !minigames.Except(x.ServerMiniGames.Select(z => z.MiniGame.Name)).Any());
            }
            if (plugins != null)
            {
                query = query.Where(x => !plugins.Except(x.ServerPlugins.Where(y => y.Enabled).Select(z => z.Plugin.Name)).Any());
            }
            if (modpacks != null)
            {
                query = query.Where(x => !modpacks.Except(x.ServerModpacks.Select(z => z.Modpack.Name)).Any());
            }
            if (!string.IsNullOrEmpty(version))
            {
                query = query.Where(x => x.Version.Name.Equals(version, StringComparison.InvariantCultureIgnoreCase));
            }
            if (!string.IsNullOrEmpty(country))
            {
                query = query.Where(x => x.Country.Name.Equals(country, StringComparison.InvariantCultureIgnoreCase));
            }
            if (!string.IsNullOrEmpty(language))
            {
                query = query.Where(x => x.Language.Name.Equals(language, StringComparison.InvariantCultureIgnoreCase));
            }

            return query.Select(x => x.ServerId);
        }

        /*********************************************************************************************************************/


        public IQueryable<Server> GetServersWithType(IEnumerable<int> serverIds, ICollection<string> tags)
        {
            var query = DataContext.Servers
                .Include(x => x.ServerTypes)
                .Include(x => x.ServerTypes.Select(z => z.Type))
                .Where(x => serverIds.Contains(x.ServerId));

            if (tags != null)
            {
                query = query.Where(x =>
                    !tags.Except(x.ServerTypes.Select(z => z.Type.Name)).Any()
                    );
            }

            return query;
        }

        public IQueryable<Server> GetServersWithGameplay(IEnumerable<int> serverIds, ICollection<string> tags)
        {
            var query = DataContext.Servers
                .Include(x => x.ServerGameplays)
                .Include(x => x.ServerGameplays.Select(z => z.Gameplay))
                .Where(x => serverIds.Contains(x.ServerId));

            if (tags != null)
            {
                query = query.Where(x =>
                    !tags.Except(x.ServerGameplays.Select(z => z.Gameplay.Name)).Any()
                    );
            }

            return query;
        }

        public IQueryable<Server> GetServersWithMiniGame(IEnumerable<int> serverIds, ICollection<string> tags)
        {
            var query = DataContext.Servers
                .Include(x => x.ServerMiniGames)
                .Include(x => x.ServerMiniGames.Select(z => z.MiniGame))
                .Include(x => x.ServerMiniGames.Select(z => z.MiniGame.ServerMiniGames))
                .Where(x => serverIds.Contains(x.ServerId));

            if (tags != null)
            {
                query = query.Where(x =>
                    !tags.Except(x.ServerMiniGames.Select(z => z.MiniGame.Name)).Any()
                    );
            }
            return query;
        }

        public IQueryable<Server> GetServersWithPlugin(IEnumerable<int> serverIds, ICollection<string> tags)
        {
            var query = DataContext.Servers
                .Include(x => x.ServerPlugins)
                .Include(x => x.ServerPlugins.Select(z => z.Plugin))
                .Where(x => serverIds.Contains(x.ServerId));

            if (tags != null)
            {
                query = query.Where(x =>
                    !tags.Except(x.ServerPlugins.Where(y => y.Enabled).Select(z => z.Plugin.Name)).Any()
                    );
            }
            return query;
        }

        public IQueryable<Server> GetServersWithMod(IEnumerable<int> serverIds, ICollection<string> tags)
        {
            var query = DataContext.Servers
                .Include(x => x.ServerModpacks)
                .Include(x => x.ServerModpacks.Select(z => z.Modpack))
                .Where(x => serverIds.Contains(x.ServerId));

            if (tags != null)
            {
                query = query.Where(x =>
                    !tags.Except(x.ServerModpacks.Select(z => z.Modpack.Name)).Any()
                    );
            }

            return query;
        }

        public IQueryable<Server> GetServersWithVersion(IEnumerable<int> serverIds, string tag)
        {
            var query = DataContext.Servers
                .Include(x => x.Version)
                .Where(x => serverIds.Contains(x.ServerId));

            if (tag != null)
            {
                query = query.Where(x =>
                    x.Version.Name.Equals(tag, StringComparison.InvariantCultureIgnoreCase)
                    );
            }
            return query;
        }

        public IQueryable<Server> GetServersWithCountry(IEnumerable<int> serverIds, string tag)
        {
            var query = DataContext.Servers
                .Include(x => x.Country)
                .Where(x => serverIds.Contains(x.ServerId));

            if (tag != null)
            {
                query = query.Where(x =>
                    x.Country.Name.Equals(tag, StringComparison.InvariantCultureIgnoreCase)
                    );
            }

            return query;
        }

        public IQueryable<Server> GetServersWithLanguage(IEnumerable<int> serverIds, string tag)
        {
            var query = DataContext.Servers
                .Include(x => x.Language)
                .Where(x => serverIds.Contains(x.ServerId));

            if (tag != null)
            {
                query = query.Where(x =>
                    x.Language.Name.Equals(tag, StringComparison.InvariantCultureIgnoreCase)
                    );
            }
            return query;
        }

        /*********************************************************************************************************************/

        public bool HasCreatedEvent(int serverId)
        {
            return DataContext.Events.Any(x => x.ServerId == serverId);
        }
    }
}
