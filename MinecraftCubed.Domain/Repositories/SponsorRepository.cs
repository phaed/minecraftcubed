﻿using System;
using System.Linq;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Static;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;

namespace MinecraftCubed.Domain.Repositories
{
    public class SponsorRepository : Repository, ISponsorRepository
    {
        public SponsorRepository(IDatabaseFactory databaseFactory) : base(databaseFactory) { }

    }
}
