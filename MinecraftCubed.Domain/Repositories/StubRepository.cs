﻿using System;
using System.Collections.Generic;
using System.Linq;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Models.Stub;
using MinecraftCubed.Domain.Repositories.Interfaces;

namespace MinecraftCubed.Domain.Repositories
{
    public class StubRepository : Repository, IStubRepository
    {
        public StubRepository(IDatabaseFactory databaseFactory) : base(databaseFactory) { }

        public Server GetServerFromStub(string accessCode)
        {
            var stub = DataContext.Stubs.FirstOrDefault(x => x.AccessCode.Equals(accessCode, StringComparison.InvariantCultureIgnoreCase));

            if (stub != null)
            {
                return stub.Server;
            }

            return null;
        }

        public Stub GetStub(string accessCode)
        {
            return DataContext.Stubs.FirstOrDefault(x => x.AccessCode.Equals(accessCode, StringComparison.InvariantCultureIgnoreCase));
        }

        public IEnumerable<UserStubs> GetStubEditors()
        {
            return DataContext.Stubs.GroupBy(x => x.User).Select(g => new UserStubs
            {
                User = g.Key,
                Stubs = g.ToList(),
                Events = g.ToList().Where(x => x.Claimed == false).Select(x => x.Server).SelectMany(x => x.Events).ToList()
            });
        }
    }
}
