﻿using System;
using System.Linq;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Static;
using Type = MinecraftCubed.Domain.Context.Type;
using Version = MinecraftCubed.Domain.Context.Version;

namespace MinecraftCubed.Domain.Repositories
{
    public class TagsRepository : Repository, ITagsRepository
    {
        public TagsRepository(IDatabaseFactory databaseFactory) : base(databaseFactory) { }

        public IQueryable<Type> GetAllTypes()
        {
            return DataContext.Types;
        }
        
        public IQueryable<Country> GetAllCountries()
        {
            return DataContext.Countries;
        }

        public IQueryable<Host> GetAllHosts()
        {
            return DataContext.Hosts;
        }

        /*********************************************************************************************************************/

        public Type GetTypeByName(string name)
        {
            return DataContext.Types.FirstOrDefault(m => m.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public EventTag GetEventTagByName(string name)
        {
            return DataContext.EventTags.FirstOrDefault(m => m.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public Gameplay GetGameplayByName(string name)
        {
            return DataContext.Gameplays.FirstOrDefault(m => m.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public Version GetVersionByName(string name)
        {
            return DataContext.Versions.FirstOrDefault(m => m.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public Language GetLanguageByName(string name)
        {
            return DataContext.Languages.FirstOrDefault(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public Country GetCountryByName(string name)
        {
            return DataContext.Countries.FirstOrDefault(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public Host GetHostByName(string name)
        {
            return DataContext.Hosts.FirstOrDefault(m => m.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        /*********************************************************************************************************************/

        public IQueryable<Type> GetTypes(int serverId)
        {
            return DataContext.Types.Where(x => x.ServerTypes.Any(z => z.ServerId == serverId));
        }

        public IQueryable<EventTag> GetEventTags(int eventId)
        {
            return DataContext.EventTags.Where(x => x.EventEventTags.Any(z => z.Event.EventId == eventId));
        }

        public IQueryable<Gameplay> GetGameplays(int serverId)
        {
            return DataContext.Gameplays.Where(x => x.ServerGameplays.Any(z => z.ServerId == serverId));
        }


        /*********************************************************************************************************************/

        public bool DeleteUnusedTypes()
        {
            var unused = DataContext.Types.Where(x => x.ServerTypes.Any() == false);
            var found = unused.Any();

            foreach (var item in unused)
            {
                DataContext.Types.Remove(item);
            }

            return found;
        }

        public bool DeleteUnusedEventTags()
        {
            var unused = DataContext.EventTags.Where(x => x.EventEventTags.Any() == false);
            var found = unused.Any();

            foreach (var item in unused)
            {
                DataContext.EventTags.Remove(item);
            }

            return found;
        }

        public bool DeleteUnusedGameplays()
        {
            var unused = DataContext.Gameplays.Where(x => x.ServerGameplays.Any() == false);
            var found = unused.Any();

            foreach (var item in unused)
            {
                DataContext.Gameplays.Remove(item);
            }

            return found;
        }

        public bool DeleteUnusedVersions()
        {
            var unused = DataContext.Versions.Where(x => x.Servers.Count == 0 && x.Events.Count == 0);
            var found = unused.Any();

            foreach (var lang in unused)
            {
                DataContext.Versions.Remove(lang);
            }

            return found;
        }

        public bool DeleteUnusedLanguages()
        {
            var unused = DataContext.Languages.Where(x => x.Servers.Count == 0);
            var found = unused.Any();

            foreach (var lang in unused)
            {
                DataContext.Languages.Remove(lang);
            }

            return found;
        }

        public bool DeleteUnusedHosts()
        {
            var unused = DataContext.Hosts.Where(x => x.Servers.Count == 0 && x.HostId != Constant.UnknownHostId);
            var found = unused.Any();

            foreach (var host in unused)
            {
                DataContext.Hosts.Remove(host);
            }

            return found;
        }

        /*********************************************************************************************************************/

        public void DeleteServerTypes(int serverId)
        {
            var all = DataContext.ServerTypes.Where(x => x.ServerId == serverId);

            foreach (var item in all)
            {
                DataContext.ServerTypes.Remove(item);
            }
        }

        public void DeleteEventEventTags(int eventId)
        {
            var all = DataContext.EventEventTags.Where(x => x.Event.EventId == eventId);

            foreach (var item in all)
            {
                DataContext.EventEventTags.Remove(item);
            }
        }
        
        public void DeleteServerGameplays(int serverId)
        {
            var all = DataContext.ServerGameplays.Where(x => x.ServerId == serverId);

            foreach (var item in all)
            {
                DataContext.ServerGameplays.Remove(item);
            }
        }
    }
}
