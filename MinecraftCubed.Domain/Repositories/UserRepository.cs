﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Static;

namespace MinecraftCubed.Domain.Repositories
{
    public class UserRepository : Repository, IUserRepository
    {
        public UserRepository(IDatabaseFactory databaseFactory) : base(databaseFactory) { }


        public User GetUserByUsername(string username)
        {
            return DataContext.Users
                .Include(x => x.Roles)
                .Include(x => x.ServerStaffs)
                .FirstOrDefault(m => m.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase));
        }

        public User GetUserByEmail(string email)
        {
            return DataContext.Users
                .Include(e => e.Roles)
                .FirstOrDefault(m => m.Email.Equals(email, StringComparison.InvariantCultureIgnoreCase));
        }

        public bool ExistsUserByEmail(string email)
        {
            return DataContext.Users.Any(m => m.Email.Equals(email, StringComparison.InvariantCultureIgnoreCase));
        }

        public bool ExistsUserByUsername(string username)
        {
            return DataContext.Users.Any(m => m.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase));
        }

        public bool ExistsUsernameIgnoreUnverified(string username)
        {
            return DataContext.Users.Any(m => m.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase) && m.Roles.Any(r => r.RoleId != (int)UserRole.Unverified));
        }
        public User FindUnverifiedDupe(string username)
        {
            return DataContext.Users.FirstOrDefault(m => m.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase) && m.Roles.Any(r => r.RoleId == (int)UserRole.Unverified));
        }

        public bool ExistsUserByUUID(string uuid)
        {
            return DataContext.Users.Any(m => m.UUID.Equals(uuid, StringComparison.InvariantCultureIgnoreCase));
        }

        public Role GetRoleById(int id)
        {
            return DataContext.Roles.Find(id);
        }

        public IQueryable<User> FindUsers(string name)
        {
            return DataContext.Users
                .Where(x => x.Username.ToLower().StartsWith(name.ToLower().Trim()))
                .OrderBy(x => x.Username);
        }

        public string GenerateAnonUsername()
        {
            var count = DataContext.Users.Count(x => x.Roles.Any(r => r.RoleId == (int)UserRole.Unverified) && x.Username.StartsWith("Anon"));

            while (true)
            {
                var username = Constant.AnonPrefix + (++count);

                if (!ExistsUserByUsername(username))
                {
                    return username;
                }
            }
        }
    }
}