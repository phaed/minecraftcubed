﻿using System;
using System.Collections.Generic;
using System.Linq;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;

namespace MinecraftCubed.Domain.Repositories
{
    public class GeneralRepository : Repository, IGeneralRepository
    {
        public GeneralRepository(IDatabaseFactory databaseFactory) : base(databaseFactory) { }

        public void AddServerPing(ServerPing ping)
        {
            DataContext.ServerPings.Add(ping);
        }
    }
}
