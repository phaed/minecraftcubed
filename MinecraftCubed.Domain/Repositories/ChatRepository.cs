﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;

namespace MinecraftCubed.Domain.Repositories
{
    public class ChatRepository : Repository, IChatRepository
    {
        public ChatRepository(IDatabaseFactory databaseFactory) : base(databaseFactory) {}

        public IEnumerable<ChatLine> GetChat(int serverId, int skip, int take)
        {
            return DataContext.ChatLines.OrderBy(o => o.CreationTime).Where(x => x.ServerId == serverId).Include(x => x.User).Skip(skip).Take(take);
        }
    }
}
