﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;

namespace MinecraftCubed.Domain.Repositories
{
    public class StaffRepository : Repository, IStaffRepository
    {
        public StaffRepository(IDatabaseFactory databaseFactory) : base(databaseFactory) { }
        
        public ServerStaff GetStaff(string identifier, string username)
        {
            return DataContext.ServerStaffs
                .Include(x => x.User)
                .FirstOrDefault(x => x.Server.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase) && x.User.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase));
        }

        public void DeleteStaff(string identifier, string username)
        {
            var staff = DataContext.ServerStaffs.FirstOrDefault(x => x.User.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase) && x.Server.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));

            if (staff != null)
            {
                DataContext.ServerStaffs.Remove(staff);
            }
        }

        public IQueryable<ServerStaff> GetStaff(string identifier)
        {
            return DataContext.ServerStaffs.Include(x => x.User).Where(x => x.Server.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase)).OrderBy(x => x.Position);
        }

        public bool IsServerManager(string identifier, string username)
        {
            return DataContext.ServerStaffs.Any(x =>
                    x.Server.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase) &&
                    x.User.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase) &&
                    (x.IsManager || x.IsAdmin)
            );
        }

        public bool IsServerAdmin(string identifier, string username)
        {
            return DataContext.ServerStaffs.Any(x =>
                    x.Server.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase) &&
                    x.User.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase) &&
                    x.IsAdmin);
        }

        public bool IsServerStaff(string identifier, string username)
        {
            return DataContext.ServerStaffs.Any(x =>
                    x.Server.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase) &&
                    x.User.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase));
        }

        public bool IsAnyServerManager(string username)
        {
            return DataContext.ServerStaffs.Any(x =>
                    x.User.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase) &&
                    (x.IsManager || x.IsAdmin)
            );
        }

        public bool IsAnyServerAdmin(string username)
        {
            return DataContext.ServerStaffs.Any(x =>
                    x.User.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase) &&
                    x.IsAdmin);
        }

        public int GetAdminCount(string identifier)
        {
            return DataContext.ServerStaffs.Count(x =>
                x.Server.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase) &&
                x.IsAdmin
            );
        }

        public bool AdminsServer(int userId, string identifier)
        {
            return DataContext.ServerStaffs.Any(x =>
                x.UserId == userId &&
                x.Server.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase) &&
                (x.IsAdmin)
            );
        }

        public bool ManagesServer(int userId, string identifier)
        {
            return DataContext.ServerStaffs.Any(x =>
                x.UserId == userId &&
                x.Server.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase) &&
                (x.IsManager || x.IsAdmin)
            );
        }

        public bool StaffsServer(int userId, string identifier)
        {
            return DataContext.ServerStaffs.Any(x =>
                x.UserId == userId &&
                x.Server.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase)
            );
        }

        public bool ManagesEvent(int userId, string identifier)
        {
            return DataContext.Events.Any(x =>
                x.EventIdentifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase) &&
                x.Server.ServerStaffs.Any(m => m.UserId == userId && (m.IsManager || m.IsAdmin))
            );
        }

        public Server GetManagedServerByIdentifier(int userId, string identifier)
        {
            return DataContext.Servers.FirstOrDefault(x => x.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase) && x.ServerStaffs.Any(y => y.UserId == userId && (y.IsManager || y.IsAdmin)));
        }

        public Event GetManagedEventByIdentifier(int userId, string identifier)
        {
            return DataContext.Events.FirstOrDefault(x => x.EventIdentifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase) && x.Server.ServerStaffs.Any(y => y.UserId == userId && (y.IsManager || y.IsAdmin)));
        }

        public Server GetFirstManagedServer(int userId)
        {
            return DataContext.Servers.FirstOrDefault(x => x.ServerStaffs.Any(y => y.UserId == userId && (y.IsManager || y.IsAdmin)));
        }

        public Event GetFirstManagedEvent(int userId)
        {
            return DataContext.Events.FirstOrDefault(x => x.Server.ServerStaffs.Any(y => y.UserId == userId && (y.IsManager || y.IsAdmin)));
        }

        public IQueryable<Server> GetManagedServers(int userId)
        {
            return DataContext.Servers.Where(x => x.ServerStaffs.Any(y => y.UserId == userId && (y.IsManager || y.IsAdmin)));
        }

        public IQueryable<Event> GetManagedEvents(int userId)
        {
            return DataContext.Events.Where(x => x.Server.ServerStaffs.Any(y => y.UserId == userId && (y.IsManager || y.IsAdmin)));
        }
    }
}
