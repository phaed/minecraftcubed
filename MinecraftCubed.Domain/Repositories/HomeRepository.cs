﻿using System;
using System.Collections.Generic;
using System.Linq;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;

namespace MinecraftCubed.Domain.Repositories
{
    public class HomeRepository : Repository, IHomeRepository
    {
        public HomeRepository(IDatabaseFactory databaseFactory) : base(databaseFactory) { }

        public void AddServerPing(ServerPing ping)
        {
            DataContext.ServerPings.Add(ping);
        }

        public IQueryable<User> GetLatestVisitingUsers(int count)
        {
            return DataContext.Users.OrderByDescending(x => x.LastLogin).Take(count);
        }
    }
}
