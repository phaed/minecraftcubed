﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;

namespace MinecraftCubed.Domain.Repositories
{
    public abstract class Repository : IRepository
    {
        private CubeEntities _dataContext;
        private readonly IDatabaseFactory _databaseFactory;

        protected Repository(IDatabaseFactory databaseFactory)
        {
            _databaseFactory = databaseFactory;
        }

        protected CubeEntities DataContext
        {
            get { return _dataContext ?? (_dataContext = _databaseFactory.Get()); }
        }

        public void DisableProxyCreation()
        {
            DataContext.Configuration.ProxyCreationEnabled = false; 
        }

        public virtual void Add<TEntity>(TEntity entity) where TEntity : class
        {
            DataContext.Set<TEntity>().Add(entity);
        }

        public virtual void Delete<TEntity>(TEntity entity) where TEntity : class
        {
            DataContext.Set<TEntity>().Remove(entity);
        }

        public virtual bool Delete<TEntity>(Func<TEntity, bool> where) where TEntity : class
        {
            var entity = DataContext.Set<TEntity>().Where(where).FirstOrDefault();

            if (entity != null)
            {
                Delete(entity);
                return true;
            }

            return false;
        }

        public bool Exists<TEntity>(Func<TEntity, bool> where) where TEntity : class
        {
            return DataContext.Set<TEntity>().Count(where) > 0;
        }

        public int Count<TEntity>(Func<TEntity, bool> where) where TEntity : class
        {
            return DataContext.Set<TEntity>().Count(where);
        }

        public virtual TEntity GetById<TEntity>(int id) where TEntity : class
        {
            return DataContext.Set<TEntity>().Find(id);
        }
        
        public virtual IEnumerable<TEntity> GetAll<TEntity>() where TEntity : class
        {
            return DataContext.Set<TEntity>().ToList();
        }

        public virtual IEnumerable<TEntity> GetWhere<TEntity>(Func<TEntity, bool> where) where TEntity : class
        {
            return DataContext.Set<TEntity>().Where(where).ToList();
        }

        public TEntity Get<TEntity>(Func<TEntity, bool> where) where TEntity : class
        {
            return DataContext.Set<TEntity>().Where(where).FirstOrDefault();
        }

        public void DeleteRange<TEntity>(Func<TEntity, bool> where) where TEntity : class
        {
            DataContext.Set<TEntity>().RemoveRange(DataContext.Set<TEntity>().Where(where));
        }

        public void ExecuteSqlCommand(string sql)
        {
            DataContext.Database.ExecuteSqlCommand(sql);
        }
    }
}
