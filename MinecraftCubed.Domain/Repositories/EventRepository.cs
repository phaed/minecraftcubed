﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Static;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;


namespace MinecraftCubed.Domain.Repositories
{
    public class EventRepository : Repository, IEventRepository
    {
        private readonly SponsorRepository _sponsorRepo;

        public EventRepository(IDatabaseFactory databaseFactory, SponsorRepository sponsorRepo)
            : base(databaseFactory)
        {
            _sponsorRepo = sponsorRepo;
        }

        public bool ExistsEvent(string name)
        {
            return DataContext.Events.Any(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public bool ExistsIdentifier(string id)
        {
            return DataContext.Events.Any(x => x.EventIdentifier.Equals(id, StringComparison.InvariantCultureIgnoreCase));
        }

        public Event GetEventFromId(int eventId)
        {
            return DataContext.Events.Find(eventId);
        }

        public IQueryable<Event> GetAllEvents()
        {
            return DataContext.Events;
        }

        /*********************************************************************************************************************/

        public void AddEventDate(EventDate ed)
        {
            DataContext.EventDates.Add(ed);
        }

        public void DeleteEventDate(int eventDateId)
        {
            var ed = DataContext.EventDates.Find(eventDateId);

            if (ed != null)
            {
                DataContext.EventDates.Remove(ed);
            }
        }

        public bool IsPastEventDate(int eventDateId)
        {
            var ed = DataContext.EventDates.Find(eventDateId);

            if (ed != null)
            {
                if (ed.StartDate.AddHours(ed.LengthInHours) < DateTime.UtcNow)
                {
                    return true;
                }
            }

            return false;
        }

        /*********************************************************************************************************************/

        public IQueryable<int> GetActiveEvenDatetIds(int take)
        {
            return DataContext.EventDates
                .Where(x => x.StartDate <= DateTime.UtcNow)
                .Where(x => DbFunctions.AddHours(x.StartDate, x.LengthInHours) > DateTime.UtcNow)
                .OrderBy(x => x.StartDate)
                .GroupBy(x => x.EventId)
                .Select(group => group.FirstOrDefault())
                .Take(take)
                .Select(x => x.EventDateId);
        }

        public IQueryable<int> GetLatestEventDateIds(int take)
        {
            return DataContext.EventDates
                .Where(x => x.StartDate > DateTime.UtcNow)
                .OrderBy(x => x.StartDate)
                .GroupBy(x => x.EventId)
                .Select(group => group.FirstOrDefault())
                .Take(take)
                .Select(x => x.EventDateId);
        }

        public IQueryable<int> GetLatesWithDupestEventDateIds(int take)
        {
            return DataContext.EventDates
                .Where(x => x.StartDate > DateTime.UtcNow)
                .OrderBy(x => x.StartDate)
                .Take(take)
                .Select(x => x.EventDateId);
        }

        public EventDate GetEventDate(int eventDateId)
        {
            return DataContext.EventDates.Find(eventDateId);
        }

        public EventDate GetEventDateForList(int eventDateId)
        {
            return DataContext.EventDates
                .Include(x => x.Event)
                .Include(x => x.Event.PlayReq)
                .Include(x => x.EventAttendees)
                .Include(x => x.EventAttendees.Select(z => z.User))
                .Include(x => x.Event.Server)
                .Include(x => x.Event.Version)
                .Include(x => x.Event.EventDescription)
                .Include(x => x.Event.Server.Country)
                .Include(x => x.Event.Server.Language)
                .Include(x => x.Event.EventEventTags)
                .Include(x => x.Event.EventEventTags.Select(z => z.EventTag))
                .FirstOrDefault(x => x.EventDateId == eventDateId);
        }

        /*********************************************************************************************************************/

        public IQueryable<int> GetMatchingEventDateIds(
            IEnumerable<string> gameplays,
            string country,
            string language)
        {
            var query = DataContext.EventDates
                .Where(x => DbFunctions.AddHours(x.StartDate, x.LengthInHours) > DateTime.UtcNow // enddates in the future only
            );

            if (gameplays != null)
            {
                query = query.Where(x => !gameplays.Except(x.Event.EventEventTags.Select(z => z.EventTag.Name)).Any());
            }
            if (!string.IsNullOrEmpty(country))
            {
                query = query.Where(x => x.Event.Server.Country.Name.Equals(country, StringComparison.InvariantCultureIgnoreCase));
            }
            if (!string.IsNullOrEmpty(language))
            {
                query = query.Where(x => x.Event.Server.Language.Name.Equals(language, StringComparison.InvariantCultureIgnoreCase));
            }

            return query.Select(x => x.EventDateId);
        }

        public IQueryable<EventDate> GetLiveEventDatesWithEventGameplay(IEnumerable<int> eventIds, IEnumerable<string> tags)
        {
            var query = DataContext.EventDates
                .Include(x => x.Event)
                .Include(x => x.Event.EventEventTags)
                .Include(x => x.Event.EventEventTags.Select(z => z.EventTag))
                .Where(x => eventIds.Contains(x.EventDateId)
            );

            if (tags != null)
            {
                return query.Where(x => !tags.Except(x.Event.EventEventTags.Select(z => z.EventTag.Name)).Any());
            }

            return query;
        }

        public IQueryable<EventDate> GetLiveEventDatesWithCountry(IEnumerable<int> eventIds, string tag)
        {
            var query = DataContext.EventDates
                .Include(x => x.Event)
                .Include(x => x.Event.Server.Country)
                .Where(x => eventIds.Contains(x.EventDateId)
            );

            if (tag != null)
            {
                return query.Where(x => x.Event.Server.Country.Name.Equals(tag, StringComparison.InvariantCultureIgnoreCase));
            }

            return query;
        }

        public IQueryable<EventDate> GetLiveEventDatesWithLanguage(IEnumerable<int> eventIds, string tag)
        {
            var query = DataContext.EventDates
                .Include(x => x.Event)
                .Include(x => x.Event.Server.Language)
                .Where(x => eventIds.Contains(x.EventDateId)
            );

            if (tag != null)
            {
                return query.Where(x => x.Event.Server.Language.Name.Equals(tag, StringComparison.InvariantCultureIgnoreCase));
            }

            return query;
        }

        /*********************************************************************************************************************/

        public void DeleteEvent(Event eve)
        {
            DataContext.Events.Remove(eve);
        }

        /*********************************************************************************************************************/

        public Event GetEventByIdentifier(string eventIdentifier)
        {
            return DataContext.Events.FirstOrDefault(x => x.EventIdentifier.Equals(eventIdentifier, StringComparison.InvariantCultureIgnoreCase));
        }

        public int GetEventIdFromIdentifier(string eventIdentifier)
        {
            return RedisStore.Current.GetOrStore(Constant.Cache.EventIdFromIdentifier + eventIdentifier.ToLower(), () =>
                DataContext.Database.SqlQuery<int>("SELECT EventId from Events where EventIdentifier={0}", eventIdentifier).FirstOrDefault<int>(), TimeSpan.FromHours(24));
        }

        public string GetIdentifierFromEventId(int eventId)
        {
            return RedisStore.Current.GetOrStore(Constant.Cache.IdentifierFromEventId + eventId, () =>
                DataContext.Database.SqlQuery<string>("SELECT EventIdentifier from Events where EventId={0}", eventId).FirstOrDefault<string>(), TimeSpan.FromHours(24));
        }

        /*********************************************************************************************************************/

        public void DeleteEventAttendee(EventAttendee ea)
        {
            DataContext.EventAttendees.Remove(ea);
        }

        public ICollection<string> GetEventDateAttendees(int eventDateId)
        {
            return DataContext.EventAttendees.Where(x => x.EventDateId == eventDateId).OrderBy(x => x.CreationTime).Select(x => x.User.Username).ToList();
        }
    }
}
