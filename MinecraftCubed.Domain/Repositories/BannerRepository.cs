﻿using System.Linq;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;

namespace MinecraftCubed.Domain.Repositories
{
    public class BannerRepository : Repository, IBannerRepository
    {
        public BannerRepository(IDatabaseFactory databaseFactory) : base(databaseFactory) { }

        /*********************************************************************************************************************/
        
        public void DeleteHero(int heroId)
        {
            var hero = DataContext.Heroes.Find(heroId);

            if (hero != null)
            {
                DataContext.Heroes.Remove(hero);
            }
        }
        
        public void DeleteVideo(int videoId)
        {
            var vid = DataContext.Videos.Find(videoId);

            if (vid != null)
            {
                DataContext.Videos.Remove(vid);
            }
        }
   
        /*********************************************************************************************************************/

        public int GetHeroCount(int serverId)
        {
            return DataContext.Heroes.Count(x => x.ServerId == serverId);
        }

        /*********************************************************************************************************************/

        public IQueryable<Hero> GetHeroes(int serverId)
        {
            return DataContext.Heroes.Where(x => x.ServerId == serverId).OrderBy(x => x.Position);
        }

        public IQueryable<Video> GetVideos(int serverId)
        {
            return DataContext.Videos.Where(x => x.ServerId == serverId).OrderBy(x => x.Position);
        }

        /*********************************************************************************************************************/

        public int GetNextHeroPosition(int serverId)
        {
            var lastItem = DataContext.Heroes.Where(x => x.ServerId == serverId).OrderByDescending(x => x.Position).FirstOrDefault();

            if (lastItem != null)
            {
                return lastItem.Position + 1;
            }

            return 1;
        }

        public int GetNextVideoPosition(int serverId)
        {
            var lastItem = DataContext.Videos.Where(x => x.ServerId == serverId).OrderByDescending(x => x.Position).FirstOrDefault();

            if (lastItem != null)
            {
                return lastItem.Position + 1;
            }

            return 1;
        }
    }
}
