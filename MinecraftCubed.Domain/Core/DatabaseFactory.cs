﻿using System;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;

namespace MinecraftCubed.Domain.Core
{
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private CubeEntities _dataContext;

        public CubeEntities Get()
        {
            return _dataContext ?? (_dataContext = new CubeEntities());
        }

        protected override void DisposeCore()
        {
            if (_dataContext != null)
            {
                _dataContext.Dispose();
            }
        }
    }
}
