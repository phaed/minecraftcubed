﻿using System;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Domain.Core.Interfaces
{
    public interface IDatabaseFactory : IDisposable
    {
        CubeEntities Get();
    }
}
