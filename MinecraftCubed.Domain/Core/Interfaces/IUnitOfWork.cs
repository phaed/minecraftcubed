﻿using System;
using System.Threading.Tasks;

namespace MinecraftCubed.Domain.Core.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
        void CommitAsync();
    }
}
