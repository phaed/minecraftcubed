//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MinecraftCubed.Domain.Context
{
    using System;
    using System.Collections.Generic;
    
    public partial class ServerStaff
    {
        public int ServerStaffId { get; set; }
        public int ServerId { get; set; }
        public int UserId { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsManager { get; set; }
        public string Rank { get; set; }
        public int Position { get; set; }
        public string Quote { get; set; }
        public string Color { get; set; }
    
        public virtual Server Server { get; set; }
        public virtual User User { get; set; }
    }
}
