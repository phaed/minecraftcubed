﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftCubed.Domain.Context
{
    public partial class VotifierInfo
    {
        public bool IsComplete()
        {
            return !string.IsNullOrEmpty(Address) && !string.IsNullOrEmpty(PublicKey);
        }
    }
}
