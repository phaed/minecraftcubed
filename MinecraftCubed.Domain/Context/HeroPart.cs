﻿using System.Security.Policy;
using MinecraftCubed.Common;
using MinecraftCubed.Static;

namespace MinecraftCubed.Domain.Context
{
    public partial class Hero
    {
        public string GetAbsoluteUrl()
        {
            return Constant.AzureStorageUrl + Bucket.MinecraftHeroes + "/" + Filename;
        }

        public Hero ToPoco()
        {
            return new Hero
            {
                HeroId = HeroId,
                Filename = Filename,
                Position = Position,
                Caption = Caption
            };
        }
    }
}
