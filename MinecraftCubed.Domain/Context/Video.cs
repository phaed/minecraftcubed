//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MinecraftCubed.Domain.Context
{
    using System;
    using System.Collections.Generic;
    
    public partial class Video
    {
        public int VideoId { get; set; }
        public int ServerId { get; set; }
        public string Url { get; set; }
        public int Position { get; set; }
        public string Caption { get; set; }
    
        public virtual Server Server { get; set; }
    }
}
