﻿using MinecraftCubed.Common;
using MinecraftCubed.Static;

namespace MinecraftCubed.Domain.Context
{
    public partial class ServerMiniGame
    {
        public string GetAbsoluteUrl()
        {
            return Constant.AzureStorageUrl + Bucket.MinecraftMiniGames + "/" + Server.Identifier + "-" + MiniGame.Identifier + ".png";
        }
    }
}
