﻿
using MinecraftCubed.Common;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Extensions;

namespace MinecraftCubed.Domain.Context
{
    public partial class Modpack
    {
        public string GetAbsoluteUrl()
        {
            if (string.IsNullOrEmpty(LogoUrl))
            {
                return string.Empty;
            }

            return Constant.AzureStorageUrl + Bucket.MinecraftModpacks + "/" + Identifier + ".png";
        }
    }
}
