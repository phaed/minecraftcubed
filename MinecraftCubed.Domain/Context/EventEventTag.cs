//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MinecraftCubed.Domain.Context
{
    using System;
    using System.Collections.Generic;
    
    public partial class EventEventTag
    {
        public int EventEventTagId { get; set; }
        public int EventTagId { get; set; }
        public int EventId { get; set; }
    
        public virtual Event Event { get; set; }
        public virtual EventTag EventTag { get; set; }
    }
}
