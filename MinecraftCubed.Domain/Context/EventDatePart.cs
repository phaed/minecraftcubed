﻿using System;
using System.Linq;
using MinecraftCubed.Common;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Extensions;
using MinecraftCubed.Domain.Models.Events;

namespace MinecraftCubed.Domain.Context
{
    public partial class EventDate
    {
        public string AttendeeListUrl
        {
            get { return Settings.Website + "events/attendees/" + EventDateId; }
        }

        public DateTime EndDate
        {
            get { return StartDate.AddHours(LengthInHours); }
        }
    }
}
