﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftCubed.Domain.Context
{
    public partial class Role
    {
        public Role ToPoco()
        {
            return new Role
            {
                RoleId = RoleId,
                Name = Name
            };
        }
    }
}
