﻿using MinecraftCubed.Common;
using MinecraftCubed.Static;

namespace MinecraftCubed.Domain.Context
{
    public partial class Event
    {public string GetBannerUrl()
        {
            if (!HasBanner)
            {
                return null;
            }

            return Constant.AzureStorageUrl + Bucket.MinecraftEventBanners + "/" + EventIdentifier + ".png";
        }
    }
}
