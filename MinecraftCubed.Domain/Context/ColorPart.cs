﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftCubed.Domain.Context
{
    public partial class Color
    {
        public Color ToPoco()
        {
            return new Color
            {
                ColorId = ColorId,
                Header = Header,
                Background = Background,
                Middle = Middle,
                IsDarkBackground = IsDarkBackground,
                IsDarkHeader = IsDarkHeader,
                IsDarkMiddle = IsDarkMiddle
            };
        }
    }
}
