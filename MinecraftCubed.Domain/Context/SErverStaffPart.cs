﻿
namespace MinecraftCubed.Domain.Context
{
    public partial class ServerStaff
    {
        public ServerStaff ToPoco()
        {
            return new ServerStaff
            {
                ServerId = ServerId,
                UserId = UserId,
                IsAdmin = IsAdmin,
                IsManager = IsManager,
                Rank = Rank,
                Position = Position,
                Quote = Quote,
                Color = Color,
                User = User.ToPoco()
            };
        }
    }
}
