//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MinecraftCubed.Domain.Context
{
    using System;
    using System.Collections.Generic;
    
    public partial class Modpack
    {
        public Modpack()
        {
            this.ServerModpacks = new HashSet<ServerModpack>();
        }
    
        public int ModpackId { get; set; }
        public string Name { get; set; }
        public string Identifier { get; set; }
        public string Description { get; set; }
        public string Website { get; set; }
        public string LogoUrl { get; set; }
    
        public virtual ICollection<ServerModpack> ServerModpacks { get; set; }
    }
}
