﻿
namespace MinecraftCubed.Domain.Context
{
    public partial class ConnectionInfo
    {
        public string GetFriendlyIp()
        {
            var ip = Address;

            if (Port != 25565)
            {
                ip += ":" + Port;
            }

            return ip;
        }
    }
}
