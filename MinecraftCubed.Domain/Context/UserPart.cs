﻿using System;
using System.Linq;
using MinecraftCubed.Common;
using MinecraftCubed.Static;

namespace MinecraftCubed.Domain.Context
{
    public partial class User
    {
        public bool HasRole(UserRole role)
        {
            return Roles.ToList().Exists(r => r.RoleId <= (int)role);
        }

        public bool HasRole(params UserRole[] roles)
        {
            return roles.Any(role => Roles.ToList().Exists(r => r.RoleId <= (int)role));
        }

        public string GetFaceUrl()
        {
            return Settings.AzureStorageUrl +"minecraftheads/" + Username + ".png";
        }

        public bool IsStaff(int serverId)
        {
            return ServerStaffs.Any(x => x.ServerId == serverId);
        }

        public bool IsManager(int serverId)
        {
            return ServerStaffs.Any(x => x.ServerId == serverId && (x.IsManager || x.IsAdmin));
        }

        public bool IsAdmin(int serverId)
        {
            return ServerStaffs.Any(x => x.ServerId == serverId && x.IsAdmin);
        }

        public User ToPoco()
        {
            return new User
            {
                UserId = UserId,
                Username = Username,
                Email = Email,
                UUID = UUID,
                CreationTime = CreationTime,
                LastLogin = LastLogin,
                Ip = Ip,
                ReceiveEventNotifications = ReceiveEventNotifications,
                Banned = Banned,
                Roles = Roles.Select(x => x.ToPoco()).ToList(),
            };
        }
    }
}
