﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using MinecraftCubed.Common;

namespace MinecraftCubed.Domain.Context
{
    public partial class Video
    {
        public Video ToPoco()
        {
            return new Video
            {
                VideoId = VideoId,
                Url = Url,
                Position = Position,
                Caption = Caption
            };
        }
    }
}
