﻿
using MinecraftCubed.Common;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Extensions;

namespace MinecraftCubed.Domain.Context
{
    public partial class Plugin
    {
        public bool IsComplete()
        {
            return !string.IsNullOrEmpty(Description) && !string.IsNullOrEmpty(Website) && !string.IsNullOrEmpty(LogoUrl);
        }
        public string GetAbsoluteUrl()
        {
            if (string.IsNullOrEmpty(LogoUrl))
            {
                return string.Empty;
            }

            return Constant.AzureStorageUrl + Bucket.MinecraftPlugins + "/" + Identifier + ".png";
        }
    }
}
