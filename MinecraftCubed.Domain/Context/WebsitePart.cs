﻿namespace MinecraftCubed.Domain.Context
{
    public partial class Website
    {
        public Website ToPoco()
        {
            return new Website
            {
                WebsiteId = WebsiteId,
                Url1 = Url1,
                Url2 = Url2,
                Url3 = Url3,
                Url4 = Url4,
                Title1 = Title1,
                Title2 = Title2,
                Title3 = Title3,
                Title4 = Title4,
            };
        }

        public bool HasLinks()
        {
            return !string.IsNullOrEmpty(Url1) ||
                    !string.IsNullOrEmpty(Url2) ||
                    !string.IsNullOrEmpty(Url3) ||
                    !string.IsNullOrEmpty(Url4);
        }
    }
}
