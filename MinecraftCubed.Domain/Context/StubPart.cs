﻿
using System;
using MinecraftCubed.Common;

namespace MinecraftCubed.Domain.Context
{
    public partial class Stub
    {
        public string GetClaimedShortDateString()
        {
            if (ClaimedTime != null)
            {
                return ((DateTime) ClaimedTime).ToShortDateString();
            }

            return string.Empty;
        }

        public string GetClaimUrl()
        {
            return Settings.Website + "claim/" + AccessCode;
        }

        public bool IsLinkContact()
        {
            if (Contact == null)
                return false;

            return Contact.StartsWith("http");
        }
    }
}
