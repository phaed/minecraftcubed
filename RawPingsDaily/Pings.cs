﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MinecraftCubed.Common.Extensions;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core;
using MinecraftCubed.Domain.Repositories;
using MinecraftCubed.Domain.Repositories.Interfaces;
using StructureMap;

namespace RawPingsDaily
{
    public class Pings
    {
        public static void ArchivingOldPings()
        {
            Console.WriteLine("");
            Console.WriteLine("Archiving old pings");

            var repo = ObjectFactory.GetInstance<IServerRepository>();
            var unit = ObjectFactory.GetInstance<UnitOfWork>();

            repo.ExecuteSqlCommand("exec ArchiveOldPings");

            unit.Commit();
        }
    }
}
