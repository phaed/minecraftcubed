﻿using MinecraftCubed.Web.DependencyResolution;

namespace RawPingsDaily
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IoC.Initialize();

            Pings.ArchivingOldPings();
        }
    }
}
