﻿using System;
using System.Data.Entity.Migrations.Sql;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using CaptchaMvc.Attributes;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Azure;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Extensions.Capture;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Common.Lang;
using MinecraftCubed.Minecraft;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Web.Mvc.Attributes;
using MinecraftCubed.Web.ViewModels.Auth;
using MinecraftCubed.Web.ViewModels.Email;

namespace MinecraftCubed.Web.Controllers
{
    public class AuthController : BaseController
    {
        private readonly IAuthService _authService;
        private readonly IUserService _userService;

        public AuthController(IAuthService authService, IUserService userService)
        {
            _authService = authService;
            _userService = userService;
        }

        [HttpPost]
        public JsonResult Status()
        {
            return Json(new { authenticated = Request.IsAuthenticated });
        }
        
        [Route("logout")]
        public ActionResult Logout()
        {
            _authService.LogOut();
            return Redirect("~/");
        }

        [HttpPost]
        public void LogoutAsync()
        {
            _authService.LogOut();
        }

        [HttpGet]
        [OutputCache(Duration = 60)]
        public PartialViewResult LoginAsync()
        {
            return PartialView(new LoginModel());
        }

        [HttpPost]
        public PartialViewResult LoginAsync(LoginModel model)
        {
            if (!model.AllowProcess)
            {
                return PartialView(model);
            }

            // forgot password form does not care about password validation errors

            if (model.IsForgotPass)
            {
                ModelState.Remove("Password");
            }

            // check that it is an email or username (16 chars)

            if (!ValidationHelper.IsEmail(model.UserOrEmail))
            {
                if (!ValidationHelper.IsAlphanumeric(model.UserOrEmail) || model.UserOrEmail.Trim().Length > 16)
                {
                    ModelState.AddModelError("UserOrEmail", Messages.BadEmailOrUser);
                }
            }

            // errors in model

            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }

            if (model.IsForgotPass)
            {
                // handle forgot pass

                var user = _userService.GetUserByEmail(model.UserOrEmail);

                if (user != null)
                {
                    // limit 3 sends every five minutes by ip
                    
                    var sent = RuntimeCache.GetOrStore(Constant.Cache.ForgotEmailLimit + UserHelper.GetIp(), 0, TimeSpan.FromMinutes(5));

                    if (sent > 3)
                    {
                        return PartialView("_ForgotPassWait");
                    }
                    
                    RuntimeCache.Store(Constant.Cache.ForgotEmailLimit + UserHelper.GetIp(), ++sent, TimeSpan.FromMinutes(5));
                    
                    var code = _userService.RenewConfirmationCode(model.UserOrEmail);

                    var body = View("ForgotPass", new ForgotPassModel
                    {
                        Email = HttpUtility.UrlEncode(user.Email),
                        Username = HttpUtility.UrlEncode(user.Username),
                        ConfirmationCode = HttpUtility.UrlEncode(code)
                    }).Capture(ControllerContext);

                    MailHelper.SendForgotPassEmail(user.Email, body);

                    return PartialView("_ForgotPassDone");
                }

                ModelState.AddModelError("Error", Messages.EmailNotFound);
            }
            else
            {
                // handle login

                var user = _authService.LogIn(model.UserOrEmail, model.Password);

                if (user != null)
                {
                    // limit 3 sends every five minutes by ip

                    var sent = RuntimeCache.GetOrStore(Constant.Cache.LoginLimit + UserHelper.GetIp(), 0, TimeSpan.FromMinutes(30));

                    if (sent > 15)
                    {
                        RedisStore.Current.Remove(Constant.Cache.LoginLimit);
                        return PartialView("_LoginWait");
                    }

                    RuntimeCache.Store(Constant.Cache.LoginLimit + UserHelper.GetIp(), ++sent, TimeSpan.FromMinutes(5));

                    FormsAuthentication.SetAuthCookie(user.Username, Settings.PersistLogin);

                    RedisStore.Current.Remove(Constant.MCUsername);
                    
                    return PartialView("_LoginDone", new AuthModel
                    {
                        Username = user.Username
                    });
                }

                user = _userService.GetUser(model.UserOrEmail);

                if (user != null)
                {
                    ModelState.AddModelError("Error", Messages.WrongPassword);
                    return PartialView(model);
                }

                ModelState.AddModelError("Error", Messages.EmailOrUserNotFound);
            }

            return PartialView(model);
        }

        [HttpGet]
        public PartialViewResult RegisterAsync()
        {
            return PartialView(new RegisterModel());
        }

        [HttpPost]
        [CaptchaVerify("Incorrect CAPTCHA code!")]
        public PartialViewResult RegisterAsync(RegisterModel model)
        {
            if (!model.AllowProcess)
            {
                return PartialView(model);
            }

            // errors in model

            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }
            
            // email exists

            if (_userService.ExistsEmail(model.Email))
            {
                ModelState.AddModelError("Error", Messages.EmailAlreadyRegistered);
                return PartialView(model);
            }
            
            // pick up username from input

            var username = model.Username;
            
            // username exists

            if (_userService.ExistsUsername(username))
            {
                ModelState.AddModelError("Error", Messages.UserAlreadyRegistered);
                return PartialView(model); 
            }

            // create the user

            var user = _userService.CreateUser(model.Email, username, model.NewPassword, null, false);

            // log him in

            FormsAuthentication.SetAuthCookie(user.Username, Settings.PersistLogin);

            Task.Run(() => _userService.SaveMinecraftHead(user.Username));

            return PartialView("_RegisterDone", new AuthModel
            {
                Username = user.Username
            });
        }

        [HttpPost]
        [Route("auth/verifyendpoint/{key}/{uuid}/{username}")]
        public bool VerifyEndpoint(string username, string uuid, string key)
        {
            try
            {
                if (key == Constant.LoginVerifySecret)
                {
                    _userService.UpdateVerifiedAccount(username, uuid);
                    _userService.SaveMinecraftHead(username);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError("Verify Endpoint Fail: " + ex.Message);
                return false;
            }

            return true;
        }

        [HttpGet]
        [OutputCache(NoStore = true, Location = OutputCacheLocation.None)]
        public ActionResult ChangePass(string email, string code)
        {
            var verified = _userService.VerifyConfirmationCode(email, code);

            if (verified)
            {
                return View(new ChangePassModel
                {
                    Email = email,
                    Code = code
                });
            }

            ModelState.AddModelError("Error", Messages.TheCodeHasExpired);
            return View(new ChangePassModel());
        }

        [HttpPost]
        [OutputCache(NoStore = true, Location = OutputCacheLocation.None)]
        public ActionResult ChangePass(ChangePassModel model)
        {
            if (ModelState.IsValid)
            {
                var verified = _userService.VerifyConfirmationCode(model.Email, model.Code);

                if (!verified)
                {
                    ModelState.AddModelError("Error", Messages.TheCodeHasExpired);
                    return View(model);
                }

                _userService.ChangeUserPassword(model.Email, model.Password);
                return View("ChangePassDone");
            }

            return View(model);
        }

        [OutputCache(NoStore = true, Location = OutputCacheLocation.None)]
        public ActionResult Sso(string sso, string sig)
        {
            if (string.IsNullOrEmpty(sso) || string.IsNullOrEmpty(sig))
                return Content("Invalid");

            var checksum = Utils.GetHash(sso, Constant.SSOSecret);
            if (checksum != sig)
                return Content("Invalid");

            var ssoBytes = Convert.FromBase64String(sso);
            var decodedSso = Encoding.UTF8.GetString(ssoBytes);
            var nvc = HttpUtility.ParseQueryString(decodedSso);
            var nonce = nvc["nonce"];

            var user = _userService.GetCurrentUserCached();

            if (user == null)
            {
                return Redirect(Constant.DiscourseURL);
            }

            var email = user.Email;
            var username = user.Username;
            var name = user.Username;
            var avatar = user.GetFaceUrl();
            var externalId = user.UserId.ToString();

            var returnPayload = "nonce=" + Server.UrlEncode(nonce) +
                                    "&email=" + Server.UrlEncode(email) +
                                    "&external_id=" + Server.UrlEncode(externalId) +
                                    "&username=" + Server.UrlEncode(username) +
                                    "&avatar_url=" + Server.UrlEncode(avatar) +
                                    "&avatar_force_update=true" +  //TODO detect when the avatar needs updating
                                    "&name=" + Server.UrlEncode(name);

            var encodedPayload = Convert.ToBase64String(Encoding.UTF8.GetBytes(returnPayload));
            var returnSig = Utils.GetHash(encodedPayload, Constant.SSOSecret);

            var redirectUrl = Constant.DiscourseURL + "session/sso_login?sso=" + encodedPayload + "&sig=" + returnSig;

            return Redirect(redirectUrl);
        }
    }
}
