﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Extensions;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Minecraft;
using MinecraftCubed.Static;
using MinecraftCubed.Domain.Models.Page;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Web.Mvc;
using MinecraftCubed.Web.Mvc.Attributes;
using MinecraftCubed.Web.ViewModels.List;

namespace MinecraftCubed.Web.Controllers
{
    //[LightMenu]
    public class ServerController : Controller
    {
        private readonly IServerService _serverService;
        private readonly IUserService _userService;
        private readonly IChatService _chatService;
        private readonly IStaffService _staffService;
        private readonly ISponsorService _sponsorService;

        public ServerController(IServerService serverService, IUserService userService, IStaffService staffService, IChatService chatService, ISponsorService sponsorService)
        {
            _serverService = serverService;
            _userService = userService;
            _staffService = staffService;
            _chatService = chatService;
            _sponsorService = sponsorService;
        }

        [Route("server/{identifier}")]
        public ActionResult Index(string identifier)
        {
            var server = _serverService.GetServerByIdentifier(identifier);

            if (server == null)
            {
                return RedirectToAction("Status404", "Home");
            }

            // compile page server data

            var model = new ServerPageModel
            {
                PageServer = RedisStore.Current.GetOrStore(Constant.Cache.PageServer + server.Identifier, () => _serverService.BuildPageServer(identifier), TimeSpan.FromSeconds(30), RedisDatabase.BuiltObjects)
            };
          
            return View(model);
        }

        [Route("serverdata/{identifier}")]
        public JsonResult GetServerData(string identifier)
        {
            var server = _serverService.GetServerByIdentifier(identifier);

            if (server == null)
            {
                return Json(new {});
            }
            
            // compile page data

            var data = RedisStore.Current.GetOrStore(Constant.Cache.PageData + server.Identifier, () => _serverService.BuildPageData(identifier), TimeSpan.FromSeconds(30));
            
            // add in non-cachable data

            var user = _userService.GetCurrentUser();

            if (user != null)
            {
                var isSysAdmin = user.HasRole(UserRole.SysAdmin);

                data.IsManager = isSysAdmin || _staffService.IsServerManager(identifier, user.Username);
                data.IsAdmin = isSysAdmin || _staffService.IsServerAdmin(identifier, user.Username);
                data.IsStaff = isSysAdmin || _staffService.IsServerStaff(identifier, user.Username);
                data.Color = _staffService.GetColor(identifier, user.Username);
            }

            // add in messages

            var msgs = _chatService.GetChat(server, 0, 100);

            data.InitialChat = msgs;
            data.Logged = Request.IsAuthenticated;
            data.Verified =  user != null && user.HasRole(UserRole.User);
            data.Username = user != null ? user.Username : string.Empty;

            // serialize

            return new JsonNetResult
            {
                Data = data
            };
        }
    }
}
