﻿using System;
using System.Web.Mvc;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Static;
using MinecraftCubed.Domain.Models.Events;
using MinecraftCubed.Domain.Models.List;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Web.Mvc.Attributes;

namespace MinecraftCubed.Web.Controllers
{
    public class TagsController : BaseController
    {
        private readonly IModpackService _modpackService;
        private readonly IMiniGameService _miniGameService;
        private readonly IUserService _userService;
        private readonly ITagsService _tagsService;

        public TagsController(ITagsService tagsService, IModpackService modpackService, IMiniGameService miniGameService, IUserService userService)
        {
            _tagsService = tagsService;
            _modpackService = modpackService;
            _miniGameService = miniGameService;
            _userService = userService;
        }

        [HttpPost]
        [Route("find/modpack")]
        public JsonResult FindModpack(string term)
        {
            if (string.IsNullOrEmpty(term))
            {
                return Json(new { });
            }

            return Json(_modpackService.FindModpacksDropdown(term));
        }

        [HttpPost]
        [Route("find/minigame")]
        public JsonResult FindMiniGame(string term)
        {
            if (string.IsNullOrEmpty(term))
            {
                return Json(new { });
            }

            return Json(_miniGameService.FindMiniGamesDropdown(term));
        }

        [HttpPost]
        [Route("find/user")]
        public JsonResult Find(string term)
        {
            if (string.IsNullOrEmpty(term))
            {
                return Json(new { });
            }

            return Json(_userService.FindUsers(term));
        }

        [HttpPost]
        public ActionResult GetCompleteCountries()
        {
            return Json(_tagsService.GetCompleteCountries()); // cached in service
        }

        [HttpPost]
        public ActionResult GetListTags(ListState state)
        {
            var data = new
            {
                countries = RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.Countries, state.ToString(), () => _tagsService.GetAllCountries(state)),
                languages = RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.Languages, state.ToString(), () => _tagsService.GetAllLanguages(state)),
                types = RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.ServerTypes, state.ToString(), () => _tagsService.GetAllTypes(state)),
                gameplays = RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.Gameplay, state.ToString(), () => _tagsService.GetAllGameplays(state)),
                minigames = RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.MiniGames, state.ToString(), () => _tagsService.GetAllMiniGames(state)),
                modpacks = RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.Modpacks, state.ToString(), () => _tagsService.GetAllModpacks(state)),
                plugins = RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.Plugins, state.ToString(), () => _tagsService.GetAllPlugins(state)),
                versions = RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.Versions, state.ToString(), () => _tagsService.GetAllVersions(state)),
            };
         
            return Json(data);
        }

        [HttpPost]
        public ActionResult GetAllCountries(ListState state)
        {
            return Json(RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.Countries, state.ToString(), () => _tagsService.GetAllCountries(state)));
        }

        [HttpPost]
        public ActionResult GetAllLanguages(ListState state)
        {
            return Json(RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.Languages, state.ToString(), () => _tagsService.GetAllLanguages(state)));
        }

        [HttpPost]
        public ActionResult GetAllTypes(ListState state)
        {
            return Json(RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.ServerTypes, state.ToString(), () => _tagsService.GetAllTypes(state)));
        }

        [HttpPost]
        public ActionResult GetAllGameplays(ListState state)
        {
            return Json(RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.Gameplay, state.ToString(), () => _tagsService.GetAllGameplays(state)));
        }

        [HttpPost]
        public ActionResult GetAllMiniGames(ListState state)
        {
            return Json(RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.MiniGames, state.ToString(), () => _tagsService.GetAllMiniGames(state)));
        }

        [HttpPost]
        public ActionResult GetAllModpacks(ListState state)
        {
            return Json(RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.Modpacks, state.ToString(), () => _tagsService.GetAllModpacks(state)));
        }

        [HttpPost]
        public ActionResult GetAllPlugins(ListState state)
        {
            return Json(RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.Plugins, state.ToString(), () => _tagsService.GetAllPlugins(state)));
        }

        [HttpPost]
        public ActionResult GetAllVersions(ListState state)
        {
            return Json(RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.Versions, state.ToString(), () => _tagsService.GetAllVersions(state)));
        }

        [HttpPost]
        public ActionResult GetAllHosts()
        {
            return Json(_tagsService.GetAllHosts());
        }

        /*********************************************************************************************************************/

        [HttpPost]
        public ActionResult GetEventTags(EventsState state)
        {
            return Json(new
            {
                eventgameplays = RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.EventTags, state.ToString(), () => _tagsService.GetAllEventEventGameplays(state)),
                countries = RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.EventCountries, state.ToString(), () => _tagsService.GetAllEventCountries(state)),
                languages = RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.EventLanguages, state.ToString(), () => _tagsService.GetAllEventLanguages(state))
            });
        }

        [HttpPost]
        public ActionResult GetAllEventEventGameplays(EventsState state)
        {
            return Json(RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.EventTags, state.ToString(), () => _tagsService.GetAllEventEventGameplays(state)));
        }

        [HttpPost]
        public ActionResult GetAllEventCountries(EventsState state)
        {
            return Json(RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.EventCountries, state.ToString(), () => _tagsService.GetAllEventCountries(state)));
        }

        [HttpPost]
        public ActionResult GetAllEventLanguages(EventsState state)
        {
            return Json(RedisStore.Current.FilterTagHashGetOrStore(Constant.Cache.Filters.EventLanguages, state.ToString(), () => _tagsService.GetAllEventLanguages(state)));
        }
    }
}
