﻿using System.Collections.Generic;
using System.Web.Mvc;
using SimpleMvcSitemap;

namespace MinecraftCubed.Web.Controllers
{
    public class SitemapController : Controller
    {
        public ActionResult Index()
        {
           var nodes = new List<SitemapNode>
            {
                new SitemapNode(Url.Action("Index","Home")),
                new SitemapNode(Url.Action("Index","Events")),
                new SitemapNode(Url.Action("Index","List")),
                new SitemapNode(Url.Action("FAQ","Info")),
                new SitemapNode(Url.Action("Api","Info")),
                new SitemapNode(Url.Action("Terms","Info")),
                new SitemapNode(Url.Action("Privacy","Info")),
                new SitemapNode(Url.Action("Index","Home")),
                new SitemapNode(Url.Action("Index","Home")),
            };

            return new SitemapProvider().CreateSitemap(HttpContext, nodes);
        }
    }
}