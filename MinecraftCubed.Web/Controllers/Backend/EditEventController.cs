﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Extensions;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Common.Lang;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Web.Mvc.Attributes;
using MinecraftCubed.Web.Mvc.FileUploading;
using MinecraftCubed.Web.ViewModels.EditEvent;
using MinecraftCubed.Web.ViewModels.EditEvent.Extras;

namespace MinecraftCubed.Web.Controllers.Backend
{
    [RequireSecure]
    public class EditEventController : BaseController
    {
        private readonly IServerService _serverService;
        private readonly ITagsService _tagsService;
        private readonly IBannerService _bannerService;
        private readonly IStaffService _staffService;
        private readonly IEventService _eventService;
        private readonly IGeneralService _generalService;
        private readonly ISponsorService _sponsorService;

        public EditEventController(IServerService serverService, ITagsService tagsService, IBannerService bannerService, IEventService eventService, IStaffService staffService, IGeneralService generalService, ISponsorService sponsorService)
        {
            _serverService = serverService;
            _tagsService = tagsService;
            _bannerService = bannerService;
            _eventService = eventService;
            _staffService = staffService;
            _generalService = generalService;
            _sponsorService = sponsorService;
        }

        [Route("edit/event/{eventIdentifier?}")]
        [EventManagersOnly]
        public ActionResult Index(string eventIdentifier)
        {
            var eve = _staffService.GetManagedEventByIdentifier(eventIdentifier);

            if (eve == null)
            {
                return RedirectToAction("Status404", "Home");
            }
            
            var model = new EditEventModel
            {
                EventIdentifier = eve.EventIdentifier,
                Event = eve
            };
            
            return View(model);
        }

        /****************************************************************************************************************************/

        [EventManagersOnly]
        public PartialViewResult EventInfo(string eventIdentifier)
        {
            var eve = _staffService.GetManagedEventByIdentifier(eventIdentifier);

            var model = Mapper.Map<EventInfoModel>(eve);

            model.EventIdentifier = eventIdentifier;
            model.ServerNameList = _staffService.GetManagedServersList();

            return PartialView(model);
        }

        [HttpPost]
        [EventManagersOnly]
        public PartialViewResult EventInfo(EventInfoModel model)
        {
            if (ModelState.IsValid)
            {
                // fix up the incoming text

                var split = model.Address.ToLower().Split(':');

                if (split.Length > 1)
                {
                    model.Address = split[0];
                }

                if (model.Port == 0)
                {
                    model.Port = 25565;
                }

                // create Id if new

                var versionId = _tagsService.CreateVersionIfNotExists(model.VersionName);

                // edit server

                var success = _eventService.EditEventInfo(model.EventIdentifier, model.Name, model.ServerIdentifier, model.Address, model.Port, versionId);

                if (!success)
                {
                    ModelState.AddModelError("Error", Messages.ServerDoesNotExist);
                    return PartialView(model);
                }

                _tagsService.DeleteUnusedVersions();

                // delete unused hosts

                ViewBag.Success = Messages.SavedSuccessfully;
            }

            model.ServerNameList = _staffService.GetManagedServersList();

            return PartialView(model);
        }

        /****************************************************************************************************************************/

        [EventManagersOnly]
        public PartialViewResult Description(string eventIdentifier)
        {
            var eve = _staffService.GetManagedEventByIdentifier(eventIdentifier);

            var model = Mapper.Map<EventDescriptionModel>(eve);

            model.EventIdentifier = eventIdentifier;

            return PartialView(model);
        }

        [HttpPost]
        [EventManagersOnly]
        public PartialViewResult Description(EventDescriptionModel model)
        {
            if (ModelState.IsValid)
            {
                // edit description

                _eventService.EditEventDescription(model.EventIdentifier, model.EventDescriptionText);

                ViewBag.Success = Messages.SavedSuccessfully;
            }

            return PartialView(model);
        }

        /****************************************************************************************************************************/

        [EventManagersOnly]
        public PartialViewResult Banners(string eventIdentifier)
        {
            var eve = _staffService.GetManagedEventByIdentifier(eventIdentifier);

            var model = new EventBannersModel
            {
                EventIdentifier = eventIdentifier,
                Url = eve.GetBannerUrl().BreakCache(),
                HasEventBanner = eve.HasBanner
            };
            return PartialView(model);
        }

        [HttpPost]
        [EventManagersOnly]
        public PartialViewResult Banners(EventBannersModel model)
        {
            var eve = _staffService.GetManagedEventByIdentifier(model.EventIdentifier);

            model.Url = eve.GetBannerUrl().BreakCache();
            model.HasEventBanner = eve.HasBanner;

            return PartialView(model);
        }

        [Route("delete/event/banner")]
        public async Task<JsonResult> DeleteBanner(ImageDeleteModel model)
        {
            if (!_staffService.ManagesEvent(model.EventIdentifier))
            {
                return Json(new
                {
                    success = false
                });
            }

            var success = await _bannerService.DeleteEventBanner(model.EventIdentifier);

            return Json(new
            {
                success
            });
        }

        [Route("upload/event/banner")]
        public async Task<FineUploaderResult> UploadBanner(FineUpload upload, string identifier)
        {
            if (!_staffService.ManagesEvent(identifier))
            {
                return null;
            }

            const string ext = ".png";

            Stream stream;

            if (ImageHelper.IsAnimatedGif(upload.InputStream))
            {
                stream = ImageHelper.GetRandomAnimatedGifFrame(upload.InputStream);
            }
            else
            {
                stream = ImageHelper.ShrinkIfTooBig(upload.InputStream, Constant.EventBanner.Width, Constant.EventBanner.Height, ext);
            }

            var success = await _bannerService.AddEventBanner(identifier, ext, stream);

            if (success)
            {
                return new FineUploaderResult(true);
            }

            return new FineUploaderResult(false, null, Messages.ErrorSavingImage);
        }
        
        /****************************************************************************************************************************/

        [EventManagersOnly]
        public PartialViewResult PlayReqs(string eventIdentifier)
        {
            var eve = _staffService.GetManagedEventByIdentifier(eventIdentifier);

            var model = Mapper.Map<EventPlayReqsModel>(eve.PlayReq);

            model.EventIdentifier = eventIdentifier;

            return PartialView(model);
        }

        [HttpPost]
        [EventManagersOnly]
        public PartialViewResult PlayReqs(EventPlayReqsModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.HasDownload)
                {
                    if (string.IsNullOrEmpty(model.DownloadLabel))
                    {
                        ModelState.AddModelError("DownloadLabel", Messages.MissingData);
                    }
                    if (string.IsNullOrEmpty(model.DownloadUrl))
                    {
                        ModelState.AddModelError("DownloadUrl", Messages.MissingData);
                    }
                }

                if (model.HasWhitelist)
                {
                    if (string.IsNullOrEmpty(model.WhitelistUrl))
                    {
                        ModelState.AddModelError("WhitelistUrl", Messages.MissingData);
                    }
                }

                if (model.HasRules)
                {
                    if (string.IsNullOrEmpty(model.RulesUrl))
                    {
                        ModelState.AddModelError("RulesUrl", Messages.MissingData);
                    }
                }

                if (ModelState.IsValid)
                {
                    _eventService.EditEventPlayReqs(model.EventIdentifier, model.HasDownload, model.DownloadLabel, model.DownloadUrl, model.HasWhitelist, model.WhitelistUrl, model.HasRules, model.RulesUrl, model.HasAttendeeWhitelist);

                    ViewBag.Success = Messages.SavedSuccessfully;
                }
            }

            return PartialView(model);
        }

        /****************************************************************************************************************************/

        [EventManagersOnly]
        public PartialViewResult Tags(string eventIdentifier)
        {
            var model = new EventTagsModel
            {
                EventIdentifier = eventIdentifier,
                EventTags = _tagsService.GetEventTagsString(eventIdentifier),
            };

            return PartialView(model);
        }

        [HttpPost]
        [EventManagersOnly]
        public PartialViewResult Tags(EventTagsModel model)
        {
            if (ModelState.IsValid)
            {
                // delete existing links

                _tagsService.DeleteEventEventTags(model.EventIdentifier);

                // add in new tags and links

                if (!string.IsNullOrEmpty(model.EventTags))
                {
                    foreach (var tag in model.EventTags.Split(','))
                    {
                        if (tag.Length <= Settings.MaxTagSize)
                        {
                            _tagsService.CreateEventTagIfNotExistsAndLink(tag, model.EventIdentifier);
                        }
                        else
                        {
                            ModelState.AddModelError("EventTags", string.Format(Messages.TagTooLong, Settings.MaxTagSize));
                        }
                    }
                }

                // delete unused tags

                _tagsService.DeleteUnusedEventTags();

                model.EventTags = _tagsService.GetEventTagsString(model.EventIdentifier);
                ViewBag.Success = Messages.SavedSuccessfully;
            }

            return PartialView(model);
        }


        /****************************************************************************************************************************/

        [EventManagersOnly]
        public PartialViewResult EventDates(string eventIdentifier)
        {
            var model = new EventDatesModel
            {
                EventIdentifier = eventIdentifier,
                EventDates = _eventService.GetVisibleEventDates(eventIdentifier),
                StartDate = DateTime.UtcNow,
                TimeZoneList = _generalService.GetTimeZoneList(),
                TimeZone = "UTC",
                LengthInHours = 1
            };

            return PartialView(model);
        }

        [HttpPost]
        [EventManagersOnly]
        public PartialViewResult EventDates(EventDatesModel model)
        {
            var eve = _staffService.GetManagedEventByIdentifier(model.EventIdentifier);

            model.EventDates = _eventService.GetVisibleEventDates(model.EventIdentifier);
            model.TimeZoneList = _generalService.GetTimeZoneList();

            if (ModelState.IsValid)
            {
                // duration can't be too long

                if (model.LengthInHours > Settings.MaxEventDuration)
                {
                    ModelState.AddModelError("LengthInHours", Messages.MaxEventDuration);
                    return PartialView(model);
                }

                // events days cant be set to close to now

                var days = (model.StartDate - DateTime.UtcNow).Days;

                if (days < Settings.EventDateMustBeDaysInTheFuture)
                {
                    ModelState.AddModelError("StartDate", string.Format(Messages.EventsMustBeInFuture, Settings.EventDateMustBeDaysInTheFuture));
                    return PartialView(model);
                }

                // the new event date should not overlap an existing event date

                var overlap = _eventService.IsEventDateOverlap(model.EventIdentifier, model.StartDate, model.TimeZone, model.LengthInHours);

                if (overlap)
                {
                    ModelState.AddModelError("StartDate", Messages.CannotCreateOverlappingEventDate);
                    return PartialView(model);
                }

                // add it

                var success = _eventService.AddEventDate(model.EventIdentifier, model.StartDate, model.TimeZone, model.LengthInHours);

                if (success)
                {
                    ViewBag.Success = Messages.SavedSuccessfully;
                    model.EventDates = _eventService.GetVisibleEventDates(model.EventIdentifier);
                }
                else
                {
                    ModelState.AddModelError("Error", Messages.CannotCreatePastDates);
                }
            }

            return PartialView(model);
        }

        [HttpPost]
        [Route("delete/event/date")]
        public JsonResult DeleteEventDate(EventDateDeleteModel model)
        {
            if (!_staffService.ManagesEvent(model.EventIdentifier))
            {
                return Json(new
                {
                    success = false
                });
            }

            var ed = _eventService.GetEventDate(model.Id);

            // cant delete event date when its inside the point of no return

            if ((ed.StartDate - DateTime.UtcNow).TotalHours < Settings.EventDateDeletePointOfNoReturnHours)
            {
                return Json(new
                 {
                     success = false,
                     error = string.Format(Messages.CannotDeleteTooCloseToEndDate, Settings.EventDateDeletePointOfNoReturnHours)
                 });
            }

            var success = _eventService.DeleteEventDate(model.Id);

            return Json(new
            {
                success,
                error = Messages.CannotDeletePastDates
            });
        }

        /****************************************************************************************************************************/

        [EventManagersOnly]
        public PartialViewResult DeleteEvent(string eventIdentifier)
        {
            var model = new EventDeleteEventModel
            {
                EventIdentifier = eventIdentifier
            };

            return PartialView(model);
        }

        [HttpPost]
        [EventManagersOnly]
        public ActionResult DeleteEvent(EventDeleteEventModel model)
        {
            if (_eventService.HasOpenEventDates(model.EventIdentifier))
            {
                ModelState.AddModelError("Error", Messages.CantDeleteEventWithEventDates);
                return PartialView(model);
            }

            _eventService.DeleteEvent(model.EventIdentifier);

            return RedirectToAction("Index", "Home");
        }

        /****************************************************************************************************************************/

        [EventManagersOnly]
        [Route("events/attendees/{eventDateId?}")]
        public ActionResult AttendeeList(int eventDateId)
        {
            var ed = _eventService.GetEventDate(eventDateId);

            var model = new AttendeeListModel
            {
                Attendees = ed.EventAttendees.Select(x => x.User).OrderBy(x => x.CreationTime).Select(x => x.Username).ToList()
            };

            return View(model);
        }
    }
}
