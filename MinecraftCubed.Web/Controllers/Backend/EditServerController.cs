﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Extensions;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Models.Backend;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Minecraft;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Common.Lang;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Web.Mvc.Attributes;
using MinecraftCubed.Web.Mvc.FileUploading;
using MinecraftCubed.Web.ViewModels.EditServer;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

using ImageDeleteModel = MinecraftCubed.Web.ViewModels.EditServer.Extras.ImageDeleteModel;
using ManagerDeleteModel = MinecraftCubed.Web.ViewModels.EditServer.Extras.StaffDeleteModel;

namespace MinecraftCubed.Web.Controllers.Backend
{
    [RequireSecure]
    public class EditServerController : BaseController
    {
        private readonly IServerService _serverService;
        private readonly IUserService _userService;
        private readonly IBannerService _bannerService;
        private readonly IQueryService _queryService;
        private readonly ITagsService _tagsService;
        private readonly IStaffService _staffService;
        private readonly IPluginService _pluginService;
        private readonly IMiniGameService _miniGameService;
        private readonly IModpackService _modpackService;
        private readonly IConnectionInfoService _connectionInfoService;

        public EditServerController(IServerService serverService, IUserService userService, IStaffService staffService,
            IBannerService bannerService, IPluginService pluginService, IQueryService queryService,
            ITagsService tagsService, IModpackService modpackService, IMiniGameService miniGameService, IConnectionInfoService connectionInfoService)
        {
            _serverService = serverService;
            _userService = userService;
            _staffService = staffService;
            _bannerService = bannerService;
            _pluginService = pluginService;
            _queryService = queryService;
            _tagsService = tagsService;
            _modpackService = modpackService;
            _miniGameService = miniGameService;
            _connectionInfoService = connectionInfoService;
        }

        [Route("edit/server/{identifier?}")]
        [ServerManagersOnly]
        public ActionResult Index(string identifier)
        {
            var server = _staffService.GetManagedServerByIdentifier(identifier);

            if (server == null)
            {
                return RedirectToAction("Status404", "Home");
            }

            var model = new EditServerModel
            {
                Identifier = server.Identifier,
                Server = server,
                EnabledCheck = server.Enabled ? "checked" : string.Empty
            };

            return View(model);
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult ServerInfo(string identifier)
        {
            var server = _staffService.GetManagedServerByIdentifier(identifier);

            var model = Mapper.Map<ServerInfoModel>(server);

            model.Identifier = identifier;

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public PartialViewResult ServerInfo(ServerInfoModel model)
        {
            if (ModelState.IsValid)
            {
                // create host if new

                var hostId = _tagsService.CreateHostIfNotExists(model.HostName);

                // create Id if new

                var versionId = _tagsService.CreateVersionIfNotExists(model.VersionName);

                // edit server

                var success = _serverService.EditServerInfo(model.Identifier, model.Name, versionId, hostId);

                if (!success)
                {
                    ModelState.AddModelError("Error", Messages.CouldNotConnect);
                    return PartialView(model);
                }

                // delete unused hosts and versions

                _tagsService.DeleteUnusedHosts();
                _tagsService.DeleteUnusedVersions();

                ViewBag.Success = Messages.SavedSuccessfully;
            }

            return PartialView(model);
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult Localization(string identifier)
        {
            var server = _staffService.GetManagedServerByIdentifier(identifier);

            var model = Mapper.Map<LocalizationModel>(server);

            model.Identifier = identifier;

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public PartialViewResult Localization(LocalizationModel model)
        {
            if (ModelState.IsValid)
            {
                // create language if new

                var languageId = _tagsService.CreateLanguageIfNotExists(model.LanguageName);

                // edit data

                _serverService.EditLocalization(model.Identifier, model.CountryName, languageId);

                // delete unused

                _tagsService.DeleteUnusedLanguages();
                
                ViewBag.Success = Messages.SavedSuccessfully;
            }

            return PartialView(model);
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult ServerLogo(string identifier)
        {
            var server = _serverService.GetServerByIdentifier(identifier);

            var model = new ServerLogoModel
            {
                Identifier = identifier,
                HasLogo = server.ServerSetting.HasLogo,
                LogoUrl = _serverService.GetServerLogoUrl(server).BreakCache()
            };

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public PartialViewResult ServerLogo(ServerLogoModel model)
        {
            var server = _serverService.GetServerByIdentifier(model.Identifier);
            model.HasLogo = server.ServerSetting.HasLogo;
            model.LogoUrl = _serverService.GetServerLogoUrl(server).BreakCache();

            return PartialView(model);
        }

        [HttpPost]
        [Route("delete/server/logo")]
        public async Task<JsonResult> DeleteServerLogo(ImageDeleteModel model)
        {
            if (!_staffService.ManagesServer(model.Identifier))
            {
                return Json(new
                {
                    success = false
                });
            }

            var success = await _bannerService.DeleteServerLogo(model.Identifier);

            return Json(new
            {
                success
            });
        }

        [Route("upload/server/logo")]
        public async Task<FineUploaderResult> UploadServerLogo(FineUpload upload, string identifier)
        {
            if (!_staffService.ManagesServer(identifier))
            {
                return null;
            }

            if (!ImageHelper.HasTransparentEdge(upload.InputStream))
            {
                return new FineUploaderResult(false, null, Messages.ErrorNeedTransparency);
            }

            var stream = ImageHelper.ShrinkIfTooBig(upload.InputStream, Constant.Logo.Width, Constant.Logo.Height, "png", "max", true, true);

            _serverService.EditLogoTransparentBottomSetting(identifier, ImageHelper.HasTransparentBottom(upload.InputStream));

            var success = await _bannerService.AddServerLogo(identifier, Path.GetExtension(upload.Filename), stream);

            if (success)
            {
                return new FineUploaderResult(true);
            }

            return new FineUploaderResult(false, null, Messages.ErrorSavingImage);
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult Banners(string identifier)
        {
            var server = _serverService.GetServerByIdentifier(identifier);

            var model = new BannersModel
            {
                Identifier = identifier,
                Url = _serverService.GetServerBannerUrl(server).BreakCache(),
                HasBanner = server.ServerSetting.HasBanner
            };

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public PartialViewResult Banners(BannersModel model)
        {
            var server = _serverService.GetServerByIdentifier(model.Identifier);

            model.Url = _serverService.GetServerBannerUrl(server).BreakCache();
            model.HasBanner = server.ServerSetting.HasBanner;

            return PartialView(model);
        }

        [HttpPost]
        [Route("delete/server/banner")]
        public async Task<JsonResult> DeleteBanner(ImageDeleteModel model)
        {
            if (!_staffService.ManagesServer(model.Identifier))
            {
                return Json(new
                {
                    success = false
                });
            }

            var success = await _bannerService.DeleteBanner(model.Identifier);
            return Json(new
            {
                success
            });
        }

        [Route("upload/server/banner")]
        public async Task<FineUploaderResult> UploadBanner(FineUpload upload, string identifier)
        {
            if (!_staffService.ManagesServer(identifier))
            {
                return null;
            }

            const string ext = ".png";

            Stream stream;

            if (ImageHelper.IsAnimatedGif(upload.InputStream))
            {
                stream = ImageHelper.GetRandomAnimatedGifFrame(upload.InputStream);
            }
            else
            {
                stream = ImageHelper.ShrinkIfTooBig(upload.InputStream, Constant.ServerBanner.Width, Constant.ServerBanner.Height, ext);
            }

            var success = await _bannerService.AddBanner(identifier, ext, stream);

            if (success)
            {
                return new FineUploaderResult(true);
            }

            return new FineUploaderResult(false, null, Messages.ErrorSavingImage);
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult FeaturedBanners(string identifier)
        {
            var server = _serverService.GetServerByIdentifier(identifier);
            
            var model = new FeaturedBannersModel
            {
                Identifier = identifier,
                Url = _serverService.GetServerFeaturedBannerUrl(server).BreakCache(),
                HasFeaturedBanner = server.ServerSetting.HasFeaturedBanner
            };

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public PartialViewResult FeaturedBanners(FeaturedBannersModel model)
        {
            var server = _serverService.GetServerByIdentifier(model.Identifier);

            model.Url = _serverService.GetServerFeaturedBannerUrl(server).BreakCache();
            model.HasFeaturedBanner = server.ServerSetting.HasFeaturedBanner;

            return PartialView(model);
        }

        [HttpPost]
        [Route("delete/featuredbanner")]
        public async Task<JsonResult> DeleteFeaturedBanner(ImageDeleteModel model)
        {
            if (!_staffService.ManagesServer(model.Identifier))
            {
                return Json(new
                {
                    success = false
                });
            }

            var success = await _bannerService.DeleteFeaturedBanner(model.Identifier);

            return Json(new
            {
                success
            });
        }

        [Route("upload/featuredbanner")]
        public async Task<FineUploaderResult> UploadFeaturedBanner(FineUpload upload, string identifier)
        {
            if (!_staffService.ManagesServer(identifier))
            {
                return null;
            }

            const string ext = ".png";

            Stream stream;

            if (ImageHelper.IsAnimatedGif(upload.InputStream))
            {
                stream = ImageHelper.GetRandomAnimatedGifFrame(upload.InputStream);
            }
            else
            {
                stream = ImageHelper.ShrinkIfTooBig(upload.InputStream, Constant.FeaturedBanner.Width, Constant.FeaturedBanner.Height, ext);
            }

            var success = await _bannerService.AddFeaturedBanner(identifier, ext, stream);

            if (success)
            {
                return new FineUploaderResult(true);
            }

            return new FineUploaderResult(false, null, Messages.ErrorSavingImage);
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult Carousel(string identifier)
        {
            var server = _staffService.GetManagedServerByIdentifier(identifier);

            var model = new CarouselModel
            {
                Identifier = identifier,
                Urls = _bannerService.GetHeroUrls(identifier).ToList(),
                VideoThumbnailUrls = _bannerService.GetThumbnailUrls(identifier).ToList(),
                ShuffleImages = server.ServerSetting.ShuffleImages,
                VideosFirst = server.ServerSetting.VideosFirst
            };

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public PartialViewResult Carousel(CarouselModel model)
        {
            if (ModelState.IsValid)
            {
                // set shuffle images setting

                _serverService.EditImageShuffle(model.Identifier, model.ShuffleImages);

                // set videos first setting

                _serverService.EditVideosFirst(model.Identifier, model.VideosFirst);

                // update the captions

                if (model.Urls != null)
                {
                    _serverService.EditCarouselCaptions(model.Identifier, model.Urls);
                }

                if (model.VideoThumbnailUrls != null)
                {
                    _serverService.EditVideoCaptions(model.Identifier, model.VideoThumbnailUrls);
                }

                // add new video if present

                if (!string.IsNullOrEmpty(model.VideoUrl))
                {
                    var cleanVideo = model.VideoUrl.Replace("?feature=player_embedded&", "?").Replace("http:", "https:");
                    _bannerService.AddVideo(model.Identifier, cleanVideo);
                }

                // clear caches

                ViewBag.Success = Messages.SavedSuccessfully;
            }

            model.Urls = _bannerService.GetHeroUrls(model.Identifier).ToList();
            model.VideoThumbnailUrls = _bannerService.GetThumbnailUrls(model.Identifier).ToList();

            return PartialView(model);
        }

        [HttpPost]
        [Route("delete/video")]
        public JsonResult DeleteVideo(ImageDeleteModel model)
        {
            if (!_staffService.ManagesServer(model.Identifier))
            {
                return Json(new
                {
                    success = false
                });
            }

            _bannerService.DeleteVideo(model.Identifier, model.Id);

            // refresh positions after delete

            _bannerService.RefreshVideos(model.Identifier);

            return Json(new
            {
                success = true
            });
        }

        [HttpPost]
        [Route("delete/carousel")]
        public async Task<JsonResult> DeleteCarousel(ImageDeleteModel model)
        {
            if (!_staffService.ManagesServer(model.Identifier))
            {
                return Json(new
                {
                    success = false
                });
            }

            var success = await _bannerService.DeleteHero(model.Identifier, model.Id, model.Key);

            // refresh positions after delete

            _bannerService.RefreshHeroes(model.Identifier);

            return Json(new
            {
                success
            });
        }

        [Route("upload/carousel")]
        public async Task<FineUploaderResult> UploadCarousel(FineUpload upload, string identifier)
        {
            if (!_staffService.ManagesServer(identifier))
            {
                return null;
            }

            var image = ImageHelper.ConvertToJpeg(upload.InputStream);

            if (image == null)
            {
                return new FineUploaderResult(false, null, Messages.HeroWrongSize);
            }

            var success = await _bannerService.AddHero(identifier, image);

            if (success)
            {
                return new FineUploaderResult(true);
            }

            return new FineUploaderResult(false, null, Messages.ErrorSavingImage);
        }

        [Route("reorder/carousel")]
        public void ReorderCarousel(ImageReorderModel model)
        {
            if (!_staffService.ManagesServer(model.Identifier))
            {
                return;
            }

            _bannerService.ReorderHeros(model.Ids, model.Identifier);
        }

        [Route("reorder/videos")]
        public void ReorderVideos(ImageReorderModel model)
        {
            if (!_staffService.ManagesServer(model.Identifier))
            {
                return;
            }

            _bannerService.ReorderVideos(model.Ids, model.Identifier);
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult Query(string identifier)
        {
            var server = _staffService.GetManagedServerByIdentifier(identifier);

            var model = Mapper.Map<QueryModel>(server.QueryInfo);

            // default the votifier address to the server address

            if (string.IsNullOrEmpty(model.Address))
            {
                model.Address = _serverService.GetLobbyConnectionInfo(server).Address;
            }

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public PartialViewResult Query(QueryModel model)
        {
            if (ModelState.IsValid)
            {
                // fix up the incoming text

                var split = model.Address.ToLower().Split(':');

                if (split.Length > 1)
                {
                    model.Address = split[0];
                }

                if (model.Port == 0)
                {
                    model.Port = 25565;
                }

                // edit server

                _serverService.EditQueryInfo(model.Identifier, model.Address, model.Port);

                ViewBag.Success = Messages.SavedSuccessfully;
            }

            return PartialView(model);
        }

        [HttpPost]
        public async Task<JsonResult> QueryTest(QueryModel model)
        {
            if (!_staffService.ManagesServer(model.Identifier))
            {
                return Json(new
                {
                    success = false,
                    error = Messages.NoServerAccess
                });
            }

            var resp = await _queryService.QueryServer(new ServerConnectInfo
            {
                QueryInfoAddress = model.Address,
                QueryInfoPort = model.Port
            }, Timeout.FifteenSeconds);

            if (resp.Success)
            {
                return Json(new
                {
                    success = true
                });
            }

            return Json(new
            {
                success = false,
                message = Messages.CouldNotQueryBlock
            });
        }
        
        /****************************************************************************************************************************/

        public PartialViewResult Votifier(string identifier)
        {
            var server = _staffService.GetManagedServerByIdentifierOrFirst(identifier);

            var model = Mapper.Map<VotifierModel>(server.VotifierInfo);

            model.Identifier = identifier;

            // default the votifier address to the server address

            if (string.IsNullOrEmpty(model.Address))
            {
                model.Address = _serverService.GetLobbyConnectionInfo(server).Address;
            }

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public PartialViewResult Votifier(VotifierModel model)
        {
            if (ModelState.IsValid)
            {
                // fix up the incoming text

                var split = model.Address.ToLower().Split(':');

                if (split.Length > 1)
                {
                    model.Address = split[0];
                }

                if (model.Port == 0)
                {
                    model.Port = 8192;
                }

                // edit server

                _serverService.EditVotifier(model.Identifier, model.Address, model.Port, model.PublicKey);

                ViewBag.Success = Messages.SavedSuccessfully;
            }

            return PartialView(model);
        }

        [HttpPost]
        public async Task<JsonResult> VoteTest(VotifierModel vote)
        {
            var success = await VotifierQuery.VoteAsync("test", vote.Address, vote.Port, "VoteTest", vote.PublicKey);

            return Json(new
            {
                success
            });
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult ApiKey(string identifier)
        {
            var server = _staffService.GetManagedServerByIdentifier(identifier);

            var model = new ApiKeyModel
            {
                Identifier = identifier,
                Key = server.ServerSetting.ApiKey
            };

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public PartialViewResult ApiKey(ApiKeyModel model)
        {
            if (ModelState.IsValid)
            {
                // edit server

                model.Key = _serverService.ChangeApiKey(model.Identifier);

                ViewBag.Success = Messages.ApiKeyGeneratedSuccessfully;
            }

            return PartialView(model);
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult Description(string identifier)
        {
            var server = _staffService.GetManagedServerByIdentifier(identifier);

            var model = Mapper.Map<DescriptionModel>(server);

            model.Identifier = identifier;
            model.MiddleColor = server.Color.Middle;

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public PartialViewResult Description(DescriptionModel model)
        {
            var server = _staffService.GetManagedServerByIdentifier(model.Identifier);
            model.MiddleColor = server.Color.Middle;

            if (ModelState.IsValid)
            {
                // edit description

                _serverService.EditDescription(model.Identifier, model.DescriptionText);

                ViewBag.Success = Messages.SavedSuccessfully;
            }

            return PartialView(model);
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult Staff(string identifier)
        {
            var model = new StaffModel
            {
                Identifier = identifier,
                Staff = _staffService.GetStaffDatas(identifier).ToList(),
                PermissionLevel = StaffModel.Level.Staff,
                Color = Constant.DefaultRankColor
            };

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public PartialViewResult Staff(StaffModel model)
        {
            var isAdmin = model.PermissionLevel == StaffModel.Level.Admin;
            var isManager = model.PermissionLevel == StaffModel.Level.Manager;

            model.Staff = _staffService.GetStaffDatas(model.Identifier).ToList();

            if (ModelState.IsValid)
            {
                var serverId = _serverService.GetServerIdFromIdentifier(model.Identifier);
                var user = _userService.GetCurrentUser();

                if (!user.IsAdmin(serverId))
                {
                    ModelState.AddModelError("Name", Messages.CannotAddStaff);
                    return PartialView(model);
                }

                if (!_userService.ExistsUsername(model.Username))
                {
                    ModelState.AddModelError("Name", Messages.ManagerNotFound);
                    return PartialView(model);
                }

                if (_staffService.IsStaff(model.Identifier, model.Username))
                {
                    // staff already added
                    return PartialView(model);
                }

                var last = _staffService.IsLastAdmin(model.Identifier, model.Username);

                if (last)
                {
                    ModelState.AddModelError("Error", Messages.CannotDemoteLastAdmin);
                    return PartialView(model);
                }

                _staffService.AddStaff(model.Identifier, model.Username, isAdmin, isManager, model.Rank, model.Color);

                model.Staff = _staffService.GetStaffDatas(model.Identifier).ToList();
                
                ViewBag.Success = Messages.SavedSuccessfully;
            }

            return PartialView(model);
        }

        [HttpPost]
        [Route("update/quote")]
        public JsonResult UpdateStaffQuote(QuoteUpdateModel model)
        {
            if (!_staffService.StaffsServer(model.Identifier))
            {
                return Json(new
                {
                    success = false,
                    error = Messages.NoServerAccess
                });
            }

            _staffService.UpdateQuote(model.Identifier, model.Username, model.Quote);

            return Json(new
            {
                success = true
            });
        }

        [HttpPost]
        [Route("delete/staff")]
        public JsonResult DeleteStaff(StaffDeleteModel model)
        {
            if (!_staffService.AdminsServer(model.Identifier))
            {
                return Json(new
                {
                    success = false,
                    error = Messages.NoServerAccess
                });
            }

            var lastAdmin = _staffService.IsLastAdmin(model.Identifier, model.Id);

            if (lastAdmin)
            {
                return Json(new
                {
                    success = false,
                    error = Messages.CannotRemoveLastAdmin
                });
            }

            _staffService.DeleteStaff(model.Identifier, model.Id);
            _staffService.RefreshStaff(model.Identifier);

            // clear caches

            RedisStore.Current.ClearServerCaches(model.Identifier);

            return Json(new
            {
                success = true
            });
        }

        [Route("reorder/staff")]
        public void ReorderStaff(ImageReorderModel model)
        {
            if (!_staffService.AdminsServer(model.Identifier))
            {
                return;
            }

            _staffService.ReorderStaff(model.Ids, model.Identifier);
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult Plugins(string identifier)
        {
            var server = _staffService.GetManagedServerByIdentifier(identifier);

            var model = new PluginsModel
            {
                Identifier = identifier,
                Plugins = _pluginService.GetServerPluginsData(identifier),
                ShowPlugins = server.ServerSetting.ShowPlugins
            };

            model.HasPlugins = model.Plugins.Count > 0;

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public async Task<PartialViewResult> Plugins(PluginsModel model)
        {
            model.HasPlugins = model.Plugins.Count > 0;

            var user = _userService.GetCurrentUserCached();

            if (ModelState.IsValid)
            {
                _serverService.EditPluginShow(model.Identifier, model.ShowPlugins);

                var tasks = model.Plugins.Select(x => SafeBrowsing.IsSafe(x.PluginWebsite));
                var results = await Task.WhenAll(tasks);

                for (var i = 0; i < model.Plugins.Count(); i++)
                {
                    var myPlug = model.Plugins[i];
                    var safe = results[i];

                    if (!safe)
                    {
                        model.Plugins = _pluginService.GetServerPluginsData(model.Identifier);

                        ModelState.AddModelError("Error", string.Format(Messages.UnsafeUrl, myPlug.PluginWebsite));
                        return PartialView(model);
                    }
                }

                var success = await _pluginService.UpdatePluginsStatus(model.Identifier, user.UserId, model.Plugins);

                if (success)
                {
                    ViewBag.Success = Messages.SavedSuccessfully;
                }
                else
                {
                    ModelState.AddModelError("error", Messages.PluginListMismatch);
                }
            }

            // clear caches

            RedisStore.Current.ClearServerCaches(model.Identifier);

            model.Plugins = _pluginService.GetServerPluginsData(model.Identifier);

            return PartialView(model);
        }

        [HttpPost]
        public async Task<JsonResult> QueryForPlugins(string identifier)
        {
            if (!_staffService.ManagesServer(identifier))
            {
                return Json(new
                {
                    success = false,
                    error = Messages.NoServerAccess
                });
            }

            var serverId = _serverService.GetServerIdFromIdentifier(identifier);

            var resp = await _queryService.QueryServer(_serverService.GetServerConnectInfo(identifier));

            await _pluginService.UpdatePlugins(serverId, resp.Plugins);

            if (resp.Success)
            {
                return Json(new
                {
                    success = true
                });
            }

            return Json(new
            {
                success = false,
                message = Messages.CouldNotQueryBlock
            });
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult ConnectionInfos(string identifier)
        {
            var model = new ConnectionInfosModel
            {
                Identifier = identifier,
                ConnectionInfos = _connectionInfoService.GetServerConnectionInfosData(identifier),
                Port = 25565
            };

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public async Task<PartialViewResult> ConnectionInfos(ConnectionInfosModel model)
        {
            if (model.ConnectionInfos == null)
            {
                model.ConnectionInfos = _connectionInfoService.GetServerConnectionInfosData(model.Identifier);
            }

            if (ModelState.IsValid)
            {
                // fix up the incoming text

                var split = model.Address.ToLower().Split(':');

                if (split.Length > 1)
                {
                    model.Address = split[0];
                }

                if (model.Port == 0)
                {
                    model.Port = 25565;
                }

                // verify the address does not already exist

                var exists = _connectionInfoService.ExistsConnectionInfo(model.Name, model.Address, model.Port);

                if (exists)
                {
                    ModelState.AddModelError("Address", Messages.ServerExists);
                }

                // check the play reqs

                var urls = new[]
                {
                    model.PlayReqDownloadUrl, model.PlayReqWhitelistUrl
                };

                var tasks = urls.Select(SafeBrowsing.IsSafe);
                var results = await Task.WhenAll(tasks);

                for (var i = 0; i < urls.Count(); i++)
                {
                    var url = urls[i];
                    var safe = results[i];

                    if (!safe)
                    {
                        ModelState.AddModelError("Error", string.Format(Messages.UnsafeUrl, url));
                        return PartialView(model);
                    }
                }

                if (model.PlayReqHasDownload)
                {
                    if (string.IsNullOrEmpty(model.PlayReqDownloadLabel))
                    {
                        ModelState.AddModelError("PlayReqDownloadLabel", string.Format(Messages.IsRequiredWhenChecked, "The Download Label"));
                    }
                    if (string.IsNullOrEmpty(model.PlayReqDownloadUrl))
                    {
                        ModelState.AddModelError("PlayReqDownloadUrl", string.Format(Messages.IsRequiredWhenChecked, "The Download Url"));
                    }
                }

                if (model.PlayReqHasWhitelist)
                {
                    if (string.IsNullOrEmpty(model.PlayReqWhitelistUrl))
                    {
                        ModelState.AddModelError("PlayReqWhitelistUrl", string.Format(Messages.IsRequiredWhenChecked, "The Whitelist Url"));
                    }
                }

                // verify addresses are pingable/queryable

                var resp = await StatusQuery.QueryAsync(model.Address, model.Port, Timeout.FiveSeconds);

                if (resp == null)
                {
                    resp = await StatusQuery.PingAsync(model.Address, model.Port, Timeout.FiveSeconds);

                    if (resp == null)
                    {
                        ModelState.AddModelError("Error", Messages.CouldNotConnect);
                    }
                    else
                    {
                        resp.HasQueryDisabled = true;
                    }
                }

                // do the update

                if (ModelState.IsValid)
                {
                    _connectionInfoService.CreateConnectionInfo(model.Identifier, model.Name, model.Address, model.Port, model.PlayReqDownloadLabel, model.PlayReqDownloadUrl, model.PlayReqHasDownload, model.PlayReqWhitelistUrl, model.PlayReqHasWhitelist);

                    ViewBag.Success = Messages.SavedSuccessfully;

                    // clear connections cache

                    RedisStore.Current.Remove(Constant.Cache.ConnectionInfosNames + model.Identifier);

                    model.ConnectionInfos = _connectionInfoService.GetServerConnectionInfosData(model.Identifier);

                }
            }

            // clear caches

            RedisStore.Current.ClearServerCaches(model.Identifier);

            return PartialView(model);
        }

        [Route("reorder/connectioninfos")]
        public void ReorderConnectionInfos(ImageReorderModel model)
        {
            if (!_staffService.ManagesServer(model.Identifier))
            {
                return;
            }

            _connectionInfoService.ReorderConnectionInfos(model.Ids, model.Identifier);
        }

        [HttpPost]
        [Route("delete/connectioninfo")]
        public JsonResult DeleteConnectionInfo(ImageDeleteModel model)
        {
            if (!_staffService.ManagesServer(model.Identifier))
            {
                return Json(new
                {
                    success = false
                });
            }

            var server = _serverService.GetServerByIdentifier(model.Identifier);

            if (server.ServerConnectionInfos.Count == 1)
            {
                return Json(new
                {
                    success = false,
                    error = Messages.CannotDeleteLastIP
                });
            }

            _connectionInfoService.DeleteConnectionInfo(model.Identifier, model.Id);

            // refresh positions after delete

            _connectionInfoService.RefreshConnectionInfos(model.Identifier);

            // clear caches

            RedisStore.Current.ClearServerCaches(model.Identifier);

            return Json(new
            {
                success = true
            });
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult MiniGames(string identifier)
        {
            var model = new MiniGamesModel
            {
                Identifier = identifier,
                MiniGames = _miniGameService.GetServerMiniGamesData(identifier),
            };

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public async Task<PartialViewResult> MiniGames(MiniGamesModel model)
        {
            model.MiniGames = _miniGameService.GetServerMiniGamesData(model.Identifier) ?? new List<ServerMiniGameData>();

            if (ModelState.IsValid)
            {
                // check for dupe

                if (_miniGameService.ServerHasMiniGame(model.Identifier, model.Name))
                {
                    ModelState.AddModelError("Name", Messages.MiniGameAlreadyAdded);
                    return PartialView(model);
                }

                var isVideo = VideoHelper.IsVideoUrl(model.LogoUrl);

                // check for small images

                if (!isVideo)
                {
                    if (await ImageHelper.ImageTooSmall(model.LogoUrl, Constant.MiniGameLogo.Width,Constant.MiniGameLogo.Height))
                    {
                        ModelState.AddModelError("LogoUrl", Messages.ImageTooSmall);
                        return PartialView(model);
                    }
                }

                // check url safety

                var tasks = model.MiniGames.Select(x => SafeBrowsing.IsSafe(x.Website));
                var results = await Task.WhenAll(tasks);

                for (var i = 0; i < model.MiniGames.Count(); i++)
                {
                    var myMiniGame = model.MiniGames[i];
                    var safe = results[i];

                    if (!safe)
                    {
                        model.MiniGames = _miniGameService.GetServerMiniGamesData(model.Identifier);

                        ModelState.AddModelError("Error", string.Format(Messages.UnsafeUrl, myMiniGame.Website));
                        return PartialView(model);
                    }
                }

                await _miniGameService.CreateMiniGame(model.Identifier, model.Name, model.Description, model.LogoUrl, model.Website);

                ViewBag.Success = Messages.SavedSuccessfully;

                model.MiniGames = _miniGameService.GetServerMiniGamesData(model.Identifier);

                // clear out the form

                ModelState.Clear();
                model.Clear();
            }

            // clear caches

            RedisStore.Current.ClearServerCaches(model.Identifier);

            return PartialView(model);
        }

        [Route("reorder/minigames")]
        public void ReorderMinigames(ImageReorderModel model)
        {
            if (!_staffService.ManagesServer(model.Identifier))
            {
                return;
            }

            _miniGameService.ReorderMiniGames(model.Ids, model.Identifier);
        }

        [HttpPost]
        [Route("delete/minigame")]
        public JsonResult DeleteMinigame(ImageDeleteModel model)
        {
            if (!_staffService.ManagesServer(model.Identifier))
            {
                return Json(new
                {
                    success = false
                });
            }

            _miniGameService.DeleteServerMiniGame(model.Identifier, model.Id);

            // refresh positions after delete

            _miniGameService.RefreshMiniGames(model.Identifier);
            _miniGameService.DeleteUnusedMiniGames();

            // clear caches

            RedisStore.Current.ClearServerCaches(model.Identifier);

            return Json(new
            {
                success = true
            });
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult Modpacks(string identifier)
        {
            var model = new ModpacksModel
            {
                Identifier = identifier,
                Modpacks = _modpackService.GetServerModpacksData(identifier),
            };

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public async Task<PartialViewResult> Modpacks(ModpacksModel model)
        {
            model.Modpacks = _modpackService.GetServerModpacksData(model.Identifier) ?? new List<ServerModpackData>();

            if (ModelState.IsValid)
            {
                // check for dupe

                if (_modpackService.ServerHasModpack(model.Identifier, model.Name))
                {
                    ModelState.AddModelError("Name", Messages.ModpackAlreadyAdded);
                    return PartialView(model);
                }

                // manuallay check dynamic fields

                if (!_modpackService.ExistsModpack(model.Name))
                {
                    if (string.IsNullOrEmpty(model.Website))
                    {
                        ModelState.AddModelError("Website", string.Format(Messages.IsRequired, "Website"));
                        return PartialView(model);
                    }

                    if (string.IsNullOrEmpty(model.Description))
                    {
                        ModelState.AddModelError("Description", string.Format(Messages.IsRequired, "Description"));
                        return PartialView(model);
                    }

                    if (string.IsNullOrEmpty(model.LogoUrl))
                    {
                        ModelState.AddModelError("LogoUrl", string.Format(Messages.IsRequired, "LogoUrl"));
                        return PartialView(model);
                    }
                }

                // check for small images

                if (await ImageHelper.ImageTooSmall(model.LogoUrl, Constant.ModpackLogo.Width, Constant.ModpackLogo.Height))
                {
                    ModelState.AddModelError("LogoUrl", Messages.ImageTooSmall);
                    return PartialView(model);
                }

                // check url safety

                var tasks = model.Modpacks.Select(x => SafeBrowsing.IsSafe(x.ModpackWebsite));
                var results = await Task.WhenAll(tasks);

                for (var i = 0; i < model.Modpacks.Count(); i++)
                {
                    var myMod = model.Modpacks[i];
                    var safe = results[i];

                    if (!safe)
                    {
                        model.Modpacks = _modpackService.GetServerModpacksData(model.Identifier);

                        ModelState.AddModelError("Error", string.Format(Messages.UnsafeUrl, myMod.ModpackWebsite));
                        return PartialView(model);
                    }
                }

                await _modpackService.CreateModpack(model.Identifier, model.Name, model.Description, model.LogoUrl, model.Website);

                ViewBag.Success = Messages.SavedSuccessfully;

                model.Modpacks = _modpackService.GetServerModpacksData(model.Identifier);

                // clear out the form

                ModelState.Clear();
                model.Clear();
            }

            // clear caches

            RedisStore.Current.ClearServerCaches(model.Identifier);

            return PartialView(model);
        }

        [Route("exists/modpack")]
        public JsonResult ExistsModpack(string identifier)
        {
            return Json(new
            {
                exists = _modpackService.ExistsModpack(identifier)
            });
        }

        [Route("reorder/modpacks")]
        public void ReorderModpacks(ImageReorderModel model)
        {
            if (!_staffService.ManagesServer(model.Identifier))
            {
                return;
            }

            _modpackService.ReorderModpacks(model.Ids, model.Identifier);
        }

        [HttpPost]
        [Route("delete/modpack")]
        public JsonResult DeleteModpack(ImageDeleteModel model)
        {
            if (!_staffService.ManagesServer(model.Identifier))
            {
                return Json(new
                {
                    success = false
                });
            }

            _modpackService.DeleteServerModpack(model.Identifier, model.Id);

            // refresh positions after delete

            _modpackService.RefreshModpacks(model.Identifier);
            _modpackService.DeleteUnusedModpacks();

            // clear caches

            RedisStore.Current.ClearServerCaches(model.Identifier);

            return Json(new
            {
                success = true
            });
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult Tags(string identifier)
        {
            var model = new TagsModel
            {
                Identifier = identifier,
                Gameplays = _tagsService.GetGameplaysString(identifier),
                Types = _tagsService.GetTypesString(identifier)
            };

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public PartialViewResult Tags(TagsModel model)
        {
            if (ModelState.IsValid)
            {
                // delete existing links

                _tagsService.DeleteServerTypes(model.Identifier);
                _tagsService.DeleteServerGameplays(model.Identifier);

                // add in new tags and links

                if (!string.IsNullOrEmpty(model.Types))
                {
                    foreach (var tag in model.Types.Split(','))
                    {
                        _tagsService.CreateTypeIfNotExistsAndLink(tag, model.Identifier);
                    }
                }

                if (!string.IsNullOrEmpty(model.Gameplays))
                {
                    foreach (var tag in model.Gameplays.Split(','))
                    {
                        _tagsService.CreateGameplayIfNotExistsAndLink(tag, model.Identifier);
                    }
                }

                // delete unused tags

                _tagsService.DeleteUnusedTypes();
                _tagsService.DeleteUnusedGameplays();

                // pull them

                model.Gameplays = _tagsService.GetGameplaysString(model.Identifier);
                model.Types = _tagsService.GetTypesString(model.Identifier);

                ViewBag.Success = Messages.SavedSuccessfully;
            }

            return PartialView(model);
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult Colors(string identifier)
        {
            var server = _staffService.GetManagedServerByIdentifier(identifier);

            var model = Mapper.Map<ColorsModel>(server.Color);

            model.Identifier = identifier;

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public PartialViewResult Colors(ColorsModel model)
        {
            if (ModelState.IsValid)
            {
                // edit description

                _serverService.EditColors(model.Identifier, model.Header, model.Background, model.Middle, model.IsDarkBackground, model.IsDarkHeader, model.IsDarkMiddle);

                // clear caches

                RedisStore.Current.ClearServerCaches(model.Identifier);

                ViewBag.Success = Messages.SavedSuccessfully;
            }

            return PartialView(model);
        }

        /****************************************************************************************************************************/

        [ServerManagersOnly]
        public PartialViewResult Websites(string identifier)
        {
            var server = _staffService.GetManagedServerByIdentifier(identifier);

            var model = Mapper.Map<WebsitesModel>(server.Website);

            model.Identifier = identifier;

            return PartialView(model);
        }

        [HttpPost]
        [ServerManagersOnly]
        public async Task<PartialViewResult> Websites(WebsitesModel model)
        {
            if (ModelState.IsValid)
            {
                var urls = new[]
                {
                    model.Url1, model.Url2, model.Url3, model.Url4
                };

                var tasks = urls.Select(SafeBrowsing.IsSafe);
                var results = await Task.WhenAll(tasks);

                for (var i = 0; i < urls.Count(); i++)
                {
                    var url = urls[i];
                    var safe = results[i];

                    if (!safe)
                    {
                        ModelState.AddModelError("Error", string.Format(Messages.UnsafeUrl, url));
                        return PartialView(model);
                    }
                }

                _serverService.EditWebsites(model.Identifier, model.Url1, model.Title1, model.Url2, model.Title2, model.Url3, model.Title3, model.Url4, model.Title4);

                ViewBag.Success = Messages.SavedSuccessfully;

                // clear caches

                RedisStore.Current.ClearServerCaches(model.Identifier);
            }

            return PartialView(model);
        }

        /****************************************************************************************************************************/

        [HttpPost]
        [ServerManagersOnly]
        public void Enable(string identifier, bool enabled)
        {
            var server = _staffService.GetManagedServerByIdentifier(identifier);

            if (server != null)
            {
                _serverService.EditEnabled(identifier, enabled);
            }

            // clear caches

            RedisStore.Current.ClearServerCaches(identifier);
        }
    }
}
