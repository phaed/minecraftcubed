﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Static;
using MinecraftCubed.Web.Mvc.Attributes;
using MinecraftCubed.Web.ViewModels.Admin;

namespace MinecraftCubed.Web.Controllers.Backend
{
    [RequireSecure]
    [RequireRole(UserRole.SysAdmin)]
    public class AdminController : Controller
    {
        private readonly IServerService _serverService;
        private readonly IEventService _eventService;
        private readonly IMiniGameService _miniGameService;
        private readonly IModpackService _modpackService;
        private readonly IPluginService _pluginService;

        public AdminController(IMiniGameService miniGameService, IModpackService modpackService, IPluginService pluginService, IServerService serverService, IEventService eventService)
        {
            _miniGameService = miniGameService;
            _modpackService = modpackService;
            _pluginService = pluginService;
            _serverService = serverService;
            _eventService = eventService;
        }

        public ActionResult Index()
        {
            return View(new AdminModel());
        }

        public ActionResult RecutImages(AdminModel model)
        {
            _miniGameService.RecutAllImages();
            //_modpackService.RecutAllImages();
            //await _pluginService.RecutAllImages();

            model.RefreshUI = true;
            return View(model);
        }
    
        public ActionResult ClearAllCaches(AdminModel model)
        {
            RedisStore.Current.ClearFilterCaches();
            _eventService.ClearAllEventCaches();
            _serverService.ClearAllServerCaches();
            RedisStore.Current.Remove(Constant.Cache.MatchingServers, RedisDatabase.BuiltObjects);

            model.RefreshUI = true;
            return View(model);
        }
    }
}