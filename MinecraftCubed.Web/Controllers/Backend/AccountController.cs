﻿using System.Net.Configuration;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Minecraft;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Lang;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Web.Mvc.Attributes;
using MinecraftCubed.Web.ViewModels.Account;
using StructureMap.Query;

namespace MinecraftCubed.Web.Controllers.Backend
{
    [RequireSecure]
    [RequireRole(UserRole.Unverified)]
    public class AccountController : BaseController
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        [Route("preferences")]
        public ActionResult Index()
        {
            var user = _userService.GetCurrentUserCached();

            var model = new AccountModel
            {
                Username = user.Username,
            };

            return View(model);
        }

        /****************************************************************************************************************************/

        public ActionResult EmailSettings()
        {
            var user = _userService.GetCurrentUserCached();

            var model = new EmailSettingsModel
            {
                Email = user.Email,
                ReceiveEventNotifications = user.ReceiveEventNotifications
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult EmailSettings(EmailSettingsModel model)
        {
            model.RefreshUI = true;

            if (ModelState.IsValid)
            {
                // change email

                var user = _userService.GetCurrentUserCached();

                if (user.Email != model.Email)
                {
                    _userService.ChangeUserEmail(model.Email);
                }

                // update settings

                _userService.UpdateUserSettings(new UserSettings
                {
                    ReceiveEventNotifications = model.ReceiveEventNotifications
                });

                ViewBag.Success = Messages.SavedSuccessfully;
            }

            return View(model);
        }

        /****************************************************************************************************************************/

        public ActionResult ChangePassword()
        {
            return View(new ChangePasswordModel());
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            model.RefreshUI = true;

            if (ModelState.IsValid)
            {
                // change password

                if (model.Password.Length < Constant.PasswordMinSize)
                {
                    ModelState.AddModelError("Password", string.Format(Messages.PasswordMinSize, Constant.PasswordMinSize));
                    return View(model);
                }

                var user = _userService.GetCurrentUserCached();

                _userService.ChangeUserPassword(user.Email, model.Password);
                ViewBag.Success = Messages.SavedSuccessfully;
            }

            return View(model);
        }


        /****************************************************************************************************************************/

        public ActionResult VerifyAccount()
        {
            return View(new VerifyAccountModel());
        }
    }
}
