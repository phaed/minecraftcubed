﻿using System.Web.Mvc;
using MinecraftCubed.Common.Lang;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Static;
using MinecraftCubed.Web.Mvc.Attributes;
using MinecraftCubed.Web.ViewModels.AddEvent;

namespace MinecraftCubed.Web.Controllers.Backend
{
    [RequireSecure]
    [RequireRole(UserRole.Unverified)]
    public class AddEventController : BaseController
    {
        private readonly IStaffService _staffService;
        private readonly IEventService _eventService;

        public AddEventController(IEventService eventService, IStaffService staffService)
        {
            _eventService = eventService;
            _staffService = staffService;
        }

        public ActionResult Index()
        {
            return View(new AddEventModel
            {
                ServerNameList = _staffService.GetManagedServersList()
            });
        }

        [HttpPost]
        [ServerManagersOnly]
        public ActionResult Index(AddEventModel model)
        {
            model.RefreshUI = true;
            model.ServerNameList = _staffService.GetManagedServersList();

            if (ModelState.IsValid)
            {
                // check if the event is already submitted

                var exists = _eventService.ExistsEvent(model.Name);

                if (exists)
                {
                    ModelState.AddModelError("Name", Messages.EventExists);
                    return View(model);
                }

                // needs server to create event

                if (string.IsNullOrEmpty(model.Identifier))
                {
                    ModelState.AddModelError("Identifier", Messages.NeedsServerToCreateEvent);
                    return View(model);
                }

                // add event

                var eve = _eventService.AddEvent(model.Name, model.Identifier);

                if (eve != null)
                {
                    return RedirectToAction("Index", "EditEvent");
                }

                ModelState.AddModelError("Error", Messages.CouldNotConnect);
            }
            
            return View(model);
        }
    }
}
