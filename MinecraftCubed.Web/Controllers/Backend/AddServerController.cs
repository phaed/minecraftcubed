﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Lang;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Minecraft;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Static;
using MinecraftCubed.Web.Mvc.Attributes;
using MinecraftCubed.Web.ViewModels.AddServer;

namespace MinecraftCubed.Web.Controllers.Backend
{
    [RequireSecure]
    public class AddServerController : BaseController
    {
        private readonly IServerService _serverService;
        private readonly ISponsorService _sponsorService;
        private readonly IQueryService _queryService;
        private readonly IStaffService _staffService;
        private readonly ITagsService _tagsService;
        private readonly IUserService _userService;
        private readonly IAuthService _authService;

        public AddServerController(IServerService serverService, ITagsService tagsService, IQueryService queryService, ISponsorService sponsorService, IStaffService staffService, IUserService userService, IAuthService authService)
        {
            _serverService = serverService;
            _tagsService = tagsService;
            _queryService = queryService;
            _sponsorService = sponsorService;
            _staffService = staffService;
            _userService = userService;
            _authService = authService;
        }

        public ActionResult Index()
        {
            var model = new AddServerModel
            {
                Port = 25565,
                NoUser = _userService.GetCurrentUser() == null
            };

            if (!model.NoUser)
            {
                model.Username = "Player";
                model.Email = "player@minecraft.com";
                model.Password = "none";
            }

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Index(AddServerModel model)
        {
            User user = null;
            model.RefreshUI = true;
            model.NoUser = _userService.GetCurrentUser() == null;

            // check the play reqs

            var urls = new[]
                {
                    model.PlayReqDownloadUrl, model.PlayReqWhitelistUrl
                };

            var tasks = urls.Select(SafeBrowsing.IsSafe);
            var results = await Task.WhenAll(tasks);

            for (var i = 0; i < urls.Count(); i++)
            {
                var url = urls[i];
                var safe = results[i];

                if (!safe)
                {
                    ModelState.AddModelError("Error", string.Format(Messages.UnsafeUrl, url));
                    return PartialView(model);
                }
            }

            if (model.PlayReqHasDownload)
            {
                if (string.IsNullOrEmpty(model.PlayReqDownloadLabel))
                {
                    ModelState.AddModelError("PlayReqDownloadLabel", "The Download Label is required when you have this section checked.");
                }
                if (string.IsNullOrEmpty(model.PlayReqDownloadUrl))
                {
                    ModelState.AddModelError("PlayReqDownloadUrl", "The Download Url is required when you have this section checked.");
                }
            }

            if (model.PlayReqHasWhitelist)
            {
                if (string.IsNullOrEmpty(model.PlayReqWhitelistUrl))
                {
                    ModelState.AddModelError("PlayReqWhitelistUrl", "The Whitelist Url is required when you have this section checked.");
                }
            }
            
            if (ModelState.IsValid)
            {
                if (model.NoUser)
                {
                    // see if they are an existing user

                    user = _authService.LogIn(model.Email, model.Password);

                    // nope, check for dupes

                    if (user == null)
                    {
                        // email exists

                        if (_userService.ExistsEmail(model.Email))
                        {
                            ModelState.AddModelError("Error", Messages.EmailAlreadyRegistered);
                            return View(model);
                        }

                        // username exists

                        if (_userService.ExistsUsername(model.Username))
                        {
                            ModelState.AddModelError("Error", Messages.UserAlreadyRegistered);
                            return View(model);
                        }
                    }
                }

                // fix up the incoming text

                var split = model.Address.ToLower().Split(':');

                if (split.Length > 1)
                {
                    model.Address = split[0];
                }

                if (model.Port == 0)
                {
                    model.Port = 25565;
                }

                // check if the server is already submitted

                var exists = _serverService.ExistsServer(model.Address, model.Port);

                if (exists)
                {
                    ModelState.AddModelError("Address", Messages.ServerExists);
                    ModelState.AddModelError("Port", string.Empty);
                    return View(model);
                }

                var hostId = _tagsService.CreateHostIfNotExists(model.HostName);

                // try a ping and a query and see if you can reach the server

                var resp = await _queryService.PingServer(new ServerConnectInfo
                {
                    Address = model.Address,
                    Port = model.Port
                }, Timeout.FiveSeconds);

                if (!resp.Success)
                {
                    ModelState.AddModelError("Error", Messages.CouldNotConnect);
                    return View(model);
                }
               
                // create the user

                if (model.NoUser)
                {
                    if (user == null)
                    {
                        user = _userService.CreateUser(model.Email, model.Username, model.Password, "", false);

                        // store head on S3

                        if (user != null)
                        {
                            var username = user.Username;

                            Task.Run(() => _userService.SaveMinecraftHead(username));
                        }
                    }

                    // log him in

                    if (user != null)
                    {
                        FormsAuthentication.SetAuthCookie(user.Username, Settings.PersistLogin);
                    }
                }

                // server reached, add it

                var server = _serverService.AddServer(model.Name, model.Address, model.Port, hostId, resp.Version, false, model.PlayReqDownloadLabel, model.PlayReqDownloadUrl, model.PlayReqHasDownload, model.PlayReqWhitelistUrl, model.PlayReqHasWhitelist);

                // delete unused hosts and versions

                _tagsService.DeleteUnusedHosts();
                _tagsService.DeleteUnusedVersions();

                // add user as staff

                if (user == null)
                {
                    user = _userService.GetCurrentUser();
                }

                _staffService.AddStaff(server.Identifier, user.Username, true, false, Constant.DefaultRank, Constant.DefaultRankColor);
                
                return RedirectToAction("Index", "EditServer", new { identifier = server.Identifier });
            }

            return View(model);
        }
    }
}
