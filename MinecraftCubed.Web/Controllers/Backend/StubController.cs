﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Models.Delete;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Lang;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Web.Mvc.Attributes;
using MinecraftCubed.Web.ViewModels.Stub_;

namespace MinecraftCubed.Web.Controllers.Backend
{
    [RequireSecure]
    [RequireRole(UserRole.Editor)]
    public class StubController : BaseController
    {
        private readonly IServerService _serverService;
        private readonly IEventService _eventService;
        private readonly IStubService _stubService;
        private readonly IQueryService _queryService;
        private readonly ISponsorService _sponsorService;
        private readonly IUserService _userService;

        public StubController(IServerService serverService, IStubService stubService, IQueryService queryService, ISponsorService sponsorService, IEventService eventService, IUserService userService)
        {
            _serverService = serverService;
            _stubService = stubService;
            _queryService = queryService;
            _sponsorService = sponsorService;
            _eventService = eventService;
            _userService = userService;
        }

        [Route("stubs")]
        public ActionResult Index()
        {
            return View();
        }

        /****************************************************************************************************************************/

        public PartialViewResult CreateStub()
        {
            return PartialView(new CreateStubModel
            {
                Port = 25565
            });
        }

        [HttpPost]
        public async Task<PartialViewResult> CreateStub(CreateStubModel model)
        {
            model.RefreshUI = true;

            if (ModelState.IsValid)
            {
                // fix up the incoming text

                var split = model.Address.ToLower().Split(':');

                if (split.Length > 1)
                {
                    model.Address = split[0];
                }

                if (model.Port == 0)
                {
                    model.Port = 25565;
                }

                // check if the server is already submitted

                var exists = _serverService.ExistsServer(model.Address, model.Port);

                if (exists)
                {
                    ModelState.AddModelError("Address", Messages.ServerExists);
                    ModelState.AddModelError("Port", string.Empty);
                    return PartialView(model);
                }

                // try a ping and a query and see if you can reach the server

                var resp = await _queryService.PingServer(new ServerConnectInfo
                {
                    Address = model.Address,
                    Port = model.Port
                });
                
                if (!resp.Success)
                {
                    ModelState.AddModelError("Error", Messages.CouldNotConnect);
                    return PartialView(model);
                }

                // server reached, add it

                var server = _serverService.AddServer(model.Name, model.Address, model.Port, Constant.UnknownHostId, resp.Version, false, string.Empty, string.Empty, false, string.Empty, false);
                
                // create the stub

                _stubService.CreateStub(server.ServerId, model.Contact);

                ViewBag.Success = Messages.AddedSuccessfully;
            }

            return PartialView(model);
        }


        /****************************************************************************************************************************/

        public PartialViewResult CreateEvent()
        {
            return PartialView(new CreateEventModel
            {
                ServerNameList = _stubService.GetUnclaimedStubServers()
            });
        }

        [HttpPost]
        public PartialViewResult CreateEvent(CreateEventModel model)
        {
            model.RefreshUI = true;

            if (ModelState.IsValid)
            {
                // check if the event is already submitted

                var exists = _eventService.ExistsEvent(model.Name);

                if (exists)
                {
                    ModelState.AddModelError("Name", Messages.EventExists);
                    return PartialView(model);
                }

                // needs server to create event

                if (string.IsNullOrEmpty(model.Identifier))
                {
                    ModelState.AddModelError("Identifier", Messages.NeedsServerToCreateEvent);
                    return PartialView(model);
                }

                // add event

                var eve = _eventService.AddEvent(model.Name, model.Identifier);

                if (eve == null)
                {
                    ModelState.AddModelError("Error", Messages.CouldNotConnect);
                }
            }

            ViewBag.Success = Messages.AddedSuccessfully;

            model.ServerNameList = _stubService.GetUnclaimedStubServers();

            return PartialView(model);
        }

        /****************************************************************************************************************************/
        
        public PartialViewResult Listing()
        {
            return PartialView(new ListingModel
            {
                Editors = _stubService.GetStubEditors().ToList(),
                ActiveUsername = _userService.GetCurrentUserCached().Username
            });
        }

        [HttpPost]
        [Route("delete/stub")]
        public JsonResult DeleteStub(StubDeleteModel model)
        {
            _stubService.DeleteStubAndServer(model.Id);

            // clear caches

            RedisStore.Current.ClearServerCaches(model.Key);

            return Json(new
            {
                success = true
            });
        }

        [HttpPost]
        [Route("delete/event")]
        public JsonResult DeleteEvent(StubDeleteModel model)
        {
            _eventService.DeleteEvent(model.Key);

            return Json(new
            {
                success = true
            });
        }
    }
}