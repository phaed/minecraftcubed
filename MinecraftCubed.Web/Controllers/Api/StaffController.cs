﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Models.WebApi;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Static;
using MinecraftCubed.Web.Mvc.Attributes;

namespace MinecraftCubed.Web.Controllers.Api
{
    [RequireSecureWebApi]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class StaffController : ApiController
    {
        private readonly IStaffRepository _repository;
        private readonly IStaffService _staffService;
        private readonly IServerRepository _serverRepository;

        public StaffController(IStaffRepository repository, IServerRepository serverRepository, IStaffService staffService)
        {
            _repository = repository;
            _serverRepository = serverRepository;
            _staffService = staffService;
        }

        // GET api/staff/5
        [Route("api/v1/staff/{identifier}")]
        public dynamic Get(string identifier)
        {
            return
                _repository.GetStaff(identifier)
                    .Include(x => x.User)
                    .Include(x => x.User.ServerStaffs)
                    .OrderBy(x => x.User.Username)
                    .ToList()
                    .Select(x =>
                    {
                        var staff =
                            x.User.ServerStaffs.Single(
                                z => z.Server.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));

                        return new
                        {
                            x.User.Username,
                            staff.Rank,
                            staff.Color,
                            staff.Quote,
                        };
                    });
        }

        // POST api/staff
        [Route("api/v1/staff/{identifier}")]
        public dynamic Post(string identifier, [FromBody]StaffPut value)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            if (server != null)
            {
                if (!server.ServerSetting.ApiKey.Equals(value.ApiKey, StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new HttpResponseException(new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.Forbidden,
                        ReasonPhrase = "Invalid API Key"
                    });
                }
            }

            _staffService.UpdateStaff(identifier, value.Username, value.IsAdmin, value.IsManager, value.Rank, value.Color, value.Quote);

            return Get(identifier);
        }

        // PUT api/staff/5
        [Route("api/v1/staff/{identifier}")]
        public dynamic Put(string identifier, [FromBody]StaffPut value)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            if (server != null)
            {
                if (!server.ServerSetting.ApiKey.Equals(value.ApiKey, StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new HttpResponseException(new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.Forbidden,
                        ReasonPhrase = "Invalid API Key"
                    });
                }
            }

            _staffService.AddStaff(identifier, value.Username, value.IsAdmin, value.IsManager, value.Rank, value.Color);

            return Get(identifier);
        }

        // DELETE api/staff/5
        [Route("api/v1/staff/{identifier}")]
        public dynamic Delete(string identifier, [FromBody]StaffDelete value)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            if (server != null)
            {
                if (!server.ServerSetting.ApiKey.Equals(value.ApiKey, StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new HttpResponseException(new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.Forbidden,
                        ReasonPhrase = "Invalid API Key"
                    });
                }
            }

            _staffService.DeleteStaff(identifier, value.Username);

            return Get(identifier);
        }
    }
}
