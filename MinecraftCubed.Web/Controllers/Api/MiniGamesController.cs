﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Static;
using MinecraftCubed.Web.Mvc.Attributes;

namespace MinecraftCubed.Web.Controllers.Api
{
    [RequireSecureWebApi]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MiniGamesController : ApiController
    {
        private readonly IMiniGameRepository _repository;

        public MiniGamesController(IMiniGameRepository repository)
        {
            _repository = repository;
        }

        // GET api/minigames
        [Route("api/v1/minigames")]
        public IEnumerable<dynamic> Get()
        {
            return _repository.GetMiniGames().OrderBy(x => x.Name).ToList().Select(x => new
            {
                x.Identifier,
                x.Name,
                ServerMiniGames = x.ServerMiniGames.Select(m => new
                {
                    ServerIdentifier = m.Server.Identifier,
                    m.Description,
                    m.LogoUrl
                })
            });
        }

        // GET api/minigames/5
        [Route("api/v1/minigames/{identifier}")]
        public dynamic Get(string identifier)
        {
            var mg = _repository.FindMiniGame(identifier);

            return new
            {
                mg.Identifier,
                mg.Name,
                ServerMiniGames = mg.ServerMiniGames.Select(m => new
                {
                    ServerIdentifier = m.Server.Identifier,
                    m.Description,
                    m.LogoUrl
                })
            };
        }

        // POST api/minigames
        [Route("api/v1/minigames")]
        public IEnumerable<dynamic> Post([FromBody]string value)
        {
            return _repository.FindMiniGames(value).ToList().Select(x => new
            {
                x.Identifier,
                x.Name,
                ServerMiniGames = x.ServerMiniGames.Select(m => new
                {
                    ServerIdentifier = m.Server.Identifier,
                    m.Description,
                    m.LogoUrl
                })
            });
        }

        // PUT api/minigames/5
        [Route("api/v1/minigames/{identifier}")]
        public void Put(string identifier, [FromBody]string value)
        {
        }

        // DELETE api/minigames/5
        [Route("api/v1/minigames/{identifier}")]
        public void Delete(string identifier)
        {
        }
    }
}
