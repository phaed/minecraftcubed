﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Web.Mvc.Attributes;

namespace MinecraftCubed.Web.Controllers.Api
{
    [RequireSecureWebApi]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ModpacksController : ApiController
    {
        private readonly IModpackRepository _repository;

        public ModpacksController(IModpackRepository repository)
        {
            _repository = repository;
        }

        // GET api/modpacks
        [Route("api/v1/modpacks")]
        public IEnumerable<dynamic> Get()
        {
            return _repository.GetModpacks().OrderBy(x => x.Name).ToList().Select(x => new
            {
                x.Identifier,
                x.Name,
                x.Description,
                WebsiteUrl = x.Website,
                x.LogoUrl,
                ResizedLogoUrl = x.GetAbsoluteUrl()
            });
        }

        // GET api/modpacks/5
        [Route("api/v1/modpacks/{identifier}")]
        public dynamic Get(string identifier)
        {
            var mp = _repository.FindModpack(identifier);

            return new
            {
                mp.Identifier,
                mp.Name,
                mp.Description,
                WebsiteUrl = mp.Website,
                OriginalLogoUrl = mp.LogoUrl,
                LogoUrl = mp.GetAbsoluteUrl()
            };
        }

        // POST api/modpacks
        [Route("api/v1/modpacks")]
        public IEnumerable<dynamic> Post([FromBody]string value)
        {
            return _repository.FindModpacks(value).ToList().Select(x => new
            {
                x.Identifier,
                x.Name,
                x.Description,
                WebsiteUrl = x.Website,
                OriginalLogoUrl = x.LogoUrl,
                LogoUrl = x.GetAbsoluteUrl()
            });
        }

        // PUT api/modpacks/5
        [Route("api/v1/modpacks/{identifier}")]
        public void Put(string identifier, [FromBody]string value)
        {
        }

        // DELETE api/modpacks/5
        [Route("api/v1/modpacks/{identifier}")]
        public void Delete(string identifier)
        {
        }
    }
}
