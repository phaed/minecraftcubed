﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using MinecraftCubed.Web.Mvc.Attributes;

namespace MinecraftCubed.Web.Controllers.Api
{
    [RequireSecureWebApi]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class VotersController : ApiController
    {
        // GET api/voters
        public IEnumerable<string> Get()
        {
            return new [] { "value1", "value2" };
        }

        // GET api/voters/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/voters
        public void Post([FromBody]string value)
        {
        }

        // PUT api/voters/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/voters/5
        public void Delete(int id)
        {
        }
    }
}
