﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Lang;
using MinecraftCubed.Minecraft;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Static;
using MinecraftCubed.Web.Mvc.Attributes;
using MinecraftCubed.Web.ViewModels.Claim;

namespace MinecraftCubed.Web.Controllers
{
    public class ClaimController : Controller
    {
        private readonly IStubService _stubService;
        private readonly IUserService _userService;
        private readonly IStaffService _staffService;
        private readonly IServerService _serverService;

        public ClaimController(IStubService stubService, IUserService userService, IStaffService staffService, IServerService serverService)
        {
            _stubService = stubService;
            _userService = userService;
            _staffService = staffService;
            _serverService = serverService;
        }

        [Route("claim/{accessCode}")]
        public ActionResult Index(string accessCode)
        {
            var server = _stubService.GetServerFromStub(accessCode);

            if (server != null)
            {
                return View(new ClaimModel
                {
                    Server = server,
                    AccessCode = accessCode,
                    BannerUrl = _serverService.GetServerBannerUrl(server)
                });
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> Index(ClaimModel model)
        {
            model.RefreshUI = true;

            // exit if access codes does not match a server

            var server = _stubService.GetServerFromStub(model.AccessCode);

            if (server == null)
            {
                ModelState.AddModelError("Error", Messages.NoServerAccess);
                return PartialView(model);
            }

            model.BannerUrl = _serverService.GetServerBannerUrl(server);
            
            // exit if errors in model

            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }

            // exit if email exists

            if (_userService.ExistsEmail(model.Email))
            {
                ModelState.AddModelError("Error", Messages.EmailAlreadyRegistered);
                return PartialView(model);
            }

            // create the user, store head on s3, and log him in

            var user = _userService.CreateUser(model.Email, model.Username, model.Password, null, false);

            FormsAuthentication.SetAuthCookie(user.Username, Settings.PersistLogin);

            await _userService.SaveMinecraftHead(model.Username);

            // add him as admin to the server
            
            _staffService.AddStaff(server.Identifier, user.Username, true, false, Constant.DefaultRank, Constant.DefaultRankColor);

            // claim the stub

            _stubService.ClaimStub(model.AccessCode);

            // redirect him to open server page

            return RedirectToAction("Index", "EditServer", new { identifier = server.Identifier });
        }
    }
}