﻿using System.Linq;
using System.Web.Mvc;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Web.Mvc.Attributes;
using MinecraftCubed.Web.ViewModels.User_;


namespace MinecraftCubed.Web.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IStaffService _staffService;

        public UserController(IUserService userService, IStaffService staffService)
        {
            _userService = userService;
            _staffService = staffService;
        }

        public ActionResult Index()
        {
            return RedirectToAction("Index", "List");
        }

        public JsonResult GetCurrentUsername()
        {
            var user = _userService.GetCurrentUserCached();

            if (user != null)
            {
                return Json(user.Username);
            }
            return null;
        }

        [ChildActionOnly]
        public PartialViewResult UserNav()
        {
            // if not authenticated show the button

            if (!Request.IsAuthenticated)
            {
                return PartialView("LoginButton");
            }

            // otherwise show the user nav

            var user = _userService.GetCurrentUserCached();
            var servers = _staffService.GetManagedServerList();
            var events = _staffService.GetManagedEventList();
        
            var model = new UserNavModel
            {
                ManagedServers = servers,
                ManagedEvents = events
            };

            if (user != null)
            {
                model.Username = user.Username;
                model.FaceUrl = user.GetFaceUrl();
            }

            return PartialView(model);
        }

        [ChildActionOnly]
        public PartialViewResult SettingsNav()
        {
            return PartialView();
        }

        [ChildActionOnly]
        public PartialViewResult EventSettingsNav()
        {
            return PartialView();
        }
    }
}
