﻿using System.Web.Mvc;
using MinecraftCubed.Common;

namespace MinecraftCubed.Web.Controllers
{
    public class InsecureController : BaseController
    {
        [Route("pluginmessage")]
        public ActionResult PluginMessage()
        {
            return Content("Sign up your server with http://minecraftcubed.net - server and event list! Having a tournament, need players? Sign up yout event!");
        }
    }
}
