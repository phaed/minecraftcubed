﻿using System.Web.Mvc;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Domain.Models.Events;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Static;
using MinecraftCubed.Web.Mvc.Attributes;
using MinecraftCubed.Web.ViewModels.Events;

namespace MinecraftCubed.Web.Controllers
{
    public class EventsController : BaseController
    {
        private readonly IEventService _eventService;
        private readonly IUserService _userService;

        public EventsController(IEventService eventService, IUserService userService)
        {
            _eventService = eventService;
            _userService = userService;
        }

        [OutputCache(Duration = 60)]
        public ActionResult Index()
        {
            return View(new EventsModel());
        }

        [HttpPost]
        public JsonResult GetEvents(EventsState filters, string requestId, int skip, int take)
        {
            var result = _eventService.GetEvents(filters, skip, take);

            // sending back the request id to later dice to discard it or not
            result.RequestId = requestId;

            return Json(result);
        }

        [HttpPost]
        public JsonResult GetHomepageEvents()
        {
            var result = _eventService.GetActiveEvents(Constant.ActiveEventsToShow);

            return Json(result);
        }

        [HttpPost]
        public void AddAsAttendee(int eventDateId)
        {
            _eventService.AddAsAttendee(eventDateId);
        }

        [HttpPost]
        public void RemoveAsAttendee(int eventDateId)
        {
            _eventService.RemoveAsAttendee(eventDateId);
        }
    }
}
