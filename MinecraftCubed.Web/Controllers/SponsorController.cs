﻿using System;
using System.Diagnostics;
using System.Net;
using System.Web.Mvc;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Static;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Web.Mvc.Attributes;


namespace MinecraftCubed.Web.Controllers
{
    [RequireSecure]
    public class SponsorController : BaseController
    {
        private readonly ISponsorService _sponsorService;
        private readonly IServerService _serverService;
        private readonly IUserService _userService;
        private readonly IStaffService _staffService;
                
        public SponsorController(ISponsorService sponsorService, IServerService serverService, IUserService userService, IStaffService staffService)
        {
            _sponsorService = sponsorService;
            _serverService = serverService;
            _userService = userService;
            _staffService = staffService;
        }

        [RequireRole(UserRole.Unverified)]
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Ipn()
        {
            var bytes = Request.BinaryRead(Request.ContentLength);

            var message = new PayPal.IPNMessage(bytes);

            if (message.Validate())
            {
                var trans = new PayPalT(message);

                if (trans.IsComplete)
                {
                    // store payment

                    //_sponsorService.AddBid(trans.ItemNumber, trans.TransactionId, trans.GrossAmount, trans.PayerEmail, trans.Currency, trans.PaymentDate);
                    
                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
                else
                {
                    Trace.TraceError("Failed Paypal IPN: " + trans.PendingReason);
                }
            }
            else
            {
                Trace.TraceError("Received Invalid IPN");
                return new HttpNotFoundResult("Not valid.");
            }

            return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
        }

        public ActionResult Success()
        {
            return View();
        }
    }
}
