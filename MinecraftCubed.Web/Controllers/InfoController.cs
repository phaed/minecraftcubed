﻿using System.Web.Mvc;
using MinecraftCubed.Web.Mvc.Attributes;

namespace MinecraftCubed.Web.Controllers
{
    public class InfoController : BaseController
    {
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }
        
        public ActionResult Api()
        {
            return View();
        }

        public ActionResult Terms()
        {
            return View();
        }
        public ActionResult Privacy()
        {
            return View();
        }
        public ActionResult FAQ()
        {
            return View();
        }
    }
}
