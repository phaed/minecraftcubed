﻿using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using MinecraftCubed.Domain.Models.List;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Web.Mvc.Attributes;
using MinecraftCubed.Web.ViewModels.Home;

namespace MinecraftCubed.Web.Controllers
{
    public class ListController : BaseController
    {
        private readonly IServerService _serverService;
        private readonly IHomeService _homeService;

        public ListController(IServerService serverService, IHomeService homeService)
        {
            _serverService = serverService;
            _homeService = homeService;
        }

        [Route("servers")]
        [OutputCache(Duration = 5)]
        public async Task<ActionResult> Index()
        {
            return View(new HomeModel
            {
                FaceUrls = await _homeService.GetFaceUrlsForHero()
            });
        }

        [HttpPost]
        public JsonResult GetServers(ListState state, string requestId, int skip, int take)
        {
            var result = _serverService.GetServers(state, skip, take);

            // sending back the request id to later dice to discard it or not
            result.RequestId = requestId;

            return Json(result);
        }
        
        [HttpPost]
        public JsonResult GetOrganicServersForPing()
        {
            var result = _serverService.GetOrganicServersForPing();

            return Json(result);
        }
    }
}
