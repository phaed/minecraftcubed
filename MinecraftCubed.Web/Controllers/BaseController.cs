﻿using System.Web.Mvc;
using MinecraftCubed.Web.Mvc.Attributes;

namespace MinecraftCubed.Web.Controllers
{
    [P3PHeader]
    public abstract class BaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Request.IsAuthenticated)
            {
                filterContext.Controller.ViewBag.NoLogin = true;
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
