﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Static;
using MinecraftCubed.Web.Mvc.Attributes;
using MinecraftCubed.Web.ViewModels.Home;

namespace MinecraftCubed.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IHomeService _homeService;
        private readonly IEventService _eventService;

        public HomeController(IHomeService homeService, IEventService eventService)
        {
            _homeService = homeService;
            _eventService = eventService;
        }

        [OutputCache(Duration = 5)]
        public ActionResult Index()
        {
            return RedirectToAction("Index", "List");
        }

        /*
        [OutputCache(Duration = 5)]
        public async Task<ActionResult> Index()
        {
            return View(new HomeModel
            {
                FaceUrls = await _homeService.GetFaceUrlsForHero()
            });
        }*/

        [HttpPost]
        public async Task<ActionResult> Index(HomeModel model)
        {
            if (ModelState.IsValid)
            {
            }

            model.FaceUrls = await _homeService.GetFaceUrlsForHero();
            return View(model);
        }

        [Route("404")]
        public ActionResult Status404()
        {
            return View();
        }
    }
}
