﻿using System.Web.Mvc;
using MinecraftCubed.Common.Helpers;

namespace MinecraftCubed.Web.Mvc.HtmlHelpers
{
    public static class ImageHelper
    {
        public static MvcHtmlString Image(this HtmlHelper helper, string src)
        {
            return Image(helper, null, src, null, null);
        }

        public static MvcHtmlString Image(this HtmlHelper helper, string id, string src)
        {
            return Image(helper, id, src, null, null);
        }

        public static MvcHtmlString Image(this HtmlHelper helper, string id, string src, string alternateText)
        {
            return Image(helper, id, src, alternateText, null);
        }

        public static MvcHtmlString Image(this HtmlHelper helper, string id, string src, string alternateText, object htmlAttributes)
        {
            var builder = new TagBuilder("img");

            if (src.Contains("~"))
            {
                var url = new UrlHelper(helper.ViewContext.RequestContext);
                builder.MergeAttribute("src", url.Content(src));
            }
            else
            {
                builder.MergeAttribute("src", src);
            }

            if (!string.IsNullOrEmpty(alternateText))
            {
                builder.MergeAttribute("alt", alternateText);
            }

            builder.GenerateId(id);
            builder.MergeAttributes(GeneralHelper.ToProperHtmlAttributes(htmlAttributes));

            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }
    }
}
