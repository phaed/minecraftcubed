﻿using System;
using System.IO;
using System.Web.Mvc;

namespace MinecraftCubed.Web.Mvc.HtmlHelpers
{
    public static class AjaxformHelper
    {
        private class AjaxFormContainer : IDisposable
        {
            private readonly TextWriter _writer;
            public AjaxFormContainer(TextWriter writer)
            {
                _writer = writer;
            }

            public void Dispose()
            {
                _writer.Write("</form>");
            }
        }

        public static IDisposable AjaxForm(this HtmlHelper helper, string actionName, string controllerName, string container, string pre, string post, string classes = null)
        {
            var builder = new TagBuilder("form");

            builder.MergeAttribute("method", "post");
            builder.MergeAttribute("data-action", actionName);
            builder.MergeAttribute("data-controller", controllerName);
            builder.MergeAttribute("data-pre", pre);
            builder.MergeAttribute("data-post", post);
            builder.MergeAttribute("data-container", container);
            builder.MergeAttribute("class", "ajax" + (classes != null ? (" " + classes) : ""));

            builder.InnerHtml = helper.AntiForgeryToken().ToHtmlString();

            var writer = helper.ViewContext.Writer;
            writer.WriteLine(builder.ToString(TagRenderMode.StartTag));
            return new AjaxFormContainer(writer);
        }
    }
}
