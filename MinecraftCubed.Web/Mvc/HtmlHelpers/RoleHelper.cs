﻿using System;
using System.Web;
using System.Linq;
using System.Web.Mvc;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Static;
using MinecraftCubed.Service.Services;
using MinecraftCubed.Service.Services.Interfaces;
using StructureMap;

namespace MinecraftCubed.Web.Mvc.HtmlHelpers
{
    public static class RoleHelper
    {
        public static bool HasRole<TModel>(this HtmlHelper<TModel> source, UserRole role)
        {
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                return false;
            }
            
            var user = ObjectFactory.GetInstance<IUserService>().GetCurrentUserCached();

            if (user == null)
            {
                return false;
            }

            var has =  user.HasRole(role);

            return has;
        }

        public static bool HasRole<TModel>(this HtmlHelper<TModel> source, params UserRole[] roles)
        {
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                return false;
            }

            var user = ObjectFactory.GetInstance<IUserService>().GetCurrentUserCached();

            if (user == null)
            {
                return false;
            }

            return roles.Any(user.HasRole);
        }

        public static bool IsServerManager<TModel>(this HtmlHelper<TModel> source, string identifier)
        {
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                return false;
            }

            var user = ObjectFactory.GetInstance<IUserService>().GetCurrentUserCached();

            if (user == null)
            {
                return false;
            }

            if (user.HasRole(UserRole.Editor))
            {
                return true;
            }

            return RedisStore.Current.UserGetOrStore(Constant.Cache.IsServerManager + identifier, () => ObjectFactory.GetInstance<IStaffService>().IsServerManager(identifier, user.Username), TimeSpan.FromMinutes(1));
        }

        public static bool IsServerAdmin<TModel>(this HtmlHelper<TModel> source, string identifier)
        {
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                return false;
            }

            var user = ObjectFactory.GetInstance<IUserService>().GetCurrentUserCached();

            if (user == null)
            {
                return false;
            }

            if (user.HasRole(UserRole.Editor))
            {
                return true;
            }

            return RedisStore.Current.UserGetOrStore(Constant.Cache.IsServerAdmin + identifier, () => ObjectFactory.GetInstance<IStaffService>().IsServerAdmin(identifier, user.Username), TimeSpan.FromMinutes(1));
        }

        public static bool IsAnyServerManager<TModel>(this HtmlHelper<TModel> source)
        {
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                return false;
            }

            var user = ObjectFactory.GetInstance<IUserService>().GetCurrentUserCached();

            if (user == null)
            {
                return false;
            }
            
            if (user.HasRole(UserRole.Editor))
            {
                return true;
            }

            return RedisStore.Current.UserGetOrStore(Constant.Cache.IsAnyServerManager, () => ObjectFactory.GetInstance<IStaffService>().IsAnyServerManager(user.Username), TimeSpan.FromMinutes(1));
        }

        public static bool IsAnyServerAdmin<TModel>(this HtmlHelper<TModel> source)
        {
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                return false;
            }

            var user = ObjectFactory.GetInstance<IUserService>().GetCurrentUserCached();

            if (user == null)
            {
                return false;
            }
            
            if (user.HasRole(UserRole.Editor))
            {
                return true;
            }

            return RedisStore.Current.UserGetOrStore(Constant.Cache.IsAnyServerAdmin, () => ObjectFactory.GetInstance<IStaffService>().IsAnyServerAdmin(user.Username), TimeSpan.FromMinutes(1));
        }
    }
}
