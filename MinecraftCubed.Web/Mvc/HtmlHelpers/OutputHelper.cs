﻿using System.Web.Mvc;

namespace MinecraftCubed.Web.Mvc.HtmlHelpers
{
    public static class OutputHelper
    {
        public static MvcHtmlString Output(this HtmlHelper helper, string text)
        {
            return new MvcHtmlString(text);
        }
    }
}
