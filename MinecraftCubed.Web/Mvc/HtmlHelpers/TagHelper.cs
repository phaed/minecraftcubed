﻿using System;
using System.Web.Mvc;
using MinecraftCubed.Static;

namespace MinecraftCubed.Web.Mvc.HtmlHelpers
{
    public static class TagHelper
    {
        public static MvcHtmlString Canonical(this HtmlHelper helper)
        {
            var uri = helper.ViewContext.HttpContext.Request.Url;

            if (uri == null)
            {
                return null;
            }

            if (uri.Host.Equals(Constant.Domain, StringComparison.InvariantCultureIgnoreCase))
            {
                return null;
            }

            var url = new UriBuilder(uri.Scheme, Constant.Domain, -1, uri.PathAndQuery);

            if (helper.ViewContext.HttpContext.Request.IsLocal)
            {
                url.Port = uri.Port;
            }

            var link = "<link rel='canonical' " + url + ">";

            return MvcHtmlString.Create(link);
        }
    }
}
