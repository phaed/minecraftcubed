﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MinecraftCubed.Web.Mvc.HtmlHelpers
{
    public static class ValidationMessageHelper
    {
        public static string SuccessMessage(this HtmlHelper helper)
        {
            if (!string.IsNullOrEmpty(helper.ViewBag.Success))
            {
                return HttpUtility.HtmlDecode(helper.ViewBag.Success);
            }

            return string.Empty;
        }

        public static MvcHtmlString AlertRibbon(this HtmlHelper helper, string classes)
        {
            var error = ErrorAlertAll(helper, classes);

            if (!string.IsNullOrEmpty(error.ToString()))
            {
                return error;
            }
            
            var info = InfoAlert(helper, classes);

            if (!string.IsNullOrEmpty(info.ToString()))
            {
                return info;
            }

            var success = SuccessAlert(helper, classes);

            if (!string.IsNullOrEmpty(success.ToString()))
            {
                return success;
            }

            return new MvcHtmlString("<div class='alert-ribbon'></div>");
        }
        
        public static MvcHtmlString ErrorAlertAll(this HtmlHelper helper, string classes)
        {
            if (!helper.ViewData.ModelState.IsValid)
            {
                var err = AllValidationsStrings(helper);

                if (!string.IsNullOrEmpty(err))
                {
                    var msg = "<div class='validation-summary-errors alert alert-error adjusted " + classes + "'><i class='icon-remove-sign'></i>" + err + "</div>";

                    return new MvcHtmlString(msg);
                }
            }

            return new MvcHtmlString(string.Empty);
        }

        public static MvcHtmlString InfoAlert(this HtmlHelper helper, string classes)
        {
            if (!string.IsNullOrEmpty(helper.ViewBag.Info))
            {
                var msg = "<div class='alert alert-info adjusted " + classes + "'><i class='icon-info-sign'></i>" + helper.ViewBag.Info + "</div>";

                return new MvcHtmlString(msg);
            }

            return new MvcHtmlString(string.Empty);
        }

        public static MvcHtmlString SuccessAlert(this HtmlHelper helper, string classes)
        {
            if (!string.IsNullOrEmpty(helper.ViewBag.Success))
            {
                var msg = "<div class='alert alert-success adjusted " + classes + "'><i class='icon-ok'></i>" + helper.ViewBag.Success + "</div>";

                return new MvcHtmlString(msg);
            }

            return new MvcHtmlString(string.Empty);
        }

        public static MvcHtmlString ErrorAlert(this HtmlHelper helper, string classes, string defaultMessage)
        {
            if (!helper.ViewData.ModelState.IsValid)
            {
                var err = ErrorMessage(helper, defaultMessage);

                if (!string.IsNullOrEmpty(err))
                {
                    var msg = "<div class='validation-summary-errors alert alert-error adjusted " + classes + "'><i class='icon-remove-sign'></i>" + err + "</div>";

                    return new MvcHtmlString(msg);
                }
            }

            return new MvcHtmlString(string.Empty);
        }

        public static string ErrorMessage(this HtmlHelper helper, string defaultMessage)
        {
            int count = helper.ViewData.ModelState.Count();

            if (count == 0)
                return String.Empty;

            for (int i = 0; i < count; ++i)
            {
                if (helper.ViewData.ModelState.ElementAt(i).Value.Errors.Count > 0)
                {
                    if (helper.ViewData.ModelState.ElementAt(i).Key == "Error")
                    {
                        var msg = helper.ViewData.ModelState.ElementAt(i).Value.Errors[0].ErrorMessage;

                        if (string.IsNullOrEmpty(msg))
                        {
                            continue;
                        }

                        return HttpUtility.HtmlDecode(msg);
                    }
                }
            }
            return defaultMessage;
        }

        public static string FirstMessage(this HtmlHelper helper)
        {
            int count = helper.ViewData.ModelState.Count();
            if (count == 0) return String.Empty;
            for (int i = 0; i < count; ++i)
            {
                if (helper.ViewData.ModelState.ElementAt(i).Value.Errors.Count > 0)
                {
                    string msg = helper.ViewData.ModelState.ElementAt(i).Value.Errors[0].ErrorMessage;
                    if (string.IsNullOrEmpty(msg))
                    {
                        continue;
                    }

                    return HttpUtility.HtmlDecode(msg);
                }
            }
            return String.Empty;
        }

        public static string DecodeHtmlHelper(this HtmlHelper helper, MvcHtmlString helperToDecode)
        {
            if (helperToDecode != null) return HttpUtility.HtmlDecode(helperToDecode.ToString());

            return string.Empty;
        }

        public static string AllValidationsStrings(this HtmlHelper helper)
        {
            int count = helper.ViewData.ModelState.Count();
            if (count == 0) return String.Empty;

            var builder = new StringBuilder();

            for (int i = 0; i < count; ++i)
            {
                if (helper.ViewData.ModelState.ElementAt(i).Value.Errors.Count > 0)
                {
                    string msg = helper.ViewData.ModelState.ElementAt(i).Value.Errors[0].ErrorMessage;

                    if (!string.IsNullOrEmpty(msg))
                    {
                        builder.Append(HttpUtility.HtmlDecode(msg) + " ");
                    }
                }
            }

            return builder.ToString();
        }
    }
}
