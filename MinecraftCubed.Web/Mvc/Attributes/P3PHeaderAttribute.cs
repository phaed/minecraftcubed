using System;
using System.Web.Mvc;

namespace MinecraftCubed.Web.Mvc.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class P3PHeaderAttribute : ActionFilterAttribute
    {
        private string CompactPolicy { get; set; }
        private string PolicyRef { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Response.Headers["P3P"] == null)
            {
                if (CompactPolicy == null)
                    CompactPolicy = "NON DSP COR DEVa PSAa IVAo CONo OUR IND UNI PUR NAV DEM LOC";

                var value = "CP=\"" + CompactPolicy + "\"";
                if (PolicyRef != null)
                    value += ", policyref=\"" + PolicyRef + "\"";

                filterContext.HttpContext.Response.AddHeader("P3P", value);
            }

            base.OnActionExecuting(filterContext);
        }
    }
}