﻿using System.Linq;
using System.Web.Mvc;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Lang;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Web.ViewModels.EditEvent.Extras;
using StructureMap;

namespace MinecraftCubed.Web.Mvc.Attributes
{
    public class EventManagersOnlyAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var model = filterContext.ActionParameters.SingleOrDefault(m => m.Value is EventManagersOnlyBaseModel).Value as EventManagersOnlyBaseModel;

            if (model != null)
            {
                model.RefreshUI = true;

                var staffService = ObjectFactory.GetInstance<IStaffService>();

                if (!staffService.ManagesEvent(model.EventIdentifier))
                {
                    filterContext.Controller.ViewData.ModelState.AddModelError("Error", Messages.NoServerAccess);
                }
            }

            var identifier = filterContext.ActionParameters.SingleOrDefault(m => m.Value is string).Value as string;

            if (identifier != null)
            {
                var staffService = ObjectFactory.GetInstance<IStaffService>();

                if (!staffService.ManagesEvent(identifier))
                {
                    filterContext.Controller.ViewData.ModelState.AddModelError("Error", Messages.NoServerAccess);
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}