﻿using System.Web.Mvc;

namespace MinecraftCubed.Web.Mvc.Attributes
{
    public class LightMenuAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewResult = filterContext.Result as ViewResult;

            if (viewResult == null)
            {
                return;
            }

            viewResult.ViewBag.LightMenu = true;

            base.OnActionExecuted(filterContext);
        }
    }
}