﻿using System.Web.Mvc;
using MinecraftCubed.Static;

namespace MinecraftCubed.Web.Mvc.Attributes
{
    public class AllowHttpsCORSAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", Constant.WebsiteClean);
            base.OnActionExecuting(filterContext);
        }
    }
}