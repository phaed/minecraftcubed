﻿using System;
using System.Configuration;
using System.Web.Mvc;
using MinecraftCubed.Common;

namespace MinecraftCubed.Web.Mvc.Attributes
{
    public class RequireSecureAttribute : FilterAttribute, IAuthorizationFilter
    {
        public virtual void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            if (Settings.InDevelopment)
            {
                return;
            }

            if (!Settings.RequiresSecure)
            {
                return;
            }

            if (filterContext.HttpContext.Request.IsSecureConnection)
            {
                return;
            }

            if(!(
                ((filterContext.HttpContext.Request.ServerVariables["HTTP_SSL"] != null) && (filterContext.HttpContext.Request.ServerVariables["HTTP_SSL"].ToLower() == "on")) ||
                ((filterContext.HttpContext.Request.ServerVariables["HTTPS"] != null) && (filterContext.HttpContext.Request.ServerVariables["HTTPS"].ToLower() == "on")) ||
                ((filterContext.HttpContext.Request.ServerVariables["HTTP_SSL"] != null) && (filterContext.HttpContext.Request.ServerVariables["HTTP_SSL"].ToLower() == "true"))
                ))
            {
                HandleNonHttpsRequest(filterContext);
            }
        }

        protected virtual void HandleNonHttpsRequest(AuthorizationContext filterContext)
        {
            // only redirect for GET requests, otherwise the browser might not propagate the verb and request
            // body correctly.

            if (!String.Equals(filterContext.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            // redirect to HTTPS version of page

            var uri = filterContext.HttpContext.Request.Url;

            if (uri == null)
            {
                return;
            }

            var url = new UriBuilder("https", uri.Host, -1, uri.PathAndQuery);

            filterContext.Result = new RedirectResult(url.ToString());
        }
    }
}