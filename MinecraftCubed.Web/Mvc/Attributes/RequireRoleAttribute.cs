﻿using System;
using System.Web;
using System.Web.Mvc;
using MinecraftCubed.Static;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Service.Services;
using MinecraftCubed.Service.Services.Interfaces;
using StructureMap;

namespace MinecraftCubed.Web.Mvc.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, Inherited = false)]
    public class RequireRoleAttribute : ActionFilterAttribute, IAuthorizationFilter
    {
        private readonly UserRole[] _roles;

        public RequireRoleAttribute(UserRole role)
        {
            _roles = new[] { role };
        }

        public RequireRoleAttribute(params UserRole[] roles)
        {
            _roles = roles;
        }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Session != null)
            {
                var userService = ObjectFactory.GetInstance<IUserService>();
                var user = userService.GetCurrentUserCached();

                if(user != null)
                {
                    var success = user.HasRole(_roles);

                    if (success)
                    {
                        // Since authorization is performed at the action level, the authorization code runs
                        // after the output caching module. In the worst case this could allow an authorized user
                        // to cause the page to be cached, then an unauthorized user would later be served the
                        // cached page. We work around this by telling proxies not to cache the sensitive page,
                        // then we hook our custom authorization code into the caching mechanism so that we have
                        // the final say on whether or not a page should be served from the cache.
                        var cache = filterContext.HttpContext.Response.Cache;
                        cache.SetProxyMaxAge(new TimeSpan(0));
                        cache.AddValidationCallback(
                            (HttpContext context, object data, ref HttpValidationStatus validationStatus) =>
                            {
                                validationStatus = OnCacheAuthorization(new HttpContextWrapper(context), user);
                            }, null);
                        return;
                    }
                }
                HandleUnauthorizedRequest(filterContext);
            }
        }
        
        private void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            // Ajax requests will return status code 500 because we don't want to return the result of the
            // redirect to the login page.

            if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.Result = new HttpStatusCodeResult(500);
            }
            else
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

        public HttpValidationStatus OnCacheAuthorization(HttpContextBase httpContext, User user)
        {
            if (httpContext.Session != null)
            {
                if (user != null)
                {
                    var success = user.HasRole(_roles);

                    if (success)
                    {
                        return HttpValidationStatus.Valid;
                    }
                }
            }

            return HttpValidationStatus.IgnoreThisRequest;
        }
    }
}
