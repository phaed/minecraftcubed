﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using MinecraftCubed.Common;

namespace MinecraftCubed.Web.Mvc.Attributes
{
    public class RequireSecureWebApiAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.Request.RequestUri.Scheme != Uri.UriSchemeHttps)
            {
                if (Settings.InDevelopment)
                {
                    return;
                }

                if (!Settings.RequiresSecure)
                {
                    return;
                }

                actionContext.Response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.Forbidden,
                    ReasonPhrase = "SSL Required (use https)"
                };
            }
        }
    }
}