﻿using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Lang;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;
using StructureMap;

namespace MinecraftCubed.Web.Mvc.Attributes
{
    public class ServerManagersOnlyAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var model = filterContext.ActionParameters.SingleOrDefault(m => m.Value is ServerManagersOnlyBaseModel).Value as ServerManagersOnlyBaseModel;

            if (model != null)
            {
                model.RefreshUI = true;

                var staffService = ObjectFactory.GetInstance<IStaffService>();
                
                if (!staffService.ManagesServer(model.Identifier))
                {
                    filterContext.Controller.ViewData.ModelState.AddModelError("Error", Messages.NoServerAccess);
                }
            }

            var identifier = filterContext.ActionParameters.SingleOrDefault(m => m.Value is string).Value as string;

            if (identifier != null)
            {
                var staffService = ObjectFactory.GetInstance<IStaffService>();

                if (!staffService.ManagesServer(identifier))
                {
                    filterContext.Controller.ViewData.ModelState.AddModelError("Error", Messages.NoServerAccess);
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}