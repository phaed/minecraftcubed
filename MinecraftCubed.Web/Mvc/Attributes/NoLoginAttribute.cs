﻿using System.Web.Mvc;

namespace MinecraftCubed.Web.Mvc.Attributes
{
    public class NoLoginAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewResult = filterContext.Result as ViewResult;

            if (viewResult == null)
            {
                return;
            }

            viewResult.ViewBag.NoLogin = true;

            base.OnActionExecuted(filterContext);
        }
    }
}