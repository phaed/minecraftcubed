﻿using System;
using System.Collections.Generic;
using CaptchaMvc.Interface;
using MinecraftCubed.Domain.Redis;

namespace MinecraftCubed.Web.Mvc.Captcha
{
    public class RedisCaptchaStorageProvider : IStorageProvider
    {
        private const string ValidateKey = "CaptchaValidate";
        private const string DrawingKey = "CaptchaDrawing";

        public uint MaxCount { get; set; }
        
        public RedisCaptchaStorageProvider(): this(150)
        {
        }

        public RedisCaptchaStorageProvider(uint maxCount)
        {
            MaxCount = maxCount;
        }

        public virtual void Add(KeyValuePair<string, ICaptchaValue> captchaPair)
        {
            var size = RedisStore.Current.HashLength(ValidateKey);

            if (size > MaxCount)
            {
                RedisStore.Current.HashDeleteAsync(ValidateKey);
            }

            size = RedisStore.Current.HashLength(DrawingKey);

            if (size > MaxCount)
            {
                RedisStore.Current.HashDeleteAsync(DrawingKey);
            }

            RedisStore.Current.HashSetItem(ValidateKey, captchaPair.Key, captchaPair.Value);
            RedisStore.Current.HashSetItem(DrawingKey, captchaPair.Key, captchaPair.Value);
        }
    
        public bool Remove(string token)
        {
            var remove = RedisStore.Current.HashRemoveItem(ValidateKey, token);
            var validation = RedisStore.Current.HashRemoveItem(DrawingKey, token);
            return remove || validation;
        }
      
        public virtual ICaptchaValue GetValue(string token, TokenType tokenType)
        {
            ICaptchaValue value;
            switch (tokenType)
            {
                case TokenType.Drawing:
                    value = RedisStore.Current.HashGetItem<ICaptchaValue>(DrawingKey, token);

                    if (value != null)
                    {
                        RedisStore.Current.HashRemoveItem(DrawingKey, token);
                    }
                    break;
                case TokenType.Validation:
                    value = RedisStore.Current.HashGetItem<ICaptchaValue>(ValidateKey, token);

                    if (value != null)
                    {
                        RedisStore.Current.HashRemoveItem(ValidateKey, token);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException("tokenType");
            }
            return value;
        }

        public virtual bool IsContains(string token, TokenType tokenType)
        {
            switch (tokenType)
            {
                case TokenType.Drawing:
                    return RedisStore.Current.HashExistsName(DrawingKey, token);
                case TokenType.Validation:
                    return RedisStore.Current.HashExistsName(ValidateKey, token);
                default:
                    throw new ArgumentOutOfRangeException("tokenType");
            }
        }
    }
}
