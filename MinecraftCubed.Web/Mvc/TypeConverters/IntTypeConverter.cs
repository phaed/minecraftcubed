﻿using AutoMapper;

namespace MinecraftCubed.Web.Mvc.TypeConverters
{
    public class IntTypeConverter : ITypeConverter<string, int>
    {
        public int Convert(ResolutionContext context)
        {
            int id;
            return int.TryParse(context.SourceValue.ToString(), out id) ? id : 0;
        }
    }
}