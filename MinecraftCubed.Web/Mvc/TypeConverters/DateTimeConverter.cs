﻿using System;
using AutoMapper;

namespace MinecraftCubed.Web.Mvc.TypeConverters
{
    public class DateTimeTimeSpanTypeConverter : ITypeConverter<DateTime?, TimeSpan?>
    {
        public TimeSpan? Convert(ResolutionContext context)
        {
            if (context == null || context.SourceValue == null)
            {
                return null;
            }

            return ((DateTime) context.SourceValue).TimeOfDay;
        }
    }

    public class TimeSpanDateTimeTypeConverter : ITypeConverter<TimeSpan?, DateTime?>
    {
        public DateTime? Convert(ResolutionContext context)
        {
            if (context == null || context.SourceValue == null)
            {
                return null;
            }

            return (new DateTime()).Add((TimeSpan)context.SourceValue);
        }
    }

    public class DateTimeConverter : ITypeConverter<DateTime, long>
    {
        public long Convert(ResolutionContext context)
        {
            return ((DateTime)context.SourceValue).Ticks;
        }
    }
}