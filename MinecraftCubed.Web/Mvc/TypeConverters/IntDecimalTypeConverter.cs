﻿using System;
using AutoMapper;

namespace MinecraftCubed.Web.Mvc.TypeConverters
{
        public class IntDecimalTypeConverter : ITypeConverter<Int32, Decimal>
        {
            public Decimal Convert(ResolutionContext context)
            {
                Decimal id;
                return Decimal.TryParse(context.SourceValue.ToString(), out id) ? id : 0;
            }
        } 
}