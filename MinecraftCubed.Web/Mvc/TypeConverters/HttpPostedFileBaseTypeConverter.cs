﻿using System.Web;
using AutoMapper;
using MinecraftCubed.Common;

namespace MinecraftCubed.Web.Mvc.TypeConverters
{
    public class HttpPostedFileBaseTypeConverter : ITypeConverter<HttpPostedFileBase, byte[]>
    {
        public byte[] Convert(ResolutionContext context)
        {
            if (context.SourceValue == null)
            {
                return null;
            }

            return Utils.ConvertImageToByteArray((HttpPostedFileBase)context.SourceValue);
        }
    }
}