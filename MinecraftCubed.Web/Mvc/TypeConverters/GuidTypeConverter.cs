﻿using System;
using AutoMapper;

namespace MinecraftCubed.Web.Mvc.TypeConverters
{
    public class GuidTypeConverter : ITypeConverter<Guid, string>
    {
        public string Convert(ResolutionContext context)
        {
            return ((Guid)context.SourceValue).ToString("N");
        }
    }
}