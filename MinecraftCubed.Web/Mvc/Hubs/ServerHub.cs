﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.Security.Application;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;
using StructureMap;
using StructureMap.Attributes;

namespace MinecraftCubed.Web.Mvc.Hubs
{
    public class ServerHub : Hub
    {
        public ServerHub()
        {
        }

        public override Task OnConnected()
        {
            var serverService = ObjectFactory.GetInstance<IServerService>();
            var username = Context.User.Identity.Name;
            var identifier = Context.QueryString["identifier"];

            if (!string.IsNullOrEmpty(username))
            {
                var exists = serverService.ExistsServer(identifier);

                if (exists)
                {
                    // track the user

                    UserConnected(identifier, username);

                    // add connection to approriate group

                    Groups.Add(Context.ConnectionId, identifier);

                    // populate existing participants

                    Clients.Caller.existingUsers(GetParticipants(identifier));

                    // announce the user to the rest of the room

                    Clients.Group(identifier).userConnected(username);
                }
            }

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCall)
        {
            var serverService = ObjectFactory.GetInstance<IServerService>();

            var identifier = Context.QueryString["identifier"];
            var username = Context.User.Identity.Name;

            if (!string.IsNullOrEmpty(username))
            {
                var exists = serverService.ExistsServer(identifier);

                if (exists)
                {
                    // remove user from group

                    Clients.Group(identifier).userDisconnected(username);

                    // remove user from tracker

                    UserDisconnected(identifier, username);
                }
            }

            return base.OnDisconnected(stopCall);
        }

        public void Send(string message)
        {
            var identifier = (string)Clients.Caller.Identifier;
            var username = Context.User.Identity.Name;
            var chatService = ObjectFactory.GetInstance<IChatService>();

            if (!string.IsNullOrEmpty(username))
            {
                var data = chatService.AddChatLine(identifier, username, Encoder.HtmlEncode(message));

                if (data != null)
                {
                    Clients.Group(identifier).newMessage(data);
                }
            }
        }

        public void Delete(int id)
        {
            var identifier = (string)Clients.Caller.Identifier;
            var username = Context.User.Identity.Name;
            var chatService = ObjectFactory.GetInstance<IChatService>();

            if (!string.IsNullOrEmpty(username))
            {
                var deleted = chatService.DeleteChatLine(identifier, username, id);

                if (deleted)
                {
                    Clients.Group(identifier).deleteMessage(id);
                }
            }
        }

        private void UserConnected(string identifier, string username)
        {
            RedisStore.Current.HashSetItem(identifier, username, RedisDatabase.ChatUsers);
        }

        private void UserDisconnected(string identifier, string username)
        {
            RedisStore.Current.HashRemoveItem(identifier, username, RedisDatabase.ChatUsers);
        }

        private HashSet<string> GetParticipants(string identifier)
        {
            return new HashSet<string>(RedisStore.Current.HashGetNamesList(identifier, RedisDatabase.ChatUsers));
        }

        private bool IsConnected(string identifier, string username)
        {
            if (!RedisStore.Current.Exists(identifier, RedisDatabase.ChatUsers))
            {
                return false;
            }

            return RedisStore.Current.HashExistsName(identifier, username, RedisDatabase.ChatUsers);
        }
    }
}