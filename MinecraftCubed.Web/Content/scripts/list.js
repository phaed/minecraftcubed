
// **************************************************************** LIST MODEL

var ListModel = function () {
    var self = this;

    this.EndOfList = false;
    this.FirstPull = false;

    this.State = new function () {
        var me = this;

        var defaultState = {
            types: [],
            gameplays: [],
            minigames: [],
            plugins: [],
            modpacks: [],
            version: "",
            country: "",
            language: "",
            sort: "activity",
            filters: []
        };

        this.TagStore = {};

        this.data = _.clone(defaultState, true);

        this.init = function () {
            // bind parser to state change

            History.Adapter.bind(window, 'statechange', function () {
                log('state changed');

                // get last history state

                var last = History.getStateByIndex(History.getCurrentIndex() - 1);

                if (last.data) {
                    me.last = _.clone(defaultState, true);
                    $.extend(me.last, last.data);
                }

                // get current history state

                var state = History.getState();

                $.extend(me.data, state.data);
                log('state data %O', state.data);

                // act on changes

                var sortChanged = me.data.sort !== me.last.sort;
                var filtersChanged = me.filtersChanged(me.data, me.last);

                if (filtersChanged) {

                    // reload selectize

                    ListUI.reloadTagSelectizes().done(function () {

                        log('filters changed');

                        // pull fresh using current state/filters
                        // skip if sort changed since we also do a pull in the sort block underneath

                        if (!sortChanged) {
                            self.pullLists(true);
                        }

                        // setup the filter tabs

                        me.setupFilterTags();
                    });
                } else {
                    log('no filter changes');
                }

                if (sortChanged) {

                    log('sort-changed');

                    // set the dropdown

                    ListUI.selectizeSort.disable();
                    ListUI.selectizeSort.setValue(me.data.sort);
                    ListUI.selectizeSort.enable();

                    // pull lists and highlight 

                    model.pullLists(true, true).done(function () {
                        $('#organic-list .server .highlight')
                            .css({ opacity: 0 })
                            .delay(250)
                            .animate({ opacity: 1 }, 250, 'linear');
                    });

                    // scroll back up the list

                    if ($(window).scrollTop() > Constant.FilterHeight) {
                        var height = $('html, body').height();
                        var speed = height / 2;

                        $('html, body').animate({ scrollTop: Constant.FilterHeight }, speed, 'easeInOutQuint');
                    }
                }
            });

            var initialState = History.getState();

            log('initial state %O', initialState.data);

            $.extend(me.data, initialState.data);

            log('merged state %O', me.data);

            // trigger statechange if empty data

            if (_.isEmpty(initialState.data)) {
                log("+ state is empty");

                // decode from url if no state in history

                log('parsing querystring %O', window.location.search.slice(1));

                var qs = _.clone(defaultState);
                $.extend(qs, $.deparam(window.location.search.slice(1), true));
                qs = me.fixArrays(qs);
                qs.filters = me.extractFilters(qs);

                me.data = qs;
                log('pushing state found in qa %O', me.data);

                // push extracted state

                me.push();
            }

            // load tags

            ListUI.reloadTagSelectizes(true).done(function () {

                // setup the filter tags

                me.setupFilterTags();
            });

            // set default sort

            ListUI.selectizeSort.disable();
            ListUI.selectizeSort.setValue(me.data.sort || "activity");
            ListUI.selectizeSort.enable();

            // pull lists

            model.pullLists(true).done(function () {
                setTimeout(function () {
                    UI.scrollTopTab(Constant.ServerListScrollTabLevel).start();
                    model.FirstPull = true;
                }, 1000);
            });

            // push the state to hashtags
            // and start the listener for the first time

            model.startPullQueue();
        };

        this.setupFilterTags = function () {
            log('+ setup filter tags');

            // set the filters bar

            ListUI.selectize.disable();
            ListUI.selectize.clear();

            var fs = me.getSelectedFilters();

            for (var i = 0; i < fs.length; i++) {
                var name = fs[i];
                ListUI.selectize.addOption(name, ListUI.getTagData(name) || { Name: name });
                ListUI.selectize.addItem(name, true);
            }

            // set filter state

            self.Filtered(fs.length);

            // enable

            ListUI.selectize.enable();

            // resize dropdown height

            ListUI.resizeDropdownWithWindow();
        }

        this.filtersChanged = function (state, last) {
            var a = _.clone(defaultState);
            var b = _.clone(defaultState);

            $.extend(a, state);
            $.extend(b, last);

            log('looking for filter changes: %O %O', state, last);

            return !(arrayCompare(a.types, b.types) &&
                arrayCompare(a.gameplays, b.gameplays) &&
                arrayCompare(a.minigames, b.minigames) &&
                arrayCompare(a.plugins, b.plugins) &&
                arrayCompare(a.modpacks, b.modpacks) &&
                a.version === b.version &&
                a.country === b.country &&
                a.language === b.language);
        }

        this.fixArrays = function (a) {
            if (a.types.length && !_.isArray(a.types)) {
                a.types = [a.types];
            }
            if (a.gameplays.length && !_.isArray(a.gameplays)) {
                a.gameplays = [a.gameplays];
            }
            if (a.minigames.length && !_.isArray(a.minigames)) {
                a.minigames = [a.minigames];
            }
            if (a.plugins.length && !_.isArray(a.plugins)) {
                a.plugins = [a.plugins];
            }
            if (a.modpacks.length && !_.isArray(a.modpacks)) {
                a.modpacks = [a.modpacks];
            }
            return a;
        };

        this.getSelectedFilters = function () {
            var a = _.clone(defaultState);
            $.extend(a, me.data);

            return me.extractFilters(a);
        };

        this.extractFilters = function (a) {
            var items = _.union(a.types, a.gameplays, a.minigames, a.plugins, a.modpacks);

            if (a.version.length)
                items.push(a.version);

            if (a.country.length)
                items.push(a.country);

            if (a.language.length)
                items.push(a.language);

            return items;
        };

        this.push = function () {
            var qs = '?' + me.getStateQuerystring();

            // delete trailing questionmark
            if (qs === '?')
                qs = '';

            log('+++ pushing', me.data, qs);
            History.pushState(me.data, '', 'servers' + qs);
        };

        this.getStateQuerystring = function () {
            var state = _.clone(me.data);

            // filters dont show on hashtags
            delete state['filters'];

            // delete default sort
            if (state.hasOwnProperty('sort') && state.sort === 'activity') {
                delete state['sort'];
            }

            return $.param(removeEmptyStrings(state), true);
        }

        this.addFilter = function (name, type) {

            if (IsNullOrEmpty(name) || IsNullOrEmpty(type)) {
                log("~adding filter failed", name, type);
                return;
            }

            log("+ adding filter", name, type);

            // do not add second filter of single-value lists

            if (type === "versions") {
                if (me.data.version.length > 0)
                    return;
            }

            if (type === "countries") {
                if (me.data.country.length > 0)
                    return;
            }

            if (type === "languages") {
                if (me.data.language.length > 0)
                    return;
            }

            // update our state

            if (type === "versions" || type === "countries" || type === "languages") {
                if (type === "versions") {
                    me.data.version = name;
                }
                if (type === "countries") {
                    me.data.country = name;
                }
                if (type === "languages") {
                    me.data.language = name;
                }
            } else {
                var found = _.contains(me.data[type], name);

                if (!found) {
                    me.data[type].push(name);
                }
            }

            // add to the filter state

            me.data.filters.push(name);

            // push state

            me.push();

            // set filtered state

            self.Filtered(me.data.filters.length);
            $('.count-label').addClass('shaded');
        };

        this.removeFilter = function (name) {

            log("+ removing filter", name);

            // remove the filter value from our state

            if (me.data.version === name) {
                me.data.version = "";
            }
            if (me.data.country === name) {
                me.data.country = "";
            }
            if (me.data.language === name) {
                me.data.language = "";
            }

            me.data.types = _.without(me.data.types, name);
            me.data.gameplays = _.without(me.data.gameplays, name);
            me.data.minigames = _.without(me.data.minigames, name);
            me.data.plugins = _.without(me.data.plugins, name);
            me.data.modpacks = _.without(me.data.modpacks, name);

            // remove from filter state

            me.data.filters.splice($.inArray(name, me.data.filters), 1);

            // push state

            me.push();

            // set filtered state

            self.Filtered(me.data.filters.length);
            $('.count-label').addClass('shaded');
        };

        this.clearFilters = function () {
            log("+ clearing filters");

            // put state back to defaults (minus sort)

            var clone = _.clone(defaultState);
            clone.sort = me.data.sort;
            me.data = clone;

            // set filtered state

            self.Filtered(0);
            model.TotalOrganic(0);

            // clear the selectize

            ListUI.selectize.clear();
            ListUI.selectize.clearOptions();

            // push state

            me.push();
        };

        this.changeSort = function (value) {

            // update the state

            me.data.sort = value;
            me.push();
        };
    };

    this.Filtered = ko.observable();

    this.AllServers = [];

    this.OrganicServers = ko.observableArray();

    this.CurrentRequest = null;
    this.CurrentOrganicCount = 0;

    this.TotalOrganic = ko.observable();

    this.ServerCount = ko.computed(function () {
        if (self.TotalOrganic() === 1)
            return " 1 Server";
        else if (self.TotalOrganic() > 1)
            return commaSeparateNumber(self.TotalOrganic()) + " Servers";

        return "";
    });

    this.AfterAddServer = function (elem, index, listServer) {

        if (elem.nodeType === 1) {

            var opts = {
                type: 'line',
                width: '70px',
                height: '20px',
                lineColor: '#555',
                fillColor: '#F5F5F5',
                lineWidth: 2,
                spotColor: '#ffffff',
                minSpotColor: 'transparent',
                maxSpotColor: 'transparent',
                highlightSpotColor: '#ff0000',
                highlightLineColor: 'transparent',
                tooltipFormatter: function (sparklines, options, fields) {
                    return '<div class="inner-tip">' + (fields.y) + ' players</div>';
                },
                tooltipClassname: 'jqstooltip qtip-default qtip-light qtip-focus',

                spotRadius: 1
            };

            $('.sparkline', elem).sparkline(listServer.PlayerData, opts);

            opts = {
                type: 'bar',
                barColor: '#555',
                width: '34px',
                height: '16px',
                chartRangeMin: 0,
                chartRangeMax: 100,
                tooltipFormatter: function (sparklines, options, fields) {
                    return '<div class="inner-tip">Uptime: ' + (fields[0].value) + '%</div>';
                },
                tooltipClassname: 'jqstooltip qtip-default qtip-light qtip-focus'
            };

            $('.sparkline-bar', elem).sparkline(listServer.UptimeData, opts);

            // add tooltips

            $('.tip', elem).qtip({
                position: {
                    my: 'bottom center',
                    at: 'top center'
                },
                style: {
                    classes: 'qtip-light'
                }
            });

            $('.bigtip', elem).each(function () {
                $(this).qtip({
                    position: {
                        my: 'bottom center',
                        at: 'top center'
                    },
                    style: {
                        classes: 'qtip-light'
                    },
                    content: {
                        text: $(this).next('.tooltiptext')
                    }
                });
            });

            // set up knobs

            opts = {
                width: "36",
                height: "36",
                fgColor: "#555",
                bgColor: "#F5F5F5",
                skin: "tron",
                thickness: ".3",
                angleOffset: "180",
                readOnly: true
            };
            
            $(".knob input").knob(opts);

            if (model.State.data.sort === "activity") {
                $(".knob input").trigger(
                    'configure',
                    {
                        "fgColor": "#378ce1"
                    }
                );
            }

            // ip click

            $('.ip, .input-wrapper .title', elem).click(function () {
                SelectText($('.ip', elem));
            });

            // flip
            if (inViewport(elem)) {
                _.delay(function () {
                    $(elem).show();
                    _.delay(function () {
                        $(elem).addClass('flip');

                        $(elem).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {

                            UI.positionFooter();
                        });
                    }, 20);
                }, Constant.CardLoadDelay * index);
            } else {
                _.delay(function () {
                    $(elem).addClass('no-transform');
                    $(elem).addClass('flip');
                    $(elem).removeClass('no-transform');
                    $(elem).show();


                    UI.positionFooter();
                }, Constant.CardLoadDelay * index);
            }

            // bind hover of carousel arrows

            $('.banner', elem).on('mouseenter', function () {
                $('.bx-wrapper .bx-pager', elem).addClass('showing');
                $('.bx-wrapper .bx-controls-direction a', elem).addClass('showing');
            }).on('mouseleave', function () {
                $('.bx-wrapper .bx-pager', elem).removeClass('showing');
                if (!isMobile) {
                    $('.bx-wrapper .bx-controls-direction a', elem).removeClass('showing');
                }
            });

            if (isMobile) {
                $('.bx-wrapper .bx-controls-direction a', elem).addClass('showing');
            }

            // clean up elements when mouseleaving the card

            $('.back', elem).on('mouseleave', function () {
                $('.timebar', elem).removeClass('small');
            });

            $('.back, .featured-banner-wrapper', elem).on('mouseleave', function () {
                $('.bx-pager', elem).removeClass('showing');
            });


            // bind card press

            $('.server-link', elem).on('mousedown', function () {
                $(elem).addClass('pressed');
            });

            $('.server-link', elem).on('mouseup mouseleave', function () {
                $(elem).removeClass('pressed');
            });
        }
    };

    this.BeforeRemoveServer = function (elem) {
        if (elem.nodeType === 1) {
            if (inViewport(elem)) {
                $('.front', elem).addClass('hidden');
                $(elem).removeClass('shadowed');
                $(elem).removeClass('flip');

                // force remove the element after the transition as knockout seems not to be able to remove 3d transformed elementsproperly

                $(elem).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
                    $(elem).remove();
                });
            } else {
                $(elem).remove();
            }
        }
    }

    this.pullLists = function (freshPull, sortOnly) {
        var deferred = $.Deferred();

        // reject non-fresh pulls once we reach end of list

        if (self.EndOfList && !freshPull) {
            deferred.reject();
            return deferred.promise();
        }

        // build request
        // store deferred here, were gonna resolve it later on

        var request = {
            id: guid(),
            state: _.clone(self.State.data),
            freshPull: freshPull,
            sortOnly: sortOnly,
            deferred: deferred,
            served: false
        };

        log('+ logged new request', request);

        // add as current pull

        self.CurrentRequest = request;

        return deferred.promise();
    };

    var processResult = function (res) {
        log('+ processing result', res);

        if (res.TotalCount === 0) {
            return;
        }

        var organicArray = self.OrganicServers();

        // store the total amount of matching servers

        self.CurrentOrganicCount = organicArray.length + res.OrganicList.length;

        // set count on the UI

        self.TotalOrganic(res.TotalCount);
        $('.count-label').removeClass('shaded');

        log('previous organic count:', organicArray.length);

        // build server objects

        var index = organicArray.length;

        var organicServers = _.map(res.OrganicList, function (serverData) {

            var existing = _.find(organicArray, function (s) {
                return s.Id === serverData.Id;
            });

            if (typeof existing !== 'undefined') {
                return existing;
            }

            var serv = new ListServer(serverData);
            serv.Index = ++index;

            return serv;
        });

        // apply changes

        organicArray.push.apply(organicArray, organicServers);
        log('new organic count:', organicArray.length);

        // update list

        self.OrganicServers.valueHasMutated();

        // signal end of list when notified so, 
        // otherwise show load more button

        if (res.EndOfList) {
            self.EndOfList = true;
            log('** EndOfList');

            $('#loading').removeClass('spinner');
            $('#load-more').fadeOut();
            _.delay(function () {
                $('footer').fadeIn();
            }, 500);
        } else {
            $('#load-more').fadeIn();
            $('#loading').removeClass('spinner');
        }
    };

    this.startPullQueue = function () {

        var worker = function () {
            if (self.CurrentRequest != null && !self.CurrentRequest.served) {

                var request = self.CurrentRequest;
                request.served = true;

                log("+ running current pull", request);

                // prepare for a fresh pull

                if (request.freshPull) {
                    log('+ fresh pull requested');
                    self.EndOfList = false;
                    self.OrganicServers([]);
                    $('footer').hide();
                }

                // pull

                self.pullServerList(request).done(function (res) {
                    log('+ request result received');

                    if (self.CurrentRequest != null && res.RequestId === self.CurrentRequest.id) {
                        processResult(res, request);
                        request.deferred.resolve();
                    } else {
                        log('+ no longer current / ignored');
                        request.deferred.reject();
                    }
                });
            }
        }

        worker();
        setInterval(function () {
            worker();
        }, 100);
    };

    this.pullServerList = function (request) {
        var data = {
            state: request.state,
            requestId: request.id,
            skip: request.freshPull ? 0 : self.OrganicServers().length,
            take: Constant.ServerListTake
        };

        return $.ajax({
            url: '/list/GetServers',
            type: 'POST',
            dataType: 'json',
            data: ko.toJSON(data),
            contentType: 'application/json; charset=utf-8'
        });
    };

    // **************************************************************** PINGER

    this.Pinger = function () {
        var open = 0;
        var cancelled = false;
        var total = ko.observable(0);
        var amountDone = ko.observable(0);
        var completeFn = function () { };

        var barWidth = ko.computed(function () {
            return "width: " + (amountDone() / total() * 100) + "%";
        });

        var history = function () {
            return $.jStorage.get('ping-history', []);
        };

        var servers = [];

        log('stored history %O', history());

        var pullFullList = function () {
            var data = {
                filters: []
            };

            return $.post('/list/GetOrganicServersForPing', data);
        };

        var postPing = function (result) {
            // save to history to map if good

            var id = result[0];
            var ms = result[2];

            // store

            if (ms !== Constant.FailedPing) {
                var h = history();
                h[id] = ms;
                $.jStorage.set('ping-history', h);
            }

            // update existing organic servers

            var match = _.find(self.OrganicServers(), function (s) { return s.ServerId === id; });

            if (match) {
                if (ms !== Constant.FailedPing) {
                    match.Ping(ms + "<small>ms</small>");
                } else {
                    match.Ping(ms);
                }
            }

            amountDone(amountDone() + 1);
        };

        var complete = function () {
            if (!cancelled) {
                cancelled = true;

                setTimeout(function () {
                    log('complete');
                    $('#progress').modal('hide');

                    completeFn();
                }, 100);
            }
        };

        var run = function () {
            setTimeout(function () {
                if (open < 4) {
                    var server = servers.pop();

                    // increment open
                    open++;

                    ping(server[0], server[1]).done(function (result) {

                        postPing(result);

                        //decrement open
                        open--;

                        if (amountDone() === total())
                            complete();
                    });
                }

                // empty queue if cancelled

                if (cancelled) {
                    servers = [];
                }

                // start next loop

                if (servers.length > 0)
                    run();
            }, 100);
        };

        return {
            amountDone: amountDone,
            barWidth: barWidth,
            total: total,
            history: history,
            cancel: function () {
                cancelled = true;
            },
            start: function (fn) {
                amountDone(0);
                cancelled = false;
                completeFn = fn;

                pullFullList().done(function (res) {
                    $('#progress').modal('show').on('hidden', function () {
                        complete();
                    });

                    total(res.length);
                    log(res);

                    servers = res;
                    run();
                });
            },
            ping: function (id, address) {
                ping(id, address).done(postPing);
            },
            pingMultiple: function (id, address) {
                var deferred = $.Deferred();

                var a = ping(id, address).done(postPing);
                var b = ping(id, address, 250).done(postPing);
                var c = ping(id, address, 500).done(postPing);

                $.when(a, b, c).done(function (ra, rb, rc) {

                    var lowest = 999999;

                    if ($.isNumeric(ra[2])) {
                        if (parseInt(ra[2]) < lowest)
                            lowest = parseInt(ra[2]);
                    }
                    if ($.isNumeric(rb[2])) {
                        if (parseInt(rb[2]) < lowest)
                            lowest = parseInt(rb[2]);
                    }
                    if ($.isNumeric(rc[2])) {
                        if (parseInt(rc[2]) < lowest)
                            lowest = parseInt(rc[2]);
                    }

                    if (lowest === 999999) {
                        lowest = Constant.FailedPing;
                    }

                    postPing([ra[0], '', lowest]);
                    deferred.resolve();
                });

                return deferred.promise();
            }
        };
    }();
};

// **************************************************************** MODELS

var ListServer = function (data) {
    data = data || {};
    var self = this;

    this.Slider = {};

    this.Index = "";
    this.IsFeatured = data.IF;
    this.ServerId = data.Id;
    this.Identifier = data.I;
    this.Name = data.N;
    this.Types = data.TS.join(" / ");
    this.Version = data.Ve;
    this.OnlineCount = commaSeparateNumber(data.O);
    this.PlayerData = data.PD;
    this.UptimeData = data.UD;
    this.PeakPlayerCount = commaSeparateNumber(data.P);
    this.Activity = data.A === -1 ? "-" : data.A;
    this.Ip = data.IP;
    this.IsOffline = data.OFF;

    this.CountryAbbreviation = data.CA;
    this.CountryName = data.CN.replace(' ', '&nbsp;');
    this.Language = data.LA;
    this.LanguageName = '(' + data.LA + ')';
    this.FlagClass = 'flag ' + this.CountryAbbreviation;

    this.HasDownload = data.HD;
    this.DownloadLabel = data.DL;
    this.DownloadUrl = data.DU;
    this.HasWhitelist = data.HW;
    this.WhitelistUrl = data.WU;

    this.BannerUrl = data.B + "?v=" + data.BV;
    this.FeaturedBannerUrl = data.FB + "?v=" + data.BV;
    this.HasBanner = data.B && data.B.length > 0;
    this.HasFeaturedBanner = data.FB && data.FB.length > 0;

    this.AltTag = this.Name;

    if (!this.HasBanner) {
        var lastNumber = this.ServerId.toString().slice(-1);
        this.BannerUrl = Data.AzureStorageUrl + 'defaultbanners/' + lastNumber + '.png';
    }

    this.IsBig = this.HasFeaturedBanner && this.IsFeatured;

    this.SetFeaturedColumn = function () {
        this.IsBig = this.HasFeaturedBanner && this.IsFeatured;
    }

    if (this.OnlineCount === 0) {
        this.OnlineCount = "-";
        this.PeakPlayerCount = "-";
    }

    var storedPing = model.Pinger.history()[self.ServerId] || Constant.DefaultPing;

    if (storedPing !== Constant.DefaultPing && storedPing !== Constant.FailedPing) {
        storedPing += "<small>ms</small>";
    }

    this.Ping = ko.observable(storedPing);

    this.pingOnce = _.once(function () {
        if (self.IsOffline) {
            return;
        }
        if (self.Ping() === Constant.DefaultPing) {
            log('ping', self.Name);
            model.Pinger.ping(self.ServerId, self.Ip);
        }
    });

    this.ping = function (data, e) {
        e.stopPropagation();

        if (this.IsOffline) {
            return;
        }

        var label = $(e.target).parents('.block').find('.ping-label');
        var button = $(e.target).parents('.block').find('.ping-button');

        label.addClass('dark');
        button.attr('disabled', 'disabled');

        model.Pinger.pingMultiple(self.ServerId, self.Ip).done(function () {
            label.removeClass('dark');
            button.removeAttr('disabled');
        });
    };

    this.PingCopy = this.IsOffline ? "Offline" : "Ping";

    this.HasPing = ko.computed(function () {
        var p = self.Ping();
        return p !== Constant.DefaultPing && p !== Constant.FailedPing;
    });

    if (this.IsOffline) {
        this.OnlineCount = "-";
        this.Ping(Constant.FailedPing);
    }

    this.isHighlighted = function (item) {
        return model.State.data.sort === item;
    };

    this.NeedsName = function () {
        if (self.IsFeatured && self.HasFeaturedBanner) {
            return false;
        }

        return !self.HasBanner;
    };

    this.PageUrl = '/server/' + data.I;
};

// **************************************************************** UI

ListUI = function () {

    var tagData = $.jStorage.get('m3-tag-data') || {};

    var extractData = function (res) {
        // extract the data
        $.each(res, function (type) {
            var tags = res[type];
            if (tags && tags.length) {
                if (tags[0].hasOwnProperty('LogoUrl') || tags[0].hasOwnProperty('Abbreviation')) {
                    log('adding logo data for', type);
                    tagData = _.uniq(_.union(tagData, tags), 'Name');
                }
            }
        });

        // save to storage
        $.jStorage.set('m3-tag-data', tagData, 300000); // 5 mins
    };

    var getAndStoreTagData = function () {

        var deferred = new $.Deferred();

        if (!_.isEmpty(tagData)) {
            return deferred.resolve().promise();
        }

        log('+ getting and storing the full set of tags');

        $.ajax({
            url: "/tags/getlisttags",
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: ko.toJSON(model.State.defaultState),
            success: function (res) {
                log('+ loaded and stored all list tags', res);

                // extract the data
                extractData(res);

                // set as resolved as we already have the data
                deferred.resolve();
            }
        });

        return deferred.promise();
    };

    var tips = function() {

        var store = [];

        return {
            add: function (element, type, name) {
                //log("# adding tip " + name);

                var tip = $(element).qtip({
                    background: '#fff',
                    position: {
                        my: 'right center',
                        at: 'center left',
                        adjust: {
                            x: -8
                        }
                    },
                    style: {
                        classes: 'qtip-light'
                    },
                    content: {
                        text: $('.tt-tip', element)
                    }
                });

                store.push({ type: type, name: name, tip: tip.qtip('api') });
            },
            killall: function (force) {
                //log("# killing all tips");

                $.each(store, function(i, t) {
                    t.tip.destroy(force);
                });
                store = [];
            }
        };
    }();

    return {
        getTagData: function (name) {
            return _.find(tagData, function (n) {
                return n.Name === name;
            });
        },

        selectize: null,
        selectizeSort: null,
        tagSelectizes: [],

        selectizeFilters: function () {
            var selectize = $("#filters").selectize({
                plugins: ['remove_button'],
                valueField: 'Name',
                labelField: 'Name',
                openOnFocus: false,
                persist: false,
                create: false,
                onItemRemove: function (value) {
                    model.State.removeFilter(value);

                    if (ListUI.selectize.items.length === 0) {
                        model.TotalOrganic(0);
                    }
                },
                onItemAdd: function () {
                },
                render: {
                    override: true,
                    item: function (item, escape) {

                        var html = '<div class="dd-item item">';

                        log("item", item);

                        if (item.hasOwnProperty('Abbreviation')) {
                            log("flag");
                            html += '<div class="flag ' + escape(item.Abbreviation.toLowerCase()) + '"></div>';
                        } else if (item.hasOwnProperty('LogoUrl')) {
                            log("logo");
                            html += '<div class="img-wrap"><img src="' + item.LogoUrl + '"/></div>';
                        } else {
                            log("none");
                            html += '<div class="img-wrap"></div>';
                        }

                        html += '<span class="name">' + escape(item.Name) + '</span>' +
                            '<a href="javascript:void(0)" class="remove" tabindex="-1" title="Remove">&times;</a>' +
                            '</div>';

                        return html;
                    }
                }
            });

            ListUI.selectize = selectize[0].selectize;
        },

        selectizeTags: function (selector, type) {
            var render = {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="name">' + escape(item.Name) + '</span>' +
                        '<span class="count">' + escape(item.Count) + '</span>' +
                        '</div>';
                }
            };

            if (type === "countries") {
                render = {
                    option: function (item, escape) {
                        return '<div class="dd-option">' +
                            '<span class="flag ' + escape(item.Abbreviation.toLowerCase()) + '"></span>' +
                            '<span class="name">' + escape(item.Name) + '</span>' +
                            '<span class="count">' + escape(item.Count) + '</span>' +
                            '</div>';
                    }
                };
            }

            if (type === "modpacks" ||
                type === "minigames" ||
                type === "plugins") {
                render = {
                    option: function (item, escape) {

                        var hasLogo = item.LogoUrl && item.LogoUrl.length;

                        if (hasLogo) {
                            return '<div class="dd-option dd-tip ' + type + '">' +
                                '<div class="img-wrap"><img src="' + escape(item.LogoUrl) + '"/></div>' +
                                '<div class="tt-tip">' +
                                    '<img class="tt-img" src="' + escape(item.LogoUrl) + '"/>' +
                                    '<div class="tt-name">' + escape(item.Name) + '</div>' +
                                    '<div class="tt-desc">' + escape(item.Description) + '</div>' +
                                '</div>' +
                                '<div class="name">' + escape(item.Name) + '</div>' +
                                '<span class="count">' + escape(item.Count) + '</span>' +
                                '</div>';
                        } else {
                            return '<div class="dd-option ' + type + '">' +
                                '<div class="img-wrap">' +
                                '</div>' +
                                '<div class="name">' + escape(item.Name) + '</div>' +
                                '<span class="count">' + escape(item.Count) + '</span>' +
                                '</div>';
                        }
                    }
                };
            }

            var selectize = $(selector).selectize({
                maxItems: 1,
                valueField: 'Name',
                labelField: 'Name',
                openOnFocus: true,
                persist: false,
                searchField: ['Name'],
                sortFieldNumeric: 'Count',
                sortDirection: 'desc',
                autoEnable: true,
                preload: false,
                create: false,
                render: render,
                load: function (query, callback) {
                    load(callback);
                    
                    // create tips for all options
                    tips.killall(type);
                    $('.dd-tip', selectize[0].selectize.$dropdown).each(function () {
                        tips.add(this, type, $('.tt-name', this).html());
                    });
                },
                onItemAdd: function (val, $item) {
                    // kill all tips
                    //log("# killing all tips");
                    tips.killall(type);

                    selectize[0].selectize.clear();
                    setTimeout(function () {
                        selectize[0].selectize.showInput();
                        selectize[0].selectize.blur();

                        // hack to fix inputs staying focused after tag selection
                        _.delay(function () { $('.selectize-input').removeClass('focus'); }, 100);
                    }, 0);

                    model.State.addFilter(val, type, $item);
                },
                onDropdownOpen: function ($dropdown) {
                    // create tips for all options
                    tips.killall(type);
                    $('.dd-tip', $dropdown).each(function () {
                        tips.add(this, type, $('.tt-name', this).html());
                    });
                },
                onDropdownClose: function ($dropdown) {
                    // kill all tips
                    //log("# killing all tips");
                    tips.killall(true);
                }
            });

            // start it up disabled
            selectize[0].selectize.disable();

            var load = function (callback) {
                // pull data from data store
                var tags = model.TagStore[type];

                callback(tags);

                if (tags && tags.length) {
                    //enable once ready
                    selectize[0].selectize.enable();
                    log('loading %s %O', type, tags);
                }
            };

            ListUI.tagSelectizes.push({ type: type, $selector: $(selector), selectize: selectize[0].selectize, load: load });
        },

        reloadTagSelectizes: function () {
            var deferred = new $.Deferred();

            // clear all dropdowns

            $.each(ListUI.tagSelectizes, function (i, sel) {
                sel.selectize.disable();
                sel.selectize.clearOptions();
            });

            // make sure we have the full set of tags

            getAndStoreTagData().done(function () {

                // pull just the ones we need and populate the selectizes

                $.ajax({
                    url: "/tags/getlisttags",
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    data: ko.toJSON(model.State.data),
                    success: function (res) {
                        log('+ loaded list tags', res);

                        // store tag data 
                        model.TagStore = res;

                        // extract the data
                        extractData(res);

                        // have selectizer read from store
                        $.each(res, function (type) {
                            var sel = _.find(ListUI.tagSelectizes, { 'type': type });
                            sel.selectize.renderCache = {};
                            sel.selectize.load.call(sel.selectize, sel.load);
                            sel.selectize.refreshOptions(false);
                        });

                        // showtabs if hidden

                        if (!$('#filter-container').is(':visible')) {
                            $('#filter-container').slideDown('slow');
                        }

                        // set as resolved as we already have the data
                        deferred.resolve();
                    }
                });

            });

            return deferred.promise();
        },

        selectizeSortDropDown: function () {
            var selectize = $("#sort").selectize({
                onChange: function (value) {
                    if (model.FirstPull) {
                        model.State.changeSort(value);
                    }
                },
                render: {
                    item: function (item, escape) {
                        return "<div>Sorted by " + escape(item.text) + "</div>";
                    }
                }
            });
            ListUI.selectizeSort = selectize[0].selectize;
        },

        resizeDropdownWithWindow: _.debounce(function () {
            var h = $(window).height();
            $('.selectize-control .selectize-dropdown', '.tag-row.one').css('max-height', (h - 300) + "px");
            $('.selectize-control .selectize-dropdown', '.tag-row.two').css('max-height', (h - 355) + "px");
        }, 250, { 'trailing': true })
    }
}();

// ****************************************************************

$(function () {

    ListUI.selectizeTags('#types', 'types');
    ListUI.selectizeTags('#gameplays', 'gameplays');
    ListUI.selectizeTags('#minigames', 'minigames');
    ListUI.selectizeTags('#plugins', 'plugins');
    ListUI.selectizeTags('#versions', 'versions');
    ListUI.selectizeTags('#languages', 'languages');
    ListUI.selectizeTags('#modpacks', 'modpacks');
    ListUI.selectizeTags('#countries', 'countries');

    ListUI.selectizeSortDropDown();
    ListUI.selectizeFilters();

    // bind the model 
    // create the model

    window.model = new ListModel();

    MC3.templates.load().done(function () {
        model.State.init();
        ko.applyBindings(model);
        log('initiated model');
    });

    // bind load more button

    $('#load-more').click(function (e) {
        $(this).fadeOut();
        model.pullLists();
        e.preventDefault();
    });

    // bind reize dropdown height

    $(window).resize(ListUI.resizeDropdownWithWindow);
});