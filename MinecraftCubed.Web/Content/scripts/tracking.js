﻿var gaProd = 'UA-44482516-1';

var _gaq = _gaq || [];
_gaq.push(['_require', 'inpage_linkid', '//www.google-analytics.com/plugins/ga/inpage_linkid.js']);
_gaq.push(['_setAccount', gaProd]);
_gaq.push(['_trackPageview']);

(function () {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
})();

var track = {
    page: function (tag) {
        if (tag && tag.length > 0) {
            _gaq.push(['_trackPageview', tag]);
            log('tracking page: ' + tag);
        }
    },

    event: function (category, action, label, value) {
        _gaq.push(['_trackEvent', category, action || 'Click', label, value]);
        log('tracking event: %s | %s | %s | %s', category, action, label, value);
    }
};