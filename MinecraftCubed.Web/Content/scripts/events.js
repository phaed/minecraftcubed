﻿// **************************************************************** EVENTS MODEL

var EventsModel = function () {
    var self = this;

    this.EndOfList = false;
    this.LastList = 4;
    this.FirstPull = false;

    var stepToNextList = function () {
        self.LastList++;
        if (self.LastList === 5) {
            self.LastList = 1;
        }
        return self.LastList;
    };

    var heightFix = function (elem) {
        $(elem).css('height', Math.max($('.banner', elem).height() + $('.body', elem).height(), 380));
    };

    this.State = new function () {
        var me = this;

        var defaultState = {
            eventgameplays: [],
            country: "",
            language: "",
            sort: "date",
            filters: []
        };

        this.TagStore = {};

        this.last = _.clone(defaultState, true);
        this.data = _.clone(defaultState, true);

        this.init = function () {

            // bind parser to state change

            History.Adapter.bind(window, 'statechange', function () {
                log('state changed');

                // get last history state

                var last = History.getStateByIndex(History.getCurrentIndex() - 1);

                if (last.data) {
                    me.last = _.clone(defaultState, true);
                    $.extend(me.last, last.data);
                }

                // get current history state

                var state = History.getState();

                $.extend(me.data, state.data);
                log('state data %O', state.data);

                // act on changes

                var sortChanged = me.data.sort !== me.last.sort;
                var filtersChanged = me.filtersChanged(me.data, me.last);

                if (filtersChanged) {

                    // reload selectize

                    ListUI.reloadTagSelectizes().done(function () {

                        log('filters changed');

                        // pull fresh using current state/filters
                        // skip if sort changed since we also do a pull in the sort block underneath

                        if (!sortChanged) {
                            self.pullList(true);
                        }

                        // setup the filter tags

                        me.setupFilterTags();
                    });
                }

                if (sortChanged) {

                    log('sort-changed');

                    // set the dropdown

                    ListUI.selectizeSort.disable();
                    ListUI.selectizeSort.setValue(me.data.sort);
                    ListUI.selectizeSort.enable();

                    // pull lists

                    model.pullList(true);

                    // scroll back up the list

                    if ($(window).scrollTop() > Constant.FilterHeight) {
                        var height = $('html, body').height();
                        var speed = height / 2;

                        $('html, body').animate({ scrollTop: Constant.FilterHeight }, speed, 'easeInOutQuint');
                    }
                }
            });

            // set initial state

            var initialState = History.getState();

            log('initial state %O', initialState.data);

            $.extend(me.data, initialState.data);

            log('merged state %O', me.data);

            if (_.isEmpty(initialState.data)) {
                log("+ state is empty");

                // decode from url if no state in history

                log('parsing querystring %O', window.location.search.slice(1));

                var qs = _.clone(defaultState);
                $.extend(qs, $.deparam(window.location.search.slice(1), true));
                qs.filters = me.extractFilters(qs);

                me.data = qs;
                log('pushing state found in qa %O', me.data);

                // push extracted state

                me.push();
            }

            // load tags

            ListUI.reloadTagSelectizes().done(function () {

                // setup the filter tags

                me.setupFilterTags();
            });

            // set default sort

            ListUI.selectizeSort.disable();
            ListUI.selectizeSort.setValue(me.data.sort || "date");
            ListUI.selectizeSort.enable();

            // pull lists

            model.pullList(true).done(function () {
                setTimeout(function () {
                    UI.scrollTopTab(Constant.EventListScrollTabLevel).start();
                    model.FirstPull = true;
                }, 1000);
            });

            // push the state to hashtags
            // and start the listener for the first time

            model.startPullQueue();
        };

        this.setupFilterTags = function () {
            log('+ setup filter tags');

            // set the filters bar

            ListUI.selectize.disable();
            ListUI.selectize.clear();

            var fs = me.getSelectedFilters();

            for (var i = 0; i < fs.length; i++) {
                var name = fs[i];
                ListUI.selectize.addOption(name, ListUI.getTagData(name) || { Name: name });
                ListUI.selectize.addItem(name, true);
            }

            // set filter state

            self.Filtered(fs.length);

            // enable

            ListUI.selectize.enable();

            // resize dropdown height

            ListUI.resizeDropdownWithWindow();
        }

        this.filtersChanged = function (state, last) {
            var a = _.clone(defaultState, true);
            var b = _.clone(defaultState, true);

            $.extend(a, state);
            $.extend(b, last);

            log('looking for filter changes: %O %O', a, b);

            return !(arrayCompare(a.eventgameplays, b.eventgameplays) &&
                a.country === b.country &&
                a.language === b.language);
        }

        this.getSelectedFilters = function () {
            var state = History.getState();
            var a = _.clone(defaultState, true);
            $.extend(a, state.data);

            return me.extractFilters(a);
        };

        this.extractFilters = function (a) {
            var items = a.eventgameplays;

            if (a.country.length)
                items.push(a.country);

            if (a.language.length)
                items.push(a.language);

            return items;
        };

        this.push = function () {
            var qs = '?' + me.getStateQuerystring();

            // delete trailing questionmark
            if (qs === '?')
                qs = '';

            log('+ pushing', me.data, qs);
            History.pushState(me.data, '', 'events' + qs);
        };

        this.getStateQuerystring = function () {
            var state = _.clone(me.data);

            // filters dont show on hashtags
            delete state['filters'];

            // delete default sort
            if (state.hasOwnProperty('sort') && state.sort === 'date') {
                delete state['sort'];
            }

            return $.param(removeEmptyStrings(state), true);
        }

        this.addFilter = function (name, type) {

            if (IsNullOrEmpty(name) || IsNullOrEmpty(type)) {
                log("~adding filter failed", name, type);
                return;
            }

            log("+ adding filter", name, type);

            // do not add second filter of single-value lists

            if (type === "countries") {
                if (me.data.country.length > 0)
                    return;
            }

            if (type === "languages") {
                if (me.data.language.length > 0)
                    return;
            }

            // update our state

            if (type === "countries" || type === "languages") {
                if (type === "countries") {
                    me.data.country = name;
                }
                if (type === "languages") {
                    me.data.language = name;
                }
            } else {
                var found = _.contains(me.data[type], name);

                if (!found) {
                    me.data[type].push(name);
                }
            }

            // add to the filter state

            me.data.filters.push(name);

            // push state

            me.push();

            // set filtered state
        
            self.Filtered(me.data.filters.length);
            $('.count-label').addClass('shaded');
        };

        this.removeFilter = function (name) {

            log("+ removing filter", name);

            // remove the filter value from our state

            if (me.data.country === name) {
                me.data.country = "";
            }
            if (me.data.language === name) {
                me.data.language = "";
            }

            me.data.eventgameplays = _.without(me.data.eventgameplays, name);

            // remove from filter state

            me.data.filters.splice($.inArray(name, me.data.filters), 1);

            // push state

            me.push();

            // set filtered state

            self.Filtered(me.data.filters.length);
            $('.count-label').addClass('shaded');
        };

        this.clearFilters = function () {
            log("+ clearing filters");

            // put state back to defaults (minus sort)

            var clone = _.clone(defaultState);
            clone.sort = me.data.sort;
            me.data = clone;

            // set filtered state

            self.Filtered(0);
            model.Total(0);

            // clear the selectize

            ListUI.selectize.clear();
            ListUI.selectize.clearOptions();

            // push state

            me.push();
        };

        this.changeSort = function (value) {

            // update the state

            me.data.sort = value;
            me.push();
        };
    };


    this.CurrentRequest = null;
    this.Filtered = ko.observable();

    this.Events1 = ko.observableArray();
    this.Events2 = ko.observableArray();
    this.Events3 = ko.observableArray();
    this.Events4 = ko.observableArray();

    this.Total = ko.observable();

    this.EventCount = ko.computed(function () {
        if (self.Total() === 0)
            return "";
        if (self.Total() === 1)
            return " 1 Event";
        else return commaSeparateNumber(self.Total()) + " Events";
    });

    this.AfterAddEvent = function (elem, index, listEvent) {

        var anonymous = MC3.CurrentUsername.length === 0;

        if (elem.nodeType === 1) {

            // bind scrollbars

            $('.nano', elem).nanoScroller({ preventPageScrolling: true });

            // init livestamps

            $('.started', elem).livestamp(listEvent.StartDate);
            $('.soon', elem).livestamp(listEvent.StartDate);
            $('.ends', elem).livestamp(listEvent.EndDate);

            // resize the timebar initially

            var doResize = function (el) {
                var timeWidth = $('.time', el).width();
                var total = timeWidth + 45; // width of clock icon + 15
                $('.timebar', el).width(total);
            };

            doResize(elem);

            // do resize when the livestamp changes

            $('.time', elem).on('change.livestamp', function () {
                _.delay(function () {
                    doResize(elem);
                    log('change based timebar resize');
                }, 10);
            });

            // add tooltip

            $('.tip', elem).qtip({
                position: {
                    my: 'bottom center',
                    at: 'top center'
                },
                style: {
                    classes: 'qtip-light'
                }
            });

            // prevent flip when using UI

            $('.ip, .btn, .attend', elem).click(function (e) {
                e.stopPropagation();
            });

            // click on ip selects text

            $('.ip', elem).click(function () {
                SelectText($('.ip', elem));
            });

            // load checkbox and other UI

            UI.refresh(elem);

            // prevent flip when clicking on server link

            $('.server', elem).click(function (e) {
                e.stopPropagation();
            });

            // bind hover of carousel arrows

            $('.banner', elem).on('mouseenter', function () {
                $('.bx-wrapper .bx-pager', elem).addClass('showing');
                $('.bx-wrapper .bx-controls-direction a', elem).addClass('showing');
            }).on('mouseleave', function () {
                $('.bx-wrapper .bx-pager', elem).removeClass('showing');
                if (!isMobile) {
                    $('.bx-wrapper .bx-controls-direction a', elem).removeClass('showing');
                }
            });

            if (isMobile) {
                $('.bx-wrapper .bx-controls-direction a', elem).addClass('showing');
            }

            // clean up elements when mouseleaving the card

            $('.banner-wrapper', elem).on('mouseleave', function () {
                _.delay(function () {
                    $('.timebar', elem).removeClass('small');
                }, 500);
                $('.bx-pager', elem).removeClass('showing');
            });

            // check checkbox if attending event

            if (!anonymous) {
                if (_.find(listEvent.Attendees(), { 'Name': MC3.CurrentUsername })) {
                    $('input.checkbox', elem).iCheck('check');

                    // show requirements

                    $('.requirements', elem).slideDown('slow');
                    $('.attend', elem).addClass('on');
                    $('.attendee-whitelist', elem).hide();
                    $('.attendee-whitelist-enabled', elem).show();
                    $(elem).addClass('attending');
                }
            }

            // bind attending checkbox

            $('input.checkbox', elem).on('ifChecked', function () {
                if (anonymous) {
                    _.delay(function () {
                        $('input.checkbox', elem).iCheck('uncheck');
                    }, 500);
                    Auth.ShowLogin();
                    return;
                }

                self.AddAsAttendee(listEvent.EventDateId);
                listEvent.AddPlayer(MC3.CurrentUsername);

                $('.nano', elem).nanoScroller({ preventPageScrolling: true });
                heightFix(elem);

                log("added", MC3.CurrentUsername, "as attendee for eventdate:", listEvent.EventDateId);

                // slide down requirements

                $('.requirements', elem).slideDown('slow');
                $('.attend', elem).addClass('on');
                $('.attendee-whitelist', elem).hide();
                $('.attendee-whitelist-enabled', elem).show();
                $(elem).addClass('attending');

                // re-render the tooltips
                $('.tip', elem).qtip({
                    position: {
                        my: 'bottom center',
                        at: 'top center'
                    },
                    style: {
                        classes: 'qtip-light'
                    }
                });

                // hack sets the height of the event box manually 

                _.delay(function () {
                    heightFix(elem);
                }, 100);
            });

            $('input.checkbox', elem).on('ifUnchecked', function () {
                if (anonymous) {
                    _.delay(function () {
                        //$('input.checkbox', elem).iCheck('uncheck');
                    }, 250);
                    Auth.ShowLogin();
                    return;
                }

                // slide up requirements

                $('.requirements', elem).slideUp();
                $('.attend', elem).removeClass('on');
                $('.attendee-whitelist', elem).show();
                $('.attendee-whitelist-enabled', elem).hide();
                $(elem).removeClass('attending');


                self.RemoveAsAttendee(listEvent.EventDateId);
                listEvent.RemovePlayer(MC3.CurrentUsername);
                $('.nano', elem).nanoScroller({ preventPageScrolling: true });
                heightFix(elem);

                log("removed", MC3.CurrentUsername, "as attendee for eventdate:", listEvent.EventDateId);

                // re-render the tooltips
                $('.tip', elem).qtip({
                    position: {
                        my: 'bottom center',
                        at: 'top center'
                    },
                    style: {
                        classes: 'qtip-light'
                    }
                });

                // hack sets the height of the event box manually 
                heightFix(elem);
            });

            // flip

            _.delay(function () {
                // hack sets the height of the event box manually 
                heightFix(elem);

                $(elem).removeClass('shadowed');
                $(elem).addClass('flip');

                $(elem).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
                    $(elem).addClass('shadowed');
                    $('.front', elem).removeClass('hidden');

                    // fix footer
                    UI.positionFooter();
                });
            }, Constant.CardLoadDelay * index);

            // bind card press

            $('.back', elem).on('mousedown', function () {
                $(elem).addClass('pressed-back');
            });

            $('.back', elem).on('mouseup mouseleave', function () {
                $(elem).removeClass('pressed-back');
            });

            // bind flip

            $('.back', elem).on('click', function () {

                // bind unflip ability to back side elements
                var front = $(elem).find('.front');

                front.on('click', function () {
                    $(elem).removeClass('shadowed');
                    $(elem).addClass('flip');

                    $(elem).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
                        $(elem).addClass('shadowed');

                        _.delay(function () {
                            //if (listEvent.Slider.length > 0)
                            //  listEvent.Slider.redrawSlider();
                        }, 50);
                    });
                });

                // flip to back

                $(elem).removeClass('shadowed');
                $(elem).removeClass('flip');

                $(elem).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
                    $(elem).addClass('shadowed');
                });
            });
        }
    };

    this.BeforeRemoveEvent = function (elem) {
        if (elem.nodeType === 1) {
            if (inViewport(elem)) {
                $(elem).removeClass('shadowed');
                $(elem).removeClass('flip');

                // force remove the element after the transition as knockout seems not to be able to remove 3d transformed elementsproperly

                $(elem).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
                    $(elem).remove();
                });
            } else {
                $(elem).remove();
            }
        }
    }

    this.pullList = function (freshPull) {
        var deferred = $.Deferred();

        // reject non-fresh pulls once we reach end of list

        if (self.EndOfList && !freshPull) {
            deferred.reject();
            return deferred.promise();
        }

        // buid request

        var request = {
            id: guid(),
            state: _.clone(self.State.data),
            freshPull: freshPull,
            deferred: deferred,
            served: false
        };

        log('+ logged new request', request);

        // add as current pull

        self.CurrentRequest = request;

        return deferred.promise();
    };

    var processResult = function (res, freshPull) {
        log('processing result: %O', res);

        if (res.TotalCount === 0) {
            return;
        }

        var arrays = {
            1: self.Events1,
            2: self.Events2,
            3: self.Events3,
            4: self.Events4
        };

        // set count on the UI

        self.Total(res.TotalCount);
        $('.count-label').removeClass('shaded');

        // build events

        var allEvents = _.map(res.List, function (eventData) {
            return new ListEvent(eventData);
        });

        // utility funciton used to find the next list

        var findNextList = function () {
            var allowedDDiff = 200;
            var heights = [
                {
                    list: 1,
                    height: $('#list1').height()
                }, {
                    list: 2,
                    height: $('#list2').height()
                }, {
                    list: 3,
                    height: $('#list3').height()
                }, {
                    list: 4,
                    height: $('#list4').height()
                }
            ];

            if (freshPull) {
                heights[0].height = 0;
                heights[1].height = 0;
                heights[2].height = 0;
                heights[3].height = 0;
            }

            // find mean

            var sorted = _.sortBy(heights, 'height');
            var mean = (sorted[1].height + sorted[2].height) / 2;

            // if the list with the smallest height is too small
            // populate that one first

            if (sorted[0].height < mean - allowedDDiff) {
                stepToNextList();
                return sorted[0].list;
            }

            // otherwise step to the next list and populate it.
            // if its too big, skip it and try the next one
            // do this for each of the lists

            for (var i = 0; i < 4; i++) {

                // step to the next list

                var nextList = stepToNextList();
                var next = _.find(heights, { 'list': nextList });

                if (next.height < mean + allowedDDiff) {
                    return next.list;
                }
            }

            // should never get here, but if it does populate it where its at

            var last = _.find(heights, { 'list': self.LastList });
            return last.list;
        };

        // place all events on the lists

        $.each(allEvents, function (i, v) {
            var next = findNextList();
            arrays[next].push(v);
        });

        // signal end of list when notified so, 
        // otherwise show load more button

        if (res.EndOfList) {
            self.EndOfList = true;
            log('** EndOfList');

            $('#loading').removeClass('spinner');
            $('#load-more').fadeOut();
        } else {
            $('#load-more').fadeIn();
            $('#loading').removeClass('spinner');
        }
    };

    this.startPullQueue = function () {

        setInterval(function () {
            if (self.CurrentRequest != null && !self.CurrentRequest.served) {

                var request = self.CurrentRequest;
                request.served = true;

                log("* running current pull", request);

                // prepare for a fresh pull

                if (request.freshPull) {
                    log('fresh pull requested');
                    self.EndOfList = false;
                    self.LastList = 4;

                    self.Events1.removeAll();
                    self.Events2.removeAll();
                    self.Events3.removeAll();
                    self.Events4.removeAll();
                    $('#list1').html('');
                    $('#list2').html('');
                    $('#list3').html('');
                    $('#list4').html('');
                }

                // pull

                self.pullEventList(request).done(function (res) {
                    log('+ request result received', res);

                    if (self.CurrentRequest != null && res.RequestId === self.CurrentRequest.id) {
                        processResult(res, request);
                        request.deferred.resolve();
                    } else {
                        log('+ no longer current - ignored');
                        request.deferred.reject();
                    }
                });
            }
        }, 100);
    };

    this.pullEventList = function (request) {
        var totalExisting = self.Events1().length + self.Events2().length + self.Events3().length + self.Events4().length;

        var data = {
            filters: request.state,
            requestId: request.id,
            skip: request.freshPull ? 0 : totalExisting,
            take: Constant.EventListTake
        };

        return $.ajax({
            url: '/events/GetEvents',
            type: 'POST',
            dataType: 'json',
            data: ko.toJSON(data),
            contentType: 'application/json; charset=utf-8'
        });
    };

    this.AddAsAttendee = function (eventDateId) {
        var data = {
            eventDateId: eventDateId
        };

        return $.post('/events/AddAsAttendee', data);
    };

    this.RemoveAsAttendee = function (eventDateId) {
        var data = {
            eventDateId: eventDateId
        };

        return $.post('/events/RemoveAsAttendee', data);
    };
};

// **************************************************************** MODELS

// listevent.js

// **************************************************************** UI

ListUI = function () {

    var tagData = $.jStorage.get('m3-event-tag-data') || {};

    var extractData = function (res) {
        // extract the data
        $.each(res, function (type) {
            var tags = res[type];
            if (tags && tags.length) {
                if (tags[0].hasOwnProperty('Abbreviation')) {
                    log('adding logo data for', type);
                    tagData = _.uniq(_.union(tagData, tags), 'Name');
                }
            }
        });

        // save to storage
        $.jStorage.set('m3-event-tag-data', tagData, 300000); // 5 mins
    };

    var getAndStoreTagData = function () {

        var deferred = new $.Deferred();

        if (!_.isEmpty(tagData)) {
            return deferred.resolve().promise();
        }

        log('+ getting and storing the full set of tags');

        $.ajax({
            url: "/tags/geteventtags",
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: ko.toJSON(model.State.defaultState),
            success: function (res) {
                log('+ loaded and stored all event tags', res);

                // extract the data
                extractData(res);

                // set as resolved as we already have the data
                deferred.resolve();
            }
        });

        return deferred.promise();
    };

    return {
        getTagData: function (name) {
            return _.find(tagData, function (n) {
                return n.Name === name;
            });
        },
        selectize: null,
        selectizeSort: null,
        tagSelectizes: [],

        selectizeFilters: function () {
            var selectize = $("#filters").selectize({
                plugins: ['remove_button'],
                valueField: 'Name',
                labelField: 'Name',
                openOnFocus: false,
                persist: false,
                create: false,
                onItemRemove: function (value) {
                    model.State.removeFilter(value);

                    if (ListUI.selectize.items.length === 0) {
                        model.Total(0);
                    }
                },
                onItemAdd: function () {
                },
                render: {
                    override: true,
                    item: function (item, escape) {

                        var html = '<div class="dd-item item">';

                        log("item", item);

                        if (item.hasOwnProperty('Abbreviation')) {
                            log("flag");
                            html += '<div class="flag ' + escape(item.Abbreviation.toLowerCase()) + '"></div>';
                        } else {
                            log("none");
                            html += '<div class="img-wrap"></div>';
                        }

                        html += '<span class="name">' + escape(item.Name) + '</span>' +
                            '<a href="javascript:void(0)" class="remove" tabindex="-1" title="Remove">&times;</a>' +
                            '</div>';

                        return html;
                    }
                }
            });

            ListUI.selectize = selectize[0].selectize;
        },

        selectizeTags: function (selector, type, successCallback, country) {
            var render = {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="name">' + escape(item.Name) + '</span>' +
                        '<span class="count">' + escape(item.Count) + '</span>' +
                        '</div>';
                }
            };

            if (country) {
                render = {
                    option: function (item, escape) {
                        return '<div class="dd-option">' +
                            '<span class="flag ' + escape(item.Abbreviation.toLowerCase()) + '"></span>' +
                            '<span class="name">' + escape(item.Name) + '</span>' +
                            '<span class="count">' + escape(item.Count) + '</span>' +
                            '</div>';
                    }
                };
            }

            var selectize = $(selector).selectize({
                maxItems: 1,
                valueField: 'Name',
                labelField: 'Name',
                openOnFocus: true,
                persist: false,
                searchField: ['Name'],
                sortFieldNumeric: 'Count',
                sortDirection: 'desc',
                autoEnable: true,
                preload: false,
                create: false,
                render: render,
                load: function (query, callback) {
                    load(callback);
                },
                onItemAdd: function (val, $item) {
                    selectize[0].selectize.clear();
                    setTimeout(function () {
                        selectize[0].selectize.showInput();
                        selectize[0].selectize.blur();

                        // hack to fix inputs staying focused after tag selection
                        _.delay(function () { $('.selectize-input').removeClass('focus'); }, 100);
                    }, 0);

                    model.State.addFilter(val, type, $item);
                }
            });

            // start it up disabled
            selectize[0].selectize.disable();

            var load = function (callback) {
                // pull data from data store
                var tags = model.TagStore[type];
                
                callback(tags);

                //enable once ready
                if (tags && tags.length) {
                    selectize[0].selectize.enable();
                    log('loading %s %O', type, tags);
                }
            };

            ListUI.tagSelectizes.push({ type: type, $selector: $(selector), selectize: selectize[0].selectize, load: load });
        },

        reloadTagSelectizes: function () {
            var deferred = new $.Deferred();

            // clear all dropdowns

            $.each(ListUI.tagSelectizes, function (i, sel) {
                sel.selectize.disable();
                sel.selectize.clearOptions();
            });

            // make sure we have the full set of tags

            getAndStoreTagData().done(function() {
                
                // populate all selectizes

                $.ajax({
                    url: "/tags/geteventtags",
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    data: ko.toJSON(model.State.data),
                    success: function(res) {
                        log('+ loaded event tags', res);

                        // store tag data
                        model.TagStore = res;

                        // have selectizer read from store
                        $.each(res, function(type) {
                            var sel = _.find(ListUI.tagSelectizes, { 'type': type });
                            sel.selectize.renderCache = {};
                            sel.selectize.load.call(sel.selectize, sel.load);
                            sel.selectize.refreshOptions(false);
                        });

                        // showtabs if hidden

                        if (!$('#filter-container').is(':visible')) {
                            $('#filter-container').slideDown('slow');
                        }

                        // set as resolved as we already have the data
                        deferred.resolve();
                    }
                });
            });

            return deferred.promise();
        },

        selectizeSortDropDown: function () {
            var selectize = $("#sort").selectize({
                onChange: function (value) {
                    model.State.changeSort(value);
                },
                render: {
                    item: function (item, escape) {
                        return "<div>Sorted by " + escape(item.text) + "</div>";
                    }
                }
            });
            ListUI.selectizeSort = selectize[0].selectize;
        },

        resizeDropdownWithWindow: _.debounce(function () {
            var h = $(window).height();
            $('.selectize-control .selectize-dropdown').css('max-height', (h - 300) + "px");
        }, 250, { 'trailing': true })
    }
}();

// ****************************************************************

$(function () {

    ListUI.selectizeTags('#eventgameplays', 'eventgameplays');
    ListUI.selectizeTags('#language', 'languages');
    ListUI.selectizeTags('#countries', 'countries', function () { }, true);

    ListUI.selectizeSortDropDown();
    ListUI.selectizeFilters();

    // bind the model

    window.model = new EventsModel();

    MC3.templates.load().done(function () {
        model.State.init();
        ko.applyBindings(model);
        log('initiated model');
    });

    // bind load more button

    $('#load-more').click(function (e) {
        $(this).fadeOut();
        model.pullList();
        e.preventDefault();
    });

    // bind resize dropdown height

    $(window).resize(ListUI.resizeDropdownWithWindow);
});