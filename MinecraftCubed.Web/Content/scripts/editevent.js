﻿$(function() {
    UI.reLayout();
    _.delay(function() {
        UI.reLayout();
        _.delay(function() {
            UI.reLayout();
        }, 2000);
    }, 1000);
});

var EditEventModel = function() {
    
    return {
        refreshEventDates: function () {
            // set up localtimes

            $('.event-date').each(function () {

                var $d = $('.date', $(this));
                var $t = $('.time', $(this));

                var startDate = moment($d.html());
                var endDate = moment($d.attr('data-end'));
                
                log('startdate', startDate);
                log('enddate', endDate);

                var dateString = startDate.format("ddd, MMM D");
                $d.html(dateString);

                var timeString = startDate.format("h:mm a");
                $t.html(timeString);

                if (moment().isAfter(endDate)) {
                    $(this).addClass('past');
                    $('.delete-link', this).remove();
                }
            });

            // set up timepicker

            $('#StartDate').datetimepicker({
                formatTime: 'g:i a',
                formatDate: 'Y/m/d',
                format: 'Y/m/d g:i a'
            });

            // load scroll tabs

            UI.scrollTopTab(Constant.SitePageScrollTabLevel).start();
            UI.scrollBottomTab();
        }
    }
}();
