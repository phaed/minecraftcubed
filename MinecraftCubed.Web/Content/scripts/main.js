//  initiate global object with defaults

var MC3 = MC3 || {};

MC3.CurrentUsername = '';

// object for loading knockout templates

MC3.templates = new function() {
    var deferred = new $.Deferred();

    return {
        load: function() {
            $.get("/content/templates/all.html", function (template) {
                $("body").prepend(template);
                log('loaded templates');
                deferred.resolve();
            });

            return deferred.promise();
        }
    }
}();

// knockout bindings

ko.bindingHandlers.slideIn = {
    init: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).toggle(value);
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        value ? $(element).slideDown('slow') : $(element).slideUp('slow');
    }
};


// highlights the current menu item

var Menu = {
    highlight: function (name) {
        $('#' + name + '-link a').addClass(name).addClass("selected");
        $('html').addClass(name);
    }
};


// pre-submit form manipulations

var OnSubmit = {
    addServer: function () {
        if ($('#add-server #Port').val() == '') {
            $('#add-server #Port').val('25565');
        }
        UI.showLoader();
    },
    reload: function () {
        _.delay(function () {
            location.reload();
        }, 2500);
    }
};

// set language for timebars

moment.lang('en', {
    calendar: {
        lastDay: '[<div>Yesterday</div>] LT',
        sameDay: '[<div>Today</div>] LT',
        nextDay: '[<div>Tomorrow</div>] LT',
        lastWeek: '[<div>last] dddd [</div>] LT',
        nextWeek: '[<div>]dddd[</div>] LT',
        sameElse: '[<div>]MMM Do[</div>] LT'
    }
});

// detect mobile device

var isMobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));


$(function () {
    // pages
    var page = Arg.get('open');

    if (page == 'login') {
        Auth.ShowLoginModal();
    }

    if (page == 'register') {
        Auth.ShowRegisterModal();
    }

    if (page == 'verify') { 
        Auth.ShowVerifyModal();
    }

    if (page == 'menu') {
        $('#nav-dropdown').dropdown('toggle');
    }

    // set up info tips

    Auth.Status(function () {
        $('#user-menu .face')
            .attr('data-position', 'left')
            .attr('data-intro', Constant.Info.UserNav);
    }, function () {
        $('#login-button a')
            .attr('data-position', 'left')
            .attr('data-intro', Constant.Info.LoginButton);
    });

    $('.tags')
        .attr('data-position', 'top')
        .attr('data-intro', Constant.Info.Tags);

    // forms that need loaders on submit

    $('.loader-on-submit').submit(function () {
        UI.showLoader();
    });
});

// notify styles

$.notify.addStyle("info", {
    html: "<div><i class='fa fa-info'></i> \n<span data-notify-text></span>\n</div>",
});

$.notify.addStyle("success", {
    html: "<div><i class='fa fa-check'></i> \n<span data-notify-text></span>\n</div>",
});

$.notify.addStyle("error", {
    html: "<div><i class='fa fa-times'></i> \n<span data-notify-text></span>\n</div>",
});

// application insights

var appInsights = window.appInsights || function (config) {
    function r(config) { t[config] = function () { var i = arguments; t.queue.push(function () { t[config].apply(t, i) }) } }
    var t = { config: config }, u = document, e = window, o = 'script', s = u.createElement(o), i, f; for (s.src = config.url || '//az416426.vo.msecnd.net/scripts/a/ai.0.js', u.getElementsByTagName(o)[0].parentNode.appendChild(s), t.cookie = u.cookie, t.queue = [], i = ['Event', 'Exception', 'Metric', 'PageView', 'Trace', 'Ajax']; i.length;)r('track' + i.pop()); return r('setAuthenticatedUserContext'), r('clearAuthenticatedUserContext'), config.disableExceptionTracking || (i = 'onerror', r('_' + i), f = e[i], e[i] = function (config, r, u, e, o) { var s = f && f(config, r, u, e, o); return s !== !0 && t['_' + i](config, r, u, e, o), s }), t
}({
    instrumentationKey: 'dc84d107-3216-4602-8fe7-9aa274a8a595'
});

window.appInsights = appInsights;
appInsights.trackPageView();