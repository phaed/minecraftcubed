﻿
Notify = new function () {

    var nots = $.jStorage.get('notifications') || {};

    var set = function (key) {
        nots[key] = true;
        $.jStorage.set('notifications', nots);
        log('notifications', nots);
    };

    return {
        info: function (text, key) {
            if (key !== undefined)
                if (nots[key] !== undefined)
                    return;

            $.notify(text, {
                autoHide: false,
                style: 'info'
            });

            if (key !== undefined)
                set(key);
        },
        success: function (text, key) {
            if (key !== undefined)
                if (nots[key] !== undefined)
                    return;

            $.notify(text, {
                style: 'success'
            });
            if (key !== undefined)
                set(key);
        },
        error: function (text, key) {
            if (key !== undefined)
                if (nots[key] !== undefined)
                    return;

            $.notify(text, {
                autoHide: false,
                style: 'error'
            });
            if (key !== undefined)
                set(key);
        },
    };
};

$(function() {
    var success = Arg.get('success');

    if (success !== undefined && success && success.length) {
        Notify.success(success);
    }

    var info = Arg.get('info');

    if (info !== undefined && info && info.length) {
        Notify.info(info);
    }

    var error = Arg.get('error');

    if (error !== undefined && error && error.length) {
        Notify.error(error);
    }
});