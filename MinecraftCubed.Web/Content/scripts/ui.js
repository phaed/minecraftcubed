﻿
$(function () {

    window.visibleBoxes = $.jStorage.get('mc3-visible-boxes' + window.location.pathname) || window.initalBoxes || [];

    // set up isotope

    $('.box-container').isotope({
        // options
        itemSelector: '.box',
        masonry: {
            columnWidth: 370,
            gutter: 35
        },
        layoutMode: 'masonry',
        animationEngine: 'jquery',
        filter: window.visibleBoxes.join(',')
    });

    $('.matrix-container').isotope({
        // options
        itemSelector: '.matbox',
        masonry: {
            columnWidth: 280,
            gutter: 20
        },
        layoutMode: 'masonry',
        animationEngine: 'jquery'
    });

    // set up tabs for config sections

    function sortAlpha(a, b) {
        return ($(b).text()) < ($(a).text()) ? 1 : -1;
    };

    $('.box-container .box').sort(sortAlpha).each(function () {
        $('.tabs').append('<button class="tab" data-filter="#' + $(this).attr('id') + '">' + $('h2', this).html() + '</button>');
    });

    // filters toggle

    $('.tabs .tab').each(function () {
        var me = $(this);
        var filter = $(this).attr('data-filter');

        if ($.inArray(filter, window.visibleBoxes) > -1) {
            me.addClass('pressed');
        }
    });

    $('.tabs .tab').click(function () {

        var me = $(this);
        var filter = $(this).attr('data-filter');

        if (me.hasClass('pressed')) {
            window.visibleBoxes = _.without(window.visibleBoxes, filter);
            me.removeClass('pressed');
        } else {
            window.visibleBoxes.push(filter);
            me.addClass('pressed');
        }

        $.jStorage.set('mc3-visible-boxes' + window.location.pathname, window.visibleBoxes);

        $('.box-container').isotope({
            filter: window.visibleBoxes.join(',')
        });
    });
});

var UI = {
    resetAutosized: function () {
        $('.full-center').each(function () {
            var item = $(this);
            var itemHeight = item.outerHeight();
            var itemWidth = item.outerWidth();
            var topSpace = ($(window).height() / 2) - (itemHeight / 2);

            item.css('top', topSpace).css('margin-left', -(itemWidth / 2));

            if (!$(this).is(':visible') && !$(this).hasClass('modal'))
                $(this).show();
        });

        $('.vertical-center').each(function () {
            var item = $(this);
            var itemHeight = item.outerHeight();
            var topSpace = ($(window).height() / 2) - (itemHeight / 2);

            item.css('top', topSpace);

            if (!$(this).is(':visible') && !$(this).hasClass('modal'))
                $(this).show();
        });

        $('.modal-vertical-center').each(function () {
            var item = $(this);
            var itemHeight = item.outerHeight();
            var topSpace = ($(window).height() / 2) - (itemHeight / 2);

            item.css('margin-top', topSpace);

            if (!$(this).is(':visible') && !$(this).hasClass('modal'))
                $(this).show();
        });
    },

    refresh: function (parent, relayout) {

        if (typeof parent == 'undefined') {
            parent = "body";
        }

        // add iCheck
        $('input[type="checkbox"]:not(.clean), input[type="radio"]:not(.clean)', parent).iCheck();

        // set up togglebuckets
        $('.toggle-check', parent).each(function () {
            var input = $('input[type=checkbox]', this);
            var bucket = $('.toggle-bucket', $(this).parent());
            var box = $(this).parents('.toggle-box');

            input.on('ifChecked', function () {
                box.addClass('open');
                bucket.slideDown(function () {
                    UI.reLayout();
                });
            }).on('ifUnchecked', function () {
                bucket.slideUp(function () {
                    UI.reLayout();
                });
                box.removeClass('open');
            });

            if (!input.is(':checked')) {
                bucket.hide();
            } else {
                box.addClass('open');
            }
        });

        //pagedown
        if (jQuery().pagedownBootstrap) {
            $("textarea.pagedown", parent).pagedownBootstrap({
                'sanatize': true
            });
        }

        // add in check/radio validatiom
        $('input[type=radio].input-validation-error, input[type=checkbox].input-validation-error', parent).each(function () {
            $(this).parent().addClass('input-validation-error');
        });

        // clearing of checkbox/radio validation errors on click
        $('input[type=checkbox].input-validation-error, input[type=radio].input-validation-error', parent).click(function () {
            $(this).parent().removeClass('input-validation-error');
        });

        // clearing of input validation errors on keydown
        $('input', parent).keydown(function () {
            $(this).removeClass("input-validation-error");
        });

        // textfill
        $('[data-max]:not(.filled)').each(function () {
            var me = $(this);
            if (!me.find(">:first-child").is('span')) {
                me.wrapInner("<span></span>");
            }
            me.css('white-space', 'nowrap');
            me.removeClass('hidden');
            var opts = { maxFontPixels: me.attr('data-max'), widthOnly: true };
            log(opts);
            me.textfill(opts);
            me.addClass('filled');
        });

        // fontsize
        $('.hidden[data-font-size]').each(function () {
            var me = $(this);
            me.css('font-size', me.attr('data-font-size') + 'px');

            if (me.attr('data-line-height') && me.attr('data-line-height').length)
                me.css('line-height', me.attr('data-line-height') + 'px');

            if (me.attr('data-top') && me.attr('data-top').length)
                me.css('top', me.attr('data-top') + 'px');

            me.removeClass('hidden');
        });

        // add selectize validation
        $('.selectize-control.input-validation-error', parent).each(function () {
            $('.selectize-input', this).addClass("input-validation-error");
        });

        // remove selectize validation
        $('.selectize-control.input-validation-error input', parent).click(function () {
            $(this).parent().removeClass("input-validation-error");
            $(this).parent().parent().removeClass("input-validation-error");
        });

        // remove icheckbox/iradio validation
        $('.iCheck-helper', parent).unbind('click').bind('click', function () {
            $(this).parent().removeClass("input-validation-error");
            $(this).parent().find('input').removeClass("input-validation-error");
        });

        // highlight on player face on usernav
        $('#user-menu .dropdown', parent).unbind('click').bind('mouseover', function () {
            $(this).find('.face').addClass('highlighted');
        });
        $('#user-menu .dropdown', parent).unbind('click').bind('mouseleave', function () {
            $(this).find('.face').removeClass('highlighted');
        });

        // fileuploader click on drop area
        $('.qq-upload-drop-area', parent).unbind('click').bind('click', function () {
            $(this).parent().find('.qq-upload-button input').click();
        });

        // expandable description buckets

        UI.bindElasticBucket(parent);

        // bind geenric qtips
        $('.tip', parent).qtip({
            position: {
                my: 'bottom center',
                at: 'top center'
            },
            style: {
                classes: 'qtip-light'
            }
        });

        // vanilla selectize if refreshing body only to prevent double doing it

        $('.selectize', parent).selectize();

        // color inputs
        $(".color", parent).spectrum({
            showButtons: false,
            showInitial: true,
            showInput: true,
            showAlpha: true,
            showPalette: true,
            showSelectionPalette: true,
            preferredFormat: "hex",
            palette: [],
            localStorageKey: "mc3"
        });

        // ajax forms
        $('form.ajax', parent).unbind('submit').bind('submit', function (e) {
            e.preventDefault();

            var pre = $(this).attr('data-pre');
            var post = $(this).attr('data-post');
            var action = $(this).attr('data-action');
            var controller = $(this).attr('data-controller');
            var container = $(this).attr('data-container');
            var data = $(this).serializeArray();

            getFunctionFromString(pre)(container);

            log("form submit: %s/%s  payload: %O", controller, action, data);

            $.ajax({
                url: Url(action, controller),
                type: "POST",
                data: data,
                complete: function (result) {
                    var classes = $(container).attr('class');
                    var style = $(container).attr('style');

                    log("form response:  %O", result.responseText);

                    $(container).replaceWith(result.responseText);
                    $(container).attr('class', classes).attr('style', style);

                    getFunctionFromString(post)(container);
                }
            });

            return false;
        });

        // hold to delete qtips

        $('.delete-hold', parent).qtip({
            position: {
                my: 'bottom center',
                at: 'top center'
            }, content: {
                text: 'Hold'
            },
            style: {
                classes: 'qtip-light'
            }
        });

        // set up delete links

        var doDelete = function (e) {
            e.preventDefault();
            var $self = $(this);
            var box = $self.closest('.box');

            UI.showLoader(box);

            var delurl = $self.attr('data-url');
            var id = $self.attr('data-id');
            var key = $self.attr('data-key');

            $.post('/' + delurl, {
                Id: id,
                Identifier: $('#Identifier', box).val(),
                EventIdentifier: $('#EventIdentifier', box).val(),
                Key: key
            }, function (res) {
                if (res.success) {
                    var p = $self.parents('.parent');

                    if (!p.length) {
                        p = $self.parent();
                    }

                    p.fadeOut(function () {
                        p.remove();
                    });

                } else {
                    var error = res.error;
                    if (error && error.length) {
                        UI.showError(box, res.error);
                    }
                }
                UI.hideLoader(box);
            });
        };

        $('.delete-link', parent).unbind('click').click(doDelete);
        $('.delete-link-hold', parent).longpress(doDelete);

        // set up sortables

        $('.sortable').each(function (i, v) {
            var $self = $(this);
            var box = $self.closest('.box');
            var sortUrl = $self.attr('data-url');

            $self.sortable({
                update: function (event, ui) {
                    var ids = $(this).sortable('toArray', { attribute: 'data-id' }).toString();

                    $.post('/' + sortUrl, {
                        Ids: ids,
                        Identifier: $('#Identifier', box).val(),
                        EventIdentifier: $('#EventIdentifier', box).val(),
                    });
                }
            });
        });

        // postback only

        if (parent != 'body') {

            // animate alert boxes

            $('.alert', parent).each(function () {

                if ($(this).hasClass('no-reset')) {
                    return;
                }

                var alert = this;

                $(alert).slideDown(function () {
                    if ($(alert).hasClass('alert-error')) {
                        UI.reLayout();
                    }
                });

                if ($(alert).hasClass('alert-success')) {
                    setTimeout(function () {
                        $(alert).slideUp(function () {
                            UI.resetAlert(parent);
                            UI.reLayout();
                        });
                    }, 2000);
                }
            });
        }

        // relayout masonry

        if (relayout) {
            $(window).load(function () {
                UI.reLayout();
            });
            UI.reLayout();
        }
    },

    reLayout: _.debounce(function () {
        if ($('.box-container').length && !window.boxIsotopeStarted) {

            window.boxIsotopeStarted = true;
            var scrollTop = $(window).scrollTop();

            $('.box-container').isotope('destroy');
            var $container = $('.box-container');

            $container.isotope({
                itemSelector: '.box',
                masonry: {
                    columnWidth: 370,
                    gutter: 35
                },
                layoutMode: 'masonry',
                filter: window.visibleBoxes.join(',')
            });

            window.boxIsotopeStarted = false;
            $(window).scrollTop(scrollTop);
        }

        if ($('.matrix-container').length && !window.matrixIsotopeStarted) {

            window.matrixIsotopeStarted = true;
            var scrollTop = $(window).scrollTop();

            $('.matrix-container').isotope('destroy');
            var $container = $('.matrix-container');

            $container.isotope({
                itemSelector: '.matbox',
                masonry: {
                    columnWidth: 280,
                    gutter: 20
                },
                layoutMode: 'masonry',
                filter: window.visibleBoxes.join(',')
            });

            window.matrixIsotopeStarted = false;
            $(window).scrollTop(scrollTop);
        }
    }, 150, { leading: true }),

    cleanErrors: function () {
        // clear of input validation errors
        $('input').each(function () {
            $(this).removeClass("input-validation-error");
        });
    },

    showLoader: function (container) {
        $(".loader", container).show();
    },

    hideLoader: function (container) {
        $(".loader", container).hide();
    },

    resetAlert: function (container) {
        var element = $(container).find('.alert');
        if (element.length) {
            if (element.is(':visible'))
                element.hide();
            element.removeAttr('class').addClass('alert-ribbon').html('').show();
            log('alert reset');
        }
    },

    openError: function (box) {
        $('.alert', box).addClass('alert-static');
    },

    showError: function (container, message, callback) {
        container = $(container);
        UI.resetAlert(container);
        var content = '<i class="icon-remove-sign"></i>' + message;

        if (container.find('.alert-ribbon').length) {
            container.find('.alert-ribbon').removeClass().addClass('alert alert-error adjusted').html(content).slideDown(function () {
                UI.reLayout();
            });
        }
        if (container.find('.alert').length) {
            container.find('.alert').removeClass().addClass('alert alert-error adjusted').html(content).slideDown(function () {
                UI.reLayout();
            });
        }

        /*
        setTimeout(function () {
            container.find('.alert').slideUp(function () {
                UI.resetAlert(container);

                if (typeof callback != 'undefined')
                    callback();
            });
        }, 2000);
        */
        log('error shown');
    },

    showSuccess: function (container, message, callback) {
        container = $(container);
        UI.resetAlert(container);
        var content = '<i class="icon-ok"></i>' + message;

        var element;
        if (container.find('.alert-ribbon').length) {
            element = container.find('.alert-ribbon');
        }
        if (container.find('.alert').length) {
            element = container.find('.alert');
        }

        if (typeof element !== 'undefined') {
            element.removeClass().addClass('alert alert-success adjusted').html(content).slideDown(function () {
                //UI.reLayout();
                //no relayout when extended since it will bounce right back
            });

            setTimeout(function () {
                element.slideUp(function () {
                    UI.resetAlert(container);
                    UI.reLayout();

                    if (typeof callback != 'undefined')
                        callback();
                });
            }, 2000);
        }
        log('success shown');
    },

    showInfo: function (container, message) {
        UI.resetAlert(container);
        var content = '<i class="icon-info-sign"></i>' + message;

        var element;
        if (container.find('.alert-ribbon').length) {
            element = container.find('.alert-ribbon');
        }
        if (container.find('.alert').length) {
            element = container.find('.alert');
        }

        if (typeof element !== 'undefined') {
            element.removeClass().addClass('alert alert-info adjusted').html(content).slideDown(function () {
                UI.reLayout();
            });
        }
        log('info shown');
    },

    selectize: function (selector, type) {
        var val = $(selector).val();
        $(selector).val('');

        var selectize = $(selector).selectize({
            maxItems: 1,
            valueField: 'Name',
            labelField: 'Name',
            openOnFocus: false,
            options: [{ Name: val }],
            persist: false,
            searchField: ['Name'],
            preload: true,
            create: true,
            createOnBlur: true,
            load: function (query, callback) {
                $.post("/tags/getall" + type, {}, function (res) {
                    callback(res);
                    reSet();
                });
            }
        });

        var reSet = function () {
            if (val && val.length > 0) {
                selectize[0].selectize.disable();
                selectize[0].selectize.setValue(val);
                selectize[0].selectize.enable();
                val = "";
            }
        };
    },

    selectizeTags: function (selector, type) {
        var val = $(selector).val();
        $(selector).val('');

        var ops = [];

        if (val && val.length) {
            ops = _.map(val.split(','), function (v) {
                return { Name: v };
            });
        }

        var selectize = $(selector).selectize({
            plugins: ['remove_button'],
            valueField: 'Name',
            labelField: 'Name',
            options: ops,
            openOnFocus: true,
            persist: false,
            searchField: ['Name'],
            preload: true,
            createOnBlur: true,
            create: true,
            load: function (query, callback) {
                $.post("/tags/getall" + type, {}, function (res) {
                    callback(res);
                    reSet();
                });
            },
            onItemAdd: function (value) {
                log('added', value);
                UI.reLayout();
            },
            onDropdownClose: function (dropdown) {
                $(dropdown).prev().find('input').blur();
            }
        });

        var reSet = function () {
            if (val && val.length > 0) {
                var vals = val.split(',');

                log('selectize val: %s', val);

                selectize[0].selectize.disable();
                $.each(vals, function (i, v) {
                    selectize[0].selectize.addItem(v, true);
                });

                _.delay(function () {
                    selectize[0].selectize.close();
                    selectize[0].selectize.blur();
                    selectize[0].selectize.enable();
                    UI.reLayout();
                }, 100);
                val = "";
            }
        };
    },

    selectizeCountries: function (selector) {
        var val = $(selector).val();
        $(selector).val('');

        var selectize = $(selector).selectize({
            delimiter: ',',
            maxItems: 1,
            create: false,
            valueField: 'Name',
            labelField: 'Name',
            openOnFocus: false,
            persist: false,
            searchField: ['Name'],
            preload: true,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="flag ' + escape(item.Abbreviation.toLowerCase()) + '"></span>' +
                        '<span class="name">' + escape(item.Name) + '</span>' +
                    '</div>';
                },
                item: function (item, escape) {
                    return '<div>' +
                        '<span class="flag ' + escape(item.Abbreviation.toLowerCase()) + '"></span>' +
                        '<span class="name">' + escape(item.Name) + '</span>' +
                    '</div>';
                }
            },
            load: function (query, callback) {
                $.post("/tags/getcompletecountries", {}, function (res) {
                    callback(res);
                    reSet();
                });
            }
        });

        var reSet = function () {
            if (val && val.length) {
                selectize[0].selectize.disable();
                selectize[0].selectize.addItem(val, true);
                selectize[0].selectize.enable();
                val = "";
            }
        };
    },

    selectizeTimeZone: function (selector) {
        // load saved time zone preference

        var selectizeTz = $(selector).selectize({
            onChange: function (value) {
                $.jStorage.set('timezone', value);
            }
        });

        var tz = $.jStorage.get('timezone');

        if (tz && tz.length) {
            selectizeTz[0].selectize.disable();
            selectizeTz[0].selectize.setValue(tz);
            selectizeTz[0].selectize.enable();

            _.delay(function () {
                selectizeTz[0].selectize.close();
                selectizeTz[0].selectize.blur();
            }, 100);
        }
    },

    setupUploader: function (element, url, identifier, skipSuccessAlert, multipleFiles) {
        var me = $(element);
        var self = this;
        var box = me.parents('.box');
        log(element);

        self.total = 0;
        self.count = 0;

        // set up uploader

        var uploader = new qq.FineUploader({
            element: $(element)[0],
            request: {
                endpoint: '/' + url,
                params: {
                    identifier: identifier
                }
            },
            multiple: multipleFiles,
            dragAndDrop: {
                hideDropzones: false,
            },
            validation: {
                allowedExtensions: ['jpeg', 'jpg', 'gif', 'png'],
                sizeLimit: 9437184 // 1024 kB = 1024 * 1024 bytes
            },
            text: {
                uploadButton: '',
                dropProcessing: '',
                dragZone: multipleFiles ? 'Drop images here or click to add' : 'Drop image here or click to add'
            },
            cors: {
                expected: true
            },
            callbacks: {
                onUpload: function () {
                    UI.showLoader(box);
                    UI.resetAlert(box);

                    self.count++;
                    self.total++;
                    log('uploading: ' + self.count);

                    var countStr = "";
                    if (self.count > 1)
                        countStr = self.count + "/" + self.total;

                    $('.qq-upload-drop-area span', me).html("Uploading " + countStr + " ...");
                },
                onComplete: function (id, name, responseJSON) {
                    console.log(responseJSON);
                    UI.hideLoader(box);

                    self.count--;
                    log('complete: ' + self.count);

                    // reset text
                    //$('.qq-upload-drop-area span', me).html(me.attr('data-text'));

                    if (responseJSON.success) {

                        var countStr = "";

                        if (self.count > 0)
                            countStr = self.count + "/" + self.total;

                        $('.qq-upload-drop-area span', me).html("Uploading " + countStr + " ...");

                        if (self.count == 0) {
                            if (skipSuccessAlert) {
                                me.parents('form').submit();
                            } else {
                                UI.showSuccess(box, "The file has been uploaded sucessfully.", function () {
                                    me.parents('form').submit();
                                });
                            }
                        }
                    } else {
                        UI.showError(box, responseJSON.error);
                    }
                }
            }
        });

        // store text so as not to lose it

        _.delay(function () {
            me.attr('data-text', $('.qq-upload-drop-area span', me).html());
        }, 500);
    },

    setupStaff: function (selector) {
        var box = $(selector);

        var val = $('#Username', selector).val();
        $('#Username', selector).val('');

        var selectize = $('#Username', selector).selectize({
            maxItems: 1,
            valueField: 'value',
            labelField: 'value',
            createOnBlur: true,
            openOnFocus: false,
            options: [{ value: val }],
            persist: false,
            searchField: ['value'],
            preload: true,
            create: true,
            render: {
                item: function (item, escape) {

                    if (!item.value)
                        return '';

                    var name = escape(item.value);

                    if (name && name.length) {
                        return '<div>' +
                            '<span><img class="small-face" src="' + MC3.CDN +'minecraftheads/' + name + '.png"/></span>' +
                            '<span class="name">' + name + '</span>' +
                            '</div>';
                    }
                }
            },
            load: function (query, callback) {
                if (query && query.length) {
                    $.post("/find/user", {
                        term: query
                    }, function (res) {
                        callback(res);
                        reSet();
                    });
                } else {
                    reSet();
                }
            }
        });

        var reSet = function () {
            if (val && val.length > 0) {
                selectize[0].selectize.setValue(val);
                val = "";
            }
        };

        $('.save', box).on('click', function (e) {
            e.preventDefault();

            var identifier = $('#Identifier', box).val();

            var posts = [];

            $('.person').each(function () {
                var username = $(this).attr('data-username');
                var quote = $('.quote input', this).val();
                var original = $('.quote input', this).attr('data-original');

                if (quote != original) {

                    UI.showLoader(box);

                    posts.push($.post('/update/quote', {
                        Identifier: identifier,
                        Username: username,
                        Quote: quote
                    }));
                }
            });

            $.when.apply($, posts).done(function (res) {
                UI.hideLoader(box);

                log(res);

                if ($.isArray(res))
                    res = res[0];

                if (res.success) {
                    UI.showSuccess(box, "Quotes were saved successfully.");
                } else {
                    UI.showError(box, res.error);
                }
            });
        });
    },

    setupModpackBox: function () {
        var selector = '#modpacks input.modpack';
        var box = $(selector).parents('.box');

        // set up autocomplete

        var val = $(selector).val();
        $(selector).val('');

        var selectize = $(selector).selectize({
            maxItems: 1,
            valueField: 'Name',
            labelField: 'Name',
            openOnFocus: false,
            persist: false,
            searchField: ['Name'],
            preload: true,
            createOnBlur: true,
            create: true,
            load: function (query, callback) {
                $.post("/find/modpack", {
                    term: query
                }, function (res) {
                    callback(res);
                    reSet();
                });
            },
            onItemAdd: function (value, $item) {
                $.post("/exists/modpack", {
                    identifier: value
                }, function (res) {
                    if (res.exists) {
                        $('.add-new', box).slideUp(function () {
                            UI.reLayout();
                        });
                    } else {
                        $('.add-new', box).slideDown(function () {
                            UI.reLayout();
                        });
                    }
                });
            }
        });

        var reSet = function () {
            if (val && val.length > 0) {
                selectize[0].selectize.disable();
                selectize[0].selectize.addOption(val, { Name: val });
                selectize[0].selectize.setValue(val);
                selectize[0].selectize.refreshOptions(false);
                selectize[0].selectize.enable();
                val = "";
            }
        };
    },


    setupMiniGameBox: function () {
        var selector = '#mini-games input.mini-game';
        var box = $(selector).parents('.box');

        // set up autocomplete

        var val = $(selector).val();
        $(selector).val('');

        var selectize = $(selector).selectize({
            maxItems: 1,
            valueField: 'Value',
            labelField: 'Name',
            openOnFocus: false,
            persist: false,
            searchField: ['Name'],
            preload: true,
            createOnBlur: true,
            create: true,
            load: function (query, callback) {
                $.post("/find/minigame", {
                    term: query
                }, function (res) {
                    callback(res);
                    reSet();
                });
            },
            onItemAdd: function (value, $item) {
                $('.add-new', box).slideDown(function () {
                    UI.reLayout();
                });
            }
        });

        var reSet = function () {
            if (val && val.length > 0) {
                selectize[0].selectize.disable();
                selectize[0].selectize.addOption(val, { Name: val });
                selectize[0].selectize.setValue(val);
                selectize[0].selectize.refreshOptions(false);
                selectize[0].selectize.enable();
                val = "";
            }
        };
    },

    setUpQueryForPlugin: function (box) {
        $('.query-for-plugin', box).unbind('click').on('click', function (e) {
            e.preventDefault();
            UI.showLoader(box);

            $.post("/editserver/QueryForPlugins", {
                Identifier: $('#Identifier', box).val(),
                Address: $('#Address', box).val(),
                Port: $('#Port', box).val()
            }, function (res) {
                log(res);
                if (res.success) {
                    UI.showSuccess(box, "The server was queried successfully.", function () {
                        location.reload();
                    });
                    UI.hideLoader(box);
                } else {
                    UI.showError(box, res.message);
                    UI.hideLoader(box);
                }
            });
        });
    },

    setUpQueryTest: function (box) {
        $('.query-test', box).unbind('click').on('click', function (e) {
            e.preventDefault();
            UI.showLoader(box);
            log('testing query');

            $.post("/editserver/QueryTest", {
                Identifier: $('#Identifier', box).val(),
                Address: $('#Address', box).val(),
                Port: $('#Port', box).val()
            }, function (res) {
                log(res);
                if (res.success) {
                    UI.showSuccess(box, "The server was queried successfully.", function () {
                    });
                    UI.hideLoader(box);
                } else {
                    UI.showError(box, res.message);
                    UI.hideLoader(box);
                }
            });
        });
    },

    setupVoteTest: function (box) {
        $('.vote-test', box).unbind('click').click(function (e) {
            UI.showLoader(box);
            log('testing vote');

            $.post("/editserver/VoteTest", {
                Identifier: $('#Identifier', box).val(),
                Address: $('#Address', box).val(),
                Port: $('#Port', box).val(),
                PublicKey: $('#PublicKey', box).val()
            }, function (res) {
                log(res);
                if (res.success) {
                    UI.showSuccess(box, "Connected to the server successfully.", function () {
                    });
                    UI.hideLoader(box);
                } else {
                    UI.showError(box, res.message);
                    UI.hideLoader(box);
                }
            });
        });
    },

    scrollBottomTab: function () {
        $(function () {
            var windowHeight = $(window).height();
            var height = $('html, body').height() - windowHeight;

            if (height < windowHeight / 2)
                return;

            var speed = height / 2;

            $('#scroll-bottom').on('click', function () {
                $('html, body').animate({ scrollTop: $(document).height() - $(window).height() }, speed, 'easeInOutQuart', function () {
                });
            }).show();
        });
    },

    scrollTopTab: function (buffer) {

        $(function () {
            $('#scroll-top').on('click', function () {
                $(this).fadeOut();
                var height = $('html, body').height() - $(window).height();
                var speed = height / 2;

                $('html, body').animate({ scrollTop: 0 }, speed, 'easeInOutQuart');
            });
        });

        var okToFadeOut;

        return new (function () {
            var $window = $(window);
            var visible = false;

            var worker = _.throttle(function () {

                if (visible) {
                    if ($window.scrollTop() < buffer) {
                        if ($('#scroll-top').is(':visible'))
                            $('#scroll-top').fadeOut();
                        visible = false;
                        okToFadeOut = false;
                    }
                } else {
                    if ($window.scrollTop() >= buffer) {
                        if (!$('#scroll-top').is(':visible')) {
                            $('#scroll-top').fadeIn();
                            visible = true;
                        }
                    }
                }
            }, 250);

            this.start = function () {
                $window.bind('scroll', worker);
                return this;
            };
        });
    },

    infiniteScroll: function (buffer) {
        return new (function () {
            var $window = $(window);
            var $document = $(document);
            var isDone = true;
            var callback = function () { };

            var worker = function () {
                if (isDone) {
                    var scrolledToBottom = $window.scrollTop() + $window.height() >= $document.height() - buffer;

                    if (scrolledToBottom) {
                        isDone = false;
                        callback();
                        setTimeout(function () {
                            isDone = true;
                        }, 1250);
                    }
                }
            };

            this.done = function (cb) {
                callback = cb;
                return this;
            },

            this.start = function () {
                setInterval(function () {
                    worker();
                }, 1000);
                return this;
            };
        });
    },

    positionFooter: _.debounce(function () {

        var bodyHeight = $('body').height();
        var windowHeight = window.innerHeight;

        if (bodyHeight >= windowHeight - 60) {
            $('footer').removeClass('absolute');
            log('removed');
        } else {
            $('footer').addClass('absolute');
            log('added');
        }
        $('footer').show();
    }, 250, { leading: false, trailing: true }),

    fixDropdowns: _.debounce(function () {
        var width = $(window).width();

        if (width > 1400) {
            $('#user-menu .dropdown > .dropdown-menu').removeClass('pull-right').addClass('pull-left');
        } else {
            $('#user-menu .dropdown > .dropdown-menu').removeClass('pull-left').addClass('pull-right');
        }
        if (width > 1800) {
            $('#user-menu .dropdown-submenu').removeClass('pull-left');
        } else {
            $('#user-menu .dropdown-submenu').addClass('pull-left');
        }

    }, 250, { 'trailing': true }),

    bindElasticBucket: function (parent) {
        $('.bucket', parent).add('img', parent).on('mouseenter', function () {
            var me = $('.bucket', $(this).parent());
            $('.nano', me).nanoScroller({ destroy: true });
            me.addClass('extended');
            _.delay(function () {
                $('.nano', me).nanoScroller({ preventPageScrolling: true });
            }, 600);
        }).on('mouseleave', function () {
            var me = $('.bucket', $(this).parent());
            $('.nano', me).nanoScroller({ destroy: true });
            me.removeClass('extended');
        });
    }
};


$(function () {
    // position footer correctly

    $(window).resize(UI.positionFooter);

    /// fix dropdown orientation

    UI.fixDropdowns();
    $(window).resize(UI.fixDropdowns);
    
    // refreshes all ui addons onto the current loaded elements

    UI.refresh();
});
