﻿var ping = function (id, ip, delay) {
    var deferred = $.Deferred();

    setTimeout(function () {
        var timeout = 15000;
        var start = new Date().getTime();
        var img = new Image();
        log("pinging: ", ip);
        img.onerror = function () {
            img = undefined;
            var end = new Date().getTime();
            log("ping result: %s", end - start);
            deferred.resolve([id, ip, (end - start)]);
        };
        img.onload = img.onerror;
        img.src = 'http://' + ip + "/" + new Date().getTime();
/*
        setTimeout(function () {
            img = undefined;
            log("ping result: timeout of %s exceeded", timeout);
            deferred.resolve([id, ip, Constant.FailedPing]);
        }, timeout);
*/
    }, delay || 0);
    
    return deferred.promise();
};