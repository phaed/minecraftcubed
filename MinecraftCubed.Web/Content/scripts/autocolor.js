﻿
function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

var RGBA = function (color) {

    this.r = 0;
    this.g = 0;
    this.b = 0;
    this.a = 0;

    var match;

    if (color.indexOf("rgba") > -1) {
        match = /rgba\((.*)\)/.exec(color);
        match = match[1].split(',');
        this.r = match[0];
        this.g = match[1];
        this.b = match[2];
        this.a = match[3];
    } else if (color.indexOf("rgb") > -1) {
        match = /rgb\((\d+).*?(\d+).*?(\d+)\)/.exec(color);
        this.r = match[1];
        this.g = match[2];
        this.b = match[3];
        this.a = 1;
    } else if (color.indexOf("#") > -1) {
        var rgb = hexToRgb(color);
        this.r = rgb.r;
        this.g = rgb.g;
        this.b = rgb.b;
        this.a = 1;
    }
};

function RGBAtoRGB(rgba, rgba2) {
    var r3 = Math.round(((1 - rgba.a) * rgba2.r) + (rgba.a * rgba.r));
    var g3 = Math.round(((1 - rgba.a) * rgba2.g) + (rgba.a * rgba.g));
    var b3 = Math.round(((1 - rgba.a) * rgba2.b) + (rgba.a * rgba.b));
    return {
        r: r3,
        g: g3,
        b: b3
    };
}

function isColorDark(color, bgColor) {

    if (typeof color === 'undefined') {
        color = 'rgba(0, 0, 0, 0)';
    }

    if (typeof bgColor === 'undefined') {
        bgColor = 'rgba(0, 0, 0, 0)';
    }

    var rgb = new RGBA(color);
    var rbgaBg = new RGBA(bgColor);

    if (rgb.a < 1) {
        rgb = RGBAtoRGB(rgb, rbgaBg);
        //log('autocolor: flattening transparency > %o', rgb);
    }


    // r+g+b should be less than half of max (3 * 256)

    return parseFloat(rgb.r) + parseFloat(rgb.g) + parseFloat(rgb.b) < 3 * 256 / 2;
}

// gets the inherited background of an elements

function getBackground(element) {
    // Is current element's background color set?
    var color = $(element).css("background-color");

    if (color !== 'rgba(0, 0, 0, 0)') {
        // if so then return that color
        return color;
    }

    // if not: are you at the body element?
    if ($(element).is("body")) {
        // return known 'false' value
        return false;
    } else {
        // call getBackground with parent item
        return getBackground($(element).parent());
    }
}

AutoColor = new function () {

    var autocolor = function () {
        var me = $(this);
        var parent = me.parent();
        
        var color = me.css("background-color");
        
        while (color == 'rgba(0, 0, 0, 0)' && $(me).is("body") == false) {
            me = parent;
            parent = me.parent();
            color = me.css("background-color");
        }

        var bgColor = getBackground(parent);
        var isDark = isColorDark(color, bgColor);

        var attr = $(this).attr('data-light') || $(this).attr('data-dark');

        if (attr && attr.length) {
            var light = $(this).attr('data-light') || "";
            var dark = $(this).attr('data-dark') || "";
            $(this).css('color', isDark ? dark : light);

            //log('color:', color, 'bgcolor:', bgColor, 'output:', isDark);
            //log('final:', isDark ? dark : light);
        }

        $(this).find('[data-light], [data-dark]').each(function () {
            light = $(this).attr('data-light') || "";
            dark = $(this).attr('data-dark') || "";
            $(this).css('color', isDark ? dark : light);

            //log('color:', color, 'bgcolor:', bgColor, 'output:', isDark);
            //log('final:', isDark ? dark : light);
        });

        attr = $(this).attr('data-light-bg') || $(this).attr('data-dark-bg');

        if ($(this).attr('data-light-bg') || $(this).attr('data-dark-bg')) {
            light = $(this).attr('data-light-bg') || "";
            dark = $(this).attr('data-dark-bg') || "";
            $(this).css('background-color', isDark ? dark : light);

            //log('color:', color, 'bgcolor:', bgColor, 'output:', isDark);
            //log('final:', isDark ? dark : light);
        }

        $(this).find('[data-light-bg], [data-dark-bg]').each(function () {
            light = $(this).attr('data-light-bg') || "";
            dark = $(this).attr('data-dark-bg') || "";
            $(this).css('background-color', isDark ? dark : light);

            //log('color:', color, 'bgcolor:', bgColor, 'output:', isDark);
            //log('final:', isDark ? dark : light);
        });
    };

    return {
        refresh: _.debounce(function (elem) {
            if (!elem)
                elem = 'body';

            log('autocolor: ', elem);

            // automatically color text based on bg
            $('.autocolor', elem).each(autocolor);

            // bind hover
            $('.autocolorhover', elem).unbind('mouseenter').mouseenter(function () {
                $(this).addClass('hover');
                autocolor.call($(this));
            }).unbind('mouseleave').mouseleave(function () {
                $(this).removeClass('hover');
                autocolor.call($(this));
            });
        }, 150, { leading: true, trailing: true })
    };
};

$(function () {
    AutoColor.refresh();
});