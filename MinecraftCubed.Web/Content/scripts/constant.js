﻿var Constant = {
    SitePageScrollTabLevel: 650,
    ServerListTake: 40,
    ServerListTakeLevel: 300,
    ServerListScrollTabLevel: 300,
    EventListTake: 100,
    EventListTakeLevel: 450,
    EventListScrollTabLevel: 650,
    CommunityScrollTabLevel: 300,
    FilterHeight: 288,
    CardLoadDelay: 50,
    CardToCardDelay: 75,
    DefaultPing: '<i class="fa fa-refresh"></i>',
    FailedPing: '<i class="fa fa-times"></i>',
    
    Info: {
        Organic: 'This is a list of all the matching servers, sorted by rank.  You can re-sort this list by a number of metrics.',
        Featured: 'This is a list of all the matching featured servers, sorted by their featured rank.  This is a subset of the main list containing featured servers currently promoting their community.',
        UserNav: 'This is your user menu',
        LoginButton: 'Click here to sign in or create an account.',
        Tags: 'These are categories of tags you can filter by.  You can select multiple tags to narrow down your search.',
        Event: 'These are events',
    },

    Url: 'http://' + location.host + (location.port ? ':' + location.port : '') + '/',
    SecureUrl: 'https://' + location.host + (location.port ? ':' + location.port : '') + '/'
}