﻿
var ListEvent = function (data) {
    data = data || {};
    var self = this;

    this.Slider = {};

    this.EventDateId = data.Id;
    this.EventUrl = '/event/' + data.EI;
    this.Name = data.N;
    this.Version = data.V;
    this.Description = data.D;
    this.BannerUrl = data.B + "?v=" +data.BV;

    this.ServerUrl = '/server/' + data.SI;
    this.ServerName = '@' + data.SN;
    this.StartDate = data.SD;
    this.EndDate = data.ED;
    this.LengthInHours = data.L;
    this.HasDownload = data.HD;
    this.DownloadLabel = data.DL;
    this.DownloadUrl = data.DU;
    this.HasWhitelist = data.HW;
    this.WhitelistUrl = data.WU;
    this.HasRules = data.HR;
    this.RulesUrl = data.RU;
    this.HasAttendeeWhitelist = data.HAW;

    this.CountryAbbreviation = data.CA;
    this.CountryName = data.CN;
    this.Language = data.LA;
    this.CountryTip = data.CN + " (" + data.LA + ")";

    this.FullIP = data.AD + ":" + data.P;

    var today = moment();
    var startDate = moment(data.SD);

    var daysDiff = startDate.diff(today, 'days');
    var hourDiff = startDate.diff(today, 'hours');

    this.FormattedDate = startDate.calendar();

    if (today.isAfter(startDate)) {
        this.Started = true;
        this.Soon = false;
        this.Today = false;
        this.ThisWeek = false;
        this.Future = false;
        this.ColorClass = "started";
    } else if (hourDiff < 1) {
        this.Started = false;
        this.Soon = true;
        this.Today = false;
        this.ThisWeek = false;
        this.Future = false;
        this.ColorClass = "soon";
    } else if (daysDiff < 1) {
        this.Started = false;
        this.Soon = false;
        this.Today = true;
        this.ThisWeek = false;
        this.Future = false;
        this.ColorClass = "today";
    } else if (daysDiff < 6) {
        this.Started = false;
        this.Soon = false;
        this.Today = false;
        this.ThisWeek = true;
        this.Future = false;
        this.ColorClass = "thisweek";
    } else {
        this.Started = false;
        this.Soon = false;
        this.Today = false;
        this.ThisWeek = false;
        this.Future = true;
        this.ColorClass = "future";
    }

    var attendees = [];

    if (data.A.length > 0) {
        attendees = _.map(data.A.split(','), function (player) {
            return new Attendee(player);
        });
    }

    this.Attendees = ko.observableArray(attendees);
    
    this.AddPlayer = function (player) {
        self.Attendees.push(new Attendee(player));
    }

    this.RemovePlayer = function (player) {
        self.Attendees.remove(function (item) { return item.Name == player; });
    }

    this.IsAttending = ko.computed(function () {
        if (MC3.CurrentUsername.length == 0)
            return false;

        return typeof _.find(self.Attendees(), { 'Name': MC3.CurrentUsername }) !== 'undefined';
    });
};

var Attendee = function (name) {
    this.Name = name;
    this.HeadUrl = MC3.CDN + "minecraftheads/" + name + ".png";
}