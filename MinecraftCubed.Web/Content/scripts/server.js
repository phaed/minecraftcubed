﻿
// **************************************************************** MODELS

var Player = function (username) {

    this.Username = username;
    this.CleanName = username;
    this.IsAdmin = false;
    this.IsManager = false;
    this.IsStaff = false;
    this.Rank = '';
    this.ColorStyle = '';

    var index = username.indexOf("-");
    if (index > -1) {
        this.CleanName = username.substring(0, index);
    }

    var data = _.find(model.PlayerDatas, { 'username': username });

    if (!_.isEmpty(data)) {
        this.IsAdmin = data.isAdmin;
        this.IsManager = data.isManager;
        this.IsStaff = data.isStaff;
        this.Rank = data.rank;

        this.ColorStyle = '';

        if (data.isAdmin || data.isManager || data.isStaff) {
            this.ColorStyle = 'color: ' + data.color;
        }
    }

    this.AvatarUrl = function () {
        if (this.CleanName !== this.Username || this.Username === 'anon') {
            return MC3.CDN + 'minecraftheads/Steve.png';
        }

        return MC3.CDN + 'minecraftheads/' + this.Username + '.png';
    };
};

var Line = function (data, username) {
    this.Id = data.id;
    this.LineId = "line" + this.Id;
    this.Message = data.message;
    this.Time = data.time;

    var playerData = _.find(model.PlayerDatas, { 'username': data.username });

    if (_.isEmpty(playerData)) {
        model.PlayerDatas.push(data);
    }

    this.OwnPost = data.username === username;
    this.Player = new Player(data.username);
};

var ServerModel = function (serverData) {
    var self = this;
    var playersLoaded = false;

    this.ServerId = serverData.ServerId;
    this.Color = serverData.Color;
    this.IsAdmin = serverData.IsAdmin;
    this.IsManager = serverData.IsManager;
    this.IsStaff = serverData.IsStaff;

    this.Players = ko.observableArray([]);
    this.Version = ko.observable(serverData.Version);
    this.PlayerCount = ko.observable($('.player-count').html());

    this.ServerTypes = ko.observableArray(serverData.ServerTypes);
    this.Gameplay = ko.observableArray(serverData.Gameplay);
    this.Plugins = ko.observableArray(serverData.Plugins);
    this.MiniGames = ko.observableArray(serverData.MiniGames);
    this.Modpacks = ko.observableArray(serverData.Modpacks);

    this.HasDual = serverData.Modpacks.length > 0 && serverData.Modpacks.length <= 2 && serverData.MiniGames.length > 0 && serverData.MiniGames.length <= 2;
    this.HasModpackTight = serverData.Modpacks.length > 0 && serverData.Modpacks.length <= 1;
    this.HasMiniGameTight = serverData.MiniGames.length > 0 && serverData.MiniGames.length <= 1;

    this.PlayerDatas = [];

    var s = _.map(serverData.Staff, function (staff) {
        staff.ColorStyle = 'color: ' + staff.Color;
        return staff;
    });

    this.Staff = ko.observableArray(s);

    this.Init = function () {

        // set up the sparklines

        /*
        // players count
        $('.sparkline .count').sparkline('html', {
            type: 'line',
            width: '70px',
            height: '20px',
            lineColor: '#e0e0e0',
            fillColor: 'rgba(255,255,255,.2)',
            lineWidth: 3,
            spotColor: '#ddd',
            tooltipSuffix: ' players',
            minSpotColor: 'transparent',
            maxSpotColor: 'transparent',
            highlightSpotColor: '#ff0000',
            tooltipFormatter: function (sparklines, options, fields) {
                return '<div class="inner-tip">' + (fields.y) + ' players</div>';
            },
            tooltipClassname: 'jqstooltip qtip-default qtip-light qtip-focus',
            highlightLineColor: 'transparent'
        });

        // uptime
        $('.sparkline-bar .count').sparkline('html', {
            type: 'bar',
            barColor: '#e0e0e0',
            width: '34px',
            height: '16px',
            chartRangeMin: 0,
            chartRangeMax: 100,
            tooltipFormatter: function (sparklines, options, fields) {
                return '<div class="inner-tip">Uptime: ' + (fields[0].value) + '%</div>';
            },
            tooltipClassname: 'jqstooltip qtip-default qtip-light qtip-focus',
            barWidth: 2,
            barSpacing: 2,
            tooltipSuffix: '%'
        });

        // change activity knob colors 
        $('.activity-input')
            .attr('data-fgcolor', 'rgba(255,255,255,.92)')
            .attr('data-bgcolor', 'rgba(255,255,255,.2)');
        */

        if ($('.server-container').hasClass('dark')) {
            // dark
            $('.sparkline .count').sparkline('html', {
                type: 'line',
                width: '208px',
                height: '40px',
                lineColor: '#e0e0e0',
                fillColor: 'rgba(255,255,255,.2)',
                lineWidth: 3,
                spotColor: '#ddd',
                tooltipSuffix: ' players',
                minSpotColor: 'transparent',
                maxSpotColor: 'transparent',
                highlightSpotColor: '#ff0000',
                tooltipFormatter: function (sparklines, options, fields) {
                    return '<div class="inner-tip">' + (fields.y) + ' players</div>';
                },
                tooltipClassname: 'jqstooltip qtip-default qtip-light qtip-focus',
                highlightLineColor: 'transparent'
            });
        } else {
            $('.sparkline .count').sparkline('html', {
                type: 'line',
                width: '208px',
                height: '40px',
                lineColor: '#333',
                fillColor: 'rgba(0,0, 0,.03)',
                lineWidth: 3,
                spotColor: '#333',
                tooltipSuffix: ' players',
                minSpotColor: 'transparent',
                maxSpotColor: 'transparent',
                highlightSpotColor: '#ff0000',
                tooltipFormatter: function (sparklines, options, fields) {
                    return '<div class="inner-tip">' + (fields.y) + ' players</div>';
                },
                tooltipClassname: 'jqstooltip qtip-default qtip-light qtip-focus',
                highlightLineColor: 'transparent'
            });
        }

        if ($('.server-container').hasClass('dark')) {
            // dark
            $('.sparkline-bar .count').sparkline('html', {
                type: 'bar',
                barColor: '#e0e0e0',
                width: '208px',
                height: '40px',
                chartRangeMin: 0,
                chartRangeMax: 100,
                tooltipFormatter: function (sparklines, options, fields) {
                    return '<div class="inner-tip">Uptime: ' + (fields[0].value) + '%</div>';
                },
                tooltipClassname: 'jqstooltip qtip-default qtip-light qtip-focus',
                barWidth: 5,
                barSpacing: 2,
                tooltipSuffix: '%'
            });
        } else {
            $('.sparkline-bar .count').sparkline('html', {
                type: 'bar',
                barColor: '#333',
                width: '208px',
                height: '40px',
                chartRangeMin: 0,
                chartRangeMax: 100,
                tooltipFormatter: function (sparklines, options, fields) {
                    return '<div class="inner-tip">Uptime: ' + (fields[0].value) + '%</div>';
                },
                tooltipClassname: 'jqstooltip qtip-default qtip-light qtip-focus',
                barWidth: 5,
                barSpacing: 2,
                tooltipSuffix: '%'
            });
        }

        // change activity knob colors if dark

        if ($('.server-container').hasClass('dark')) {
            //dark
            $('.activity-input')
                .attr('data-fgcolor', 'rgba(255,255,255,.92)')
                .attr('data-bgcolor', 'rgba(255,255,255,.05)');
        }

        // set up knobs

        var opts = {
            width: "60",
            height: "60",
            fgColor: "#333333",
            bgColor: "rgba(0, 0, 0, 0.03)",
            skin: "tron",
            thickness: ".3",
            angleOffset: "180",
            readOnly: true
        };

        if ($('.server-container').hasClass('dark')) {
            opts.fgColor = "#ffffff";
            opts.bgColor = "rgba(255, 255, 255, 0.1)";
        }

        $(".knob input").knob(opts);

        // big qtips

        $('.bigtip').each(function () {
            $(this).qtip({
                position: {
                    my: 'bottom center',
                    at: 'top center'
                },
                style: {
                    classes: 'qtip-light'
                },
                content: {
                    text: $(this).next('.tooltiptext')
                }
            });
        });

        // select ip when clicked

        $('.ip, .ip-wrapper .title').click(function () {
            SelectText($(this).parent().find('.ip'));
        });

        $('.sub-ip .select').click(function () {
            SelectText($(this));
        });

        // fix chat width as window resizes

        $(window).bind('resize', function () {
            self.FixChat();
        });

        setTimeout(function () {
            self.FixChat();
        }, 1000);

        // bind ping button

        $('#ping-button').click(function () {
            $('#pingms').addClass('dark');
            self.PingServer().done(function () {
                $('#pingms').removeClass('dark');
            });
        });

        // bins subserver toggle

        $('.subserver-toggle').click(function (e) {
            var me = this;
            var well = $('.servers-well');

            if (well.is(':visible')) {
                well.slideUp(function () {
                    $('.fa', me).removeClass('fa-caret-up').addClass('fa-caret-down');
                });
            } else {
                well.slideDown(function () {
                    $('.fa', me).removeClass('fa-caret-down').addClass('fa-caret-up');
                });
            }

            e.preventDefault();
        });

        // add initial chat lines

        self.Chat.AddInitial();

        // init pinger

        //if (self.InitialPing === Constant.DefaultPing || self.InitialPing === Constant.FailedPing) {
        //    setTimeout(self.PingServer, 1500);
        //    setTimeout(self.PingServer, 10000);
        //}

        // load scroll tabs

        UI.scrollTopTab(Constant.ServerListScrollTabLevel).start();
        UI.scrollBottomTab();

        // set up local time

        var m = moment($('time').html());

        if (m) {
            $('time').html(m.format("ha"));
        } else {
            $('time').html('-');
        }

        // hide arrows on start

        $('.fotorama__wrap').addClass('fotorama__wrap--no-controls');

        // start chat
        
        $('.chat-section').bind('inview', function (event, isInView) {
            if (isInView) {
                self.Chat.InitChat();
            }
        });
     
        // preload quote image

        $(['/content/images/bg-quote.png']).preload();

        // show carousel navbar shadow

        _.delay(function () {
            $('.fotorama__nav-wrap').fadeTo("slow", 1);
        }, 250);
    };


    var ms = $.jStorage.get('ping-history', [])[serverData.ServerId] || Constant.DefaultPing;

    if (ms !== Constant.DefaultPing && ms !== Constant.FailedPing) {
        ms += '<small>ms</small>';
    }

    this.InitialPing = ms;

    this.AfterPluginsDone = function () {

        // add tooltip

        $('.tip', '.plugins-section').each(function () {

            $(this).qtip({
                position: {
                    my: 'bottom center',
                    at: 'top center'
                },
                style: {
                    classes: 'qtip-light'
                },
                content: {
                    text: $(this).next('.tooltiptext')
                }
            });
        });

        // autocolor

        AutoColor.refresh();
    };

    this.AfterMiniGamesDone = function () {

        // elastic bucket 

        UI.bindElasticBucket('.mini-games-section', true);

        // account for bigger than normal header

        $('.elastic', '.mini-games-section').each(function () {
            var header = $('h3', this);
            var desc = $('.desc', this);

            if (header.height() > 24) {
                desc.addClass('small');
            }
        });

        // autocolor

        AutoColor.refresh();
    };

    this.AfterModpacksDone = function () {

        // elastic bucket 

        UI.bindElasticBucket('.modpack-section', true);

        // account for bigger than normal header

        $('.elastic', '.modpack-section').each(function () {
            var header = $('h3', this);
            var desc = $('.desc', this);

            if (header.height() > 24) {
                desc.addClass('small');
            }
        });

        // autocolor

        AutoColor.refresh();
    };

    this.AfterStaffDone = function () {
        $('.staff').each(function () {
            if (!$('.tooltiptext', this).length)
                return;

            $(this).qtip({
                position: {
                    my: 'bottom center',
                    at: 'top center'
                },
                style: {
                    classes: 'qtip-light'
                },
                content: {
                    text: $('.tooltiptext', this)
                }
            }).addClass('pointer');
        });

        $('.staff .name').textfill({ maxFontPixels: 18 });
        $('.staff .rank').textfill({ maxFontPixels: 13 });

        // autocolor

        AutoColor.refresh();
    };

    this.AfterPlayerAdd = function (elem) {
        if (elem.nodeType === 1) {
            // fade in
            $('img', elem).addClass('animated fadeInUp');

            // add tooltip
            $('img', elem).qtip({
                position: {
                    my: 'bottom center',
                    at: 'top center'
                },
                style: {
                    classes: 'qtip-light'
                }
            });
        }
    };

    this.BeforePlayerRemove = function (elem) {
        if (elem.nodeType === 1) {
            // fade out
            $('img', elem).addClass('animated fadeOutDown');
            // remove
            _.delay(function () {
                $(elem).remove();
            }, 350);
        }
    };

    var showRails = _.debounce(function () {
        $('.nicescroll-rails').show();
    }, 1500);

    var flushChat = _.debounce(function (elem) {

        // flush to bottom
        $('.chat').scrollTop($('.chat')[0].scrollHeight);

        // fix width
        if (elem) {
            log('chat fixed');
            $(elem).css('width', ($('.chat').width() - 10) + "px");
        } else {
            $('.line').css('width', ($('.chat').width() - 10) + "px");
        }

    }, 250, { leading: true, trailing: true });

    this.FixChat = _.debounce(function (elem) {
        if (elem) {
            log('chat fixed');
            $(elem).css('width', ($('.chat').width() - 10) + "px");
        } else {
            $('.line').css('width', ($('.chat').width() - 10) + "px");
        }
    }, 250, { leading: true, trailing: true });

    this.AfterAddLine = function (elem) {
        if (elem.nodeType === 1) {

            // bind delete button

            $(".delete", elem).click(function () {
                var id = $(this).attr('data-id');
                self.Chat.hub.server.delete(id);
                log('deleting %O', id);
            });

            if (playersLoaded) {
                $(elem).addClass('animated flipInX');
                $(".chat").getNiceScroll().resize();
            }

            // hide scrollbar so it doesnt look ugly flashing

            $('.nicescroll-rails').hide();

            // show scrollbar when needed

            if ($('.chat').children().length > 7) {
                showRails();
            }

            // init timestamp

            $('time', elem).livestamp($('time', elem).attr('data-time'));

            // add player rank qtip

            $('.head', elem).qtip({
                position: {
                    my: 'center right',
                    at: 'center left'
                },
                style: {
                    classes: 'qtip-light'
                }
            });

            flushChat(elem);
            self.FixChat();
        }
    };

    this.AfterLinesDone = function (elem) {
        if (elem.nodeType === 1) {
            if ($('.chat').children().length >= serverData.InitialChat.length) {

                $(".chat").getNiceScroll().resize();
                self.FixChat(elem);
            }
        }
    };

    this.PingServer = function () {
        var address = serverData.IP + ":" + serverData.Port;
        var deferred = $.Deferred();
        var id = 1;

        var postPing = function (res) {
            var ms = res[2];

            // store

            if (ms !== Constant.FailedPing) {
                var h = $.jStorage.get('ping-history', []);
                h[self.ServerId] = ms;
                $.jStorage.set('ping-history', h);
            }

            // show

            if (ms !== Constant.DefaultPing && ms !== Constant.FailedPing) {
                ms += '<small>ms</small>';
            }

            $('#pingms').html(ms);
        };

        var a = ping(id, address).done(postPing);
        var b = ping(id, address, 250).done(postPing);
        var c = ping(id, address, 500).done(postPing);
        var d = ping(id, address, 750).done(postPing);
        var e = ping(id, address, 1000).done(postPing);
        var f = ping(id, address, 1000).done(postPing);
        var g = ping(id, address, 1200).done(postPing);
        var h = ping(id, address, 1300).done(postPing);
        var i = ping(id, address, 1400).done(postPing);
        var j = ping(id, address, 1500).done(postPing);

        $.when(a, b, c, d, e, f, g, h, i, j).done(function (ra, rb, rc, rd, re, rf, rg, rh, ri, rj) {

            var lowest = 999999;

            if ($.isNumeric(ra[2])) {
                if (parseInt(ra[2]) < lowest)
                    lowest = parseInt(ra[2]);
            }
            if ($.isNumeric(rb[2])) {
                if (parseInt(rb[2]) < lowest)
                    lowest = parseInt(rb[2]);
            }
            if ($.isNumeric(rc[2])) {
                if (parseInt(rc[2]) < lowest)
                    lowest = parseInt(rc[2]);
            }
            if ($.isNumeric(rd[2])) {
                if (parseInt(rd[2]) < lowest)
                    lowest = parseInt(rd[2]);
            }
            if ($.isNumeric(re[2])) {
                if (parseInt(re[2]) < lowest)
                    lowest = parseInt(re[2]);
            }
            if ($.isNumeric(rf[2])) {
                if (parseInt(rf[2]) < lowest)
                    lowest = parseInt(rf[2]);
            }
            if ($.isNumeric(rg[2])) {
                if (parseInt(rg[2]) < lowest)
                    lowest = parseInt(rg[2]);
            }
            if ($.isNumeric(rh[2])) {
                if (parseInt(rh[2]) < lowest)
                    lowest = parseInt(rh[2]);
            }
            if ($.isNumeric(ri[2])) {
                if (parseInt(ri[2]) < lowest)
                    lowest = parseInt(ri[2]);
            }
            if ($.isNumeric(rj[2])) {
                if (parseInt(rj[2]) < lowest)
                    lowest = parseInt(rj[2]);
            }

            if (lowest === 999999) {
                lowest = Constant.FailedPing;
            }

            postPing([ra[0], '', lowest]);
            deferred.resolve();
        });

        return deferred.promise();
    };

    this.Chat = new function () {
        var me = this;
        this.hub = $.connection.serverHub;
        this.hub.logging = false;
        
        this.Lines = ko.observableArray([]);

        // add in default chat content

        var addMessage = function (res) {

            if (res.length === 4) {
                res.push(new Date().toISOString());
            }

            me.Lines.push(new Line({
                id: res[0],
                username: res[1],
                isAdmin: res[2] === 1,
                isManager: res[3] === 1,
                isStaff: res[4] === 1,
                color: res[5],
                rank: res[6],
                message: res[7],
                time: res[8]
            }, serverData.Username));
        };

        this.AddInitial = function () {
            _.each(serverData.InitialChat, function (res) {
                addMessage(res);
            });
        };

        $.connection.hub.disconnected(function () {
            setTimeout(function () {
                $.connection.hub.start().done(function () {
                    log('reconnecting hub...');
                });
            }, 5000);
            log('hub disconnected');
        });
        
        me.hub.client.newMessage = function (res) {
            addMessage(res);
            log('hub line received');
        };

        me.hub.client.deleteMessage = function (id) {
            $('#line' + id).addClass('animated fadeOutRight');
            setTimeout(function () {
                me.Lines.remove(function (line) { return line.Id === id; });
            }, 750);
            log('hub delete line');
        };

        me.hub.client.userConnected = function (username) {
            log('player connected');

            var contains = _.first(self.Players(), { 'Username': username });

            if (contains.length === 0) {
                self.Players.push(new Player(username));
                log('player added ' + username);
            }
        };

        me.hub.client.userDisconnected = function (username) {
            log('player disconnected');

            self.Players.remove(function (player) { return player.Username === username; });
            self.Players.valueHasMutated();

            log('player removed ' + username);
        };

        me.hub.client.existingUsers = function (users) {
            log('loading existing users %O', users);

            self.Players.removeAll();

            $.each(users, function (i, username) {
                var contains = _.first(self.Players(), { 'Username': username });

                if (contains.length === 0) {
                    self.Players.push(new Player(username));
                    log('player loaded ' + username);
                }
            });
            playersLoaded = true;
        };

        me.hub.state.Identifier = serverData.Identifier;

        var isInit = false;
        this.InitChat = _.once(function () {

            if (isInit)
                return;

            isInit = true;

            log('init chat');

            $.connection.hub.qs = "identifier=" + serverData.Identifier;
            //$.connection.hub.logging = true;
            $.connection.hub.start().done(function () {
                log("Connected, transport = " + $.connection.hub.transport.name);

                // bind chat input and show it

                $('#chat-input').keypress(function (e) {
                    if (e.which === 13) {
                        if ($(this).val().trim().length > 0) {
                            log('sending line', $(this).val());
                            $.connection.serverHub.server.send($(this).val());
                            $(this).val('');
                        }
                    }
                });

                $('#chat-input').fadeIn();
            }).fail(function () { log('Could not connect'); });
        });

        // hide the chat input

        $('#chat-input').hide();

        // chat input hide text on click

        $('#chat-input').attr('data-placeholder', $('#chat-input').attr('placeholder')).click(function () {
            $(this).attr('placeholder', '');
        }).blur(function () {
            $(this).attr('placeholder', $(this).attr('data-placeholder'));
        });

        // pretty scroller

        var opts = {
            bouncescroll: true,
            horizrailenabled: false
        };

        if (isMobile) {
            opts.touchbehavior = true;
        }

        $(".chat").niceScroll(opts);

        // set up fotorama

        $('.fotorama')
            .on('fotorama:loadvideo',
                function (e, fotorama) {
                    fotorama.resize({
                        ratio: '16/9',
                        time: 1000
                    });
                }
            )
            .on('fotorama:unloadvideo',
                function (e, fotorama) {
                    fotorama.resize({
                        ratio: '16/6',
                        time: 1000
                    });
                }
            )
            .fotorama();

    }();
};

ServerPreInit = function () {

    // load rest of page

    $.post(Url($('h1').attr('data-identifier'), 'serverdata')).done(function (result) {

        log(result);

        $(function () {
            $('.page-container').show();

            UI.refresh();
            UI.positionFooter();

            $('#loading').removeClass('spinner').hide();

            // init model

            window.model = new ServerModel(result);
            ko.applyBindings(model);

            model.Init();
        });
    });

};