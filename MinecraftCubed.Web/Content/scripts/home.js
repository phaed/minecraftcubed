// **************************************************************** EVENTS MODEL

var HomeModel = function () {
    var self = this;
    
    this.Filtered = ko.observable();
    this.Events = ko.observableArray();
    
    var heightFix = function (elem) {

        var height = Math.max($('.banner', elem).height() + $('.body', elem).height(), 380);

        $(elem).css('height', height);
    };


    this.AfterAddEvent = function (elem, index, listEvent) {
        
        var anonymous = MC3.CurrentUsername.length == 0;

        if (elem.nodeType === 1) {
            log('adding recent event', listEvent);

            // init livestamps

            $('.started', elem).livestamp(listEvent.StartDate);
            $('.soon', elem).livestamp(listEvent.StartDate);
            $('.ends', elem).livestamp(listEvent.EndDate);

            // resize the timebar initially

            var doResize = function (el) {
                var timeWidth = $('.time', el).width();
                var total = timeWidth + 40; // width of clock icon + 10
                $('.timebar', el).width(total);
            };

            doResize(elem);

            // do resize when the livestamp changes

            $('.time', elem).on('change.livestamp', function (e, from, to) {
                _.delay(function () {
                    doResize(elem);
                    log('change based timebar resize');
                }, 10);
            });

            // add tooltip

            $('.tip', elem).qtip({
                position: {
                    my: 'bottom center',
                    at: 'top center'
                },
                style: {
                    classes: 'qtip-light'
                }
            });

            // prevent flip when using UI

            $('.ip, .btn, .attend', elem).click(function (e) {
                e.stopPropagation();
            });

            // click on ip selects text

            $('.ip', elem).click(function (e) {
                SelectText($('.ip', elem));
            });

            // load checkbox and other UI

            UI.refresh(elem);

            // prevent flip when clicking on server link

            $('.server', elem).click(function (e) {
                e.stopPropagation();
            });

            // bind hover of carousel arrows

            $('.banner', elem).on('mouseenter', function () {
                $('.bx-wrapper .bx-pager', elem).addClass('showing');
                $('.bx-wrapper .bx-controls-direction a', elem).addClass('showing');
            }).on('mouseleave', function () {
                $('.bx-wrapper .bx-pager', elem).removeClass('showing');
                if (!isMobile) {
                    $('.bx-wrapper .bx-controls-direction a', elem).removeClass('showing');
                }
            });

            if (isMobile) {
                $('.bx-wrapper .bx-controls-direction a', elem).addClass('showing');
            }

            // clean up elements when mouseleaving the card

            $('.banner-wrapper', elem).on('mouseleave', function () {
                _.delay(function () {
                    $('.timebar', elem).removeClass('small');
                }, 500);
                $('.bx-pager', elem).removeClass('showing');
            });

            // bind scrollbars

            $('.nano', elem).nanoScroller({ preventPageScrolling: true });

            // check checkbox if attending event

            if (!anonymous) {
                if (_.find(listEvent.Attendees(), { 'Name': MC3.CurrentUsername })) {
                    $('input.checkbox', elem).iCheck('check');

                    // show requirements

                    $('.requirements', elem).slideDown();
                    $('.attend', elem).addClass('on');
                    $('.attendee-whitelist', elem).hide();
                    $('.attendee-whitelist-enabled', elem).show();
                    $(elem).addClass('attending');
                }
            }

            // bind attending checkbox

            $('input.checkbox', elem).on('ifChecked', function (e) {
                if (anonymous) {
                    _.delay(function () {
                        $('input.checkbox', elem).iCheck('uncheck');
                    }, 500);
                    Auth.ShowLogin();
                    return;
                }

                self.AddAsAttendee(listEvent.EventDateId);
                listEvent.AddPlayer(MC3.CurrentUsername);
                $('.nano', elem).nanoScroller({ preventPageScrolling: true });
                heightFix(elem);

                log("added", MC3.CurrentUsername, "as attendee for eventdate:", listEvent.EventDateId);

                // slide down requirements

                $('.requirements', elem).slideDown();
                $('.attend', elem).addClass('on');
                $('.attendee-whitelist', elem).hide();
                $('.attendee-whitelist-enabled', elem).show();
                $(elem).addClass('attending');

                // re-render the tooltips
                $('.tip', elem).qtip({
                    position: {
                        my: 'bottom center',
                        at: 'top center'
                    },
                    style: {
                        classes: 'qtip-light'
                    }
                });

                // hack sets the height of the event box manually 

                _.delay(function () {
                   heightFix(elem);
                }, 100);
            });

            $('input.checkbox', elem).on('ifUnchecked', function (e) {
                if (anonymous) {
                    _.delay(function () {
                        //$('input.checkbox', elem).iCheck('uncheck');
                    }, 250);
                    Auth.ShowLogin();
                    return;
                }

                // slide up requirements

                $('.requirements', elem).slideUp();
                $('.attend', elem).removeClass('on');
                $('.attendee-whitelist', elem).show();
                $('.attendee-whitelist-enabled', elem).hide();
                $(elem).removeClass('attending');


                self.RemoveAsAttendee(listEvent.EventDateId);
                listEvent.RemovePlayer(MC3.CurrentUsername);
                $('.nano', elem).nanoScroller({ preventPageScrolling: true });
                heightFix(elem);

                log("removed", MC3.CurrentUsername, "as attendee for eventdate:", listEvent.EventDateId);

                // re-render the tooltips
                $('.tip', elem).qtip({
                    position: {
                        my: 'bottom center',
                        at: 'top center'
                    },
                    style: {
                        classes: 'qtip-light'
                    }
                });

                // hack sets the height of the event box manually 
               heightFix(elem);
            });

            // flip

            log('flipping');

            _.delay(function () {
                // hack sets the height of the event box manually 
               heightFix(elem);

                $(elem).removeClass('shadowed');
                $(elem).addClass('flip');

                $(elem).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function (e) {
                    $(elem).addClass('shadowed');
                    $('.front', elem).removeClass('hidden');
                    log('end transition');
                    // fix footer
                    UI.positionFooter();
                });
            }, Constant.CardLoadDelay * index);

            // bind card press

            $('.back', elem).on('mousedown', function () {
                $(elem).addClass('pressed-back');
            });

            $('.back', elem).on('mouseup mouseleave', function () {
                $(elem).removeClass('pressed-back');
            });

            // bind flip

            $('.back', elem).on('click', function () {

                // bind unflip ability to back side elements
                var front = $(elem).find('.front');

                front.on('click', function () {
                    $(elem).removeClass('shadowed');
                    $(elem).addClass('flip');

                    $(elem).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function (e) {
                        $(elem).addClass('shadowed');

                        _.delay(function () {
                            //if (listEvent.Slider.length > 0)
                            //  listEvent.Slider.redrawSlider();
                        }, 50);
                    });
                });

                // flip to back

                $(elem).removeClass('shadowed');
                $(elem).removeClass('flip');

                $(elem).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function (e) {
                    $(elem).addClass('shadowed');
                });
            });
        }
    };

    this.BeforeRemoveEvent = function (elem) {
        if (elem.nodeType === 1) {
            if (inViewport(elem)) {
                $(elem).removeClass('shadowed');
                $(elem).removeClass('flip');

                // force remove the element after the transition as knockout seems not to be able to remove 3d transformed elementsproperly

                $(elem).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function (e) {
                    $(elem).remove();
                });
            } else {
                $(elem).remove();
            }
        }
    }

    this.pullEvents = function () {
        var deferred = $.Deferred();
        
        // pull
        
        $.ajax({
            url: '/events/GetHomepageEvents',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8'
        }).done(function (res) {

            // place all events on the list

            $.each(res, function (i, eventData) {
                self.Events.push(new ListEvent(eventData));
            });

            log('Loaded events', res);

            // resolve

            deferred.resolve();
        });

        return deferred.promise();
    };


    this.AddAsAttendee = function (eventDateId) {
        var data = {
            eventDateId: eventDateId,
        };

        return $.post('/events/AddAsAttendee', data);
    };

    this.RemoveAsAttendee = function (eventDateId) {
        var data = {
            eventDateId: eventDateId,
        };

        return $.post('/events/RemoveAsAttendee', data);
    };
};

// **************************************************************** MODELS

var Attendee = function (name) {
    this.Name = name;
    this.HeadUrl = MC3.CDN + "minecraftheads/" + name + ".png";
}

// load templates immediately

$(function() {

    // bind the model

    var model = new HomeModel();

    MC3.templates.load().done(function () {
        ko.applyBindings(model);
        model.pullEvents();
        log('initiated model');
    });
});