﻿// initial boxes

window.initalBoxes = ['#banners', '#description'];


$(function () {
    UI.reLayout();
    _.delay(function () {
        UI.reLayout();
        _.delay(function () {
            UI.reLayout();
        }, 2000);
    }, 1000);

    // set up big blue button

    var bigBlue = $('#big-blue');
    var bigBlueLabel = $('.big-blue-label');

    var sendChange = function (enabled) {
        $.post('/' + 'editserver/enable', {
            identifier: $('#Identifier').val(),
            enabled: enabled
        }, function () {
            log('enable change complete');
        });
    }

    var updateBugBlue = function () {
        if (bigBlue.is(':checked')) {
            bigBlue.parent().addClass('enabled');
            bigBlueLabel.html('Listed');
            sendChange(true);
        } else {
            bigBlue.parent().removeClass('enabled');
            bigBlueLabel.html('List');
            sendChange(false);
        }
    };

    bigBlue.change(function () {
        updateBugBlue();
    });

    if (bigBlue.is(':checked')) {
        bigBlue.parent().addClass('enabled');
        bigBlueLabel.html('Listed');
    }

    $('.enable').removeClass('hidden');

    // load scroll tabs

    UI.scrollTopTab(Constant.SitePageScrollTabLevel).start();
    UI.scrollBottomTab();

    // load first timer notification

    //Notify.info("You can get back to this page using the drop-down above with your face on it.", 'manage-server-menu');
});