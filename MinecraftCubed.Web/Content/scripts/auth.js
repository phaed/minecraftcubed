
var Auth = (function () {
    var callbackStore = null;
    var registerLoaded = false;

    var storeCallback = function (callback) {
        if (callback) {
            callbackStore = callback;
            log('stored callback', callback);
        }
    };

    var clearCallback = function () {
        callbackStore = undefined;
        log('cleared callback');
    }

    var getCallback = function () {
        log('pulled callback', callbackStore);
        return callbackStore;
    };

    var resetModal = function () {
        clearCallback();
        UI.refresh('#authcombo');
        UI.cleanErrors();
        $('#authcombo .alert').hide();
    };

    return {
        Status: function (on, off) {
            $.post(Url('status', 'auth'), {}, function (result) {
                if (result.authenticated) {
                    if (typeof on === 'function')
                        on();
                } else {
                    if (typeof off === 'function')
                        off();
                }
            }, "json").error(function () {
                if (typeof off === 'function')
                    off();
            });
        },

        ApplyCallback: function (type) {

            if (type == 'spinner') {
                $('#authcombo').addClass('spinner');
                UI.resetAutosized();
            }

            setTimeout(function () {
                var returnUrl = Arg.get('ReturnUrl');

                var theCallback = getCallback();

                if (theCallback) {
                    log('applying', theCallback);
                    theCallback();
                    return;
                }

                if (returnUrl) {
                    window.location.replace(window.location.protocol + '//' + window.location.host + '/' + $.trim(returnUrl));
                    return;
                } else {
                    log('reload');
                    window.location.replace(window.location.protocol + '//' + window.location.host + window.location.pathname);
                }
            }, 250);
        },

        ShowLoginModal: function (callback) {
            resetModal();
            Auth.ShowLogin(callback);
        },

        ShowRegisterModal: function (callback) {
            resetModal();
            Auth.ShowRegister(callback);
        },

        ShowLogin: function (callback) {

            $('#logincombo').show();
            $('#registercombo').hide();

            // set up and show modal

            $('#authcombo').addClass('login').removeClass('register').removeClass('spinner');
            if ($('#authcombo').hasClass('modal')) {
                $('#authcombo').modal('show');
            }

            UI.resetAutosized();
            storeCallback(callback);
            Auth.ForgotPass.Hide();

            // focus on user or password depending if user is stored

            var hasName = $.jStorage.get('mc3-username');

            if (hasName) {
                $('#logincombo #Password').focus();
                _.delay(function () {
                    $('#logincombo #Password').focus();
                }, 500);
            } else {
                $('#UserOrEmail').focus();
            }

            if (!registerLoaded) {
                $('#registercombo').load(window.location.protocol + '//' + window.location.host + '/auth/registerasync', function () {
                    registerLoaded = true;
                });
            }
        },

        ShowRegister: function (callback) {

            var show = function () {
                $('#registercombo').show();
                $('#logincombo').hide();

                // set up and show modal

                $('#authcombo').addClass('register').removeClass('login').removeClass('spinner');
                if ($('#authcombo').hasClass('modal')) {
                    $('#authcombo').modal('show');
                }

                // focus on username

                if ($('#registercombo #Username').is(':visible')) {
                    $('#registercombo #Username').focus();
                } else {
                    $('#registercombo #Email').focus();
                }

                UI.resetAutosized();
                storeCallback(callback);
                Auth.ForgotPass.Hide();
            }

            if (registerLoaded) {
                show();
            } else {
                $('#registercombo').load(window.location.protocol + '//' + window.location.host + '/auth/registerasync', function () {
                    registerLoaded = true;
                    show();
                });
            }
        },

        LoginAjax: function () {
            $('#authcombo .loader').show();

            $.post(Url('loginasync', 'auth'), {
                UserOrEmail: $('#UserOrEmail', "#logincombo").val(),
                Password: $('#Password', "#logincombo").val(),
                AllowProcess: true
            }).done(function (result) {
                $('#authcombo .loader').hide();
                Auth.LoginDone(result);
            });
        },

        RegisterAjax: function () {
            $('#authcombo .loader').show();

            $.post(Url('registerasync', 'auth'), {
                Username: $('#Username', "#registercombo").val(),
                CaptchaDeText: $('#CaptchaDeText', "#registercombo").val(),
                CaptchaInputText: $('#CaptchaInputText', "#registercombo").val(),
                Email: $('#Email', "#registercombo").val(),
                NewPassword: $('#NewPassword', "#registercombo").val(),
                AgreeTerms: $('#AgreeTerms', "#registercombo").prop('checked'),
                AllowProcess: true
            }).done(function (result) {
                $('#authcombo .loader').hide();
                Auth.RegisterDone(result);
            });
        },

        LoginDone: function (result) {
            $("#logincombo").html(result);

            UI.refresh('#authcombo');
            $('.alert', "#logincombo").slideDown();

            $.jStorage.set('mc3-username', $('#UserOrEmail', "#logincombo").val());
            Auth.ForgotPass.Hide();
            clearCallback();
        },

        RegisterDone: function (result) {
            $("#registercombo").html(result);

            UI.refresh('#authcombo');
            $('.alert', "#registercombo").slideDown();
            clearCallback();
        },

        Logout: function () {
            $.post(Url('logoutasync', 'auth'), {}, function () {
                location.reload();
            });
        },

        ForgotPass: {
            Show: function () {
                UI.cleanErrors();

                var title = $('#logincombo .title');

                title.html(title.attr('data-off'));
                $('#logincombo .forgot-pass-actions').show();
                $('#logincombo .login-actions').hide();
                $('#logincombo .forgot-pass-copy').show();
                $('#logincombo .register-copy').hide();
                $('#logincombo .password-group').hide();
                $('#logincombo #Password').val("PlaceHolder22");
                $('#logincombo .alert').hide();

                $('#UserOrEmail').attr('placeholder', 'Email');

                if ($('#UserOrEmail').val().indexOf('@') < 0) {
                    $('#UserOrEmail').val('');
                }
            },

            Hide: function () {
                UI.cleanErrors();

                var title = $('#logincombo .title');

                title.html(title.attr('data-on'));
                $('#logincombo .forgot-pass-actions').hide();
                $('#logincombo .login-actions').show();
                $('#logincombo .forgot-pass-copy').hide();
                $('#logincombo .register-copy').show();
                $('#logincombo .password-group').show();
                $('#logincombo #Password').val("");

                $('#UserOrEmail').attr('placeholder', 'Username or Email');
            },

            Send: function (options) {
                var settings = $.extend({
                    destination: "#logincombo"
                }, options);

                if ($('#UserOrEmail', settings.destination).val() == '') {
                    $('#UserOrEmail', settings.destination).addClass('input-validation-error');
                    return;
                }
                $('#authcombo .loader').show();

                $.post(Url('loginasync', 'auth'), {
                    UserOrEmail: $('#UserOrEmail', settings.destination).val(),
                    IsForgotPass: true,
                    AllowProcess: true
                }).done(function (result) {
                    $(settings.destination).html(result);
                    $('#authcombo .loader').hide();
                    $('.alert', settings.destination).slideDown();
                });
            }
        },

        BindEnter: function () {
            $('#ConfirmPassword, #AgreeTerms').unbind('keypress').keypress(function (event) {
                if ((event.keyCode ? event.keyCode : event.which) == '13') {
                    Auth.RegisterAjax();
                }
            });

            $('#logincombo #Password').unbind('keypress').keypress(function (event) {
                if ((event.keyCode ? event.keyCode : event.which) == '13') {
                    Auth.LoginAjax();
                }
            });

            $('#UserOrEmail').val($.jStorage.get('mc3-username'));
        },
        GetCurrentUser: function () {
            $.ajax({
                url: '/user/GetCurrentUsername',
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            }).done(function (res) {
                MC3.CurrentUsername = res;
                log('CurrentUsername', res);
            });
        }
    };
})();

// pull current user now

Auth.GetCurrentUser();

$(function () {

    // init
    Auth.ForgotPass.Hide();
    $('#registercombo').hide();
    $('#authcombo .alert').hide();

    // bind enter buttons

    Auth.BindEnter();

    // reset the dimensions on load and on browser resize

    $(window).bind('resize', _.throttle(UI.resetAutosized, 20));
    _.defer(UI.resetAutosized);
});

