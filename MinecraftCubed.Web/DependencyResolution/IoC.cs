// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IoC.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------


using MinecraftCubed.Domain.Azure;
using MinecraftCubed.Domain.Core;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Interfaces;
using MinecraftCubed.Domain.Repositories;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Service.Services;
using MinecraftCubed.Service.Services.Interfaces;

namespace MinecraftCubed.Web.DependencyResolution
{
    using StructureMap;
    using StructureMap.Graph;
    using StructureMap.Web;

    public static class IoC
    {
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(action =>
            {
                action.Scan(scan => { 
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                });

                action.For<IGeneralRepository>().HybridHttpOrThreadLocalScoped().Use<GeneralRepository>();
                action.For<IUserRepository>().HybridHttpOrThreadLocalScoped().Use<UserRepository>();
                action.For<IServerRepository>().HybridHttpOrThreadLocalScoped().Use<ServerRepository>();
                action.For<IEventRepository>().HybridHttpOrThreadLocalScoped().Use<EventRepository>();
                action.For<IPluginRepository>().HybridHttpOrThreadLocalScoped().Use<PluginRepository>();
                action.For<IModpackRepository>().HybridHttpOrThreadLocalScoped().Use<ModpackRepository>();
                action.For<IStaffRepository>().HybridHttpOrThreadLocalScoped().Use<StaffRepository>();
                action.For<IBannerRepository>().HybridHttpOrThreadLocalScoped().Use<BannerRepository>();
                action.For<ITagsRepository>().HybridHttpOrThreadLocalScoped().Use<TagsRepository>();
                action.For<IChatRepository>().HybridHttpOrThreadLocalScoped().Use<ChatRepository>();
                action.For<ISponsorRepository>().HybridHttpOrThreadLocalScoped().Use<SponsorRepository>();
                action.For<IStubRepository>().HybridHttpOrThreadLocalScoped().Use<StubRepository>();
                action.For<IMiniGameRepository>().HybridHttpOrThreadLocalScoped().Use<MiniGameRepository>();
                action.For<IConnectionInfoRepository>().HybridHttpOrThreadLocalScoped().Use<ConnectionInfoRepository>();
                action.For<IHomeRepository>().HybridHttpOrThreadLocalScoped().Use<HomeRepository>();

                action.For<IAuthService>().HybridHttpOrThreadLocalScoped().Use<AuthService>();
                action.For<IUserService>().HybridHttpOrThreadLocalScoped().Use<UserService>();
                action.For<IServerService>().HybridHttpOrThreadLocalScoped().Use<ServerService>();
                action.For<IEventService>().HybridHttpOrThreadLocalScoped().Use<EventService>();
                action.For<IGeneralService>().HybridHttpOrThreadLocalScoped().Use<GeneralService>();
                action.For<IQueryService>().HybridHttpOrThreadLocalScoped().Use<QueryService>();
                action.For<IPluginService>().HybridHttpOrThreadLocalScoped().Use<PluginService>();
                action.For<IModpackService>().HybridHttpOrThreadLocalScoped().Use<ModpackService>();
                action.For<IStaffService>().HybridHttpOrThreadLocalScoped().Use<StaffService>();
                action.For<IBannerService>().HybridHttpOrThreadLocalScoped().Use<BannerService>();
                action.For<ITagsService>().HybridHttpOrThreadLocalScoped().Use<TagsService>();
                action.For<IChatService>().HybridHttpOrThreadLocalScoped().Use<ChatService>();
                action.For<ISponsorService>().HybridHttpOrThreadLocalScoped().Use<SponsorService>();
                action.For<IStubService>().HybridHttpOrThreadLocalScoped().Use<StubService>();
                action.For<IMiniGameService>().HybridHttpOrThreadLocalScoped().Use<MiniGameService>();
                action.For<IConnectionInfoService>().HybridHttpOrThreadLocalScoped().Use<ConnectionInfoService>();
                action.For<IHomeService>().HybridHttpOrThreadLocalScoped().Use<HomeService>();

                action.For<IDatabaseFactory>().HybridHttpOrThreadLocalScoped().Use<DatabaseFactory>();
                action.For<IUnitOfWork>().HybridHttpOrThreadLocalScoped().Use<UnitOfWork>();

                action.For<IRedisServerSettings>().Singleton().Use(x => RedisServerSettings.Settings.Value);
                action.For<IRedisConnectionWrapper>().Singleton().HybridHttpOrThreadLocalScoped().Use<RedisConnectionWrapper>();

                action.For<IRedisStore>().Singleton().HybridHttpOrThreadLocalScoped().Use<RedisStore>();
                action.For<IAzureStore>().Singleton().HybridHttpOrThreadLocalScoped().Use<AzureStore>();
            });

            return ObjectFactory.Container;
        }
    }
}