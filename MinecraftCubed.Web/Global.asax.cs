﻿using System;
using System.Diagnostics;
using System.Web;
using MinecraftCubed.Common;
using StackExchange.Profiling;

namespace MinecraftCubed.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_BeginRequest()
        {
            if (Settings.Profiling)
            {
                MiniProfiler.Start();
            }
        }

        protected void Application_EndRequest()
        {
            MiniProfiler.Stop();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // Any exceptions encountered will bubble up to here
            // and then based on web.config error handling will get forwarded to an error page
            // or in our case the ErrorController

            var serverException = Server.GetLastError();

            Trace.TraceError(serverException.Message + '\n' + serverException.StackTrace);

            if (serverException.InnerException != null)
            {
                Trace.TraceError(serverException.InnerException.Message + '\n' + serverException.InnerException.StackTrace);
            }
        }
    }
}