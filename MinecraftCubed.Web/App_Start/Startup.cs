﻿using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CaptchaMvc.Infrastructure;
using Microsoft.Owin;
using MinecraftCubed.Web.Mvc.Captcha;
using Owin;
using StackExchange.Profiling.EntityFramework6;

[assembly: OwinStartup(typeof(MinecraftCubed.Web.Startup))]
namespace MinecraftCubed.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();

            MiniProfilerEF6.Initialize();

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapConfig.RegisterMappings();

            //CaptchaUtils.CaptchaManager.StorageProvider = new RedisCaptchaStorageProvider();
            CaptchaUtils.CaptchaManager.StorageProvider = new CookieStorageProvider();
            CaptchaUtils.ImageGenerator = new CubedItemGenerator();

            // use JSON.Net for controller action parameters
            ValueProviderFactories.Factories.Remove(ValueProviderFactories.Factories.OfType<JsonValueProviderFactory>().Single());
            ValueProviderFactories.Factories.Add(new JsonValueProviderFactory());
        }
    }
}
