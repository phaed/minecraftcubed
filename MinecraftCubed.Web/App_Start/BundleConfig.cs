﻿using System.Web.Optimization;

namespace MinecraftCubed.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Clear();
            bundles.ResetAll();

            //**************************************************************************** GENERAL

            bundles.Add(new Bundle("~/scripts/general", new JsMinify()).Include(
                "~/scripts/html5shiv.js",
                "~/scripts/lodash.js",
                "~/scripts/knockout-3.1.0.js",
                "~/scripts/knockout.mapping-latest.js",
                "~/scripts/jquery-1.10.0.js",
                "~/scripts/jquery.browser.js",
                "~/scripts/jquery.velocity.js",
                "~/scripts/jquery.history.js",
                "~/scripts/jquery.ui.js",
                "~/scripts/jquery.ui.touch-punch.js",
                "~/scripts/jquery.signalR-{version}.js",
                "~/scripts/jquery.mb.browser.js",
                "~/scripts/jquery.bxslider.js",
                "~/scripts/jquery.dataTables.js",
                "~/scripts/imagesloaded.pkg.js",
                "~/scripts/jquery.qtip.js",
                "~/scripts/jquery.isotope.js",
                "~/scripts/jquery.fineuploader.js",
                "~/scripts/jquery.sticky.js",
                "~/scripts/jquery.zclip.js",
                "~/scripts/jquery.nicescroll.js",
                "~/scripts/jquery.textfill.js",
                "~/scripts/jquery.longpress.js",
                "~/scripts/jquery.inview.js",
                "~/scripts/jquery.knob.js",
                "~/scripts/jquery.nanoscroller.js",
                "~/scripts/jquery-deparam.js",
                "~/scripts/three.min.js",
                "~/scripts/2dskin.js",
                "~/scripts/notify.js",
                "~/scripts/randomColor.js",
                "~/scripts/waypoints.js",
                "~/scripts/DT_bootstrap.js",
                "~/scripts/TableTools.js",
                "~/scripts/jquery.icheck.js",
                "~/scripts/select2.js",
                "~/scripts/selectize.js",
                "~/scripts/json2.js",
                "~/scripts/arg.js",
                "~/scripts/jquery.excanvas.js",
                "~/scripts/mobiledevices.js",
                "~/scripts/jquery.cookie.js",
                "~/scripts/jquery.storage.js",
                "~/scripts/jquery.fullcalendar.js",
                "~/scripts/jquery.transit.js",
                "~/scripts/jquery.easing.1.3.js",
                "~/scripts/moment.js",
                "~/scripts/livestamp.js",
                "~/scripts/spectrum.js",
                "~/scripts/jquery.sparkline.js",
                "~/scripts/jquery.validate.js",
                "~/scripts/bootstrap-datepicker.js",
                "~/scripts/bootstrap-timepicker.js",
                "~/scripts/bootstrap-progressbar.js",
                "~/scripts/bootstrap-sortable.js",
                "~/scripts/bootbox.js",
                "~/scripts/bootstrap.js",
                "~/scripts/d3.v3.js"));
            
            bundles.Add(new Bundle("~/content/css/libs/general", new CssMinify()).Include(
                "~/Content/css/libs/datepicker.css",
                "~/Content/css/libs/flag-icon.css",
                "~/Content/css/libs/flags.css",
                "~/Content/css/libs/fullcalendar.css",
                "~/Content/css/libs/fileuploader.css",
                "~/Content/css/libs/ui.css",
                "~/Content/css/libs/TableTools.css",
                "~/Content/css/libs/jquery.qtip.css",
                "~/Content/css/libs/bootstrap.css",
                "~/Content/css/libs/jquery.bxslider.css",
                "~/Content/css/libs/bootstrap-responsive.css",
                "~/Content/css/libs/spectrum.css",
                "~/Content/css/libs/nanoscroller.css",
                "~/Content/css/libs/font-awesome.css",
                "~/Content/css/libs/cus-icons.css",
                "~/Content/css/libs/DT_bootstrap.css",
                "~/Content/css/libs/responsive-tables.css",
                "~/Content/css/libs/select2.css",
                "~/Content/css/libs/selectize.css",
                "~/Content/css/libs/animate.css",
                "~/Content/css/libs/icheck.css"));


            //**************************************************************************** BACKEND

            bundles.Add(new Bundle("~/scripts/backend", new JsMinify()).Include(
                "~/scripts/jquery.pagedown-bootstrap.combined.js"));

            bundles.Add(new Bundle("~/content/css/libs/backend", new CssMinify()).Include(
                "~/Content/css/libs/jquery.pagedown-bootstrap.css"));
            
            //**************************************************************************** SITE

            bundles.Add(new Bundle("~/content/scripts/site", new JsMinify()).Include(
                "~/Content/scripts/constant.js",
                "~/Content/scripts/functions.js",
                "~/Content/scripts/autocolor.js",
                "~/Content/scripts/notifications.js",
                "~/Content/scripts/tracking.js",
                "~/Content/scripts/social.js",
                "~/Content/scripts/ui.js",
                "~/Content/scripts/auth.js",
                "~/Content/scripts/main.js"));

            bundles.Add(new Bundle("~/content/css/site", new CssMinify()).Include(
                "~/Content/css/fonts.css",
                "~/Content/css/base.css",
                "~/Content/css/site.css",
                "~/Content/css/theme.css"));

            //**************************************************************************** HOME

            bundles.Add(new Bundle("~/content/scripts/home", new JsMinify()).Include(
                "~/Content/scripts/models/listevent.js",
                "~/Content/scripts/home.js"));

            bundles.Add(new Bundle("~/content/css/home", new CssMinify()).Include(
                "~/Content/css/events.css",
                "~/Content/css/home.css"));


            //**************************************************************************** EDIT SERVER
            
            bundles.Add(new Bundle("~/content/scripts/editserver", new JsMinify()).Include(
                "~/Content/scripts/editserver.js"));

            bundles.Add(new Bundle("~/content/css/editserver", new CssMinify()).Include(
                "~/Content/css/editserver.css"));

            //**************************************************************************** EDIT EVENT
            
            bundles.Add(new Bundle("~/content/scripts/editevent", new JsMinify()).Include(
                "~/scripts/jquery.datetimepicker.js",
                "~/Content/scripts/editevent.js"));

            bundles.Add(new Bundle("~/content/css/editevent", new CssMinify()).Include(
                "~/Content/css/libs/jquery.datetimepicker.css",
                "~/Content/css/editevent.css"));

            //**************************************************************************** LIST
            
            bundles.Add(new Bundle("~/content/scripts/list", new JsMinify()).Include(
                "~/Content/scripts/ping.js",
                "~/Content/scripts/list.js"));

            bundles.Add(new Bundle("~/content/css/list", new CssMinify()).Include(
                "~/Content/css/list.css"));

            //**************************************************************************** EVENTS

            bundles.Add(new Bundle("~/content/scripts/events", new JsMinify()).Include(
                "~/Content/scripts/models/listevent.js",
                "~/Content/scripts/events.js"));

            bundles.Add(new Bundle("~/content/css/events", new CssMinify()).Include(
                "~/Content/css/events.css"));

            //**************************************************************************** SERVER
            
            bundles.Add(new Bundle("~/content/scripts/server", new JsMinify()).Include(
                "~/scripts/fotorama.js",
                "~/Content/scripts/ping.js",
                "~/Content/scripts/server.js"));

            bundles.Add(new Bundle("~/content/css/server", new CssMinify()).Include(
                "~/Content/css/libs/fotorama.css",
                "~/Content/css/server.css"));
            //**************************************************************************** STUB

            bundles.Add(new Bundle("~/content/scripts/stub", new JsMinify()).Include(
                "~/Content/scripts/stub.js"));

            bundles.Add(new Bundle("~/content/css/stub", new CssMinify()).Include(
                "~/Content/css/stub.css"));
            
            //**************************************************************************** INFO

            bundles.Add(new Bundle("~/content/scripts/info", new JsMinify()).Include(
                "~/Content/scripts/info.js"));

            bundles.Add(new Bundle("~/content/css/info", new CssMinify()).Include(
                "~/Content/css/info.css"));
            //**************************************************************************** ADMIN

            bundles.Add(new Bundle("~/content/scripts/admin", new JsMinify()).Include(
                "~/Content/scripts/admin.js"));

            bundles.Add(new Bundle("~/content/css/admin", new CssMinify()).Include(
                "~/Content/css/admin.css"));
        }
    }
}