using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using MinecraftCubed.Web;
using SoundInTheory.DynamicImage;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(DynamicImage), "PreStart")]

namespace MinecraftCubed.Web
{
	public static class DynamicImage
	{
		public static void PreStart()
		{
			DynamicModuleUtility.RegisterModule(typeof(DynamicImageModule));
		}
	}
}