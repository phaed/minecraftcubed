﻿using AutoMapper;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Models.Backend;
using MinecraftCubed.Web.ViewModels.EditEvent;
using MinecraftCubed.Web.ViewModels.EditServer;

namespace MinecraftCubed.Web
{
    public class AutoMapConfig
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<User, UserSettings>();
            Mapper.CreateMap<UserSettings, User>();

            Mapper.CreateMap<ServerInfoModel, Server>();
            Mapper.CreateMap<Server, ServerInfoModel>();

            Mapper.CreateMap<EventInfoModel, Event>();
            Mapper.CreateMap<Event, EventInfoModel>();

            Mapper.CreateMap<QueryInfo, QueryModel>();
            Mapper.CreateMap<QueryModel, QueryInfo>();

            Mapper.CreateMap<VotifierInfo, VotifierModel>();
            Mapper.CreateMap<VotifierModel, VotifierInfo>();

            Mapper.CreateMap<ConnectionInfo, ServerConnectInfo>();
            Mapper.CreateMap<ServerConnectInfo, ConnectionInfo>();
            
            Mapper.CreateMap<ServerConnectionInfoData, ServerConnectionInfo>();
            Mapper.CreateMap<ServerConnectionInfo, ServerConnectionInfoData>();

            Mapper.CreateMap<Server, DescriptionModel>();
            Mapper.CreateMap<DescriptionModel, Server>();

            Mapper.CreateMap<Color, ColorsModel>();
            Mapper.CreateMap<ColorsModel, Color>();

            Mapper.CreateMap<Server, LocalizationModel>();
            Mapper.CreateMap<LocalizationModel, Server>();

            Mapper.CreateMap<PlayReq, EventPlayReqsModel>();
            Mapper.CreateMap<EventPlayReqsModel, PlayReq>();

            Mapper.CreateMap<Website, WebsitesModel>();
            Mapper.CreateMap<WebsitesModel, Website>();

            Mapper.CreateMap<ServerPlugin, ServerPluginData>();
            Mapper.CreateMap<ServerPluginData, ServerPlugin>();

            Mapper.CreateMap<ServerModpack, ServerModpackData>();
            Mapper.CreateMap<ServerModpackData, ServerModpack>();

            Mapper.CreateMap<ServerMiniGame, ServerMiniGameData>();
            Mapper.CreateMap<ServerMiniGameData, ServerMiniGame>();

            Mapper.CreateMap<Event, EventDescriptionModel>();
            Mapper.CreateMap<EventDescriptionModel, Event>();
        }
    }
}