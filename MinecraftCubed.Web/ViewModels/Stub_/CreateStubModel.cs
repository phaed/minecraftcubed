﻿using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using MinecraftCubed.Web.ViewModels.Stub_.Extras;

namespace MinecraftCubed.Web.ViewModels.Stub_
{
    public class CreateStubModel : StubBaseModel
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        [RegularExpression("([a-zA-Z0-9 .&'-]+)", ErrorMessage = "Only alphanumeric characters are allowed.")]
        public string Name { get; set; }
        public string Contact { get; set; }

        [Required]
        [DataAnnotationsExtensions.Url(UrlOptions.DisallowProtocol)]
        public string Address { get; set; }

        [Integer]
        public int Port { get; set; }
    }
}