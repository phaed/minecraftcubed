﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MinecraftCubed.Web.ViewModels.Stub_.Extras;

namespace MinecraftCubed.Web.ViewModels.Stub_
{
    public class CreateEventModel : StubBaseModel
    {
        [Required]
        [StringLength(100, MinimumLength = 3)]
        public string Name { get; set; }
        
        [Required]
        public string Identifier { get; set; }

        public ICollection<SelectListItem> ServerNameList { get; set; }
    }
}