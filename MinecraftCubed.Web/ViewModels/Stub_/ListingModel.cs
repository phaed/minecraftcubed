﻿using System.Collections.Generic;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models.Stub;
using MinecraftCubed.Web.ViewModels.Stub_.Extras;

namespace MinecraftCubed.Web.ViewModels.Stub_
{
    public class ListingModel : StubBaseModel
    {
        public string ActiveUsername { get; set; }
        public List<UserStubs> Editors { get; set; }
    }
}