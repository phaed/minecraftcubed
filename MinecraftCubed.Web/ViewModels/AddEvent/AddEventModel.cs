﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MinecraftCubed.Web.ViewModels.AddEvent
{
    public class AddEventModel
    {
        [Required]
        [StringLength(100, MinimumLength = 3)]
        public string Name { get; set; }
        
        [Required]
        public string Identifier { get; set; }

        public ICollection<SelectListItem> ServerNameList { get; set; }

        public bool RefreshUI { get; set; }
    }
}