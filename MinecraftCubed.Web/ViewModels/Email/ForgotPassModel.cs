﻿namespace MinecraftCubed.Web.ViewModels.Email
{
    public class ForgotPassModel
    {
        public string Email { get; set; }
        public string Username { get; set; }
        public string ConfirmationCode { get; set; }
    }
}