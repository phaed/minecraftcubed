﻿using System.Collections.Generic;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Models.Responses;

namespace MinecraftCubed.Web.ViewModels.User_
{
    public class UserNavModel
    {
        public string Username { get; set; }
        public string FaceUrl { get; set; }
        public ICollection<NameAndIdentifier> ManagedServers { get; set; }
        public ICollection<NameAndIdentifier> ManagedEvents { get; set; }
    }
}