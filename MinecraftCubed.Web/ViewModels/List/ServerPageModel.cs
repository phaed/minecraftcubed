﻿
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models.Page;

namespace MinecraftCubed.Web.ViewModels.List
{
    public class ServerPageModel
    {
        public PageServer PageServer { get; set; }
    }
}