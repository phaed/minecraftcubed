﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MinecraftCubed.Web.ViewModels.EditEvent.Extras;

namespace MinecraftCubed.Web.ViewModels.EditEvent
{
    public class EventDescriptionModel : EventManagersOnlyBaseModel
    {
        [AllowHtml]
        [Required]
        public string EventDescriptionText { get; set; }
    }
}