﻿using System.Collections.Generic;
using MinecraftCubed.Web.ViewModels.EditEvent.Extras;

namespace MinecraftCubed.Web.ViewModels.EditEvent
{
    public class AttendeeListModel : EventManagersOnlyBaseModel
    {
        public IList<string> Attendees { get; set; }
    }
}