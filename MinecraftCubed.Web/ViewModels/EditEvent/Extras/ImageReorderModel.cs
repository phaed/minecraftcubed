﻿
namespace MinecraftCubed.Web.ViewModels.EditEvent.Extras
{
    public class ImageReorderModel
    {
        public string Ids { get; set; }
        public string EventIdentifier { get; set; }
    }
}
