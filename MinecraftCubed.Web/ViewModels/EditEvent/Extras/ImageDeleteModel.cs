﻿
namespace MinecraftCubed.Web.ViewModels.EditEvent.Extras
{
    public class ImageDeleteModel
    {
        public int Id { get; set; }
        public string EventIdentifier { get; set; }
        public string Key { get; set; }
    }
}
