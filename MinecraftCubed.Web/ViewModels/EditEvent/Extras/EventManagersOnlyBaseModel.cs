﻿
namespace MinecraftCubed.Web.ViewModels.EditEvent.Extras
{
    public class EventManagersOnlyBaseModel
    {
        public string EventIdentifier { get; set; }
        public bool RefreshUI { get; set; }
    }
}