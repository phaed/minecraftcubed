﻿
namespace MinecraftCubed.Web.ViewModels.EditEvent.Extras
{
    public class EventDateDeleteModel
    {
        public int Id { get; set; }
        public string EventIdentifier { get; set; }
    }
}
