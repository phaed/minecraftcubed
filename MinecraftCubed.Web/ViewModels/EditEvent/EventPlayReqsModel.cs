﻿using DataAnnotationsExtensions;
using MinecraftCubed.Web.ViewModels.EditEvent.Extras;

namespace MinecraftCubed.Web.ViewModels.EditEvent
{
    public class EventPlayReqsModel : EventManagersOnlyBaseModel
    {
        public bool HasDownload { get; set; }
        public string DownloadLabel { get; set; }
        [Url]
        public string DownloadUrl { get; set; }

        public bool HasWhitelist { get; set; }
        [Url]
        public string WhitelistUrl { get; set; }

        public bool HasAttendeeWhitelist { get; set; }

        public bool HasRules { get; set; }
        [Url]
        public string RulesUrl { get; set; }
    }
}