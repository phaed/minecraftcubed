﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DataAnnotationsExtensions;
using MinecraftCubed.Web.ViewModels.EditEvent.Extras;

namespace MinecraftCubed.Web.ViewModels.EditEvent
{
    public class EventInfoModel : EventManagersOnlyBaseModel
    {
        [Required]
        [StringLength(100, MinimumLength = 3)]
        public string Name { get; set; }

        [Required]
        [DataAnnotationsExtensions.Url(UrlOptions.DisallowProtocol)]
        public string Address { get; set; }

        [Integer]
        public int Port { get; set; }

        [Required]
        public string VersionName { get; set; }

        [Required]
        public string ServerIdentifier { get; set; }
        public ICollection<SelectListItem> ServerNameList { get; set; }
    }
}