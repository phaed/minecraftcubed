﻿using MinecraftCubed.Domain.Context;
using MinecraftCubed.Web.ViewModels.EditEvent.Extras;

namespace MinecraftCubed.Web.ViewModels.EditEvent
{
    public class EditEventModel : EventManagersOnlyBaseModel
    {
        public Event Event { get; set; }
    }
}