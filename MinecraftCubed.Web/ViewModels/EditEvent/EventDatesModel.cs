﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DataAnnotationsExtensions;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models.Events;
using MinecraftCubed.Web.ViewModels.EditEvent.Extras;

namespace MinecraftCubed.Web.ViewModels.EditEvent
{
    public class EventDatesModel : EventManagersOnlyBaseModel
    {
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        [Min(1)]
        public int LengthInHours { get; set; }

        public ICollection<EventDate> EventDates { get; set; }

        [Required]
        public string TimeZone { get; set; }
        public ICollection<SelectListItem> TimeZoneList { get; set; }
    }
}