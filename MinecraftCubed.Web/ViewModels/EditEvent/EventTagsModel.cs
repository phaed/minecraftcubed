﻿using MinecraftCubed.Web.ViewModels.EditEvent.Extras;

namespace MinecraftCubed.Web.ViewModels.EditEvent
{
    public class EventTagsModel : EventManagersOnlyBaseModel
    {
        public string EventTags { get; set; }
    }
}