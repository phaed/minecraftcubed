﻿using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Static;

namespace MinecraftCubed.Web.ViewModels.Claim
{
    public class ClaimModel
    {
        public Server Server { get; set; }
        public string Username { get; set; }

        [Required]
        [Email]
        [StringLength(255)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [StringLength(100, MinimumLength = Constant.PasswordMinSize)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public bool RefreshUI { get; set; }
        public string AccessCode { get; set; }
        public string BannerUrl { get; set; }
    }
}