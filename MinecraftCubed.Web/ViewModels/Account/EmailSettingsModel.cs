﻿using System;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;

namespace MinecraftCubed.Web.ViewModels.Account
{
    public class EmailSettingsModel
    {
        [Required]
        [Email]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public Boolean ReceiveEventNotifications { get; set; }
        public bool RefreshUI { get; set; }
    }
}