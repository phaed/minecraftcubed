﻿using System.ComponentModel.DataAnnotations;

namespace MinecraftCubed.Web.ViewModels.Account
{
    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        public bool RefreshUI { get; set; }
    }
}