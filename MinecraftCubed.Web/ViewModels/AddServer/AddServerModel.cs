﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DataAnnotationsExtensions;
using MinecraftCubed.Static;
using MinecraftCubed.Web.Mvc.Attributes;

namespace MinecraftCubed.Web.ViewModels.AddServer
{
    public class AddServerModel
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        [RegularExpression("([a-zA-Z0-9 .&'-]+)", ErrorMessage = "Only alphanumeric characters are allowed.")]
        public string Name { get; set; }
        
        [Required]
        [DataAnnotationsExtensions.Url(UrlOptions.DisallowProtocol)]
        public string Address { get; set; }
        
        [Integer]
        public int Port { get; set; }
        
        [Required]
        [IsTrue]
        public bool IsOwner { get; set; }

        [Required]
        [RegularExpression("([a-zA-Z0-9 .&'-]+)", ErrorMessage = "Only alphanumeric characters are allowed.")]
        [StringLength(50, MinimumLength = 3)]
        public string HostName { get; set; }
        
        public bool RefreshUI { get; set; }
        public bool NoUser { get; set; }
        [Required]
        public string Username { get; set; }

        [Email]
        [StringLength(255)]
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [StringLength(100, MinimumLength = Constant.PasswordMinSize)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool PlayReqHasDownload { get; set; }
        public string PlayReqDownloadLabel { get; set; }
        [System.ComponentModel.DataAnnotations.Url]
        public string PlayReqDownloadUrl { get; set; }

        public bool PlayReqHasWhitelist { get; set; }
        [System.ComponentModel.DataAnnotations.Url]
        public string PlayReqWhitelistUrl { get; set; }
        
    }
}