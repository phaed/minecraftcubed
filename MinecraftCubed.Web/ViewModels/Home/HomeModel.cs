﻿
using System.Collections.Generic;
using MinecraftCubed.Domain.Models.Events;
using MinecraftCubed.Domain.Models.Home;

namespace MinecraftCubed.Web.ViewModels.Home
{
    public class HomeModel
    {
        public ICollection<FaceData> FaceUrls { get; set; }
    }
}