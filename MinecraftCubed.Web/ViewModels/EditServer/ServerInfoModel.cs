﻿using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class ServerInfoModel : ServerManagersOnlyBaseModel
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }
        
        [Required]
        public string VersionName { get; set; }

        [Required]
        public string HostName { get; set; }
    }
}