﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Models.Backend;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class PluginsModel : ServerManagersOnlyBaseModel
    {
        [Required]
        public List<ServerPluginData> Plugins { get; set; }

        public bool HasPlugins { get; set; }
        public bool ShowPlugins { get; set; }
    }
}