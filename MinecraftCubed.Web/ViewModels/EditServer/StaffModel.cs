﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Models.Backend;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class StaffModel : ServerManagersOnlyBaseModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public Level PermissionLevel { get; set; }
        [Required]
        public string Color { get; set; }
        [Required]
        public string Rank { get; set; }
        public bool IsSave { get; set; }

        public List<ServerStaffData> Staff { get; set; }

        public enum Level
        {
            Admin,
            Manager,
            Staff
        }
    }
}