﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models.Backend;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class ConnectionInfosModel : ServerManagersOnlyBaseModel
    {
        public List<ServerConnectionInfoData> ConnectionInfos { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }
        public int Port { get; set; }
        public bool PlayReqHasDownload { get; set; }
        public string PlayReqDownloadLabel { get; set; }
        [Url]
        public string PlayReqDownloadUrl { get; set; }

        public bool PlayReqHasWhitelist { get; set; }
        [Url]
        public string PlayReqWhitelistUrl { get; set; }
        
        public void Clear()
        {
            Name = string.Empty;
            Address = string.Empty;
            Port = 25565;
            PlayReqDownloadLabel = string.Empty;
            PlayReqDownloadUrl = string.Empty;
            PlayReqHasDownload = false;
            PlayReqHasWhitelist = false;
            PlayReqWhitelistUrl = string.Empty;
        }
    }
}