﻿
using System.Collections.Generic;
using MinecraftCubed.Domain.Models.Responses;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class CarouselModel : ServerManagersOnlyBaseModel
    {
        public bool ShuffleImages { get; set; }
        public bool VideosFirst { get; set; }
        public string VideoUrl { get; set; }

        public List<ValueIdModel> Urls { get; set; }
        public List<ValueIdModel> VideoThumbnailUrls { get; set; }
    }
}