﻿using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class QueryModel: ServerManagersOnlyBaseModel
    {
        [Required]
        [DataAnnotationsExtensions.Url(UrlOptions.DisallowProtocol)]
        public string Address { get; set; }

        [Integer]
        public int Port { get; set; }
    }
}