﻿
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class EditServerModel: ServerManagersOnlyBaseModel
    {
        public Server Server { get; set; }
        public string EnabledCheck { get; set; }
    }
}