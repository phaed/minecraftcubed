﻿using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class TagsModel : ServerManagersOnlyBaseModel
    {
        public string Types { get; set; }
        public string Gameplays { get; set; }
    }
}