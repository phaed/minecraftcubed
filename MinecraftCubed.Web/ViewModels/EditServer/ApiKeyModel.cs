﻿using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class ApiKeyModel: ServerManagersOnlyBaseModel
    {
        public string Key { get; set; }
    }
}