﻿
using System.Collections.Generic;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class ServerLogoModel : ServerManagersOnlyBaseModel
    {
        public bool HasLogo { get; set; }
        public string LogoUrl { get; set; }
    }
}