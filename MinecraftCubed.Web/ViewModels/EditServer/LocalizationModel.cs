﻿using System.ComponentModel.DataAnnotations;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class LocalizationModel : ServerManagersOnlyBaseModel
    {
        [Required]
        public string CountryName { get; set; }

        [Required]
        public string LanguageName { get; set; }
    }
}