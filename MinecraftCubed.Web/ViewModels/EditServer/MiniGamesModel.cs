﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MinecraftCubed.Domain.Models.Backend;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class MiniGamesModel : ServerManagersOnlyBaseModel
    {
        public List<ServerMiniGameData> MiniGames { get; set; }

        [Required]
        [MinLength(3)]
        public string Name { get; set; }
        [MaxLength(1024)]
        [Required]
        public string Description { get; set; }
        [MaxLength(255)]
        public string Website { get; set; }
        [MaxLength(255)]
        [Required]
        public string LogoUrl { get; set; }

        public void Clear()
        {
            Name = string.Empty;
            Website = string.Empty;
            Description = string.Empty;
            LogoUrl = string.Empty;
        }
    }
}