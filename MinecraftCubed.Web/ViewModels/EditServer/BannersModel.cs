﻿
using System.Collections.Generic;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class BannersModel : ServerManagersOnlyBaseModel
    {
        public string Url { get; set; }
        public bool HasBanner { get; set; }
    }
}