﻿
namespace MinecraftCubed.Web.ViewModels.EditServer.Extras
{
    public class ServerManagersOnlyBaseModel
    {
        public string Identifier { get; set; }
        public bool RefreshUI { get; set; }
    }
}