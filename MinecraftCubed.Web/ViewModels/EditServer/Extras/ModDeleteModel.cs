﻿
namespace MinecraftCubed.Web.ViewModels.EditServer.Extras
{
    public class ModDeleteModel
    {
        public string Name { get; set; }
        public string Identifier { get; set; }
    }
}
