﻿
namespace MinecraftCubed.Web.ViewModels.EditServer.Extras
{
    public class QuoteUpdateModel
    {
        public string Username { get; set; }
        public string Identifier { get; set; }
        public string Quote { get; set; }
    }
}
