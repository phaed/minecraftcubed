﻿
namespace MinecraftCubed.Web.ViewModels.EditServer.Extras
{
    public class StaffDeleteModel
    {
        public string Id { get; set; }
        public string Identifier { get; set; }
        public string Key { get; set; }
    }
}
