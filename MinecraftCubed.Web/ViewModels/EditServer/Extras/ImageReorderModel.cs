﻿
namespace MinecraftCubed.Web.ViewModels.EditServer.Extras
{
    public class ImageReorderModel
    {
        public string Ids { get; set; }
        public string Identifier { get; set; }
    }
}
