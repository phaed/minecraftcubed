﻿using System.ComponentModel.DataAnnotations;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class WebsitesModel : ServerManagersOnlyBaseModel
    {
        [Url]
        public string Url1 { get; set; }
        [Url]
        public string Url2 { get; set; }
        [Url]
        public string Url3 { get; set; }
        [Url]
        public string Url4 { get; set; }
        public string Title1 { get; set; }
        public string Title2 { get; set; }
        public string Title3 { get; set; }
        public string Title4 { get; set; }
    }
}