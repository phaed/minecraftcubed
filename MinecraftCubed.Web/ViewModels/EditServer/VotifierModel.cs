﻿using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class VotifierModel : ServerManagersOnlyBaseModel
    {
        [Required]
        [DataAnnotationsExtensions.Url(UrlOptions.DisallowProtocol)]
        public string Address { get; set; }

        [Required]
        public int Port { get; set; }

        [Required]
        public string PublicKey { get; set; }
    }
}