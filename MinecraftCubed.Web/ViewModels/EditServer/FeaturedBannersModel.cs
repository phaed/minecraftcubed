﻿
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class FeaturedBannersModel : ServerManagersOnlyBaseModel
    {
        public string Url { get; set; }
        public bool HasFeaturedBanner { get; set; }
    }
}