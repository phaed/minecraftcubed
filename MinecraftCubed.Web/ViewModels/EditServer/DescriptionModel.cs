﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class DescriptionModel : ServerManagersOnlyBaseModel
    {
        [AllowHtml]
        public string DescriptionText { get; set; }
        public string MiddleColor { get; set; }
    }
}