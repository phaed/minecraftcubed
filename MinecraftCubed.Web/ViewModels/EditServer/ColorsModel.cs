﻿using System.ComponentModel.DataAnnotations;
using MinecraftCubed.Web.ViewModels.EditServer.Extras;

namespace MinecraftCubed.Web.ViewModels.EditServer
{
    public class ColorsModel : ServerManagersOnlyBaseModel
    {
        public string Header { get; set; }
        public string Background { get; set; }
        public string Middle { get; set; }
        public bool IsDarkBackground { get; set; }
        public bool IsDarkHeader { get; set; }
        public bool IsDarkMiddle { get; set; }
    }
}