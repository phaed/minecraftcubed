﻿using System.ComponentModel.DataAnnotations;
using MinecraftCubed.Common;
using MinecraftCubed.Static;

namespace MinecraftCubed.Web.ViewModels.Auth
{
    public class ChangePassModel
    {
        public string Email { get; set; }
        public string Code { get; set; }

        [Required]
        [StringLength(100, MinimumLength = Constant.PasswordMinSize)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}