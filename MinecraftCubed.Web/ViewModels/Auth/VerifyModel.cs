﻿namespace MinecraftCubed.Web.ViewModels.Auth
{
    public class VerifyModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}