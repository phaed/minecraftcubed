﻿using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using MinecraftCubed.Static;
using MinecraftCubed.Web.Mvc.Attributes;

namespace MinecraftCubed.Web.ViewModels.Auth
{
    public class RegisterModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [Email]
        [StringLength(255)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [StringLength(100, MinimumLength = Constant.PasswordMinSize)]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }
        
        [IsTrue]
        public bool AgreeTerms { get; set; }

        public bool AllowProcess { get; set; }
    }
}
