﻿using System.ComponentModel.DataAnnotations;

namespace MinecraftCubed.Web.ViewModels.Auth
{
    public class LoginModel
    {
        [Required]
        [StringLength(255)]
        public string UserOrEmail { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool IsForgotPass { get; set; }

        public bool AllowProcess { get; set; }
    }
}