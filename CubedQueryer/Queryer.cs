﻿using System;
using System.Threading.Tasks;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Repositories;
using MinecraftCubed.Minecraft;
using MinecraftCubed.Minecraft.Models;
using MinecraftCubed.Service.Services;
using MinecraftCubed.Static;

namespace Queryer
{
    public class Queryer
    {
        public static async Task<QueryResponse> QueryServer(ServerConnectInfo info, Timeout timeout)
        {
            try
            {
                var resp = await StatusQuery.QueryAsync(info.QueryInfoAddress, info.QueryInfoPort);

                using (var context = new DatabaseFactory())
                {
                    using (var unit = new UnitOfWork(context))
                    {
                        var spondorRepo = new SponsorRepository(context);
                        var repo = new ServerRepository(context);
                        var eventRepo = new EventRepository(context, spondorRepo);
                        var tagsRepo = new TagsRepository(context);
                        var tagsService = new TagsService(tagsRepo, unit, repo, eventRepo);

                        // no server no donut

                        var s = repo.GetServerById(info.ServerId);

                        if (resp != null)
                        {
                            // only testing, return result

                            if (info.ServerId == 0)
                            {
                                return resp;
                            }

                            resp.ServerId = info.ServerId;

                            if (s == null)
                            {
                                return new QueryResponse
                                {
                                    Success = false
                                };
                            }

                            // create version if new
                            var formatted = GeneralHelper.FormatVersion(resp.Version);
                            var id = tagsService.CreateVersionIfNotExists(formatted);

                            Console.WriteLine("[" + info.Address + "] Version " + formatted + " id:" + id);

                            // save server info

                            s.VersionId = id;
                            if (s.HasQueryDisabled)
                            {
                                s.HasQueryDisabled = false;
                            }

                            unit.Commit();

                            return resp;
                        }

                        // has query disabled

                        s.HasQueryDisabled = true;
                        unit.Commit();
                        Console.WriteLine("[" + info.Address + "] Query done.");
                    }
                }

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("attempt failed"))
                {
                    Console.WriteLine("[" + info.Address + "] Failed.");
                }
                else if (ex.Message.Contains("forcibly"))
                {
                    Console.WriteLine("[" + info.Address + "] Forcibly closed.");
                }
                else if (ex.Message.Contains("timeout")|| ex.Message.Contains("timedout"))
                {
                    Console.WriteLine("[" + info.Address + "] Timeout.");
                }
                else
                {
                    Console.WriteLine("[" + info.Address + "] " + ex.Message);

                }
            }

            return new QueryResponse
            {
                Success = false
            };
        }
    }
}
