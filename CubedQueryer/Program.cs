﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Core;
using MinecraftCubed.Domain.Repositories;
using MinecraftCubed.Minecraft;
using MinecraftCubed.Service.Services;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Static;
using MinecraftCubed.Web.DependencyResolution;
using Nito.AsyncEx;
using StructureMap;
using Timeout = MinecraftCubed.Static.Timeout;

namespace Queryer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IoC.Initialize();
            AsyncContext.Run(() => MainAsync(args));
        }

        public async static void MainAsync(string[] args) 
        {
            try
            {
                var serverService = ObjectFactory.GetInstance<IServerService>();
                var pluginService = ObjectFactory.GetInstance<IPluginService>();

                var servers = serverService.GetServerConnectInfos();

                Console.WriteLine("Querying " + servers.Count + " servers");

                var res = await Task.WhenAll(servers.Select(x => Queryer.QueryServer(x, Timeout.FifteenSeconds))).ConfigureAwait(false);

                await Task.WhenAll(res.Select(x => pluginService.UpdatePlugins(x.ServerId, x.Plugins))).ConfigureAwait(false);

                // delete unused versions

                using (var context = new DatabaseFactory())
                {
                    using (var unit = new UnitOfWork(context))
                    {
                        var spondorRepo = new SponsorRepository(context);
                        var repo = new ServerRepository(context);
                        var eventRepo = new EventRepository(context, spondorRepo);
                        var tagsRepo = new TagsRepository(context);
                        var tagsService = new TagsService(tagsRepo, unit, repo, eventRepo);

                        tagsService.DeleteUnusedVersions();
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                Console.WriteLine(ex.Message);
            }
        }
    }
}
