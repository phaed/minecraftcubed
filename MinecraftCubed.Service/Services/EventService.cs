﻿using System;
using System.Collections.Generic;
using System.Linq;
using Itenso.TimePeriod;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Models.Events;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Common.Extensions;

namespace MinecraftCubed.Service.Services
{
    public class EventService : IEventService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserService _userService;
        private readonly ITagsService _tagsService;
        private readonly IServerService _serverService;
        private readonly IServerRepository _serverRepository;
        private readonly IEventRepository _repository;

        public EventService(IUserService userService, IEventRepository repository, ITagsService tagsService, IUnitOfWork unitOfWork, IServerRepository serverRepository, IServerService serverService)
        {
            _userService = userService;
            _repository = repository;
            _tagsService = tagsService;
            _unitOfWork = unitOfWork;
            _serverRepository = serverRepository;
            _serverService = serverService;
        }

        public Event AddEvent(string name, string identifier)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            if (server == null)
            {
                return null;
            }

            // create the new event

            var lobby = _serverService.GetLobbyConnectionInfo(server);

            var eve = new Event
            {
                ServerId = server.ServerId,
                Address = lobby.Address,
                VersionId = server.VersionId,
                Port = lobby.Port,
                CreationTime = DateTime.UtcNow,
                Name = name.Trim(),
                EventIdentifier = FindAvailableIdentifier(name),
                EventDescription = new EventDescription
                {
                    Text = Constant.DefaultEventDescription
                },
                PlayReq = new PlayReq
                {
                    HasDownload = lobby.PlayReq.HasDownload,
                    DownloadLabel = lobby.PlayReq.DownloadLabel,
                    DownloadUrl = lobby.PlayReq.DownloadUrl,
                    HasWhitelist = lobby.PlayReq.HasWhitelist,
                    WhitelistUrl = lobby.PlayReq.WhitelistUrl
                },
                BannerVersion = 0
            };
            
            _repository.Add(eve);
            _unitOfWork.Commit();

            // clear cache

            RedisStore.Current.UserRemove(Constant.Cache.ManagedEvents);
            return eve;
        }

        public string FindAvailableIdentifier(string name)
        {
            var id = name.ToIdentifier();

            for (var i = 2; i < 1000; i++)
            {
                var exists = _repository.ExistsIdentifier(id);

                if (!exists)
                {
                    return id;
                }

                id += i;
            }

            // should never happen

            return Guid.NewGuid().Packed();
        }

        public bool ExistsEvent(string name)
        {
            return _repository.ExistsEvent(name);
        }

        /*********************************************************************************************************************/

        public ListEvent BuildListEventDate(int eventDateId)
        {
            var eventDate = _repository.GetEventDateForList(eventDateId);
            
            var le = new ListEvent();

            var descriptionText = (new MarkdownDeep.Markdown
            {
                SafeMode = false,
                MaxImageWidth = Constant.EventBanner.Width
            
            }).Transform(eventDate.Event.EventDescription.Text);

            le.Id = eventDate.EventDateId;
            le.N = eventDate.Event.Name;
            le.AD = eventDate.Event.Address;
            le.P = eventDate.Event.Port;
            le.EI = eventDate.Event.EventIdentifier;
            le.SN = eventDate.Event.Server.Name;
            le.D = descriptionText;
            le.SI = eventDate.Event.Server.Identifier;
            le.SD = eventDate.StartDate.ToISOString();
            le.ED = eventDate.StartDate.AddHours(eventDate.LengthInHours).ToISOString();
            le.L = eventDate.LengthInHours;
            le.AC = eventDate.EventAttendees.Count;
            le.AC = eventDate.EventAttendees.Count;
            le.HD = eventDate.Event.PlayReq.HasDownload;
            le.DL = eventDate.Event.PlayReq.DownloadLabel;
            le.DU = eventDate.Event.PlayReq.DownloadUrl;
            le.HW = eventDate.Event.PlayReq.HasWhitelist;
            le.WU = eventDate.Event.PlayReq.WhitelistUrl;
            le.HR = eventDate.Event.PlayReq.HasRules;
            le.RU = eventDate.Event.PlayReq.RulesUrl;
            le.LA = eventDate.Event.Server.Language.Name;
            le.CA = eventDate.Event.Server.Country.Abbreviation.ToLower();
            le.CN = eventDate.Event.Server.Country.Name;
            le.V = eventDate.Event.Version.Name;
            le.HAW = eventDate.Event.PlayReq.HasAttendeeWhitelist;
            le.B = eventDate.Event.GetBannerUrl();
            le.F = false;
            le.BV = eventDate.Event.BannerVersion;

            return le;
        }

        public ICollection<ListEvent> GetActiveEvents(int take)
        {
            // take active events

            var ids = RedisStore.Current.GetOrStore(Constant.Cache.ActiveEventDates + take, () => _repository.GetActiveEvenDatetIds(take).ToList(), TimeSpan.FromSeconds(30), RedisDatabase.BuiltObjects);

            // if no enough pad with the most recent events

            if (ids.Count < take)
            {
                var latest = RedisStore.Current.GetOrStore(Constant.Cache.LatestEventDates + take, () => _repository.GetLatestEventDateIds(take).ToList(), TimeSpan.FromSeconds(30), RedisDatabase.BuiltObjects);

                // if still not enought, pick up dupes

                if (ids.Count + latest.Count < take)
                {
                    latest = RedisStore.Current.GetOrStore(Constant.Cache.LatestEventDatesWithDupes + take, () => _repository.GetLatesWithDupestEventDateIds(take).ToList(), TimeSpan.FromSeconds(30), RedisDatabase.BuiltObjects);
                }
                
                ids.AddRange(latest.Take(take - ids.Count));
            }

            var built = ids.Select(x => RedisStore.Current.GetOrStore(Constant.Cache.EventDate + x, () => BuildListEventDate(x), TimeSpan.FromMinutes(5), RedisDatabase.BuiltObjects)).ToList();

            // add in attendees

            foreach (var date in built)
            {
                var eventDateId = date.Id;
                date.A = string.Join(",", RedisStore.Current.GetOrStore(Constant.Cache.EventDateAttendees + eventDateId, () => _repository.GetEventDateAttendees(eventDateId), TimeSpan.FromSeconds(5), RedisDatabase.BuiltObjects));
            }

            return built;
        }

        public EventResult GetEvents(EventsState state, int skip, int take)
        {
            // convert to list events

            var ids = RedisStore.Current.GetOrStore(Constant.Cache.MatchingEventDates + state, () => _tagsService.GetMatchingEventDateIds(state).ToList(), TimeSpan.FromMinutes(5), RedisDatabase.BuiltObjects);

            var listEvents = ids.Select(x => RedisStore.Current.GetOrStore(Constant.Cache.EventDate + x, () => BuildListEventDate(x), TimeSpan.FromMinutes(5), RedisDatabase.BuiltObjects)).ToList();

            switch (state.Sort)
            {
                case "date":
                    {
                        listEvents = listEvents.OrderBy(x => x.SD).ToList();
                        break;
                    }
                case "attendees":
                    {
                        listEvents = listEvents.OrderByDescending(x => x.AC).ToList();
                        break;
                    }
            }

            // add in attendees

            foreach (var date in listEvents)
            {
                var eventDateId = date.Id;
                date.A = string.Join(",", RedisStore.Current.GetOrStore(Constant.Cache.EventDateAttendees + eventDateId, () => _repository.GetEventDateAttendees(eventDateId), TimeSpan.FromSeconds(5), RedisDatabase.BuiltObjects));
            }

            var result = new EventResult
            {
                List = listEvents.Skip(skip).Take(take).ToList(),
                TotalCount = listEvents.Select(x => x.EI).Distinct().Count()
            };

            // signal end of list when taking the last bit

            if (skip + take >= listEvents.Count)
            {
                result.EndOfList = true;
            }

            return result;
        }

        /*********************************************************************************************************************/

        public bool EditEventInfo(string eventIdentifier, string name, string identifier, string address, int port, int versionId)
        {
            var eve = _repository.GetEventByIdentifier(eventIdentifier);
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);

            if (serverId > 0)
            {
                eve.Name = name;
                eve.ServerId = serverId;
                eve.Address = address;
                eve.Port = port;
                eve.VersionId = versionId;

                _unitOfWork.Commit();
                RedisStore.Current.ClearEventCaches(eve);
                return true;
            }

            return false;
        }

        public void EditEventDescription(string eventIdentifier, string description)
        {
            var eve = _repository.GetEventByIdentifier(eventIdentifier);

            eve.EventDescription.Text = SanitationHelper.CleanHtml(description);

            _unitOfWork.Commit();
            RedisStore.Current.ClearEventCaches(eve);
        }

        public void EditEventPlayReqs(string eventIdentifier, bool hasDownload, string downloadLabel, string downloadUrl, bool hasWhitelist, string whitelistUrl, bool hasRules, string rulesUrl, bool attendeeWhitelist)
        {
            var eve = _repository.GetEventByIdentifier(eventIdentifier);

            eve.PlayReq.HasDownload = hasDownload;
            eve.PlayReq.DownloadLabel = downloadLabel;
            eve.PlayReq.DownloadUrl = downloadUrl;
            eve.PlayReq.HasWhitelist = hasWhitelist;
            eve.PlayReq.WhitelistUrl = whitelistUrl;
            eve.PlayReq.HasAttendeeWhitelist = attendeeWhitelist;
            eve.PlayReq.HasRules = hasRules;
            eve.PlayReq.RulesUrl = rulesUrl;

            _unitOfWork.Commit();
            RedisStore.Current.ClearEventCaches(eve);
        }

        /*********************************************************************************************************************/

        public bool AddEventDate(string eventIdentifier, DateTime startDate, string timeZoneId, int lengthInHours)
        {
            var eve = _repository.GetEventByIdentifier(eventIdentifier);

            // convert chosen timezone to utc
            var startDateUtc = startDate.ConvertTimeZoneToUtc(timeZoneId);

            // cannot create events in the past
            if (startDateUtc < DateTime.UtcNow)
            {
                return false;
            }

            var ed = new EventDate
            {
                EventId = eve.EventId,
                StartDate = startDateUtc,
                LengthInHours = lengthInHours
            };

            _repository.AddEventDate(ed);
            _unitOfWork.Commit();
            return true;
        }

        public bool DeleteEventDate(int eventDateId)
        {
            var isPast = _repository.IsPastEventDate(eventDateId);

            if (!isPast)
            {
                _repository.DeleteEventDate(eventDateId);
                _unitOfWork.Commit();
                return true;
            }

            return false;
        }

        // add one that checks if server is featured or not and limits the time period added, and limits the max hours per day of events can be had. 

        public bool IsEventDateOverlap(string eventIdentifier, DateTime startDate, string timeZoneId, int lengthInHours)
        {
            var eve = _repository.GetEventByIdentifier(eventIdentifier);
            var existingDates = eve.EventDates.OrderBy(x => x.StartDate).ToList();

            if (!existingDates.Any())
            {
                return false;
            }

            var newPeriod = new TimeBlock(startDate.ConvertTimeZoneToUtc(timeZoneId), Duration.Hours(lengthInHours));

            var existingPeriods = existingDates.Select(date => new TimeBlock(date.StartDate, Duration.Hours(date.LengthInHours))).ToList();

            if (existingPeriods.Any(x => x.OverlapsWith(newPeriod)))
            {
                return true;
            }

            return false;
        }

        public bool HasOpenEventDates(string eventIdentifier)
        {
            var eve = _repository.GetEventByIdentifier(eventIdentifier);

            return eve.EventDates.Any(x => x.EndDate > DateTime.UtcNow);
        }

        /*********************************************************************************************************************/

        public void DeleteEvent(string eventIdentifier)
        {
            var eve = _repository.GetEventByIdentifier(eventIdentifier);

            _repository.DeleteEvent(eve);
            _unitOfWork.Commit();
        }

        /*********************************************************************************************************************/

        public void AddAsAttendee(int eventDateId)
        {
            var ed = _repository.GetEventDate(eventDateId);
            var user = _userService.GetCurrentUserCached();
            
            if (ed != null)
            {
                if (ed.EventAttendees.Any(x => x.UserId == user.UserId))
                {
                    return;
                }

                if (user != null)
                {
                    ed.EventAttendees.Add(new EventAttendee
                    {
                        UserId = user.UserId,
                        EventDateId = eventDateId,
                        CreationTime = DateTime.UtcNow
                    });

                    _unitOfWork.Commit();
                }
            }

            RedisStore.Current.Remove(Constant.Cache.EventDateAttendees + eventDateId, RedisDatabase.BuiltObjects);
        }

        public void RemoveAsAttendee(int eventDateId)
        {
            var ed = _repository.GetEventDate(eventDateId);
            var user = _userService.GetCurrentUserCached();

            if (ed != null)
            {
                if (!ed.EventAttendees.Any(x => x.UserId == user.UserId))
                {
                    return;
                }

                if (user != null)
                {
                    var ea = ed.EventAttendees.FirstOrDefault(x => x.UserId == user.UserId);

                    if (ea != null)
                    {
                        _repository.DeleteEventAttendee(ea);
                    }

                    _unitOfWork.Commit();
                }
            }

            RedisStore.Current.Remove(Constant.Cache.EventDateAttendees + eventDateId, RedisDatabase.BuiltObjects);
        }

        /*********************************************************************************************************************/

        public EventDate GetEventDate(int eventDateId)
        {
            return _repository.GetEventDate(eventDateId);
        }

        public ICollection<EventDate> GetVisibleEventDates(string eventIdentifier)
        {
            var eve = _repository.GetEventByIdentifier(eventIdentifier);
            
            // show all events on the database that have not ended yet

            var visible = eve.EventDates
                .Where(x => x.EndDate > DateTime.UtcNow)
                .OrderBy(x => x.StartDate).ToList();

            return visible;
        }

        public void ClearAllEventCaches()
        {
            _repository.GetAllEvents().ForEachWithIndex((e, i) =>
            {
                RedisStore.Current.ClearEventCaches(e);
            });
        }
    }
}
