﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MinecraftCubed.Common.Models;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Models.Backend;

namespace MinecraftCubed.Service.Services.Interfaces
{
    public interface IMiniGameService
    {
        Task CreateMiniGame(string identifier, string name, string description, string mediaUrl, string websiteUrl);
        List<ServerMiniGameData> GetServerMiniGamesData(string identifier);
        Task<bool> SaveMiniGameLogo(string identifier, string mediaUrl, string minigameIdentifier);
        IEnumerable<object> FindMiniGamesDropdown(string term);
        void ReorderMiniGames(string ids, string identifier);
        void RefreshMiniGames(string identifier);
        void DeleteServerMiniGame(string identifier, int id);
        void DeleteUnusedMiniGames();
        bool ServerHasMiniGame(string identifier, string miniGameName);
        void RecutAllImages();
    }
}
