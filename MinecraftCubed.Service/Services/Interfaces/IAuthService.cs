﻿using System.Threading.Tasks;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Service.Services.Interfaces
{
    public interface IAuthService
    {
        User LogIn(string userOrEmail, string password);
        void LogOut();
    }
}