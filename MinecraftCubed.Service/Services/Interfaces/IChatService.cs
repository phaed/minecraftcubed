﻿using System.Collections.Generic;
using MinecraftCubed.Domain.Context;

namespace MinecraftCubed.Service.Services.Interfaces
{
    public interface IChatService
    {
        List<string> AddChatLine(string identifier, string username, string message);
        bool DeleteChatLine(string identifier, string username, int id);
        List<List<string>> GetChat(Server server, int skip, int take);
    }
}
