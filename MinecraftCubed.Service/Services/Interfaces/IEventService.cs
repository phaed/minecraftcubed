﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models.Events;

namespace MinecraftCubed.Service.Services.Interfaces
{
    public interface IEventService
    {
        Event AddEvent(string name, string serverName);
        bool ExistsEvent(string name);
        ListEvent BuildListEventDate(int eventDateId);
        ICollection<ListEvent> GetActiveEvents(int take);
        EventResult GetEvents(EventsState filters, int skip, int take);

        bool EditEventInfo(string eventIdentifier, string name, string identifier, string address, int port, int versionId);
        void EditEventDescription(string eventIdentifier, string description);
        void EditEventPlayReqs(string eventIdentifier, bool hasDownload, string downloadLabel, string downloadUrl, bool hasWhitelist, string whitelistUrl, bool hasRules, string rulesUrl, bool attendeeWhitelist);

        bool AddEventDate(string eventIdentifier, DateTime startDate, string timeZoneId, int lengthInHours);
        bool DeleteEventDate(int eventDateId);

        void DeleteEvent(string eventIdentifier);
        bool IsEventDateOverlap(string eventIdentifier, DateTime startDate, string timeZoneId, int lengthInHours);
        bool HasOpenEventDates(string eventIdentifier);

        void AddAsAttendee(int eventDateId);
        void RemoveAsAttendee(int eventDateId);

        EventDate GetEventDate(int eventDateId);
        ICollection<EventDate> GetVisibleEventDates(string eventIdentifier);

        void ClearAllEventCaches();
    }
}
