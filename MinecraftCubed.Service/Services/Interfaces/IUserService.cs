﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MinecraftCubed.Static;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models;

namespace MinecraftCubed.Service.Services.Interfaces
{
    public interface IUserService
    {
        User CreateUser(string email, string username, string password, string uuid, bool verified);
        User GetCurrentUser();
        User GetCurrentUserCached();
        User GetUser(string usernameOrEmail);
        User GetUserByEmail(string email);
        bool ExistsEmail(string email);
        bool ExistsUsername(string username);
        bool ExistsUsernameIgnoreUnverified(string username);
        void MakeRoomFromUnverifiedDupe(string username);
        bool ExistsUUID(string uuid);
        string RenewConfirmationCode(string email);
        bool VerifyConfirmationCode(string email, string code);
        void ChangeUserPassword(string email, string newPassword);
        void ChangeUserEmail(string newEmail);
        void UpdateUserSettings(UserSettings settings);
        void GiveRole(UserRole role, User user = null);
        void RemoveRole(UserRole role, User user = null);
        IEnumerable<object> FindUsers(string term);
        string GenerateAnonUsername();
        void UpdateVerifiedAccount(string username, string uuid);
        Task SaveMinecraftHead(string username);
        Task<byte[]> GetMinecraftHead(string username);
    }
}