﻿
using System.Collections.Generic;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models.Events;
using MinecraftCubed.Domain.Models.List;
using MinecraftCubed.Domain.Models.Tags;

namespace MinecraftCubed.Service.Services.Interfaces
{
    public interface ITagsService
    {
        List<CountryTagData> GetCompleteCountries();

        IEnumerable<int> GetMatchingServerIds(ListState state);
        IEnumerable<int> GetMatchingEventDateIds(EventsState state);

        List<GeneralTagData> GetAllTypes(ListState state);
        List<GeneralTagData> GetAllGameplays(ListState state);
        List<LogoTagData> GetAllMiniGames(ListState state);
        List<LogoTagData> GetAllPlugins(ListState state);
        List<GeneralTagData> GetAllVersions(ListState state);
        List<CountryTagData> GetAllCountries(ListState state);
        List<GeneralTagData> GetAllLanguages(ListState state);
        List<LogoTagData> GetAllModpacks(ListState state);
        List<GeneralTagData> GetAllHosts();

        List<GeneralTagData> GetAllEventEventGameplays(EventsState state);
        List<CountryTagData> GetAllEventCountries(EventsState state);
        List<GeneralTagData> GetAllEventLanguages(EventsState state);
        
        void CreateTypeIfNotExistsAndLink(string tag, string identifier);
        void CreateEventTagIfNotExistsAndLink(string tag, string eventIdentifier);
        void CreateGameplayIfNotExistsAndLink(string tag, string identifier);
        int CreateVersionIfNotExists(string tag);
        int CreateLanguageIfNotExists(string tag);
        int CreateHostIfNotExists(string tag);

        string GetTypesString(string identifier);
        string GetEventTagsString(string eventIdentifier);
        string GetGameplaysString(string identifier);

        void DeleteUnusedTypes();
        void DeleteUnusedEventTags();
        void DeleteUnusedGameplays();
        void DeleteUnusedVersions();
        void DeleteUnusedLanguages();
        void DeleteUnusedHosts();

        void DeleteServerTypes(string identifier);
        void DeleteEventEventTags(string eventIdentifier);
        void DeleteServerGameplays(string identifier);
    }
}
