﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MinecraftCubed.Common.Models;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Models.Backend;

namespace MinecraftCubed.Service.Services.Interfaces
{
    public interface IModpackService
    {
        Task CreateModpack(string identifier, string name, string description, string logoUrl, string websiteUrl);
        List<ServerModpackData> GetServerModpacksData(string identifier);
        Task<bool> SaveModpackLogo(string logoUrl, string modpackIdentifier);
        IEnumerable<object> FindModpacksDropdown(string term);
        bool ServerHasModpack(string identifier, string modpackName);
        void ReorderModpacks(string ids, string identifier);
        void RefreshModpacks(string identifier);
        void DeleteServerModpack(string identifier, int id);
        bool ExistsModpack(string name);
        void DeleteUnusedModpacks();
        void RecutAllImages();
    }
}
