﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Static;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Minecraft.Models;

namespace MinecraftCubed.Service.Services.Interfaces
{
    public interface IQueryService
    {
        Task<QueryResponse> PingServer(ServerConnectInfo server, Timeout timeout = Timeout.FifteenSeconds);
        Task<QueryResponse> QueryServer(ServerConnectInfo server, Timeout timeout = Timeout.ThirtySeconds);
    }
}
