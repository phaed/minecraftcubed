﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Models.List;
using MinecraftCubed.Domain.Models.Page;
using MinecraftCubed.Domain.Models.Page.Datas;
using MinecraftCubed.Domain.Models.Responses;

namespace MinecraftCubed.Service.Services.Interfaces
{
    public interface IServerService
    {
        Server AddServer(string name, string address, int port, int hostId, string version, bool enabled, string downloadLabel, string downloadUrl, bool hasDownload, string whitelistUrl, bool hasWhitelist);
        bool ExistsServer(string address, int port);
        bool ExistsServer(string identifier);
        string FindAvailableIdentifier(string name);
        ICollection<ServerConnectInfo> GetServerConnectInfos();
        ServerConnectInfo GetServerConnectInfo(string identifier);
        int GetServerIdFromIdentifier(string identifier);
        string GetIdentifierFromServerId(int serverId);
        Server GetServerByIdentifier(string identifier);
        void DeleteServer(Server server);
        
        bool EditServerInfo(string identifier, string name, int versionId, int hostId);
        void EditLocalization(string identifier, string countryName, int languageId);
        void EditQueryInfo(string identifier, string queryAddress, int queryPort);
        void EditDescription(string identifier, string description);
        void EditColors(string identifier, string header, string background, string middle, bool isDarkBackground, bool isDarkHeader, bool isDarkMiddle);
        void EditImageShuffle(string identifier, bool shuffle);
        void EditVideosFirst(string identifier, bool first);
        void EditWebsites(string identifier, string url1, string title1, string url2, string title2, string url3, string title3, string url4, string title4);
        void EditEnabled(string identifier, bool enabled);
        void EditPluginShow(string identifier, bool show);
        void EditCarouselCaptions(string identifier, ICollection<ValueIdModel> urls);
        void EditVideoCaptions(string identifier, ICollection<ValueIdModel> urls);
        void EditVotifier(string identifier, string votifierAddress, int votifierPort, string votifierPublicKey);
        void EditLogoTransparentBottomSetting(string identifier, bool has);

        ListServer BuildListServer(int serverId);
        PageServer BuildPageServer(string identifier);
        PageData BuildPageData(string identifier);
        ConnectionInfo GetLobbyConnectionInfo(Server server);
        ICollection<string> BuildPingServer(Server server);

        ListResult GetServers(ListState state, int skip, int take);
        ICollection<ICollection<string>> GetOrganicServersForPing();


        IEnumerable<NameData> GetServerTypesNames(Server server);
        IEnumerable<NameData> GetGameplaysNames(Server server);
        IEnumerable<MiniGameData> GetMiniGameData(Server server);
        IEnumerable<PluginData> GetPluginData(Server server);
        IEnumerable<ModpackData> GetModpackData(Server server);
        IEnumerable<StaffData> GetStaffData(Server server);
        object GetTagGraphData(string identifier);

        int[] GetUptimeData(Server server, int days);
        int[] GetPlayerCounts(Server server, int days, bool skipToday = false);
        int GetPeakPlayerCount(Server server);
        int GetLowestPlayerCount(Server server);
        string GetBusiestTime(Server server);
        int GetOnlinePlayerCount(Server server);
        bool IsOffline(Server server);
        string GetPlayerCountsString(Server server, int days, bool skipToday = false);
        string GetUptimeDataString(Server server, int days);

        string ChangeApiKey(string identifier);

        
        string GetServerLogoUrl(Server server);
        string GetServerBannerUrl(Server server);
        string GetServerFeaturedBannerUrl(Server server);
        void ClearAllServerCaches();
    }
}
