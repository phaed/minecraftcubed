﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MinecraftCubed.Common.Models;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Models.Backend;

namespace MinecraftCubed.Service.Services.Interfaces
{
    public interface IPluginService
    {
        Task UpdatePlugins(int serverId, HashSet<string> plugins);
        Task<bool> UpdatePluginsStatus(string identifier, int userId, List<ServerPluginData> plugins);
        List<ServerPluginData> GetServerPluginsData(string identifier);
        Task<Plugin> PullPluginData(string name);
        Task<bool> SavePluginLogo(LogoData data);
        Task DeletePluginLogo(string name);
        Task RecutAllImages();
    }
}
