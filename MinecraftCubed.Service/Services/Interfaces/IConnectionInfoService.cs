﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models.Backend;
using MinecraftCubed.Domain.Models.Page;

namespace MinecraftCubed.Service.Services.Interfaces
{
    public interface IConnectionInfoService
    {
         void CreateConnectionInfo(string identifier, string name, string address, int port, string downloadLabel, string downloadUrl, bool hasDownload, string whitelistUrl, bool hasWhitelist);
        List<ServerConnectionInfoData> GetServerConnectionInfosData(string identifier);
        bool ExistsConnectionInfo(string name, string address, int port);
        void ReorderConnectionInfos(string ids, string identifier);
        void RefreshConnectionInfos(string identifier);
        void DeleteConnectionInfo(string identifier, int id);

    }
}
