﻿using System.Collections.Generic;
using System.Web.Mvc;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models.Stub;

namespace MinecraftCubed.Service.Services.Interfaces
{
    public interface IStubService
    {
        Server GetServerFromStub(string accessCode);
        void CreateStub(int serverId, string contact);
        void ClaimStub(string accessCode);
        IEnumerable<Stub> GetStubs();
        IEnumerable<Stub> GetUnclaimedStubs();
        IEnumerable<Event> GetStubEvents();
        void DeleteStubAndServer(int stubId);
        ICollection<SelectListItem> GetUnclaimedStubServers();
        ICollection<UserStubs> GetStubEditors();
    }
}
