﻿using System.Collections.Generic;
using System.Web.Mvc;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Models.Backend;
using MinecraftCubed.Domain.Models.Responses;

namespace MinecraftCubed.Service.Services.Interfaces
{
    public interface IStaffService
    {
        void AddStaff(string identifier, string username, bool isAdmin, bool isManager, string rank, string color);
        void UpdateStaff(string identifier, string username, bool isAdmin, bool isManager, string rank, string color, string quote);
        void UpdateQuote(string identifier, string username, string quote);
        void DeleteStaff(string identifier, string username);
        IEnumerable<ServerStaff> GetStaff(string identifier);
        IEnumerable<ServerStaffData> GetStaffDatas(string identifier);
        ServerStaff GetStaff(string identifier, string username);
        bool IsServerManager(string identifier, string username);
        bool IsServerAdmin(string identifier, string username);
        bool IsServerStaff(string identifier, string username);
        bool IsAnyServerManager(string username);
        bool IsAnyServerAdmin(string username);
        int GetAdminCount(string identifier);
        bool IsStaff(string identifier, string username);
        bool IsLastAdmin(string identifier, string username);
        Server GetManagedServerByIdentifier(string identifier);
        Event GetManagedEventByIdentifier(string identifier);
        Server GetManagedServerByIdentifierOrFirst(string identifier);
        Event GetManagedEventByIdentifierOrFirst(string identifier);
        Server GetFirstManagedServer();
        ICollection<Server> GetManagedServers();
        ICollection<Event> GetManagedEvents();
        bool AdminsServer(string identifier);
        bool StaffsServer(string identifier);
        bool ManagesServer(string identifier);
        bool ManagesEvent(string identifier);
        ICollection<SelectListItem> GetManagedServersList();
        ICollection<NameAndIdentifier> GetManagedServerList();
        ICollection<NameAndIdentifier> GetManagedEventList();

        void RefreshStaff(string identifier);
        void ReorderStaff(string ids, string identifier);
        string GetColor(string identifier, string username);
    }
}
