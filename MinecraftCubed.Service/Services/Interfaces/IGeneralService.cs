﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MinecraftCubed.Service.Services.Interfaces
{
    public interface IGeneralService
    {
        ICollection<SelectListItem> GetTimeZoneList();
    }
}
