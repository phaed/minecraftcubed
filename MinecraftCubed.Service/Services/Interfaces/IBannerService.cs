﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MinecraftCubed.Domain.Models.Responses;

namespace MinecraftCubed.Service.Services.Interfaces
{
    public interface IBannerService
    {
        Task<bool> AddServerLogo(string identifier, string ext, Stream stream);
        Task<bool> AddBanner(string identifier, string ext, Stream stream);
        Task<bool> AddFeaturedBanner(string identifier, string ext, Stream stream);
        Task<bool> AddHero(string identifier, Stream stream);
        Task<bool> AddEventBanner(string eventIdentifier, string ext, Stream stream);
        void AddVideo(string identifier, string url);

        int GetHeroCount(string identifier);

        IEnumerable<ValueIdModel> GetHeroUrls(string identifier);
        IEnumerable<ValueIdModel> GetVideoUrls(string identifier);
        IEnumerable<ValueIdModel> GetThumbnailUrls(string identifier);

        Task<bool> DeleteServerLogo(string identifier);
        Task<bool> DeleteBanner(string identifier);
        Task<bool> DeleteFeaturedBanner(string identifier);
        Task<bool> DeleteEventBanner(string eventIdentifier);
        Task<bool> DeleteHero(string identifier, int heroId, string key);
        void DeleteVideo(string identifier, int id);
        
        void ReorderHeros(string ids, string identifier);
        void ReorderVideos(string ids, string identifier);

        void RefreshHeroes(string identifier);
        void RefreshVideos(string identifier);
    }
}
