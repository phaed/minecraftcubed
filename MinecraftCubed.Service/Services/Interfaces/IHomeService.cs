﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MinecraftCubed.Domain.Models.Home;

namespace MinecraftCubed.Service.Services.Interfaces
{
    public interface IHomeService
    {
        Task<ICollection<FaceData>> GetFaceUrlsForHero();
    }
}
