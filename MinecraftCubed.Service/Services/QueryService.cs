﻿using System;
using System.Threading.Tasks;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Static;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Minecraft;
using MinecraftCubed.Minecraft.Models;
using MinecraftCubed.Service.Services.Interfaces;

namespace MinecraftCubed.Service.Services
{
    public class QueryService : IQueryService
    {
        private readonly IGeneralRepository _generalRepository;
        private readonly ISponsorRepository _sponsorRepository;
        private readonly ITagsService _tagsService;
        private readonly IServerRepository _serverRepository;
        private readonly IUnitOfWork _unitOfWork;

        public QueryService(IGeneralRepository generalRepository, ISponsorRepository sponsorRepository, IServerRepository serverRepository, IUnitOfWork unitOfWork, ITagsService tagsService)
        {
            _generalRepository = generalRepository;
            _sponsorRepository = sponsorRepository;
            _serverRepository = serverRepository;
            _unitOfWork = unitOfWork;
            _tagsService = tagsService;
        }

        public async Task<QueryResponse> PingServer(ServerConnectInfo info, Timeout timeout)
        {
            try
            {
                // try ping

                var resp = await StatusQuery.PingAsync(info.Address, info.Port, timeout).ConfigureAwait(false);

                if (resp != null)
                {
                    // only testing, return result

                    if (info.ServerId == 0)
                    {
                        return resp;
                    }

                    resp.ServerId = info.ServerId;

                    _generalRepository.AddServerPing(new ServerPing
                    {
                        CreationTime = DateTime.UtcNow,
                        IsOnline = true,
                        PlayersOnline = resp.PlayersOnline,
                        ServerId = info.ServerId,
                    });

                    _unitOfWork.Commit();
                    Console.WriteLine("[" + info.Address + "] Ping done.");

                    return resp;
                }

                // fail

                _generalRepository.AddServerPing(new ServerPing
                {
                    CreationTime = DateTime.UtcNow,
                    IsOnline = false,
                    PlayersOnline = 0,
                    ServerId = info.ServerId,
                });

                _unitOfWork.Commit();
                Console.WriteLine("[" + info.Address + "] Offline.");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("attempt failed"))
                {
                    Console.WriteLine("[" + info.Address + "] Failed.");
                }
                else
                {
                    Console.WriteLine("[" + info.Address + "] " + ex.Message);
                }
            }
            return new QueryResponse
            {
                Success = false
            };
        }

        public async Task<QueryResponse> QueryServer(ServerConnectInfo info, Timeout timeout)
        {
            try
            {
                var resp = await StatusQuery.QueryAsync(info.QueryInfoAddress, info.QueryInfoPort, timeout).ConfigureAwait(false);

                // no server no donut

                var s = _serverRepository.GetServerById(info.ServerId);

                if (resp != null)
                {
                    // only testing, return result

                    if (info.ServerId == 0)
                    {
                        return resp;
                    }

                    resp.ServerId = info.ServerId;

                    if (s == null)
                    {
                        return new QueryResponse
                        {
                            Success = false
                        };
                    }

                    // create version if new

                    var version = _tagsService.CreateVersionIfNotExists(resp.Version);

                    // save server info

                    s.VersionId = version;
                    if (s.HasQueryDisabled)
                    {
                        s.HasQueryDisabled = false;
                    }
                    _unitOfWork.Commit();

                    // delete unused versions

                    _tagsService.DeleteUnusedVersions();

                    return resp;
                }

                // has query disabled

                s.HasQueryDisabled = true;
                _unitOfWork.Commit();
                Console.WriteLine("[" + info.Address + "] Query done.");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("attempt failed"))
                {
                    Console.WriteLine("[" + info.Address + "] Failed.");
                }
                else
                {
                    Console.WriteLine("[" + info.Address + "] " + ex.Message);

                }
            }

            return new QueryResponse
            {
                Success = false
            };
        }
    }
}
