﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Static;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;

namespace MinecraftCubed.Service.Services
{
    public class GeneralService : IGeneralService
    {
        private readonly IGeneralRepository _repository;
        private readonly IUserService _userService;
        private readonly IUnitOfWork _unitOfWork;

        public GeneralService(IGeneralRepository repository, IUnitOfWork unitOfWork, IUserService userService)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _userService = userService;
        }
        
        public ICollection<SelectListItem> GetTimeZoneList()
        {
            return RedisStore.Current.GetOrStore(Constant.Cache.TimeZoneList, () => (TimeZoneInfo.GetSystemTimeZones().Select(x => new SelectListItem
            {
                Text = HttpUtility.HtmlDecode(x.DisplayName),
                Value = x.Id
            })).ToList(), TimeSpan.FromHours(24));
        }
    }
}
