﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Models.Stub;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Static;

namespace MinecraftCubed.Service.Services
{
    public class StubService : IStubService
    {
        private readonly IStubRepository _repository;
        private readonly IUserService _userService;
        private readonly IUnitOfWork _unitOfWork;

        public StubService(IStubRepository repository, IUnitOfWork unitOfWork, IUserService userService)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _userService = userService;
        }

        public Server GetServerFromStub(string accessCode)
        {
            return _repository.GetServerFromStub(accessCode);
        }

        public void CreateStub(int serverId, string contact)
        {
            var user = _userService.GetCurrentUserCached();

            var stub = new Stub
            {
                AccessCode = EncryptionHelper.GenerateConfirmationCode(32).ToLower(),
                ServerId = serverId,
                UserId = user.UserId,
                Claimed = false,
                Contact = contact,
                CreationTime = DateTime.UtcNow
            };

            _repository.Add(stub);

            _unitOfWork.Commit();
        }

        public void ClaimStub(string accessCode)
        {
            var stub = _repository.GetStub(accessCode);

            if (stub != null)
            {
                stub.Claimed = true;
                stub.ClaimedTime = DateTime.UtcNow;
                _unitOfWork.Commit();
            }
        }

        public IEnumerable<Stub> GetStubs()
        {
            return _repository.GetAll<Stub>().OrderByDescending(x => x.CreationTime).ToList();
        }

        public IEnumerable<Stub> GetUnclaimedStubs()
        {
            return GetStubs().Where(x => x.Claimed == false);
        }

        public IEnumerable<Event> GetStubEvents()
        {
            return GetUnclaimedStubs().Select(x => x.Server).SelectMany(x => x.Events);
        }

        public void DeleteStubAndServer(int stubId)
        {
            var stub = _repository.GetById<Stub>(stubId);

            if (stub != null)
            {
                _repository.Delete(stub.Server);
                _repository.Delete(stub);
                _unitOfWork.Commit();
            }
        }

        public ICollection<SelectListItem> GetUnclaimedStubServers()
        {
            var stubs = GetUnclaimedStubs().OrderBy(x => x.Server.Name);

            return (stubs.Select(x => new SelectListItem
            {
                Text = x.Server.Name,
                Value = x.Server.Identifier
            })).ToList();
        }

        public ICollection<UserStubs> GetStubEditors()
        {
            return _repository.GetStubEditors().ToList();
        }
    }
}
