﻿using System;
using System.Web;
using System.Web.Security;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;

namespace MinecraftCubed.Service.Services
{
    public class AuthService : IAuthService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserService _userService;

        public AuthService(IUnitOfWork unitOfWork, IUserService userService)
        {
            _userService = userService;
            _unitOfWork = unitOfWork;
        }

        public User LogIn(string userOrEmail, string password)
        {
            if (String.IsNullOrEmpty(userOrEmail))
            {
                return null;
            }

            var user = _userService.GetUser(userOrEmail);

            if (user != null)
            {
                var hash = EncryptionHelper.ComputeSaltedHash(password, user.PasswordSalt);

                if (user.PasswordHash == hash)
                {
                    // if we don't have an ip for this user yet, store it now

                    user.Ip = UserHelper.GetIp();
                    user.LastLogin = DateTime.UtcNow;
                    user.ConfirmationCode = string.Empty;
                    _unitOfWork.Commit();
                    return user;
                }
            }
            return null;
        }

        public void LogOut()
        {
            FormsAuthentication.SignOut();
            HttpContext.Current.Session.Abandon();
            RedisStore.Current.UserRemove(Constant.Cache.User);
        }
    }
}