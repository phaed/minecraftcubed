﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AutoMapper;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Models;
using MinecraftCubed.Domain.Azure;
using MinecraftCubed.Domain.Models.Backend;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Extensions;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;
using Newtonsoft.Json.Linq;

namespace MinecraftCubed.Service.Services
{
    public class PluginService : IPluginService
    {
        private readonly IPluginRepository _repository;
        private readonly IServerRepository _serverRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PluginService(IPluginRepository repository, IUnitOfWork unitOfWork, IServerRepository serverRepository)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _serverRepository = serverRepository;
        }

        public async Task UpdatePlugins(int serverId, HashSet<string> plugins)
        {
            if (plugins == null || plugins.Count == 0)
            {
                return;
            }

            // add plugins if they don't exist

            var existing = _repository.GetPlugins().ToList();
            var notExists = plugins.Where(p => !existing.Any(x => x.Name.Equals(p, StringComparison.InvariantCultureIgnoreCase))).ToList();

            foreach (var p in notExists)
            {
                _repository.Add(new Plugin
                {
                    Name = p,
                    Identifier = p.ToIdentifier()
                });
            }
            _unitOfWork.Commit();

            // get all current server plugins

            var serverPlugins = _repository.GetServerPlugins(serverId).ToList();

            // add in any new server plugin

            foreach (var plugin in plugins)
            {
                var ident = plugin.ToIdentifier();

                if (!serverPlugins.Any(x => x.Plugin.Identifier.Equals(ident, StringComparison.InvariantCultureIgnoreCase)))
                {
                    var plug = _repository.GetPluginByIdentifier(ident);

                    // give authorship if new

                    var isNew = !plug.ServerPlugins.Any(x => x.IsAuthor);

                    _repository.AddServerPlugin(serverId, new ServerPlugin
                    {
                        PluginId = plug.PluginId,
                        ServerId = serverId,
                        Enabled = true,
                        IsAuthor = isNew
                    });
                }
            }

            // delete all old server plugins that no longer show on the server

            foreach (var serverPlugin in serverPlugins)
            {
                if (!plugins.Contains(serverPlugin.Plugin.Name))
                {
                    _repository.DeleteServerPlugin(serverPlugin);
                }
            }

            _unitOfWork.Commit();

            // pull plugin data for all new plugins

            var updatedLogos = new HashSet<Plugin>();

            var plugDatas = await Task.WhenAll(notExists.Select(PullPluginData)).ConfigureAwait(false);

            if (plugDatas.Any())
            {
                // get all current server plugins again

                serverPlugins = _repository.GetServerPlugins(serverId).ToList();

                foreach (var plugData in plugDatas)
                {
                    if (plugData != null)
                    {
                        var plugin = serverPlugins.SingleOrDefault(x => x.Plugin.Identifier.Equals(plugData.Identifier, StringComparison.InvariantCultureIgnoreCase));

                        if (plugin != null)
                        {
                            if (string.IsNullOrEmpty(plugin.Plugin.Description))
                            {
                                plugin.Plugin.Description = plugData.Description;
                            }
                            if (string.IsNullOrEmpty(plugin.Plugin.LogoUrl))
                            {
                                plugin.Plugin.LogoUrl = plugData.LogoUrl;
                                updatedLogos.Add(plugin.Plugin);
                            }
                            if (string.IsNullOrEmpty(plugin.Plugin.Website))
                            {
                                plugin.Plugin.Website = plugData.Website;
                            }
                            if (string.IsNullOrEmpty(plugin.Plugin.Categories))
                            {
                                plugin.Plugin.Categories = plugData.Categories;
                            }
                        }
                    }
                }
            }

            // pull all their logo data

            var datas = await Task.WhenAll(updatedLogos.Select(x => ImageHelper.GetLogoData(x.LogoUrl, x.Identifier))).ConfigureAwait(false);

            // save logos on S3

            await Task.WhenAll(datas.Select(SavePluginLogo)).ConfigureAwait(false); 
            
            RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.Plugins);
        }

        public async Task<bool> UpdatePluginsStatus(string identifier, int userId, List<ServerPluginData> plugins)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);

            // get all current server plugins

            var serverPlugins = _repository.GetServerPlugins(serverId).ToList();

            // make sure the form hasnt been interfered with

            if (plugins.Count != serverPlugins.Count)
            {
                return false;
            }

            // give all new plugins identifiers

            foreach (var p in plugins.Where(p => string.IsNullOrEmpty(p.PluginIdentifier)))
            {
                p.PluginIdentifier = p.PluginName.ToIdentifier();
            }

            // update the changed parts

            var updatedLogos = new HashSet<ServerPluginData>();

            for (var i = 0; i < plugins.Count; i++)
            {
                var pluginData = plugins[i];
                var serverPlugin = serverPlugins.SingleOrDefault(x => x.ServerPluginId == pluginData.ServerPluginId);

                if (serverPlugin != null)
                {
                    if (Settings.RePullAllLogos)
                    {
                        updatedLogos.Add(pluginData);
                    }

                    if (serverPlugin.IsAuthor)
                    {
                        if (serverPlugin.Plugin.LogoUrl != pluginData.PluginLogoUrl)
                        {
                            updatedLogos.Add(pluginData);
                        }

                        serverPlugin.Plugin.Description = pluginData.PluginDescription;
                        serverPlugin.Plugin.Website = pluginData.PluginWebsite;
                        serverPlugin.Plugin.LogoUrl = pluginData.PluginLogoUrl;
                        serverPlugin.IsAuthor = true;

                        if (string.IsNullOrEmpty(pluginData.PluginLogoUrl) && !string.IsNullOrEmpty(pluginData.PluginLogoUrl))
                        {
                            serverPlugin.Plugin.LogoUrl = string.Empty;

                            await DeletePluginLogo(serverPlugin.Plugin.Name);
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(serverPlugin.Plugin.Description))
                        {
                            serverPlugin.Plugin.Description = pluginData.PluginDescription;
                            serverPlugin.IsAuthor = true;
                        }

                        if (string.IsNullOrEmpty(serverPlugin.Plugin.Website))
                        {
                            serverPlugin.Plugin.Website = pluginData.PluginWebsite;
                            serverPlugin.IsAuthor = true;
                        }

                        if (string.IsNullOrEmpty(serverPlugin.Plugin.LogoUrl))
                        {
                            if (serverPlugin.Plugin.LogoUrl != pluginData.PluginLogoUrl)
                            {
                                serverPlugin.Plugin.LogoUrl = pluginData.PluginLogoUrl;
                                serverPlugin.IsAuthor = true;

                                updatedLogos.Add(pluginData);
                            }
                        }
                    }

                    serverPlugin.Enabled = pluginData.Enabled;
                }
            }

            // commit all before awaiting

            _unitOfWork.Commit();

            // pull logo data in parallel

            var datas = await Task.WhenAll(updatedLogos.Select(x => ImageHelper.GetLogoData(x.PluginLogoUrl, x.PluginIdentifier))).ConfigureAwait(false);

            _unitOfWork.Commit();

            /* Force pull of all logo urls from BukGet (overrides all logo urls on db)
             * 
            if (Settings.RePullAllLogos)
            {
                // pull plugin data for all new plugins

                var plugDatas = await Task.WhenAll(serverPlugins.Select(x => PullPluginData(x.Plugin.Name))).ConfigureAwait(false);

                if (plugDatas.Any())
                {
                    // get all current server plugins again

                    serverPlugins = _repository.GetServerPlugins(serverId).ToList();

                    foreach (var plugData in plugDatas)
                    {
                        if (plugData != null)
                        {
                            var plugin = serverPlugins.SingleOrDefault( x => x.Plugin.Name.Equals(plugData.Name, StringComparison.InvariantCultureIgnoreCase));

                            if (plugin != null)
                            {
                                plugin.Plugin.LogoUrl = plugData.LogoUrl;
                            }
                        }
                    }
                }

                _unitOfWork.Commit();
            }*/

            // save logos on S3

            await Task.WhenAll(datas.Select(SavePluginLogo)).ConfigureAwait(false);

            RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.Plugins);
            return true;
        }

        public async Task DeletePluginLogo(string name)
        {
            await AzureStore.Current.Delete(Bucket.MinecraftPlugins, name.ToIdentifier() + ".png").ConfigureAwait(false);
        }

        public List<ServerPluginData> GetServerPluginsData(string identifier)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);
            var plugins = _repository.GetServerPlugins(serverId).ToList();

            var datas = Mapper.Map<List<ServerPluginData>>(plugins);

            return datas;
        }

        public async Task<Plugin> PullPluginData(string name)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constant.BukGetUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.GetAsync(name.ToLower());

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    var o = JObject.Parse(content);

                    // save the data to db

                    if (o["slug"] != null)
                    {
                        var description = o["description"].Value<string>();
                        var logoUrl = o["logo_full"].Value<string>();
                        var website = o["website"].Value<string>();
                        var categories = string.Join(",", o["categories"].Values<string>());

                        return new Plugin
                        {
                            Name = name,
                            Description = description,
                            LogoUrl = logoUrl,
                            Website = website,
                            Categories = categories
                        };
                    }
                }
            }

            return null;
        }

        public async Task<bool> SavePluginLogo(LogoData data)
        {
            try
            {
                if (data != null && data.Stream != null)
                {
                    var s = ImageHelper.AutoSize(data.Stream, Constant.TipLogo.Width, Constant.TipLogo.Height);

                    if (s == null)
                    {
                        return false;
                    }

                    await AzureStore.Current.Store(Bucket.MinecraftPlugins, data.ObjectIdentifier + ".png", s).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError("Problem saving plugin logo", ex);
            }

            return true;
        }

        public async Task RecutAllImages()
        {
            var all = _repository.GetPlugins().ToList();

            var datas = await Task.WhenAll(all.Select(x => ImageHelper.GetLogoData(x.LogoUrl, x.Identifier))).ConfigureAwait(false);

            foreach (var logoData in datas)
            {
                await SavePluginLogo(logoData);
            }
        }
    }
}