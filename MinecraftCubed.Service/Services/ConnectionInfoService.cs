﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Models;
using MinecraftCubed.Domain.Models.Backend;
using MinecraftCubed.Domain.Models.Page;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Extensions;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;

namespace MinecraftCubed.Service.Services
{
    public class ConnectionInfoService : IConnectionInfoService
    {
        private readonly IConnectionInfoRepository _repository;
        private readonly IServerRepository _serverRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ConnectionInfoService(IConnectionInfoRepository repository, IUnitOfWork unitOfWork, IServerRepository serverRepository)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _serverRepository = serverRepository;
        }

        public void CreateConnectionInfo(string identifier, string name, string address, int port, string downloadLabel, string downloadUrl, bool hasDownload, string whitelistUrl, bool hasWhitelist)
        {
            // make identifier

            var ip = name.ToIdentifier() + address;

            if (port != 25565)
            {
                ip += "-" + port;
            }

            ip = ip.ToIdentifier();

            // add

            var server = _serverRepository.GetServerByIdentifier(identifier);
            
            server.ServerConnectionInfos.Add(new ServerConnectionInfo
            {
                Position = _repository.GetNextConnectionInfoPosition(server.ServerId),
                ConnectionInfo = new ConnectionInfo
                {
                    Identifier = ip,
                    Name = name.Trim(),
                    Address = address.Trim().ToLower(),
                    Port = port,
                    PlayReq = new PlayReq
                    {
                        HasWhitelist = hasWhitelist,
                        WhitelistUrl = whitelistUrl,
                        HasDownload = hasDownload,
                        DownloadUrl = downloadUrl,
                        DownloadLabel = downloadLabel,
                        HasAttendeeWhitelist = false,
                        HasRules = false
                    }
                }
            });

            _unitOfWork.Commit();
        }

        public void DeleteConnectionInfo(string identifier, int id)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            if (server.ServerConnectionInfos.Any(x => x.ConnectionInfoId == id) == false)
                return;

            _repository.DeleteConnectionInfo(id);
            _unitOfWork.Commit();
        }

        public void ReorderConnectionInfos(string ids, string identifier)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            var contactInfos = server.ServerConnectionInfos;

            var positionCounter = 1;

            foreach (var idstr in ids.Trim().Split(','))
            {
                var id = int.Parse(idstr);
                var item = contactInfos.SingleOrDefault(x => x.ConnectionInfoId == id);

                if (item != null)
                {
                    item.Position = positionCounter++;
                }
            }
            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void RefreshConnectionInfos(string identifier)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            var contactInfos = server.ServerConnectionInfos;

            int pos = 1;
            foreach (var info in contactInfos)
            {
                info.Position = pos++;
            }
            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public List<ServerConnectionInfoData> GetServerConnectionInfosData(string identifier)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);
            var cis = _repository.GetServerConnectionInfos(serverId).ToList();

            var datas = Mapper.Map<List<ServerConnectionInfoData>>(cis);

            return datas;
        }

        public bool ExistsConnectionInfo(string name, string address, int port)
        {
            return _repository.ExistsConnectionInfo(name, address, port);
        }
    }
}