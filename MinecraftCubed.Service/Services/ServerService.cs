﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using MarkdownDeep;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Extensions;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Models.List;
using MinecraftCubed.Domain.Models.Page;
using MinecraftCubed.Domain.Models.Page.Datas;
using MinecraftCubed.Domain.Models.Responses;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Static;
using Omu.ValueInjecter;

namespace MinecraftCubed.Service.Services
{
    public class ServerService : IServerService
    {
        private readonly ITagsService _tagsService;
        private readonly IServerRepository _repository;
        private readonly ITagsRepository _tagsRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ServerService(IServerRepository repository, IUnitOfWork unitOfWork, ITagsRepository tagsRepository, ITagsService tagsService)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _tagsRepository = tagsRepository;
            _tagsService = tagsService;
        }

        public Server AddServer(string name, string address, int port, int hostId, string version, bool enabled, string downloadLabel, string downloadUrl, bool hasDownload, string whitelistUrl, bool hasWhitelist)
        {
            // create version if new

            var ver = _tagsService.CreateVersionIfNotExists(version);

            // create the new server

            var server = new Server
            {
                CreationTime = DateTime.UtcNow,
                Name = name.Trim(),
                Identifier = FindAvailableIdentifier(name),
                Enabled = enabled,
                VotifierInfo = new VotifierInfo
                {
                    Address = address.ToLower().Trim(),
                    Port = 8192
                },
                QueryInfo = new QueryInfo
                {
                    Address = address.ToLower().Trim(),
                    Port = 25565
                },
                HasQueryDisabled = true,
                HostId = hostId,
                VersionId = ver,
                CountryId = 232, // usa
                LanguageId = 1, //english
                ServerSetting = new ServerSetting
                {
                    ShowPlugins = true,
                    ShuffleImages = false,
                    VideosFirst = false,
                    ApiKey = EncryptionHelper.GenerateConfirmationCode(32),
                    HasLogo = false,
                    BannerVersion = 0
                },
                Description = new Description
                {
                    Text = Constant.DefaultDescription
                },
                Color = new Color
                {
                    Header = Constant.DefaultHeaderColor,
                    Background = Constant.DefaultBackgroundColor,
                    Middle = Constant.DefaultMiddle,
                    IsDarkBackground = false,
                    IsDarkHeader = false,
                    IsDarkMiddle = true,
                },
                PlayersOnline = new PlayersOnline
                {
                    LastUpdated = DateTime.UtcNow,
                    Players = string.Empty
                },
                Website = new Website()
            };

            // make ip identifier

            var ip = address;

            if (port != 25565)
            {
                ip += "-" + port;
            }

            ip = ip.ToIdentifier();

            // add lobby connection info

            server.ServerConnectionInfos.Add(new ServerConnectionInfo
            {
                ConnectionInfo = new ConnectionInfo
                {
                    Identifier = ip,
                    Name = Constant.DefaultConnectionInfoName,
                    Address = address.ToLower().Trim(),
                    Port = port,

                    PlayReq = new PlayReq
                    {
                        DownloadLabel = downloadLabel,
                        DownloadUrl = downloadUrl,
                        HasDownload = hasDownload,
                        WhitelistUrl = whitelistUrl,
                        HasWhitelist = hasWhitelist,
                        HasRules = false,
                        HasAttendeeWhitelist = false
                    },
                },
            });

            _repository.Add(server);
            _unitOfWork.Commit();

            // clear cache

            // now they manage the server
            RedisStore.Current.UserRemove(Constant.Cache.ManagedServers);
            RedisStore.Current.UserRemove(Constant.Cache.ManagedServersListItems);
            RedisStore.Current.Remove(Constant.Cache.ServerCount);
            RedisStore.Current.Remove(Constant.Cache.OrganicPingListResults);
            RedisStore.Current.Remove(Constant.Cache.MatchingServers, RedisDatabase.BuiltObjects);

            return server;
        }

        public void DeleteServer(Server server)
        {
            _repository.Delete(server.ServerSetting);
            RedisStore.Current.Remove(Constant.Cache.MatchingServers, RedisDatabase.BuiltObjects);
        }

        public bool ExistsServer(string address, int port)
        {
            return _repository.ExistsServer(address, port);
        }

        public bool ExistsServer(string identifier)
        {
            return _repository.ExistsServer(identifier);
        }

        public string FindAvailableIdentifier(string name)
        {
            var id = name.ToIdentifier();

            for (var i = 2; i < 1000; i++)
            {
                var exists = _repository.ExistsIdentifier(id);

                if (!exists)
                {
                    return id;
                }

                id += i;
            }

            // should never happen

            return Guid.NewGuid().Packed();
        }

        public ICollection<ServerConnectInfo> GetServerConnectInfos()
        {
            return _repository.GetAllServerConnectInfos().ToList();
        }

        public ServerConnectInfo GetServerConnectInfo(string identifier)
        {
            var serverId = _repository.GetServerIdFromIdentifier(identifier);

            return _repository.GetServerConnectInfo(serverId);
        }

        public Server GetServerByIdentifier(string identifier)
        {
            return _repository.GetServerByIdentifier(identifier);
        }

        public int GetServerIdFromIdentifier(string identifier)
        {
            return _repository.GetServerIdFromIdentifier(identifier);
        }

        public string GetIdentifierFromServerId(int serverId)
        {
            return _repository.GetIdentifierFromServerId(serverId);
        }

        /*********************************************************************************************************************/

        public bool EditServerInfo(string identifier, string name, int versionId, int hostId)
        {
            var server = _repository.GetServerByIdentifier(identifier);

            server.Name = name.Trim();
            server.HostId = hostId;
            server.VersionId = versionId;

            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
            return true;
        }

        public void EditQueryInfo(string identifier, string queryAddress, int queryPort)
        {
            var server = _repository.GetServerByIdentifier(identifier);

            server.QueryInfo.Address = queryAddress;
            server.QueryInfo.Port = queryPort;

            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void EditLocalization(string identifier, string countryName, int languageId)
        {
            var server = _repository.GetServerByIdentifier(identifier);

            var c = _tagsRepository.GetCountryByName(countryName);

            server.CountryId = c.CountryId;
            server.LanguageId = languageId;

            _unitOfWork.Commit();

            // clear cache

            RedisStore.Current.ClearServerCaches(identifier);
            RedisStore.Current.Remove(Constant.Cache.CountryList);
            RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.Countries);
            RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.EventCountries);
            RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.Languages);
            RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.EventLanguages);
        }

        public void EditDescription(string identifier, string description)
        {
            var server = _repository.GetServerByIdentifier(identifier);

            server.Description.Text = SanitationHelper.CleanHtml(description, allowImages: true, allowFontSizing: true);

            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void EditColors(string identifier, string header, string background, string middle, bool isDarkBackground, bool isDarkHeader, bool isDarkMiddle)
        {
            var server = _repository.GetServerByIdentifier(identifier);

            server.Color.Header = header;
            server.Color.Background = background;
            server.Color.Middle = middle;
            server.Color.IsDarkHeader = isDarkHeader;
            server.Color.IsDarkMiddle = isDarkMiddle;
            server.Color.IsDarkBackground = isDarkBackground;

            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void EditImageShuffle(string identifier, bool shuffle)
        {
            var server = _repository.GetServerByIdentifier(identifier);

            server.ServerSetting.ShuffleImages = shuffle;

            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void EditVideosFirst(string identifier, bool first)
        {
            var server = _repository.GetServerByIdentifier(identifier);

            server.ServerSetting.VideosFirst = first;

            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void EditWebsites(string identifier, string url1, string title1, string url2, string title2, string url3, string title3, string url4, string title4)
        {
            var server = _repository.GetServerByIdentifier(identifier);

            server.Website.Url1 = url1;
            server.Website.Url2 = url2;
            server.Website.Url3 = url3;
            server.Website.Url4 = url4;
            server.Website.Title1 = title1;
            server.Website.Title2 = title2;
            server.Website.Title3 = title3;
            server.Website.Title4 = title4;

            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void EditEnabled(string identifier, bool enabled)
        {
            var server = _repository.GetServerByIdentifier(identifier);

            server.Enabled = enabled;
            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
            RedisStore.Current.Remove(Constant.Cache.MatchingServers, RedisDatabase.BuiltObjects);
        }

        public void EditPluginShow(string identifier, bool show)
        {
            var server = _repository.GetServerByIdentifier(identifier);

            server.ServerSetting.ShowPlugins = show;
            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void EditCarouselCaptions(string identifier, ICollection<ValueIdModel> urls)
        {
            var server = _repository.GetServerByIdentifier(identifier);

            foreach (var url in urls)
            {
                var hero = server.Heroes.SingleOrDefault(x => x.HeroId == url.Id);

                if (hero != null)
                {
                    hero.Caption = url.Extra;
                }
            }
            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void EditVideoCaptions(string identifier, ICollection<ValueIdModel> urls)
        {
            var server = _repository.GetServerByIdentifier(identifier);

            foreach (var url in urls)
            {
                var vid = server.Videos.SingleOrDefault(x => x.VideoId == url.Id);

                if (vid != null)
                {
                    vid.Caption = url.Extra;
                }
            }
            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void EditVotifier(string identifier, string votifierAddress, int votifierPort, string votifierPublicKey)
        {
            var server = _repository.GetServerByIdentifier(identifier);

            server.VotifierInfo.Address = votifierAddress;
            server.VotifierInfo.Port = votifierPort;
            server.VotifierInfo.PublicKey = votifierPublicKey;

            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void EditLogoTransparentBottomSetting(string identifier, bool has)
        {
            var server = _repository.GetServerByIdentifier(identifier);

            server.ServerSetting.LogoTransparentBottom = has;

            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        /*********************************************************************************************************************/

        public ListServer BuildListServer(int serverId)
        {
            var server = _repository.GetServerForList(serverId);

            var online = GetOnlinePlayerCount(server);
            var peak = GetPeakPlayerCount(server);
            var lowest = GetLowestPlayerCount(server);
            var activity = peak > 0 ? ((double)online / (double)peak) * 100.0 : 0;

            var pastMonth = DateTime.UtcNow.AddDays(-30).Date;

            // only give activity if server has been online > ActivityBuffferDays

            var seq = server.ServerPings.Where(z => z.CreationTime > pastMonth && z.IsOnline && z.PlayersOnline > 0).OrderBy(x => x.CreationTime).FirstOrDefault();

            if (seq != null)
            {
                var days = DateTime.UtcNow.Subtract(seq.CreationTime).TotalDays;

                if (days < Settings.ActivityBufferDays)
                {
                    activity = -1;
                }
            }

            if (peak == lowest)
            {
                online = 0;
                peak = 0;
                activity = 0;
            }

            var lobby = GetLobbyConnectionInfo(server);

            var ls = new ListServer
            {
                Id = server.ServerId,
                I = server.Identifier,
                N = server.Name,
                Ve = server.Version.Name,
                TS = server.ServerTypes.Select(x => x.Type.Name).ToArray(),
                LA = server.Language.Name,
                O = online,
                PD = GetPlayerCounts(server, 1),
                CA = server.Country.Abbreviation.ToLower(),
                CN = server.Country.Name,
                P = peak,
                A = (int) activity,
                UD = GetUptimeData(server, 7),
                B = GetServerBannerUrl(server),
                FB = GetServerFeaturedBannerUrl(server),
                IP = lobby.GetFriendlyIp(),
                OFF = IsOffline(server),
                HD = lobby.PlayReq.HasDownload,
                DL = lobby.PlayReq.DownloadLabel,
                DU = lobby.PlayReq.DownloadUrl,
                HW = lobby.PlayReq.HasWhitelist,
                WU = lobby.PlayReq.WhitelistUrl,
                BV = server.ServerSetting.BannerVersion
            };

            return ls;
        }

        public PageServer BuildPageServer(string identifier)
        {
            var server = _repository.GetServerForPage(identifier);

            var ps = new PageServer();

            // covert markdown
            var descriptionText = (new Markdown { SafeMode = false }).Transform(server.Description.Text);

            ps.Name = server.Name;
            ps.Identifier = server.Identifier;
            ps.SubsServers = server.ServerConnectionInfos.Select(x =>
            {
                var data = new SubServerData();
                data.InjectFrom(x.ConnectionInfo);
                data.InjectFrom(x.ConnectionInfo.PlayReq);
                data.Ip = x.ConnectionInfo.GetFriendlyIp();
                return data;
            }).ToList();
            ps.LogoUrl = GetServerLogoUrl(server);
            ps.DarkBackgroundClass = server.Color.IsDarkBackground ? "dark" : "";
            ps.DarkHeaderClass = server.Color.IsDarkHeader ? "dark-header" : "";
            ps.DarkMiddleClass = server.Color.IsDarkMiddle ? "dark-middle" : "";
            ps.ShuffleImages = server.ServerSetting.ShuffleImages;
            ps.LogoTransparentBottomClass = server.ServerSetting.LogoTransparentBottom ? "transparent-bottom" : string.Empty;
            ps.VideosFirst = server.ServerSetting.VideosFirst;
            ps.DescriptionText = descriptionText;
            ps.CountryAbbreviation = server.Country.Abbreviation.ToLower();
            ps.CountryName = server.Country.Name;
            ps.LanguageName = server.Language.Name;
            ps.PlayerCountsString = GetPlayerCountsString(server, 30, skipToday: true);
            ps.UptimeDataString = GetUptimeDataString(server, 30);
            ps.BusiestTime = GetBusiestTime(server);
            ps.OnlinePlayerCount = GetOnlinePlayerCount(server);
            ps.PeakPlayerCount = GetPeakPlayerCount(server);
            ps.ShowPlugins = server.ServerSetting.ShowPlugins;
            ps.Website = server.Website.ToPoco();
            ps.HasLinks = server.Website.HasLinks();
            ps.Color = server.Color.ToPoco();
            ps.Heroes = server.Heroes.OrderBy(x => x.Position).Select(x => x.ToPoco()).ToList();
            ps.Videos = server.Videos.OrderBy(x => x.Position).Select(x => x.ToPoco()).ToList();

            if (ps.PeakPlayerCount > 0)
            {
                ps.Activity = (int)((ps.OnlinePlayerCount / (double)ps.PeakPlayerCount) * 100);
            }

            return ps;
        }

        public PageData BuildPageData(string identifier)
        {
            var server = _repository.GetServerForPage(identifier);
            var lobby = GetLobbyConnectionInfo(server);

            var pd = new PageData
            {
                ServerId = server.ServerId,
                IsManager = false,
                Identifier = server.Identifier,
                Version = server.Version.Name,
                IP = lobby.Address,
                LogoUrl = GetServerLogoUrl(server),
                Port = lobby.Port,
                ServerTypes = GetServerTypesNames(server),
                Gameplay = GetGameplaysNames(server),
                Plugins = GetPluginData(server),
                MiniGames = GetMiniGameData(server),
                Modpacks = GetModpackData(server),
                TagData = GetTagGraphData(identifier),
                Staff = GetStaffData(server)
            };

            return pd;
        }

        public ConnectionInfo GetLobbyConnectionInfo(Server server)
        {
            var info = server.ServerConnectionInfos.OrderBy(x => x.Position).FirstOrDefault();

            if (info != null)
            {
                return info.ConnectionInfo;
            }

            // if none send out an empty connectioninfo

            return new ConnectionInfo
            {
                Address = "None",
                Identifier = "None",
                Name = "Lobby",
                Port = 25565,
                PlayReq = new PlayReq
                {
                    HasDownload = false,
                    DownloadLabel = string.Empty,
                    DownloadUrl = string.Empty,
                    HasWhitelist = false,
                    WhitelistUrl = string.Empty,
                    HasRules = false,
                    HasAttendeeWhitelist = false
                }
            };
        }

        public ICollection<string> BuildPingServer(Server server)
        {
            var ci = GetLobbyConnectionInfo(server);

            var ip = ci.Address + ":" + ci.Port;

            return new List<string>
            {
                server.ServerId.ToString(CultureInfo.InvariantCulture),
                ip
            };
        }

        /*********************************************************************************************************************/

        public ListResult GetServers(ListState state, int skip, int take)
        {
            // convert to list servers

            var matchingIds = RedisStore.Current.HashGetOrStore(Constant.Cache.MatchingServers, state.ToString(), () => _tagsService.GetMatchingServerIds(state).ToList(), RedisDatabase.BuiltObjects);

            var organic = matchingIds.Select(x => RedisStore.Current.GetOrStore(Constant.Cache.BuiltServer + _repository.GetIdentifierFromServerId(x), () => BuildListServer(x), TimeSpan.FromMinutes(5), RedisDatabase.BuiltObjects)).OrderByDescending(x => x.A).ToList();

            // sort it

            switch (state.Sort)
            {
                case "online":
                    {
                        organic = organic.OrderByDescending(x => x.O).ToList();
                        break;
                    }
                case "peak":
                    {
                        organic = organic.OrderByDescending(x => x.P).ToList();
                        break;
                    }
                case "activity":
                    {
                        organic = organic.OrderByDescending(x => x.A).ToList();
                        break;
                    }
            }

            var result = new ListResult
            {
                OrganicList = organic.Skip(skip).Take(take).ToList(),
                TotalCount = organic.Count,
            };

            // signal end of list when taking the last bit

            if (skip + take >= organic.Count)
            {
                result.EndOfList = true;
            }

            return result;
        }

        public ICollection<ICollection<string>> GetOrganicServersForPing()
        {
            // convert to list servers

            var listServers = RedisStore.Current.GetOrStore(Constant.Cache.OrganicPingListResults, () =>
                _repository.GetEnabledServersForPinger().ToList().Select(BuildPingServer).ToList()
            , TimeSpan.FromMinutes(1));

            return listServers;
        } 

        /*********************************************************************************************************************/

        public IEnumerable<NameData> GetServerTypesNames(Server server)
        {
            return server.ServerTypes.OrderByDescending(x => x.Type.ServerTypes.Count).Select(x => new NameData
            {
                Name = x.Type.Name
            });
        }

        public IEnumerable<NameData> GetGameplaysNames(Server server)
        {
            return server.ServerGameplays.OrderByDescending(x => x.Gameplay.ServerGameplays.Count).Select(x => new NameData
            {
                Name = x.Gameplay.Name
            });
        }

        public IEnumerable<MiniGameData> GetMiniGameData(Server server)
        {
            return server.ServerMiniGames.OrderBy(x => x.Position).Select(x => new MiniGameData
            {
                Name = x.MiniGame.Name,
                Description = string.IsNullOrEmpty(x.Description) ? string.Empty : x.Description.Replace(Environment.NewLine, "<br />"),
                Website = x.Website ?? string.Empty,
                LogoUrl = x.GetAbsoluteUrl(),
                HasVideo = !string.IsNullOrEmpty(x.VideoUrl),
                EmbedUrl = VideoHelper.GenerateEmbedUrl(x.VideoUrl)
            });
        }

        public IEnumerable<PluginData> GetPluginData(Server server)
        {
            return server.ServerPlugins.Where(x => x.Plugin.IsComplete() && x.Enabled).OrderBy(x => x.Plugin.Name).Select(x => new PluginData
            {
                Name = x.Plugin.Name,
                Description = string.IsNullOrEmpty(x.Plugin.Description) ? string.Empty : x.Plugin.Description.Replace(Environment.NewLine, "<br />"),
                Website = x.Plugin.Website,
                LogoUrl = x.Plugin.GetAbsoluteUrl()
            });
        }

        public IEnumerable<ModpackData> GetModpackData(Server server)
        {
            return server.ServerModpacks.OrderBy(x => x.Position).Select(x => new ModpackData
            {
                Name = x.Modpack.Name,
                Description = string.IsNullOrEmpty(x.Modpack.Description) ? string.Empty : x.Modpack.Description.Replace(Environment.NewLine, "<br />"),
                Website = x.Modpack.Website ?? string.Empty,
                LogoUrl = x.Modpack.GetAbsoluteUrl()
            });
        }

        public IEnumerable<StaffData> GetStaffData(Server server)
        {
            return server.ServerStaffs.OrderBy(x => x.Position).Select(x => new StaffData
            {
                Username = x.User.Username,
                Rank = x.Rank ?? string.Empty,
                Quote = x.Quote ?? string.Empty,
                Color = x.Color ?? Constant.DefaultRankColor,
                FaceUrl = x.User.GetFaceUrl()
            });
        }

        public object GetTagGraphData(string identifier)
        {
            var server = _repository.GetServerByIdentifier(identifier);

            var types = server.ServerTypes.Select(x => new
            {
                name = x.Type.Name,
                group = "1"
            });
            var gameplays = server.ServerGameplays.Select(x => new
            {
                name = x.Gameplay.Name,
                group = "2"
            });
            var miniGames = server.ServerMiniGames.Select(x => new
            {
                name = x.MiniGame.Name,
                group = "3"
            });

            return types.Concat(gameplays).Concat(miniGames);
        }

        /*********************************************************************************************************************/

        public int[] GetUptimeData(Server server, int days)
        {
            var result = new List<int>();

            for (var i = days; i >= 1; i--)
            {
                var minusDays = (i - 1) * -1;
                var date = DateTime.UtcNow.Date.AddDays(minusDays).Date;
                var total = server.ServerPings.Count(x => x.CreationTime.Date == date);
                var online = server.ServerPings.Count(x => (x.CreationTime.Date == date) && x.IsOnline);
                var bar = (int)((online / (double)Math.Max(total, 1)) * 100);
                result.Add(bar);
            }

            return result.ToArray();
        }
        public string GetUptimeDataString(Server server, int days)
        {
            return string.Join(",", GetUptimeData(server, days));
        }

        public int[] GetPlayerCounts(Server server, int days, bool skipToday = false)
        {
            var minusDays = days * -1;

            List<int> pings;

            if (days == 1)
            {
                pings = server.ServerPings.Where(x => x.CreationTime > DateTime.UtcNow.Date.AddDays(minusDays).Date)
                        .GroupBy(x => new { x.CreationTime.DayOfYear, x.CreationTime.Hour })
                        .Select(g => g.Max(s => s.PlayersOnline)).ToList();
            }
            else
            {
                var results = server.ServerPings.Where(x => x.CreationTime > DateTime.UtcNow.Date.AddDays(minusDays).Date);

                if (skipToday)
                {
                    results = results.Where(x => x.CreationTime < DateTime.UtcNow.Date);
                }

                pings = results.GroupBy(x => x.CreationTime.DayOfYear).Select(g => (int)g.Max(s => s.PlayersOnline)).ToList();
            }

            if (!pings.Any())
            {
                return new[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            }

            return pings.ToArray();
        }

        public int GetPeakPlayerCount(Server server)
        {
            var pastMonth = DateTime.UtcNow.AddDays(-30).Date;

            var seq = server.ServerPings.Where(z => z.CreationTime > pastMonth && z.IsOnline && z.PlayersOnline > 0).OrderByDescending(x => x.PlayersOnline).FirstOrDefault();

            if (seq != null)
            {
                return seq.PlayersOnline;
            }

            return 0;
        }

        public int GetLowestPlayerCount(Server server)
        {
            var pastMonth = DateTime.UtcNow.AddDays(-30).Date;

            var seq = server.ServerPings.Where(z => z.CreationTime > pastMonth && z.IsOnline && z.PlayersOnline > 0).OrderBy(x => x.PlayersOnline).FirstOrDefault();

            if (seq != null)
            {
                return seq.PlayersOnline;
            }

            return 0;
        }

        public string GetBusiestTime(Server server)
        {
            // TODO: Test
            var pastMonth = DateTime.UtcNow.AddDays(-30).Date;

            var seq = server.ServerPings
                .Where(z => z.CreationTime > pastMonth && z.IsOnline && z.PlayersOnline > 0)
                .GroupBy(x => x.CreationTime.Hour)
                .Select(g => new { Hour = g.Key, Count = g.Average(x => x.PlayersOnline) })
                .OrderByDescending(x => x.Count).FirstOrDefault();

            if (seq != null)
            {
                var dateNow = DateTime.UtcNow;
                var time = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, seq.Hour, 0, 0);
                return time.ToISOString();
            }

            return string.Empty;
        }

        public int GetOnlinePlayerCount(Server server)
        {
            if (server.ServerPings.Count == 0)
                return 0;

            return server.ServerPings.OrderByDescending(z => z.CreationTime).First().PlayersOnline;
        }

        public bool IsOffline(Server server)
        {
            if (server.ServerPings.Count == 0)
                return true;

            return !server.ServerPings.OrderByDescending(z => z.CreationTime).First().IsOnline;
        }

        public string GetPlayerCountsString(Server server, int days, bool skipToday = false)
        {
            return string.Join(",", GetPlayerCounts(server, days, skipToday));
        }

        public string ChangeApiKey(string identifier)
        {
            var server = _repository.GetServerByIdentifier(identifier);

            server.ServerSetting.ApiKey = EncryptionHelper.GenerateConfirmationCode(32);

            _unitOfWork.Commit();

            return server.ServerSetting.ApiKey;
        }

        public string GetServerLogoUrl(Server server)
        {
            if (!server.ServerSetting.HasLogo)
            {
                return null;
            }

            return Constant.AzureStorageUrl + Bucket.MinecraftServerLogos + "/" + server.Identifier + ".png";
        }

        public string GetServerBannerUrl(Server server)
        {
            if (!server.ServerSetting.HasBanner)
            {
                return null;
            }

            return Constant.AzureStorageUrl + Bucket.MinecraftBanners + "/" + server.Identifier + ".png";
        }

        public string GetServerFeaturedBannerUrl(Server server)
        {
            if (!server.ServerSetting.HasFeaturedBanner)
            {
                return null;
            }

            return Constant.AzureStorageUrl + Bucket.MinecraftFeaturedBanners + "/" + server.Identifier + ".png";
        }

        public void ClearAllServerCaches()
        {
            _repository.GetAllServers().ForEachWithIndex((s, i) =>
            {
                RedisStore.Current.ClearServerCaches(s.Identifier);
            });
        }
    }
}
