﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Domain.Azure;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Static;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Models.Responses;
using MinecraftCubed.Domain.Repositories;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Common.Extensions;

namespace MinecraftCubed.Service.Services
{
    public class BannerService : IBannerService
    {
        private readonly IBannerRepository _repository;
        private readonly IEventRepository _eventRepository;
        private readonly IServerRepository _serverRepository;
        private readonly IUnitOfWork _unitOfWork;

        public BannerService(IBannerRepository repository, IUnitOfWork unitOfWork, IServerRepository serverRepository, IEventRepository eventRepository)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _serverRepository = serverRepository;
            _eventRepository = eventRepository;
        }
        public async Task<bool> AddServerLogo(string identifier, string ext, Stream stream)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            var name = identifier + ext;

            var success = await AzureStore.Current.Store(Bucket.MinecraftServerLogos, name, stream).ConfigureAwait(false);

            if (success)
            {
                server.ServerSetting.HasLogo = true;
                _unitOfWork.Commit();

                RedisStore.Current.ClearServerCaches(identifier);
                return true;
            }

            return false;
        }

        public async Task<bool> AddBanner(string identifier, string ext, Stream stream)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            var name = identifier + ext;

            var success = await AzureStore.Current.Store(Bucket.MinecraftBanners, name, stream).ConfigureAwait(false);

            if (success)
            {
                server.ServerSetting.HasBanner = true;
                server.ServerSetting.BannerVersion = server.ServerSetting.BannerVersion + 1;
                _unitOfWork.Commit();

                RedisStore.Current.ClearServerCaches(identifier);
                return true;
            }

            return false;
        }

        public async Task<bool> AddFeaturedBanner(string identifier, string ext, Stream stream)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            var name = identifier + ext;

            var success = await AzureStore.Current.Store(Bucket.MinecraftFeaturedBanners, name, stream).ConfigureAwait(false);

            if (success)
            {
                server.ServerSetting.HasFeaturedBanner = true;
                server.ServerSetting.BannerVersion = server.ServerSetting.BannerVersion + 1;
                _unitOfWork.Commit();

                RedisStore.Current.ClearServerCaches(identifier);
                return true;
            }

            return false;
        }

        public async Task<bool> AddEventBanner(string eventIdentifier, string ext, Stream stream)
        {
            var eve = _eventRepository.GetEventByIdentifier(eventIdentifier);

            var name = eventIdentifier + ext;

            var success = await AzureStore.Current.Store(Bucket.MinecraftEventBanners, name, stream).ConfigureAwait(false);

            if (success)
            {
                eve.HasBanner = true;
                eve.BannerVersion = eve.BannerVersion + 1;
                _unitOfWork.Commit();

                RedisStore.Current.ClearEventCaches(eve);
                return true;
            }

            return false;
        }
        
        public async Task<bool> AddHero(string identifier, Stream stream)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);
            var nextNumber = GetNextHeroNumber(serverId);

            var name = identifier + "-" + nextNumber + ".jpg";

            var success = await AzureStore.Current.Store(Bucket.MinecraftHeroes, name, stream).ConfigureAwait(false);

            if (success)
            {
                var hero = new Hero
                {
                    ServerId = serverId,
                    Filename = name,
                    Position = _repository.GetNextHeroPosition(serverId)
                };

                _repository.Add(hero);
                _unitOfWork.Commit();

                RedisStore.Current.ClearServerCaches(identifier);
                return true;
            }

            return false;
        }

        public void AddVideo(string identifier, string url)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);

            var video = new Video
            {
                ServerId = serverId,
                Url = url,
                Position = _repository.GetNextVideoPosition(serverId)
            };

            _repository.Add(video);
            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        /*********************************************************************************************************************/

        private int GetNextHeroNumber(int serverId)
        {
            var banners = _repository.GetHeroes(serverId);

            var numbers = new List<int>();

            foreach (var banner in banners)
            {
                var last = banner.Filename.Split('-').Last();
                var number = last.Remove(last.IndexOf('.'));

                numbers.Add(Int32.Parse(number));
            }

            return Enumerable.Range(1, Int32.MaxValue).Except(numbers).First();
        }

        /*********************************************************************************************************************/
        
        public int GetHeroCount(string identifier)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);
            return _repository.GetHeroCount(serverId);
        }

        /*********************************************************************************************************************/
        
        public IEnumerable<ValueIdModel> GetHeroUrls(string identifier)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            if (server != null)
            {
                return server.Heroes.OrderBy(x => x.Position).Select(x => new ValueIdModel
                {
                    Value = x.GetAbsoluteUrl(),
                    Id = x.HeroId,
                    Extra = x.Caption
                });
            }

            return new List<ValueIdModel>();
        }


        public IEnumerable<ValueIdModel> GetVideoUrls(string identifier)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            if (server != null)
            {
                return server.Videos.OrderBy(x => x.Position).Select(x => new ValueIdModel
                {
                    Value = x.Url,
                    Id = x.VideoId,
                    Extra = x.Caption
                });
            }

            return new List<ValueIdModel>();
        }

        public IEnumerable<ValueIdModel> GetThumbnailUrls(string identifier)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            if (server != null)
            {
                return server.Videos.OrderBy(x => x.Position).Select(x => new ValueIdModel
                {
                    Value = VideoHelper.GetThumbnailUrl(x.Url),
                    Id = x.VideoId,
                    Extra = x.Caption
                });
            }

            return new List<ValueIdModel>();
        }

        /*********************************************************************************************************************/

        public async Task<bool> DeleteServerLogo(string identifier)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);
            server.ServerSetting.HasLogo = false;
            _unitOfWork.Commit();

            await AzureStore.Current.Delete(Bucket.MinecraftServerLogos, identifier + ".png").ConfigureAwait(false);

            RedisStore.Current.ClearServerCaches(identifier);
            return true;
        }

        public async Task<bool> DeleteBanner(string identifier)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);
            server.ServerSetting.HasBanner = false;
            _unitOfWork.Commit();

            await AzureStore.Current.Delete(Bucket.MinecraftBanners, identifier + ".png").ConfigureAwait(false);

            RedisStore.Current.ClearServerCaches(identifier);
            return true;
        }

        public async Task<bool> DeleteFeaturedBanner(string identifier)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);            
            server.ServerSetting.HasFeaturedBanner = false;
            _unitOfWork.Commit();

            await AzureStore.Current.Delete(Bucket.MinecraftFeaturedBanners, identifier + ".png").ConfigureAwait(false);

            RedisStore.Current.ClearServerCaches(identifier);
            return true;
        }
        
        public async Task<bool> DeleteEventBanner(string eventIdentifier)
        {
            var eve = _eventRepository.GetEventByIdentifier(eventIdentifier);

            eve.HasBanner = false;
            _unitOfWork.Commit();

            await AzureStore.Current.Delete(Bucket.MinecraftEventBanners, eventIdentifier + ".png").ConfigureAwait(false);

            RedisStore.Current.ClearEventCaches(eve);
            return true;
        }

        public async Task<bool> DeleteHero(string identifier, int id, string key)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            if (server.Heroes.Any(x => x.HeroId == id) == false)
                return false;

            _repository.DeleteHero(id);
            _unitOfWork.Commit();

            var cleanKey = key.Replace(Constant.AzureStorageUrl + Bucket.MinecraftHeroes + "/", "");

            await AzureStore.Current.Delete(Bucket.MinecraftHeroes, cleanKey).ConfigureAwait(false);

            RedisStore.Current.ClearServerCaches(identifier);
            return true;
        }

        public void DeleteVideo(string identifier, int id)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            if (server.Videos.Any(x => x.VideoId == id) == false)
                return;

            _repository.DeleteVideo(id);
            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }
        

        /*********************************************************************************************************************/

        public void ReorderHeros(string ids, string identifier)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);

            var banners = _repository.GetHeroes(serverId);

            var positionCounter = 1;

            foreach (var idstr in ids.Trim().Split(','))
            {
                var id = int.Parse(idstr);
                var item = banners.SingleOrDefault(x => x.HeroId == id);

                if (item != null)
                {
                    item.Position = positionCounter++;
                }
            }
            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void ReorderVideos(string ids, string identifier)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);

            var banners = _repository.GetVideos(serverId);

            var positionCounter = 1;

            foreach (var idstr in ids.Trim().Split(','))
            {
                var id = int.Parse(idstr);
                var item = banners.SingleOrDefault(x => x.VideoId == id);

                if (item != null)
                {
                    item.Position = positionCounter++;
                }
            }
            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }
        
        /*********************************************************************************************************************/

        public void RefreshHeroes(string identifier)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);

            var banners = _repository.GetHeroes(serverId);

            int pos = 1;
            foreach (var banner in banners)
            {
                banner.Position = pos++;
            }
            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void RefreshVideos(string identifier)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);

            var banners = _repository.GetVideos(serverId);

            int pos = 1;
            foreach (var banner in banners)
            {
                banner.Position = pos++;
            }
            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }
    }
}
