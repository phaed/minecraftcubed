﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Models;
using MinecraftCubed.Domain.Azure;
using MinecraftCubed.Domain.Models.Backend;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Extensions;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;

namespace MinecraftCubed.Service.Services
{
    public class ModpackService : IModpackService
    {
        private readonly IModpackRepository _repository;
        private readonly IServerRepository _serverRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ModpackService(IModpackRepository repository, IUnitOfWork unitOfWork, IServerRepository serverRepository)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _serverRepository = serverRepository;
        }

        public async Task CreateModpack(string identifier, string name, string description, string logoUrl, string websiteUrl)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            var modpackIdentifier = name.ToIdentifier();
            var mg = _repository.FindModpack(modpackIdentifier);

            if (mg == null)
            {
                mg = new Modpack
                {
                    Name = name.Trim(),
                    Identifier = modpackIdentifier,
                    Description = description == null ? null : description.Trim(),
                    LogoUrl = logoUrl,
                    Website = websiteUrl == null ? null : websiteUrl.Trim(),
                };
            }

            server.ServerModpacks.Add(new ServerModpack
            {
                Position = _repository.GetNextModpackPosition(server.ServerId),
                IsAuthor = true,
                Modpack = mg
            });

            await SaveModpackLogo(logoUrl, modpackIdentifier).ConfigureAwait(false);

            _unitOfWork.Commit();
            RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.Modpacks);
            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void DeleteServerModpack(string identifier, int id)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            if (server.ServerModpacks.Any(x => x.ServerModpackId == id) == false)
                return;

            var item = server.ServerModpacks.FirstOrDefault(x => x.ServerModpackId == id);

            if (item != null)
            {
                _repository.Delete(item);
            }

            _unitOfWork.Commit();
            RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.Modpacks);
            RedisStore.Current.ClearServerCaches(identifier);
        }
        
        public void RefreshModpacks(string identifier)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            var contactInfos = server.ServerModpacks;

            int pos = 1;
            foreach (var info in contactInfos)
            {
                info.Position = pos++;
            }
            _unitOfWork.Commit();
            RedisStore.Current.ClearServerCaches(identifier);
        }

        public bool ServerHasModpack(string identifier, string modpackName)
        {
            var modpackIdentifier = modpackName.ToIdentifier();
            
            var pack = _repository.FindModpack(modpackIdentifier);

            if (pack == null)
            {
                return false;
            }

            return pack.ServerModpacks.Any(x => x.Server.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));
        }

        public void ReorderModpacks(string ids, string identifier)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            var contactInfos = server.ServerModpacks;

            var positionCounter = 1;

            foreach (var idstr in ids.Trim().Split(','))
            {
                var id = int.Parse(idstr);
                var item = contactInfos.SingleOrDefault(x => x.ServerModpackId == id);

                if (item != null)
                {
                    item.Position = positionCounter++;
                }
            }
            _unitOfWork.Commit();
            RedisStore.Current.ClearServerCaches(identifier);
        }

        public List<ServerModpackData> GetServerModpacksData(string identifier)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);
            var modpacks = _repository.GetServerModpacks(serverId).ToList();

            var datas = Mapper.Map<List<ServerModpackData>>(modpacks);

            return datas;
        }

        public IEnumerable<object> FindModpacksDropdown(string term)
        {
            return _repository.FindModpacks(term).Select(x => new
            {
                x.Name,
                Value = x.Identifier
            });
        }

        public async Task<bool> SaveModpackLogo(string logoUrl, string modpackIdentifier)
        {
            var stream = await ImageHelper.DownloadImage(logoUrl).ConfigureAwait(false);

            try
            {
                if (stream != null)
                {
                    var s = ImageHelper.AutoSize(stream, Constant.ModpackLogo.Width, Constant.ModpackLogo.Height);
                    
                    if (s == null)
                    {
                        return false;
                    }

                    await AzureStore.Current.Store(Bucket.MinecraftModpacks, modpackIdentifier + ".png", s).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError("Problem saving modpack logo", ex);
            }

            return true;
        }

        public bool ExistsModpack(string name)
        {
            return _repository.ExistsModpack(name);
        }

        public void DeleteUnusedModpacks()
        {
            _repository.DeleteUnusedModpacks();
            _unitOfWork.Commit();
        }

        public void RecutAllImages()
        {
            var all = _repository.GetModpacks().ToList();
            
            Parallel.ForEach(all, x => {

                if(!string.IsNullOrEmpty(x.LogoUrl))
                {
                    SaveModpackLogo(x.LogoUrl, x.Identifier);
                }
            });
        }
    }
}