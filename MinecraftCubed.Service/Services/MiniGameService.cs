﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Models;
using MinecraftCubed.Domain.Azure;
using MinecraftCubed.Domain.Models.Backend;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Extensions;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;

namespace MinecraftCubed.Service.Services
{
    public class MiniGameService : IMiniGameService
    {
        private readonly IMiniGameRepository _repository;
        private readonly IServerRepository _serverRepository;
        private readonly IUnitOfWork _unitOfWork;

        public MiniGameService(IMiniGameRepository repository, IUnitOfWork unitOfWork, IServerRepository serverRepository)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _serverRepository = serverRepository;
        }

        public async Task CreateMiniGame(string identifier, string name, string description, string mediaUrl, string websiteUrl)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            var minigameIdentifier = name.ToIdentifier();
            var mg = _repository.FindMiniGame(minigameIdentifier);

            if (mg == null)
            {
                mg = new MiniGame
                {
                    Name = name.Trim(),
                    Identifier = minigameIdentifier
                };
            }

            var serverMinigame = new ServerMiniGame
            {
                Position = _repository.GetNextMiniGamePosition(server.ServerId),
                Description = description.Trim(),
                MiniGame = mg,
                Website = websiteUrl == null ? string.Empty : websiteUrl.Trim()
            };

            if (VideoHelper.IsVideoUrl(mediaUrl))
            {
                serverMinigame.VideoUrl = mediaUrl;
                serverMinigame.LogoUrl = VideoHelper.GetThumbnailUrl(mediaUrl);
            }
            else
            {
                serverMinigame.LogoUrl = mediaUrl;
            }

            server.ServerMiniGames.Add(serverMinigame);

            await SaveMiniGameLogo(identifier, serverMinigame.LogoUrl, minigameIdentifier).ConfigureAwait(false);

            _unitOfWork.Commit();
            RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.MiniGames);
            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void DeleteServerMiniGame(string identifier, int id)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            if (server.ServerMiniGames.Any(x => x.ServerMiniGameId == id) == false)
                return;

            var item = server.ServerMiniGames.FirstOrDefault(x => x.ServerMiniGameId == id);

            if (item != null)
            {
                _repository.Delete(item);
            }

            _unitOfWork.Commit();
            RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.MiniGames);
            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void RefreshMiniGames(string identifier)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            var contactInfos = server.ServerMiniGames;

            int pos = 1;
            foreach (var info in contactInfos)
            {
                info.Position = pos++;
            }
            _unitOfWork.Commit();
            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void ReorderMiniGames(string ids, string identifier)
        {
            var server = _serverRepository.GetServerByIdentifier(identifier);

            var contactInfos = server.ServerMiniGames;

            var positionCounter = 1;

            foreach (var idstr in ids.Trim().Split(','))
            {
                var id = int.Parse(idstr);
                var item = contactInfos.SingleOrDefault(x => x.ServerMiniGameId == id);

                if (item != null)
                {
                    item.Position = positionCounter++;
                }
            }
            _unitOfWork.Commit();
            RedisStore.Current.ClearServerCaches(identifier);
        }

        public List<ServerMiniGameData> GetServerMiniGamesData(string identifier)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);
            var minigames = _repository.GetServerMiniGames(serverId).ToList();

            var datas = Mapper.Map<List<ServerMiniGameData>>(minigames);

            datas.ForEach(x =>
            {
                x.HasVideo = VideoHelper.IsVideoUrl(x.LogoUrl);
                x.EmbedUrl = VideoHelper.GenerateEmbedUrl(x.LogoUrl);
            });

            datas.ForEach(x =>
            {
                x.HasVideo = VideoHelper.IsVideoUrl(x.LogoUrl);
                x.EmbedUrl = VideoHelper.GenerateEmbedUrl(x.LogoUrl);
            });


            return datas;
        }

        public IEnumerable<object> FindMiniGamesDropdown(string term)
        {
            return _repository.FindMiniGames(term).Select(x => new
            {
                x.Name,
                Value = x.Identifier
            });
        }

        public async Task<bool> SaveMiniGameLogo(string identifier, string mediaUrl, string minigameIdentifier)
        {
            var data = await ImageHelper.GetLogoData(mediaUrl).ConfigureAwait(false);

            try
            {
                if (data != null && data.Stream != null)
                {
                    var s = ImageHelper.AutoSize(data.Stream, Constant.MiniGameLogo.Width, Constant.MiniGameLogo.Height);

                    if (s == null)
                    {
                        return false;
                    }

                    await AzureStore.Current.Store(Bucket.MinecraftMiniGames, identifier + "-" + minigameIdentifier + ".png", s).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError("Problem saving minigame logo", ex);
                return false;
            }

            return true;
        }

        public void DeleteUnusedMiniGames()
        {
            _repository.DeleteUnusedMiniGames();
            _unitOfWork.Commit(); 
        }

        public bool ServerHasMiniGame(string identifier, string miniGameName)
        {
            var miniGameIdentifier = miniGameName.ToIdentifier();

            var mg = _repository.FindMiniGame(miniGameIdentifier);

            if (mg == null)
            {
                return false;
            }

            return mg.ServerMiniGames.Any(x => x.Server.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));
        }

        public void RecutAllImages()
        {
            var all = _repository.GetServerMiniGames().ToList();
           
            Parallel.ForEach(all, x =>
            {
                if (x.MiniGame != null)
                {
                    if (!string.IsNullOrEmpty(x.VideoUrl))
                    {
                        x.LogoUrl = VideoHelper.GetThumbnailUrl(x.VideoUrl);
                        SaveMiniGameLogo(x.Server.Identifier, x.LogoUrl, x.MiniGame.Identifier);
                    }
                }
            });
            
            _unitOfWork.Commit();
        }
    }
}