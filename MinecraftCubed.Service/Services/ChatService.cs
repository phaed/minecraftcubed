﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Extensions;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;

namespace MinecraftCubed.Service.Services
{
    public class ChatService : IChatService
    {
        private readonly IChatRepository _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserService _userService;
        private readonly IStaffService _staffService;
        private readonly IServerRepository _serverRepository;
        private readonly ISponsorRepository _sponsorRepository;

        public ChatService(IChatRepository repository, IUnitOfWork unitOfWork, IUserService userService, IServerRepository serverRepository, IStaffService staffService, ISponsorRepository sponsorRepository)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _userService = userService;
            _serverRepository = serverRepository;
            _staffService = staffService;
            _sponsorRepository = sponsorRepository;
        }

        private List<string> BuildChatLineData(string identifier, ChatLine line, User user)
        {
            var username = user.Username;
            var isAdmin = false;
            var isManager = false;
            var isStaff = false;
            var color = string.Empty;
            var rank = string.Empty;

            var staff = RedisStore.Current.GetOrStore(Constant.Cache.Staff + identifier + "_" + username, () =>
            {
                var s = _staffService.GetStaff(identifier, username);

                if (s != null)
                {
                    return s.ToPoco();
                }

                return null;

            }, TimeSpan.FromMinutes(1));
            
            if (staff != null)
            {
                isStaff = true;
                isManager = staff.IsManager;
                isAdmin = staff.IsAdmin;
                rank = staff.Rank;
                color = staff.Color;
            }

            return new List<string>
            {
                line.ChatLineId.ToString(),
                username,
                isAdmin ? "1" : "",
                isManager ? "1" : "",
                isStaff ? "1" : "",
                color,
                rank,
                line.Text,
                line.CreationTime.ToISOString()
            };
        }

        public List<string> AddChatLine(string identifier, string username, string message)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);
            var user = _userService.GetUser(username);

            if (user == null)
            {
                return null;
            }

            var line = new ChatLine
            {
                ServerId = serverId,
                UserId = user.UserId,
                CreationTime = DateTime.UtcNow,
                Text = message
            };

            _repository.Add(line);
            _unitOfWork.Commit();

            return RedisStore.Current.GetOrStore(Constant.Cache.ChatLineData + identifier + "_" + line.ChatLineId, () => BuildChatLineData(identifier, line, user), TimeSpan.FromDays(7));
        }

        public List<List<string>> GetChat(Server server, int skip, int take)
        {
            return _repository.GetChat(server.ServerId, skip, take).Select(line => RedisStore.Current.GetOrStore(Constant.Cache.ChatLineData + server.Identifier + "_" + line.ChatLineId, () => BuildChatLineData(server.Identifier, line, line.User), TimeSpan.FromDays(7))).ToList();
        }

        public bool DeleteChatLine(string identifier, string username, int id)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);
            var user = _userService.GetUser(username);

            if (user == null)
            {
                return false;
            }

            var line = _repository.GetById<ChatLine>(id);

            if (line.ServerId != serverId)
            {
                return false;
            }

            // can only delete if own or manager or sysadmin

            if (!line.User.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
            {
                var sysAdmin = user.HasRole(UserRole.SysAdmin);

                if (!sysAdmin)
                {
                    var isManager = _staffService.IsServerManager(identifier, username);

                    if (!isManager)
                    {
                        return false;
                    }
                }
            }

            _repository.Delete(line);
            _unitOfWork.Commit();
            return true;
        }
    }
}
