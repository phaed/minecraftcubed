﻿using System;
using System.Collections.Generic;
using System.Linq;
using MinecraftCubed.Domain.Models.Tags;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Extensions;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Models.Events;
using MinecraftCubed.Domain.Models.List;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;
using Type = MinecraftCubed.Domain.Context.Type;
using Version = MinecraftCubed.Domain.Context.Version;

namespace MinecraftCubed.Service.Services
{
    public class TagsService : ITagsService
    {
        private readonly ITagsRepository _repository;
        private readonly IServerRepository _serverRepository;
        private readonly IEventRepository _eventRepository;
        private readonly IUnitOfWork _unitOfWork;

        public TagsService(ITagsRepository repository, IUnitOfWork unitOfWork, IServerRepository serverRepository, IEventRepository eventRepository)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _serverRepository = serverRepository;
            _eventRepository = eventRepository;
        }

        public List<CountryTagData> GetCompleteCountries()
        {
            return RedisStore.Current.GetOrStore(Constant.Cache.CountryList, () => _repository.GetAllCountries().Select(tag => new CountryTagData
            {
                Name = tag.Name,
                Abbreviation = tag.Abbreviation
            }).ToList(), TimeSpan.FromDays(1));
        }

        /*********************************************************************************************************************/

        public IEnumerable<int> GetMatchingServerIds(ListState state)
        {
            return _serverRepository.GetMatchingEnabledServerIds(
                state.Types,
                state.Gameplays,
                state.MiniGames,
                state.Plugins,
                state.Modpacks,
                state.Version,
                state.Country,
                state.Language
            );
        }

        public IEnumerable<int> GetMatchingEventDateIds(EventsState state)
        {
            return _eventRepository.GetMatchingEventDateIds(
                state.EventGameplays,
                state.Country,
                state.Language
            );
        }

        /*********************************************************************************************************************/

        public List<GeneralTagData> GetAllTypes(ListState state)
        {
            var matchingIds = RedisStore.Current.HashGetOrStore(Constant.Cache.MatchingServers, state.ToString(), () => GetMatchingServerIds(state).ToList(), RedisDatabase.BuiltObjects);

            var matched = _serverRepository.GetServersWithType(matchingIds, state.Types).ToList();
            var items = matched.SelectMany(x => x.ServerTypes.Select(t => t.Type)).Distinct();

            if (state.Types != null)
            {
                items = items.Where(x => !state.Types.Contains(x.Name));
            }

            return items.Select(tag => new GeneralTagData
            {
                Name = tag.Name,
                Count = matched.Count(x => x.ServerTypes.Any(z => z.Type.Name.Equals(tag.Name)))
            }).OrderBy(x => x.Name).ToList();
        }

        public List<GeneralTagData> GetAllGameplays(ListState state)
        {
            var matchingIds = RedisStore.Current.HashGetOrStore(Constant.Cache.MatchingServers, state.ToString(), () => GetMatchingServerIds(state).ToList(), RedisDatabase.BuiltObjects);

            var matched = _serverRepository.GetServersWithGameplay(matchingIds, state.Gameplays).ToList();
            var items = matched.SelectMany(x => x.ServerGameplays.Select(t => t.Gameplay)).Distinct();

            if (state.Gameplays != null)
            {
                items = items.Where(x => !state.Gameplays.Contains(x.Name));
            }

            return items.Select(tag => new GeneralTagData
            {
                Name = tag.Name,
                Count = matched.Count(x => x.ServerGameplays.Any(z => z.Gameplay.Name.Equals(tag.Name)))
            }).OrderBy(x => x.Name).ToList();
        }

        public List<CountryTagData> GetAllCountries(ListState state)
        {
            var matchingIds = RedisStore.Current.HashGetOrStore(Constant.Cache.MatchingServers, state.ToString(), () => GetMatchingServerIds(state).ToList(), RedisDatabase.BuiltObjects);

            var matched = _serverRepository.GetServersWithCountry(matchingIds, state.Country).ToList();
            var items = matched.Select(x => x.Country).Distinct();

            if (!string.IsNullOrEmpty(state.Country))
            {
                items = items.Where(x => state.Country != x.Name);
            }

            return items.Select(tag => new CountryTagData
            {
                Name = tag.Name,
                Abbreviation = tag.Abbreviation,
                Count = matched.Count(x => x.Country.Name.Equals(tag.Name))
            }).OrderBy(x => x.Name).ToList();
        }

        public List<LogoTagData> GetAllPlugins(ListState state)
        {
            var matchingIds = RedisStore.Current.HashGetOrStore(Constant.Cache.MatchingServers, state.ToString(), () => GetMatchingServerIds(state).ToList(), RedisDatabase.BuiltObjects);

            var matched = _serverRepository.GetServersWithPlugin(matchingIds, state.Plugins).ToList();
            var items = matched.SelectMany(x => x.ServerPlugins.Where(z => z.Enabled && !string.IsNullOrEmpty(z.Plugin.LogoUrl) && !string.IsNullOrEmpty(z.Plugin.Description) && !string.IsNullOrEmpty(z.Plugin.Website)).Select(t => t.Plugin)).Distinct();

            if (state.Plugins != null)
            {
                items = items.Where(x => !state.Plugins.Contains(x.Name));
            }

            return items.Select(item => new LogoTagData
            {
                Name = item.Name,
                Description = item.Description,
                LogoUrl = item.GetAbsoluteUrl(),
                Count = matched.Count(x => x.ServerPlugins.Any(z => z.PluginId == item.PluginId))
            }).OrderBy(x => x.Name).ToList();
        }

        public List<LogoTagData> GetAllMiniGames(ListState state)
        {
            var matchingIds = RedisStore.Current.HashGetOrStore(Constant.Cache.MatchingServers, state.ToString(), () => GetMatchingServerIds(state).ToList(), RedisDatabase.BuiltObjects);

            var matched = _serverRepository.GetServersWithMiniGame(matchingIds, state.MiniGames).ToList();
            var items = matched.SelectMany(x => x.ServerMiniGames.Select(t => t.MiniGame)).Distinct();

            if (state.MiniGames != null)
            {
                items = items.Where(x => !state.MiniGames.Contains(x.Name));
            }

            return items.Select(item => new LogoTagData
            {
                Name = item.Name,
                Description = item.ServerMiniGames.First().Description,
                LogoUrl = item.ServerMiniGames.First().GetAbsoluteUrl(),
                Count = matched.Count(x => x.ServerMiniGames.Any(z => z.MiniGameId == item.MiniGameId))
            }).OrderBy(x => x.Name).ToList();
        }

        public List<LogoTagData> GetAllModpacks(ListState state)
        {
            var matchingIds = RedisStore.Current.HashGetOrStore(Constant.Cache.MatchingServers, state.ToString(), () => GetMatchingServerIds(state).ToList(), RedisDatabase.BuiltObjects);

            var matched = _serverRepository.GetServersWithMod(matchingIds, state.Modpacks).ToList();
            var items = matched.SelectMany(x => x.ServerModpacks.Select(t => t.Modpack)).Distinct();

            if (state.Modpacks != null)
            {
                items = items.Where(x => !state.Modpacks.Contains(x.Name));
            }

            return items.Select(item => new LogoTagData
            {
                Name = item.Name,
                Description = item.Description,
                LogoUrl = item.GetAbsoluteUrl(),
                Count = matched.Count(x => x.ServerModpacks.Any(z => z.ModpackId == item.ModpackId))
            }).OrderBy(x => x.Name).ToList();
        }

        public List<GeneralTagData> GetAllVersions(ListState state)
        {
            var matchingIds = RedisStore.Current.HashGetOrStore(Constant.Cache.MatchingServers, state.ToString(), () => GetMatchingServerIds(state).ToList(), RedisDatabase.BuiltObjects);

            var matched = _serverRepository.GetServersWithVersion(matchingIds, state.Version).ToList();
            var items = matched.Select(x => x.Version).Distinct();

            if (!string.IsNullOrEmpty(state.Version))
            {
                items = items.Where(x => state.Version != x.Name);
            }

            return items.Select(tag => new GeneralTagData
            {
                Name = tag.Name,
                Count = matched.Count(x => x.Version.Name.Equals(tag.Name))
            }).OrderBy(x => x.Name).ToList();
        }

        public List<GeneralTagData> GetAllLanguages(ListState state)
        {
            var matchingIds = RedisStore.Current.HashGetOrStore(Constant.Cache.MatchingServers, state.ToString(), () => GetMatchingServerIds(state).ToList(), RedisDatabase.BuiltObjects);

            var matched = _serverRepository.GetServersWithLanguage(matchingIds, state.Language).ToList();
            var items = matched.Select(x => x.Language).Distinct();

            if (!string.IsNullOrEmpty(state.Language))
            {
                items = items.Where(x => state.Language != x.Name);
            }

            return items.Select(tag => new GeneralTagData
            {
                Name = tag.Name,
                Count = matched.Count(x => x.Language.Name.Equals(tag.Name))
            }).OrderBy(x => x.Name).ToList();
        }

        public List<GeneralTagData> GetAllHosts()
        {
            return RedisStore.Current.GetOrStore(Constant.Cache.HostList, () => _repository.GetAllHosts().Select(host => new GeneralTagData
            {
                Name = host.Name,
                Count = 1
            }).ToList(), TimeSpan.FromDays(1));
        }

        public List<GeneralTagData> GetAllEventEventGameplays(EventsState state)
        {
            var matchingIds = RedisStore.Current.HashGetOrStore(Constant.Cache.MatchingServers, state.ToString(), () => GetMatchingEventDateIds(state).ToList(), RedisDatabase.BuiltObjects);

            var matched = _eventRepository.GetLiveEventDatesWithEventGameplay(matchingIds, state.EventGameplays).ToList();
            var items = matched.SelectMany(x => x.Event.EventEventTags.Select(t => t.EventTag)).Distinct();

            if (state.EventGameplays != null)
            {
                items = items.Where(x => !state.EventGameplays.Contains(x.Name));
            }

            return items.Select(tag => new GeneralTagData
            {
                Name = tag.Name,
                Count = matched.Where(x => x.Event.EventEventTags.Any(z => z.EventTag.Name == tag.Name)).Select(x => x.Event).Distinct().Count()
            }).OrderBy(x => x.Name).ToList();
        }

        public List<CountryTagData> GetAllEventCountries(EventsState state)
        {
            var matchingIds = RedisStore.Current.GetOrStore(Constant.Cache.MatchingEventDates + state, () => GetMatchingEventDateIds(state).ToList(), TimeSpan.FromDays(1), RedisDatabase.BuiltObjects);

            var matched = _eventRepository.GetLiveEventDatesWithCountry(matchingIds, state.Country).ToList();
            var items = matched.Select(x => x.Event.Server.Country).Distinct();

            if (!string.IsNullOrEmpty(state.Country))
            {
                items = items.Where(x => state.Country != x.Name);
            }

            return items.Select(tag => new CountryTagData
            {
                Name = tag.Name,
                Abbreviation = tag.Abbreviation,
                Count = matched.Where(x => x.Event.Server.Country.Name.Equals(tag.Name)).Select(x => x.Event).Distinct().Count()
            }).OrderBy(x => x.Name).ToList();
        }

        public List<GeneralTagData> GetAllEventLanguages(EventsState state)
        {
            var matchingIds = RedisStore.Current.GetOrStore(Constant.Cache.MatchingEventDates + state, () => GetMatchingEventDateIds(state).ToList(), TimeSpan.FromDays(1), RedisDatabase.BuiltObjects);

            var matched = _eventRepository.GetLiveEventDatesWithLanguage(matchingIds, state.Language).ToList();
            var items = matched.Select(x => x.Event.Server.Language).Distinct();

            if (!string.IsNullOrEmpty(state.Language))
            {
                items = items.Where(x => state.Language != x.Name);
            }

            return items.Select(tag => new GeneralTagData
            {
                Name = tag.Name,
                Count = matched.Where(x => x.Event.Server.Language.Name.Equals(tag.Name)).Select(x => x.Event).Distinct().Count()
            }).OrderBy(x => x.Name).ToList();
        }

        /*********************************************************************************************************************/

        public void CreateTypeIfNotExistsAndLink(string tag, string identifier)
        {
            tag = tag.ToTag();

            var type = _repository.GetTypeByName(tag);
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);

            if (type == null)
            {
                type = new Type
                {
                    Name = tag,
                    ServerTypes = new List<ServerType>
                    {
                        new ServerType
                        {
                            ServerId = serverId
                        }
                    }
                };

                _repository.Add(type);
            }
            else
            {
                _repository.Add(new ServerType
                {
                    ServerId = serverId,
                    TypeId = type.TypeId
                });
            }
            _unitOfWork.Commit();

            RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.ServerTypes);
            RedisStore.Current.ClearServerCaches(identifier);
            RedisStore.Current.Remove(Constant.Cache.MatchingServers, RedisDatabase.BuiltObjects);
        }

        public void CreateEventTagIfNotExistsAndLink(string tag, string eventIdentifier)
        {
            tag = tag.ToTag();

            var eve = _eventRepository.GetEventByIdentifier(eventIdentifier);
            var eventTag = _repository.GetEventTagByName(tag);

            if (eventTag == null)
            {
                eventTag = new EventTag
                {
                    Name = tag,
                    EventEventTags = new List<EventEventTag>
                    {
                        new EventEventTag
                        {
                            EventId = eve.EventId
                        }
                    }
                };

                _repository.Add(eventTag);
            }
            else
            {
                _repository.Add(new EventEventTag
                {
                    EventId = eve.EventId,
                    EventTagId = eventTag.EventTagId
                });
            }
            _unitOfWork.Commit();

            RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.EventTags);
            RedisStore.Current.ClearEventCaches(eve);
            RedisStore.Current.Remove(Constant.Cache.MatchingEventDates, RedisDatabase.BuiltObjects);
        }

        public void CreateGameplayIfNotExistsAndLink(string tag, string identifier)
        {
            tag = tag.ToTag();

            var gameplay = _repository.GetGameplayByName(tag);
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);

            if (gameplay == null)
            {
                gameplay = new Gameplay
                {
                    Name = tag,
                    ServerGameplays = new List<ServerGameplay>
                    {
                        new ServerGameplay
                        {
                            ServerId = serverId
                        }
                    }
                };

                _repository.Add(gameplay);
            }
            else
            {
                _repository.Add(new ServerGameplay
                {
                    ServerId = serverId,
                    GameplayId = gameplay.GameplayId
                });
            }
            _unitOfWork.Commit();

            RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.Gameplay);
            RedisStore.Current.ClearServerCaches(identifier);
            RedisStore.Current.Remove(Constant.Cache.MatchingServers, RedisDatabase.BuiltObjects);
            RedisStore.Current.Remove(Constant.Cache.MatchingEventDates, RedisDatabase.BuiltObjects);
        }

        public int CreateVersionIfNotExists(string tag)
        {
            tag = tag.ToTag();

            var found = _repository.GetVersionByName(tag);

            if (found == null)
            {
                var ver = new Version
                {
                    Name = tag
                };

                _repository.Add(ver);
                _unitOfWork.Commit();

                RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.Versions);
                RedisStore.Current.Remove(Constant.Cache.MatchingServers, RedisDatabase.BuiltObjects);

                return ver.VersionId;
            }

            return found.VersionId;
        }

        public int CreateLanguageIfNotExists(string tag)
        {
            tag = tag.ToTag();

            var found = _repository.GetLanguageByName(tag);

            if (found == null)
            {
                var lang = new Language
                {
                    Name = tag
                };

                _repository.Add(lang);
                _unitOfWork.Commit();

                RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.Languages);
                RedisStore.Current.Remove(Constant.Cache.MatchingServers, RedisDatabase.BuiltObjects);
                RedisStore.Current.Remove(Constant.Cache.MatchingEventDates, RedisDatabase.BuiltObjects);

                return lang.LanguageId;
            }

            return found.LanguageId;
        }

        public int CreateHostIfNotExists(string tag)
        {
            tag = tag.ToTag();

            var found = _repository.GetHostByName(tag);

            if (found == null)
            {
                var host = new Host
                {
                    CreationTime = DateTime.UtcNow,
                    Name = tag
                };

                _repository.Add(host);
                _unitOfWork.Commit();

                RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.Hosts);
                RedisStore.Current.Remove(Constant.Cache.MatchingServers, RedisDatabase.BuiltObjects);
                RedisStore.Current.Remove(Constant.Cache.HostList);
                return host.HostId;
            }

            return found.HostId;
        }

        /*********************************************************************************************************************/

        public string GetTypesString(string identifier)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);

            var list = _repository.GetTypes(serverId).ToList();

            return list.Any() ? string.Join(",", list.Select(x => x.Name)) : string.Empty;
        }

        public string GetEventTagsString(string eventIdentifier)
        {
            var eventId = _eventRepository.GetEventIdFromIdentifier(eventIdentifier);

            var list = _repository.GetEventTags(eventId).ToList();

            return list.Any() ? string.Join(",", list.Select(x => x.Name)) : string.Empty;
        }

        public string GetGameplaysString(string identifier)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);

            var list = _repository.GetGameplays(serverId).ToList();

            return list.Any() ? string.Join(",", list.Select(x => x.Name)) : string.Empty;
        }

        /*********************************************************************************************************************/

        public void DeleteUnusedTypes()
        {
            var found = _repository.DeleteUnusedTypes();

            if (found)
            {
                _unitOfWork.Commit();
                RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.ServerTypes);
            }
        }

        public void DeleteUnusedEventTags()
        {
            var found = _repository.DeleteUnusedEventTags();
            if (found)
            {
                _unitOfWork.Commit();
                RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.ServerTypes);
            }
        }

        public void DeleteUnusedGameplays()
        {
            var found = _repository.DeleteUnusedGameplays();
            if (found)
            {
                _unitOfWork.Commit();
                RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.Gameplay);
            }
        }

        public void DeleteUnusedVersions()
        {
            var found = _repository.DeleteUnusedVersions();
            if (found)
            {
                _unitOfWork.Commit();
                RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.Versions);
            }
        }

        public void DeleteUnusedLanguages()
        {
            var found = _repository.DeleteUnusedLanguages();
            if (found)
            {
                _unitOfWork.Commit();
                RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.Languages);
                RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.EventLanguages);
            }
        }

        public void DeleteUnusedHosts()
        {
            var found = _repository.DeleteUnusedHosts();
            if (found)
            {
                _unitOfWork.Commit();
                RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.Hosts);
            }
        }

        /*********************************************************************************************************************/

        public void DeleteServerTypes(string identifier)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);
            _repository.DeleteServerTypes(serverId);
            _unitOfWork.Commit();

            RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.ServerTypes);
            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void DeleteEventEventTags(string eventIdentifier)
        {
            var eve = _eventRepository.GetEventByIdentifier(eventIdentifier);
            _repository.DeleteEventEventTags(eve.EventId);
            _unitOfWork.Commit();

            RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.EventTags);
            RedisStore.Current.ClearEventCaches(eve);
        }

        public void DeleteServerGameplays(string identifier)
        {
            var serverId = _serverRepository.GetServerIdFromIdentifier(identifier);
            _repository.DeleteServerGameplays(serverId);
            _unitOfWork.Commit();

            RedisStore.Current.FilterTagsRemove(Constant.Cache.Filters.Gameplay);
            RedisStore.Current.ClearServerCaches(identifier);
        }
    }
}
