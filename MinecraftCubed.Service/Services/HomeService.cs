﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using MinecraftCubed.Common;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Domain.Models.Home;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Minecraft;
using MinecraftCubed.Static;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;

namespace MinecraftCubed.Service.Services
{
    public class HomeService : IHomeService
    {
        private readonly IHomeRepository _repository;
        private readonly IUserService _userService;
        private readonly IUnitOfWork _unitOfWork;

        public HomeService(IHomeRepository repository, IUnitOfWork unitOfWork, IUserService userService)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _userService = userService;
        }

        private async Task<FaceData> PullFaceDataBytes(FaceData data)
        {
            data.Bytes = await _userService.GetMinecraftHead(data.Username).ConfigureAwait(false);
            return data;
        }

        public async Task<ICollection<FaceData>> GetFaceUrlsForHero()
        {
            return await RedisStore.Current.GetOrStoreAsync(Constant.Cache.FaceData, async () =>
            {
                var users = _repository.GetLatestVisitingUsers(Constant.HeroHeadCount).ToList();
                
                var faces = users.Select(x => new FaceData
                {
                    Url = Settings.AzureStorageUrl + "minecraftheads/" + x.Username + ".png",
                    Username = x.Username,
                }).ToList();

                if (faces.Count() < Constant.HeroHeadCount)
                {
                    var need = Constant.HeroHeadCount - faces.Count();
                    var extraHeads = MinecraftUtils.GetRandomHeadNames();

                    for (var i = 0; i < need; i++)
                    {
                        var name = extraHeads[i];

                        faces.Add(new FaceData
                        {
                            Url = Settings.AzureStorageUrl + "minecraftheads/" + name + ".png",
                            Username = name,
                        });
                    }
                }

                var steve = await _userService.GetMinecraftHead("Steve").ConfigureAwait(false);

                var tasks = faces.Select(PullFaceDataBytes).ToList();

                var results = (await Task.WhenAll(tasks).ConfigureAwait(false)).ToList();

                return results.Where(data => data?.Bytes != null && !data.Bytes.SequenceEqual(steve)).ToList();

            }, TimeSpan.FromSeconds(30), RedisDatabase.General).ConfigureAwait(false);
        }
    }
}
