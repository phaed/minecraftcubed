﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using MinecraftCubed.Common;
using MinecraftCubed.Domain.Azure;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;

namespace MinecraftCubed.Service.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _repository;
        private readonly IServerRepository _serverRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork, IUserRepository repository, IServerRepository serverRepository)
        {
            _repository = repository;
            _serverRepository = serverRepository;
            _unitOfWork = unitOfWork;
        }

        public User CreateUser(string email, string username, string password, string uuid, bool verified)
        {
            // if no username verified generate anon one

            var roles = new List<Role>
            {
                _repository.GetRoleById((int) UserRole.Unverified)
            };

            if (verified)
            {
                roles.Add(_repository.GetRoleById((int)UserRole.User));
            }
            else
            {
                uuid = EncryptionHelper.GenerateConfirmationCode(32);
            }

            var user = new User
            {
                CreationTime = DateTime.UtcNow,
                LastLogin = DateTime.UtcNow,
                Email = email.ToLower(),
                Username = username,
                UUID = uuid,
                Ip = UserHelper.GetIp(),
                Roles = roles,
                PasswordSalt = EncryptionHelper.CreateRandomSalt(),
                ReceiveEventNotifications = true,
                Banned = false,
            };

            // add password if valid

            if (!string.IsNullOrEmpty(password))
            {
                user.PasswordHash = EncryptionHelper.ComputeSaltedHash(password, user.PasswordSalt);
            }

            _repository.Add(user);
            _unitOfWork.Commit();
            return user;
        }

        public async Task SaveMinecraftHead(string username)
        {
            var stream = await ImageHelper.DownloadImage("http://minotar.net/avatar/" + username + "/" + Constant.CachedHeadSize + ".png").ConfigureAwait(false);
            await AzureStore.Current.Store(AzureContainer.MinecraftHeads, username + ".png", stream).ConfigureAwait(false);
        }

        public async Task<byte[]> GetMinecraftHead(string username)
        {
            return await RedisStore.Current.GetOrStoreAsync(username, async () =>
            {
                var headBytes = await AzureStore.Current.GetBytes(AzureContainer.MinecraftHeads, username + ".png").ConfigureAwait(false);

                if (headBytes == null)
                {
                    await SaveMinecraftHead(username).ConfigureAwait(false);
                    headBytes = await AzureStore.Current.GetBytes(AzureContainer.MinecraftHeads, username + ".png").ConfigureAwait(false);
                }

                return headBytes;

            }, TimeSpan.FromDays(1), RedisDatabase.Heads).ConfigureAwait(false);
        }

        public User GetCurrentUserCached()
        {
            if (HttpContext.Current == null)
            {
                return null;
            }

            return RuntimeCache.GetOrStore(Constant.Cache.User + HttpContext.Current.User.Identity.Name, () =>
            {
                var user = _repository.GetUserByUsername(HttpContext.Current.User.Identity.Name);

                if (user != null)
                {
                    return user.ToPoco();
                }
                return null;

            }, TimeSpan.FromMinutes(5));
        }

        public User GetCurrentUser()
        {
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                return null;
            }

            return _repository.GetUserByUsername(HttpContext.Current.User.Identity.Name);
        }

        public User GetUser(string usernameOrEmail)
        {
            if (string.IsNullOrEmpty(usernameOrEmail))
            {
                return null;
            }

            if (usernameOrEmail.Contains("@"))
            {
                return _repository.GetUserByEmail(usernameOrEmail);
            }

            return _repository.GetUserByUsername(usernameOrEmail);
        }

        public User GetUserByEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return null;
            }

            return _repository.GetUserByEmail(email);
        }

        public bool ExistsEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return false;
            }

            return _repository.ExistsUserByEmail(email);
        }

        public bool ExistsUsername(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                return false;
            }

            return _repository.ExistsUserByUsername(username);
        }

        public bool ExistsUsernameIgnoreUnverified(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                return false;
            }

            return _repository.ExistsUsernameIgnoreUnverified(username);
        }

        public void MakeRoomFromUnverifiedDupe(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                return;
            }

            var dupe = _repository.FindUnverifiedDupe(username);

            if (dupe != null)
            {
                dupe.Username = _repository.GenerateAnonUsername();
            }
            _unitOfWork.Commit();
        }

        public bool ExistsUUID(string uuid)
        {
            if (string.IsNullOrEmpty(uuid))
            {
                return false;
            }

            return _repository.ExistsUserByUUID(uuid);
        }

        public string RenewConfirmationCode(string email)
        {
            var user = GetUser(email);

            if (user != null)
            {
                user.ConfirmationCode = EncryptionHelper.GenerateConfirmationCode(8);
                _unitOfWork.Commit();
                return user.ConfirmationCode;
            }
            return string.Empty;
        }

        public bool VerifyConfirmationCode(string email, string code)
        {
            var user = GetUser(email);

            if (user != null)
            {
                return user.ConfirmationCode.Equals(code, StringComparison.CurrentCultureIgnoreCase);
            }
            return false;
        }

        public void ChangeUserPassword(string email, string newPassword)
        {
            var user = GetUserByEmail(email);

            if (user != null)
            {
                user.PasswordHash = EncryptionHelper.ComputeSaltedHash(newPassword, user.PasswordSalt);
                user.ConfirmationCode = string.Empty;
                _unitOfWork.Commit();
            }

            // clear cache
            RedisStore.Current.UserRemove(Constant.Cache.User);
        }

        public void ChangeUserEmail(string newEmail)
        {
            var user = GetCurrentUser();

            if (user != null)
            {
                user.Email = newEmail;
                _unitOfWork.Commit();
            }

            // clear cache
            RedisStore.Current.UserRemove(Constant.Cache.User);
        }

        public void UpdateUserSettings(UserSettings settings)
        {
            var user = GetCurrentUser();

            if (user != null)
            {
                Mapper.Map(settings, user);
                _unitOfWork.Commit();
            }

            // clear cache
            RedisStore.Current.UserRemove(Constant.Cache.User);
        }

        public void GiveRole(UserRole role, User user = null)
        {
            if (user == null)
            {
                user = GetCurrentUser();
            }

            if (user != null)
            {
                user.Roles.Add(_repository.GetRoleById((int)role));
                _unitOfWork.Commit();
            }

            // clear cache
            RedisStore.Current.UserRemove(Constant.Cache.User);
        }

        public void RemoveRole(UserRole role, User user = null)
        {
            if (user == null)
            {
                user = GetCurrentUser();
            }
            if (user != null)
            {
                user.Roles.Remove(_repository.GetRoleById((int)role));
                _unitOfWork.Commit();
            }

            // clear cache
            RedisStore.Current.UserRemove(Constant.Cache.User);
        }

        public IEnumerable<object> FindUsers(string term)
        {
            return _repository.FindUsers(term).Select(x => new
            {
                label = x.Username,
                value = x.Username
            });
        }

        public string GenerateAnonUsername()
        {
            return _repository.GenerateAnonUsername();
        }

        public void UpdateVerifiedAccount(string username, string uuid)
        {
            var user = GetUser(username);

            if (user != null && !user.HasRole(UserRole.User))
            {
                user.Username = username;
                user.UUID = uuid.Replace("-", "");
                user.Roles.Add(_repository.GetRoleById((int)UserRole.User));

                _unitOfWork.Commit();
            }
        }
    }
}