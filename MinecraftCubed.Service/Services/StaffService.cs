﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Redis;
using MinecraftCubed.Domain.Redis.Extensions;
using MinecraftCubed.Static;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core.Interfaces;
using MinecraftCubed.Domain.Models.Backend;
using MinecraftCubed.Domain.Models.Responses;
using MinecraftCubed.Domain.Repositories.Interfaces;
using MinecraftCubed.Service.Services.Interfaces;

namespace MinecraftCubed.Service.Services
{
    public class StaffService : IStaffService
    {
        private readonly IStaffRepository _repository;
        private readonly IEventRepository _eventRepository;
        private readonly IServerRepository _serverRepository;
        private readonly IUserService _userService;
        private readonly IServerService _serverService;
        private readonly IUnitOfWork _unitOfWork;

        public StaffService(IStaffRepository repository, IUnitOfWork unitOfWork, IUserService userService, IServerService serverService, IEventRepository eventRepository, IServerRepository serverRepository)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _userService = userService;
            _serverService = serverService;
            _eventRepository = eventRepository;
            _serverRepository = serverRepository;
        }
        
        public void AddStaff(string identifier, string username, bool isAdmin, bool isManager, string rank, string color)
        {
            var user = _userService.GetUser(username);
            var server = _serverService.GetServerByIdentifier(identifier);

            _repository.Add(new ServerStaff
            {
                ServerId = server.ServerId,
                UserId = user.UserId,
                IsAdmin = isAdmin,
                IsManager = isManager,
                Rank = rank,
                Quote = string.Empty,
                Color = color,
                Position = server.ServerStaffs.Count + 1
            });

            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void UpdateStaff(string identifier, string username, bool isAdmin, bool isManager, string rank, string color, string quote)
        {
            var staff = _repository.GetStaff(identifier, username);

            staff.IsAdmin = isAdmin;
            staff.IsManager = isManager;
            staff.Rank = rank;
            staff.Quote = quote;
            staff.Color = color;

            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void UpdateQuote(string identifier, string username, string quote)
        {
            var staff = _repository.GetStaff(identifier, username);
            staff.Quote = quote;
            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void DeleteStaff(string identifier, string username)
        {
            _repository.DeleteStaff(identifier, username);
            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public IEnumerable<ServerStaff> GetStaff(string identifier)
        {
            return _repository.GetStaff(identifier);
        }

        public ServerStaff GetStaff(string identifier, string username)
        {
            return _repository.GetStaff(identifier, username);
        }

        public IEnumerable<ServerStaffData> GetStaffDatas(string identifier)
        {
            var staff = _repository.GetStaff(identifier).ToList();

            return staff.Select(x => new ServerStaffData
            {
                Username = x.User.Username,
                Color = x.Color,
                Rank = x.Rank,
                FaceUrl = x.User.GetFaceUrl(),
                IsAdmin = x.IsAdmin,
                IsManager = x.IsManager,
                Quote = x.Quote
            }).ToList();
        }

        public bool IsServerManager(string identifier, string username)
        {
            return _repository.IsServerManager(identifier, username);
        }

        public bool IsServerAdmin(string identifier, string username)
        {
            return _repository.IsServerAdmin(identifier, username);
        }

        public bool IsServerStaff(string identifier, string username)
        {
            return _repository.IsServerStaff(identifier, username);
        }

        public bool IsAnyServerManager(string username)
        {
            return _repository.IsAnyServerManager(username);
        }

        public bool IsAnyServerAdmin(string username)
        {
            return _repository.IsAnyServerAdmin(username);
        }

        public int GetAdminCount(string identifier)
        {
            return _repository.GetAdminCount(identifier);
        }

        public bool IsStaff(string identifier, string username)
        {
            var user = _userService.GetCurrentUserCached();

            if (user == null || string.IsNullOrEmpty(identifier))
            {
                return false;
            }

            var server = _serverService.GetServerByIdentifier(identifier);

            return server.ServerStaffs.Any(x => x.User.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase));
        }

        public bool IsLastAdmin(string identifier, string username)
        {
            if (_repository.GetAdminCount(identifier) > 1)
            {
                return false;
            }

            var staff = _repository.GetStaff(identifier, username);

            if (staff == null)
            {
                return false;
            }

            return staff.IsAdmin;
        }

        public Server GetManagedServerByIdentifier(string identifier)
        {
            var user = _userService.GetCurrentUserCached();

            if (user == null || string.IsNullOrEmpty(identifier))
            {
                return null;
            }

            if (user.HasRole(UserRole.Editor))
            {
                return _serverRepository.GetServerByIdentifier(identifier);
            }

            return _repository.GetManagedServerByIdentifier(user.UserId, identifier);
        }

        public Event GetManagedEventByIdentifier(string identifier)
        {
            var user = _userService.GetCurrentUserCached();

            if (user == null || string.IsNullOrEmpty(identifier))
            {
                return null;
            }

            if (user.HasRole(UserRole.Editor))
            {
                return _eventRepository.GetEventByIdentifier(identifier);
            }

            return _repository.GetManagedEventByIdentifier(user.UserId, identifier);
        }

        public Server GetManagedServerByIdentifierOrFirst(string identifier)
        {
            var user = _userService.GetCurrentUserCached();

            if (user == null || string.IsNullOrEmpty(identifier))
            {
                return null;
            }

            if (user.HasRole(UserRole.Editor))
            {
                return _serverRepository.GetServerByIdentifier(identifier);
            }

            var server = _repository.GetManagedServerByIdentifier(user.UserId, identifier);

            if (server == null)
            {
                server = _repository.GetFirstManagedServer(user.UserId);
            }

            return server;
        }

        public Event GetManagedEventByIdentifierOrFirst(string eventIdentifier)
        {
            var user = _userService.GetCurrentUserCached();

            if (user == null || string.IsNullOrEmpty(eventIdentifier))
            {
                return null;
            }

            if (user.HasRole(UserRole.Editor))
            {
                return _eventRepository.GetEventByIdentifier(eventIdentifier);
            }

            var ev = _repository.GetManagedEventByIdentifier(user.UserId, eventIdentifier);

            if (ev == null)
            {
                ev = _repository.GetFirstManagedEvent(user.UserId);
            }

            return ev;
        }

        public Server GetFirstManagedServer()
        {
            var user = _userService.GetCurrentUserCached();

            if (user == null)
            {
                return null;
            }

            return _repository.GetFirstManagedServer(user.UserId);
        }

        public ICollection<Server> GetManagedServers()
        {
            var user = _userService.GetCurrentUserCached();

            if (user == null)
            {
                return new Collection<Server>();
            }

            return _repository.GetManagedServers(user.UserId).ToList();
        }

        public ICollection<Event> GetManagedEvents()
        {
            var user = _userService.GetCurrentUserCached();

            if (user == null)
            {
                return new Collection<Event>();
            }

            return _repository.GetManagedEvents(user.UserId).ToList();
        }

        public bool AdminsServer(string identifier)
        {
            var user = _userService.GetCurrentUserCached();

            if (user == null || string.IsNullOrEmpty(identifier))
            {
                return false;
            }

            if (user.HasRole(UserRole.Editor))
            {
                return true;
            }

            return _repository.AdminsServer(user.UserId, identifier);
        }

        public bool ManagesServer(string identifier)
        {
            var user = _userService.GetCurrentUserCached();

            if (user == null || string.IsNullOrEmpty(identifier))
            {
                return false;
            }

            if (user.HasRole(UserRole.Editor))
            {
                return true;
            }

            return _repository.ManagesServer(user.UserId, identifier);
        }

        public bool StaffsServer(string identifier)
        {
            var user = _userService.GetCurrentUserCached();

            if (user == null || string.IsNullOrEmpty(identifier))
            {
                return false;
            }

            if (user.HasRole(UserRole.Editor))
            {
                return true;
            }

            return _repository.StaffsServer(user.UserId, identifier);
        }

        public bool ManagesEvent(string eventIdentifier)
        {
            var user = _userService.GetCurrentUserCached();

            if (user == null || string.IsNullOrEmpty(eventIdentifier))
            {
                return false;
            }

            if (user.HasRole(UserRole.Editor))
            {
                return true;
            }

            return _repository.ManagesEvent(user.UserId, eventIdentifier);
        }

        public ICollection<SelectListItem> GetManagedServersList()
        {
            return RedisStore.Current.UserGetOrStore(Constant.Cache.ManagedServersListItems, () =>
            { 
               var servers = GetManagedServers();

               return (servers.Select(x => new SelectListItem
               {
                   Text = x.Name,
                   Value = x.Identifier
               })).ToList();
           }, TimeSpan.FromMinutes(1));
        }

        public ICollection<NameAndIdentifier> GetManagedServerList()
        {
            return RedisStore.Current.UserGetOrStore(Constant.Cache.ManagedServers, () =>
            {
                var servers = GetManagedServers();

                return servers.Select(x => new NameAndIdentifier
                {
                    Name = x.Name,
                    Identifier = x.Identifier
                }).ToList();
            }, TimeSpan.FromMinutes(1));
        }

        public ICollection<NameAndIdentifier> GetManagedEventList()
        {
            return RedisStore.Current.UserGetOrStore(Constant.Cache.ManagedEvents, () =>
            {
                var servers = GetManagedEvents();

                return servers.Select(x => new NameAndIdentifier
                {
                    Name = x.Name,
                    Identifier = x.EventIdentifier
                }).ToList();
            }, TimeSpan.FromMinutes(1));
        }

        public void RefreshStaff(string identifier)
        {
            var staffs = _repository.GetStaff(identifier);

            int pos = 1;
            foreach (var staff in staffs)
            {
                staff.Position = pos++;
            }
            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public void ReorderStaff(string ids, string identifier)
        {
            var staffs = _repository.GetStaff(identifier);

            var positionCounter = 1;

            foreach (var idstr in ids.Trim().Split(','))
            {
                var item = staffs.SingleOrDefault(x => x.User.Username.Equals(idstr, StringComparison.InvariantCultureIgnoreCase));

                if (item != null)
                {
                    item.Position = positionCounter++;
                }
            }
            _unitOfWork.Commit();

            RedisStore.Current.ClearServerCaches(identifier);
        }

        public string GetColor(string identifier, string username)
        {
            var staff = _repository.GetStaff(identifier, username);

            if (staff != null)
            {
                return staff.Color;
            }

            return null;
        }
    }
}
