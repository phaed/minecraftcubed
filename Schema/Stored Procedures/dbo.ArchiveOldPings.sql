SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Marcelo Delgado
-- Create date: 8/29/2014
-- Description:	Archives old pings
-- =============================================

CREATE PROCEDURE [dbo].[ArchiveOldPings]	
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO ArchivedPings (ArchivedPingId, ServerId, CreationTime, PlayersOnline, IsOnline)
	SELECT ServerPingId, ServerId, CreationTime, PlayersOnline, IsOnline
	FROM dbo.ServerPings
	WHERE CreationTime <= DATEADD(mm, -1, GETDATE());

	DELETE FROM ServerPings
	WHERE ServerPingId in (SELECT ArchivedPingId FROM ArchivedPings)
END
GO
