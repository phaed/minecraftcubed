SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[AllPings]
AS
SELECT ServerId, CreationTime, PlayersOnline, IsOnline FROM  dbo.ArchivedPings
UNION ALL
SELECT ServerId, CreationTime, PlayersOnline, IsOnline  FROM  dbo.ServerPings


GO
