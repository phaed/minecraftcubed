CREATE TABLE [dbo].[ServerModpacks]
(
[ServerModpackId] [int] NOT NULL IDENTITY(1, 1),
[ServerId] [int] NOT NULL,
[ModpackId] [int] NOT NULL,
[IsAuthor] [bit] NOT NULL,
[Position] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ServerModpacks] ADD CONSTRAINT [PK_ServerModpacks] PRIMARY KEY CLUSTERED  ([ServerModpackId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerModpacks] ADD CONSTRAINT [IX_ServerModpacks_Unique] UNIQUE NONCLUSTERED  ([ServerId], [ModpackId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerModpacks] ADD CONSTRAINT [FK_ServerModpacks_Modpacks] FOREIGN KEY ([ModpackId]) REFERENCES [dbo].[Modpacks] ([ModpackId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ServerModpacks] ADD CONSTRAINT [FK_ServerModpacks_Servers] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
