CREATE TABLE [dbo].[ConnectionInfos]
(
[ConnectionInfoId] [int] NOT NULL IDENTITY(1, 1),
[Identifier] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Address] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Port] [int] NOT NULL,
[PlayReqId] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ConnectionInfos] ADD CONSTRAINT [PK_ConnectionInfos] PRIMARY KEY CLUSTERED  ([ConnectionInfoId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConnectionInfos] ADD CONSTRAINT [IX_ConnectionInfos_AddressUnique] UNIQUE NONCLUSTERED  ([Address], [Port]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConnectionInfos] ADD CONSTRAINT [IX_ConnectionInfos_Identifier] UNIQUE NONCLUSTERED  ([Identifier]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConnectionInfos] ADD CONSTRAINT [FK_ConnectionInfos_PlayReqs] FOREIGN KEY ([PlayReqId]) REFERENCES [dbo].[PlayReqs] ([PlayReqId]) ON DELETE CASCADE
GO
