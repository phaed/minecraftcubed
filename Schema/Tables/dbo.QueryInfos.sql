CREATE TABLE [dbo].[QueryInfos]
(
[QueryInfoId] [int] NOT NULL IDENTITY(1, 1),
[Address] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Port] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[QueryInfos] ADD CONSTRAINT [PK_QueryInfos] PRIMARY KEY CLUSTERED  ([QueryInfoId]) ON [PRIMARY]
GO
