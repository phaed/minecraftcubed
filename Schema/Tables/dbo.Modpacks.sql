CREATE TABLE [dbo].[Modpacks]
(
[ModpackId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Identifier] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Website] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LogoUrl] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Modpacks] ADD CONSTRAINT [PK_Modpacks] PRIMARY KEY CLUSTERED  ([ModpackId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Modpacks] ADD CONSTRAINT [IX_Modpacks_Id] UNIQUE NONCLUSTERED  ([Identifier]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Modpacks] ADD CONSTRAINT [IX_Modpacks] UNIQUE NONCLUSTERED  ([Name]) ON [PRIMARY]
GO
