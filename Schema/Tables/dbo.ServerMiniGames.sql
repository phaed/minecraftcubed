CREATE TABLE [dbo].[ServerMiniGames]
(
[ServerMiniGameId] [int] NOT NULL IDENTITY(1, 1),
[ServerId] [int] NOT NULL,
[MiniGameId] [int] NOT NULL,
[LogoUrl] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VideoUrl] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Position] [int] NOT NULL,
[Website] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ServerMiniGames] ADD CONSTRAINT [PK_ServerMiniGames] PRIMARY KEY CLUSTERED  ([ServerMiniGameId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerMiniGames] ADD CONSTRAINT [IX_ServerMiniGames_Unique] UNIQUE NONCLUSTERED  ([ServerId], [MiniGameId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerMiniGames] ADD CONSTRAINT [FK_ServerMiniGames_MiniGames] FOREIGN KEY ([MiniGameId]) REFERENCES [dbo].[MiniGames] ([MiniGameId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ServerMiniGames] ADD CONSTRAINT [FK_ServerMiniGames_Servers] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
