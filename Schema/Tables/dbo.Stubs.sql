CREATE TABLE [dbo].[Stubs]
(
[StubId] [int] NOT NULL IDENTITY(1, 1),
[ServerId] [int] NOT NULL,
[AccessCode] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserId] [int] NOT NULL,
[Claimed] [bit] NOT NULL,
[CreationTime] [datetime] NOT NULL,
[ClaimedTime] [datetime] NULL,
[Contact] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Stubs] ADD CONSTRAINT [PK_Stubs] PRIMARY KEY CLUSTERED  ([StubId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Stubs] ON [dbo].[Stubs] ([ServerId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Stubs] ADD CONSTRAINT [FK_Stubs_Stubs] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Stubs] ADD CONSTRAINT [FK_Stubs_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId]) ON DELETE CASCADE
GO
