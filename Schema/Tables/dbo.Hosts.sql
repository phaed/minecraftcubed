CREATE TABLE [dbo].[Hosts]
(
[HostId] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreationTime] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[Hosts] ADD CONSTRAINT [PK_Hosts] PRIMARY KEY CLUSTERED  ([HostId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Hosts] ON [dbo].[Hosts] ([Name]) ON [PRIMARY]
GO
