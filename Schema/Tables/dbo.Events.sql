CREATE TABLE [dbo].[Events]
(
[EventId] [int] NOT NULL IDENTITY(1, 1),
[ServerId] [int] NOT NULL,
[EventIdentifier] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreationTime] [datetime] NOT NULL,
[EventDescriptionId] [int] NOT NULL,
[PlayReqId] [int] NOT NULL,
[Address] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Port] [int] NOT NULL,
[VersionId] [int] NOT NULL,
[HasBanner] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[Events] ADD CONSTRAINT [PK_Events] PRIMARY KEY CLUSTERED  ([EventId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events] ADD CONSTRAINT [FK_Events_EventDescriptions] FOREIGN KEY ([EventDescriptionId]) REFERENCES [dbo].[EventDescriptions] ([EventDescriptionId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Events] ADD CONSTRAINT [FK_Events_PlayReqs] FOREIGN KEY ([PlayReqId]) REFERENCES [dbo].[PlayReqs] ([PlayReqId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Events] ADD CONSTRAINT [FK_Events_Servers] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Events] ADD CONSTRAINT [FK_Events_Versions] FOREIGN KEY ([VersionId]) REFERENCES [dbo].[Versions] ([VersionId])
GO
