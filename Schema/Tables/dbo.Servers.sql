CREATE TABLE [dbo].[Servers]
(
[ServerId] [int] NOT NULL IDENTITY(1, 1),
[CreationTime] [datetime] NOT NULL,
[Identifier] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Enabled] [bit] NOT NULL,
[HostId] [int] NOT NULL,
[VersionId] [int] NOT NULL,
[HasQueryDisabled] [bit] NOT NULL,
[CountryId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[DescriptionId] [int] NOT NULL,
[PlayersOnlineId] [int] NOT NULL,
[WebsitesId] [int] NOT NULL,
[QueryInfoId] [int] NOT NULL,
[ColorId] [int] NOT NULL,
[ServerSettingsId] [int] NOT NULL,
[VotifierInfoId] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[Servers] ADD CONSTRAINT [PK_Servers] PRIMARY KEY CLUSTERED  ([ServerId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Servers] ADD CONSTRAINT [IX_Servers_Identifier] UNIQUE NONCLUSTERED  ([Identifier]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Servers] ADD CONSTRAINT [FK_Servers_Colors] FOREIGN KEY ([ColorId]) REFERENCES [dbo].[Colors] ([ColorId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Servers] ADD CONSTRAINT [FK_Servers_Countries] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Countries] ([CountryId])
GO
ALTER TABLE [dbo].[Servers] ADD CONSTRAINT [FK_Servers_Descriptions] FOREIGN KEY ([DescriptionId]) REFERENCES [dbo].[Descriptions] ([DescriptionId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Servers] ADD CONSTRAINT [FK_Servers_Hosts] FOREIGN KEY ([HostId]) REFERENCES [dbo].[Hosts] ([HostId])
GO
ALTER TABLE [dbo].[Servers] ADD CONSTRAINT [FK_Servers_Languages] FOREIGN KEY ([LanguageId]) REFERENCES [dbo].[Languages] ([LanguageId])
GO
ALTER TABLE [dbo].[Servers] ADD CONSTRAINT [FK_Servers_PlayersOnline] FOREIGN KEY ([PlayersOnlineId]) REFERENCES [dbo].[PlayersOnline] ([PlayersOnlineId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Servers] ADD CONSTRAINT [FK_Servers_QueryInfos] FOREIGN KEY ([QueryInfoId]) REFERENCES [dbo].[QueryInfos] ([QueryInfoId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Servers] ADD CONSTRAINT [FK_Servers_ServerSettings] FOREIGN KEY ([ServerSettingsId]) REFERENCES [dbo].[ServerSettings] ([ServerSettingsId]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Servers] ADD CONSTRAINT [FK_Servers_Versions] FOREIGN KEY ([VersionId]) REFERENCES [dbo].[Versions] ([VersionId])
GO
ALTER TABLE [dbo].[Servers] ADD CONSTRAINT [FK_Servers_VotifierInfos] FOREIGN KEY ([VotifierInfoId]) REFERENCES [dbo].[VotifierInfos] ([VotifierInfoId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Servers] ADD CONSTRAINT [FK_Servers_Websites] FOREIGN KEY ([WebsitesId]) REFERENCES [dbo].[Websites] ([WebsiteId]) ON DELETE CASCADE
GO
