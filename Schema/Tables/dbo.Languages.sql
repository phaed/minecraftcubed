CREATE TABLE [dbo].[Languages]
(
[LanguageId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[Languages] ADD CONSTRAINT [PK_Languages] PRIMARY KEY CLUSTERED  ([LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Languages] ADD CONSTRAINT [IX_Languages] UNIQUE NONCLUSTERED  ([Name]) ON [PRIMARY]
GO
