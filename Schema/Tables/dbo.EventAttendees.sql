CREATE TABLE [dbo].[EventAttendees]
(
[EventAttendeeId] [int] NOT NULL IDENTITY(1, 1),
[EventDateId] [int] NOT NULL,
[UserId] [int] NOT NULL,
[CreationTime] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[EventAttendees] ADD CONSTRAINT [PK_EventAtendees] PRIMARY KEY CLUSTERED  ([EventAttendeeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_EventAttendee_User] ON [dbo].[EventAttendees] ([EventAttendeeId], [UserId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EventAttendees] ADD CONSTRAINT [FK_EventAttendees_Events] FOREIGN KEY ([EventDateId]) REFERENCES [dbo].[EventDates] ([EventDateId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EventAttendees] ADD CONSTRAINT [FK_EventAtendees_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId]) ON DELETE CASCADE
GO
