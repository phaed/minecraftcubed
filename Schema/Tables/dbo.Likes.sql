CREATE TABLE [dbo].[Likes]
(
[LikeId] [int] NOT NULL IDENTITY(1, 1),
[ServerId] [int] NOT NULL,
[UserId] [int] NOT NULL,
[CreationTime] [date] NOT NULL
)
GO
ALTER TABLE [dbo].[Likes] ADD CONSTRAINT [PK_Likes] PRIMARY KEY CLUSTERED  ([LikeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Likes] ADD CONSTRAINT [FK_Likes_Likes] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Likes] ADD CONSTRAINT [FK_Likes_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId]) ON DELETE CASCADE
GO
