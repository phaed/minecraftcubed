CREATE TABLE [dbo].[RawPings]
(
[RawPingId] [int] NOT NULL IDENTITY(1, 1),
[ServerId] [int] NOT NULL,
[CreationTime] [datetime] NOT NULL,
[PlayersOnline] [int] NOT NULL,
[IsOnline] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[RawPings] ADD CONSTRAINT [PK_RawPings] PRIMARY KEY CLUSTERED  ([RawPingId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RawPings] ADD CONSTRAINT [FK_RawPings_Servers] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
