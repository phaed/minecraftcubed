CREATE TABLE [dbo].[ArchivedPings]
(
[ArchivedPingId] [int] NOT NULL,
[ServerId] [int] NOT NULL,
[CreationTime] [datetime] NOT NULL,
[PlayersOnline] [int] NOT NULL,
[IsOnline] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[ArchivedPings] ADD CONSTRAINT [PK_ArchivedPings_1] PRIMARY KEY CLUSTERED  ([ArchivedPingId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ArchivedPings] ADD CONSTRAINT [FK_ArchivedPings_Servers] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
