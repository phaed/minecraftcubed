CREATE TABLE [dbo].[Versions]
(
[VersionId] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[Versions] ADD CONSTRAINT [PK_Versions] PRIMARY KEY CLUSTERED  ([VersionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Versions] ADD CONSTRAINT [IX_Versions] UNIQUE NONCLUSTERED  ([Name]) ON [PRIMARY]
GO
