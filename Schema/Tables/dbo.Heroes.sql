CREATE TABLE [dbo].[Heroes]
(
[HeroId] [int] NOT NULL IDENTITY(1, 1),
[ServerId] [int] NOT NULL,
[Filename] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Position] [int] NOT NULL,
[Caption] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Heroes] ADD CONSTRAINT [PK_Heroes] PRIMARY KEY CLUSTERED  ([HeroId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Heroes] ADD CONSTRAINT [FK_Heroes_Servers] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
