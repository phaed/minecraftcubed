CREATE TABLE [dbo].[ServerStaff]
(
[ServerStaffId] [int] NOT NULL IDENTITY(1, 1),
[ServerId] [int] NOT NULL,
[UserId] [int] NOT NULL,
[IsAdmin] [bit] NOT NULL,
[IsManager] [bit] NOT NULL,
[Rank] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Position] [int] NOT NULL,
[Quote] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Color] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ServerStaff] ADD CONSTRAINT [PK_ServerManagers] PRIMARY KEY CLUSTERED  ([ServerStaffId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerStaff] ADD CONSTRAINT [IX_ServerManagers_Both] UNIQUE NONCLUSTERED  ([UserId], [ServerId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerStaff] ADD CONSTRAINT [FK_ServerManagers_Servers] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ServerStaff] ADD CONSTRAINT [FK_ServerManagers_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId]) ON DELETE CASCADE
GO
