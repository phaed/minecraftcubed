CREATE TABLE [dbo].[ServerGameplays]
(
[ServerGameplayId] [int] NOT NULL IDENTITY(1, 1),
[ServerId] [int] NOT NULL,
[GameplayId] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ServerGameplays] ADD CONSTRAINT [PK_ServerThemes] PRIMARY KEY CLUSTERED  ([ServerGameplayId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerGameplays] ADD CONSTRAINT [IX_ServerThemes_Unique] UNIQUE NONCLUSTERED  ([ServerId], [GameplayId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerGameplays] ADD CONSTRAINT [FK_ServerGameplays_Gameplays] FOREIGN KEY ([GameplayId]) REFERENCES [dbo].[Gameplays] ([GameplayId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ServerGameplays] ADD CONSTRAINT [FK_ServerThemes_Servers] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
