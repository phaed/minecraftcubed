CREATE TABLE [dbo].[ServerPings]
(
[ServerPingId] [int] NOT NULL IDENTITY(1, 1),
[ServerId] [int] NOT NULL,
[CreationTime] [datetime] NOT NULL,
[PlayersOnline] [int] NOT NULL,
[IsOnline] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[ServerPings] ADD CONSTRAINT [PK_ServerQueries] PRIMARY KEY CLUSTERED  ([ServerPingId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ServerPings_CreationTime] ON [dbo].[ServerPings] ([CreationTime]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ServerPings_CreationTime_IsOnline] ON [dbo].[ServerPings] ([CreationTime], [IsOnline]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ServerPings_IsOnline] ON [dbo].[ServerPings] ([IsOnline]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ServerPings_PlayersOnline] ON [dbo].[ServerPings] ([PlayersOnline]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerPings] ADD CONSTRAINT [FK_ServerQueries_Servers] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
