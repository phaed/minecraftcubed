CREATE TABLE [dbo].[Descriptions]
(
[DescriptionId] [int] NOT NULL IDENTITY(1, 1),
[Text] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Descriptions] ADD CONSTRAINT [PK_Descriptions] PRIMARY KEY CLUSTERED  ([DescriptionId]) ON [PRIMARY]
GO
