CREATE TABLE [dbo].[Scores]
(
[ScoreId] [int] NOT NULL IDENTITY(1, 1),
[ServerId] [int] NOT NULL,
[Number] [int] NOT NULL,
[CreationTime] [date] NOT NULL
)
GO
ALTER TABLE [dbo].[Scores] ADD CONSTRAINT [PK_Scores] PRIMARY KEY CLUSTERED  ([ScoreId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Scores] ADD CONSTRAINT [FK_Scores_Servers] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
