CREATE TABLE [dbo].[VotifierInfos]
(
[VotifierInfoId] [int] NOT NULL IDENTITY(1, 1),
[Address] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Port] [int] NOT NULL,
[PublicKey] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[VotifierInfos] ADD CONSTRAINT [PK_VotifierInfos] PRIMARY KEY CLUSTERED  ([VotifierInfoId]) ON [PRIMARY]
GO
