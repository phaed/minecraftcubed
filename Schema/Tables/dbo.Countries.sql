CREATE TABLE [dbo].[Countries]
(
[CountryId] [int] NOT NULL IDENTITY(1, 1),
[Abbreviation] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[Countries] ADD CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED  ([CountryId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Countries] ADD CONSTRAINT [IX_Countries_Abbr] UNIQUE NONCLUSTERED  ([Abbreviation]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Countries] ADD CONSTRAINT [IX_Countries_Name] UNIQUE NONCLUSTERED  ([Name]) ON [PRIMARY]
GO
