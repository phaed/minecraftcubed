CREATE TABLE [dbo].[EventEventTags]
(
[EventEventTagId] [int] NOT NULL IDENTITY(1, 1),
[EventTagId] [int] NOT NULL,
[EventId] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[EventEventTags] ADD CONSTRAINT [PK_AssignedEventTags] PRIMARY KEY CLUSTERED  ([EventEventTagId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EventEventTags] ADD CONSTRAINT [IX_AssignedEventTags_Both] UNIQUE NONCLUSTERED  ([EventTagId], [EventId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EventEventTags] ADD CONSTRAINT [FK_AssignedEventTags_Events] FOREIGN KEY ([EventId]) REFERENCES [dbo].[Events] ([EventId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EventEventTags] ADD CONSTRAINT [FK_AssignedEventTags_EventTags] FOREIGN KEY ([EventTagId]) REFERENCES [dbo].[EventTags] ([EventTagId]) ON DELETE CASCADE
GO
