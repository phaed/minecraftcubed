CREATE TABLE [dbo].[ServerPlugins]
(
[ServerPluginId] [int] NOT NULL IDENTITY(1, 1),
[PluginId] [int] NOT NULL,
[ServerId] [int] NOT NULL,
[Enabled] [bit] NOT NULL,
[IsAuthor] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[ServerPlugins] ADD CONSTRAINT [PK_ServerPlugins] PRIMARY KEY CLUSTERED  ([ServerPluginId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerPlugins] ADD CONSTRAINT [IX_ServerPlugins_Plugin-Server] UNIQUE NONCLUSTERED  ([PluginId], [ServerId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerPlugins] ADD CONSTRAINT [FK_ServerPlugins_Plugins] FOREIGN KEY ([PluginId]) REFERENCES [dbo].[Plugins] ([PluginId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ServerPlugins] ADD CONSTRAINT [FK_ServerPlugins_Servers] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
