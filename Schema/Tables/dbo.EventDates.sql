CREATE TABLE [dbo].[EventDates]
(
[EventDateId] [int] NOT NULL IDENTITY(1, 1),
[EventId] [int] NOT NULL,
[StartDate] [datetime] NOT NULL,
[LengthInHours] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[EventDates] ADD CONSTRAINT [PK_EventDates] PRIMARY KEY CLUSTERED  ([EventDateId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EventDates] ADD CONSTRAINT [FK_EventDates_Events] FOREIGN KEY ([EventId]) REFERENCES [dbo].[Events] ([EventId]) ON DELETE CASCADE
GO
