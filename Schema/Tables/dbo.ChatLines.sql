CREATE TABLE [dbo].[ChatLines]
(
[ChatLineId] [int] NOT NULL IDENTITY(1, 1),
[CreationTime] [datetime] NOT NULL,
[ServerId] [int] NOT NULL,
[UserId] [int] NOT NULL,
[Text] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ChatLines] ADD CONSTRAINT [PK_ChatLines] PRIMARY KEY CLUSTERED  ([ChatLineId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ChatLines] ADD CONSTRAINT [FK_ChatLines_Servers] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ChatLines] ADD CONSTRAINT [FK_ChatLines_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId]) ON DELETE CASCADE
GO
