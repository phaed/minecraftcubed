CREATE TABLE [dbo].[Colors]
(
[ColorId] [int] NOT NULL IDENTITY(1, 1),
[Header] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Background] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Middle] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsDarkBackground] [bit] NOT NULL,
[IsDarkHeader] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[Colors] ADD CONSTRAINT [PK_Colors] PRIMARY KEY CLUSTERED  ([ColorId]) ON [PRIMARY]
GO
