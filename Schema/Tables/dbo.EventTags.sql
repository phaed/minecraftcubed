CREATE TABLE [dbo].[EventTags]
(
[EventTagId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[EventTags] ADD CONSTRAINT [PK_EventTags] PRIMARY KEY CLUSTERED  ([EventTagId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EventTags] ADD CONSTRAINT [IX_EventTags_Name] UNIQUE NONCLUSTERED  ([Name]) ON [PRIMARY]
GO
