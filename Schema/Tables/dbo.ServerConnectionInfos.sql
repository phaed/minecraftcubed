CREATE TABLE [dbo].[ServerConnectionInfos]
(
[ServerConnectionInfos] [int] NOT NULL IDENTITY(1, 1),
[ServerId] [int] NOT NULL,
[ConnectionInfoId] [int] NOT NULL,
[Position] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ServerConnectionInfos] ADD CONSTRAINT [PK_ServerConnectionInfos] PRIMARY KEY CLUSTERED  ([ServerConnectionInfos]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerConnectionInfos] ADD CONSTRAINT [FK_ServerConnectionInfos_ConnectionInfos] FOREIGN KEY ([ConnectionInfoId]) REFERENCES [dbo].[ConnectionInfos] ([ConnectionInfoId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ServerConnectionInfos] ADD CONSTRAINT [FK_ServerConnectionInfos_Servers] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
