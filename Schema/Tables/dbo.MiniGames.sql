CREATE TABLE [dbo].[MiniGames]
(
[MiniGameId] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Identifier] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[MiniGames] ADD CONSTRAINT [PK_MiniGames] PRIMARY KEY CLUSTERED  ([MiniGameId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MiniGames] ADD CONSTRAINT [IX_MiniGames] UNIQUE NONCLUSTERED  ([Name]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_MiniGames_Identifier] ON [dbo].[MiniGames] ([Identifier]) ON [PRIMARY]
GO
