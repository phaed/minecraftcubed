CREATE TABLE [dbo].[EventDescriptions]
(
[EventDescriptionId] [int] NOT NULL IDENTITY(1, 1),
[Text] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[EventDescriptions] ADD CONSTRAINT [PK_EventDescriptions] PRIMARY KEY CLUSTERED  ([EventDescriptionId]) ON [PRIMARY]
GO
