CREATE TABLE [dbo].[Videos]
(
[VideoId] [int] NOT NULL IDENTITY(1, 1),
[ServerId] [int] NOT NULL,
[Url] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Position] [int] NOT NULL,
[Caption] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Videos] ADD CONSTRAINT [PK_Videos] PRIMARY KEY CLUSTERED  ([VideoId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Videos] ADD CONSTRAINT [FK_Videos_Servers] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
