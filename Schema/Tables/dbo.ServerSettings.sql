CREATE TABLE [dbo].[ServerSettings]
(
[ServerSettingsId] [int] NOT NULL IDENTITY(1, 1),
[ShowPlugins] [bit] NOT NULL,
[ShuffleImages] [bit] NOT NULL,
[VideosFirst] [bit] NOT NULL,
[ApiKey] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LogoTransparentBottom] [bit] NOT NULL,
[HasLogo] [bit] NOT NULL,
[HasBanner] [bit] NOT NULL,
[HasFeaturedBanner] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[ServerSettings] ADD CONSTRAINT [PK_ServerSettings] PRIMARY KEY CLUSTERED  ([ServerSettingsId]) ON [PRIMARY]
GO
