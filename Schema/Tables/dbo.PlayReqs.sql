CREATE TABLE [dbo].[PlayReqs]
(
[PlayReqId] [int] NOT NULL IDENTITY(1, 1),
[HasDownload] [bit] NOT NULL,
[DownloadLabel] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DownloadUrl] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HasWhitelist] [bit] NOT NULL,
[WhitelistUrl] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HasRules] [bit] NOT NULL,
[RulesUrl] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HasAttendeeWhitelist] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[PlayReqs] ADD CONSTRAINT [PK_PlayReqs] PRIMARY KEY CLUSTERED  ([PlayReqId]) ON [PRIMARY]
GO
