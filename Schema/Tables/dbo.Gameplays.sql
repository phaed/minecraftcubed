CREATE TABLE [dbo].[Gameplays]
(
[GameplayId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[Gameplays] ADD CONSTRAINT [PK_Themes] PRIMARY KEY CLUSTERED  ([GameplayId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Gameplays] ADD CONSTRAINT [IX_Themes] UNIQUE NONCLUSTERED  ([Name]) ON [PRIMARY]
GO
