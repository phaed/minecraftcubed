CREATE TABLE [dbo].[Logging]
(
[LogId] [bigint] NOT NULL IDENTITY(1, 1),
[LogDate] [datetime] NULL,
[Thread] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LogLevel] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Logger] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Exception] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Logging] ADD CONSTRAINT [PK_Promo_AppLog] PRIMARY KEY CLUSTERED  ([LogId]) ON [PRIMARY]
GO
