CREATE TABLE [dbo].[ServerVersions]
(
[ServerVersionId] [int] NOT NULL IDENTITY(1, 1),
[ServerId] [int] NOT NULL,
[VersionId] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ServerVersions] ADD CONSTRAINT [PK_ServerVersions] PRIMARY KEY CLUSTERED  ([ServerVersionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerVersions] ADD CONSTRAINT [IX_ServerVersions_Unique] UNIQUE NONCLUSTERED  ([ServerId], [VersionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerVersions] ADD CONSTRAINT [FK_ServerVersions_Servers] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ServerVersions] ADD CONSTRAINT [FK_ServerVersions_Versions] FOREIGN KEY ([VersionId]) REFERENCES [dbo].[Versions] ([VersionId]) ON DELETE CASCADE
GO
