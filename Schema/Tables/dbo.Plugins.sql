CREATE TABLE [dbo].[Plugins]
(
[PluginId] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Identifier] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Website] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LogoUrl] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Categories] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Plugins] ADD CONSTRAINT [PK_Plugins] PRIMARY KEY CLUSTERED  ([PluginId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Plugins] ADD CONSTRAINT [IX_Plugins_id] UNIQUE NONCLUSTERED  ([Identifier]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Plugins] ADD CONSTRAINT [IX_Plugins_Name] UNIQUE NONCLUSTERED  ([Name]) ON [PRIMARY]
GO
