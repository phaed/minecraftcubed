CREATE TABLE [dbo].[PlayersOnline]
(
[PlayersOnlineId] [int] NOT NULL IDENTITY(1, 1),
[Players] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaxPlayers] [int] NOT NULL,
[LastUpdated] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[PlayersOnline] ADD CONSTRAINT [PK_PlayersOnline] PRIMARY KEY CLUSTERED  ([PlayersOnlineId]) ON [PRIMARY]
GO
