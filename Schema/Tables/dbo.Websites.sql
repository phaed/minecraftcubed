CREATE TABLE [dbo].[Websites]
(
[WebsiteId] [int] NOT NULL IDENTITY(1, 1),
[Url1] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Url2] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Url3] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Url4] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title1] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title2] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title3] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title4] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Websites] ADD CONSTRAINT [PK_Websites] PRIMARY KEY CLUSTERED  ([WebsiteId]) ON [PRIMARY]
GO
