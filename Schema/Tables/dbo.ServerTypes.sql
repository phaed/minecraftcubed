CREATE TABLE [dbo].[ServerTypes]
(
[ServerTypeId] [int] NOT NULL IDENTITY(1, 1),
[ServerId] [int] NOT NULL,
[TypeId] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ServerTypes] ADD CONSTRAINT [PK_ServerGameTypes] PRIMARY KEY CLUSTERED  ([ServerTypeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerTypes] ADD CONSTRAINT [IX_ServerGameTypes_Unique] UNIQUE NONCLUSTERED  ([ServerId], [TypeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerTypes] ADD CONSTRAINT [FK_ServerGameTypes_Servers] FOREIGN KEY ([ServerId]) REFERENCES [dbo].[Servers] ([ServerId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ServerTypes] ADD CONSTRAINT [FK_ServerGameTypes_GameTypes] FOREIGN KEY ([TypeId]) REFERENCES [dbo].[Types] ([TypeId]) ON DELETE CASCADE
GO
