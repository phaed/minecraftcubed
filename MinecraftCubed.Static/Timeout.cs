﻿namespace MinecraftCubed.Static
{
    public enum Timeout
    {
        FiveSeconds = 5000,
        TenSeconds = 10000,
        FifteenSeconds = 15000,
        ThirtySeconds = 30000,
        OneMinute = 60000,
        TwoMinutes = 60000,
        FiveMinutes = 300000
    }
}