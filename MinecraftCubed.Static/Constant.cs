﻿using System.Runtime.InteropServices;

namespace MinecraftCubed.Static
{
    public static class Constant
    {
        public const int PasswordMinSize = 4;

        public const string EncryptionKey = "2312jk3hk0";
        public const string Domain = "minecraftcubed.net";
        public const string AnonPrefix = "Anon";
        public const int UnknownHostId = 1000;
        
        public const int HeroHeadCount = 300;
        public const int CachedHeadSize = 70;
        public const int ActiveEventsToShow = 4;

        public const string BukGetUrl = "http://api.bukget.org/3/plugins/bukkit/";
        public const string DiscourseURL = "https://forum.minecraftcubed.net/";
        public const string DiscourseLoginURL = "https://forum.minecraftcubed.net/session/sso";
        public const string LoginVerifySecret = "e2dsfghjh3r234f34f34f4817a62c30ca97e0dsadasda";

        public const string AzureStorageLocalUrl = "http://127.0.0.1:10000/devstoreaccount1/";
        public const string AzureStorageUrl = "http://minecraftcubed.blob.core.windows.net/";

        public const string Website = "http://minecraftcubed.net/";
        public const string WebsiteClean = "http://minecraftcubed.net";
        public const string WebsiteDev = "http://localhost:1618/";

        public const string PaypalEndpoint = "https://www.paypal.com/cgi-bin/webscr";
        public const string PaypalEndpointDev = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        public const string IPNUrl = "http://minecraftcubed.net/sponsor/ipn";

        public const string SponsorSuccessUrl = Website + "sponsor/success";
        public const string SponsorUrl = Website + "sponsor";

        public const string MCUsername = "MC_Username";

        public const string SSOSecret = "ec02c59dee6faaca3189bace969c22d3";

        public const string DefaultConnectionInfoName = "Lobby";
        public const string DefaultHeaderColor = "#fff";
        public const string DefaultBackgroundColor = "#fff";
        public const string DefaultMiddle = "#303030";
        public const string DefaultRankColor = "#bb0000";
        public const string DefaultRank = "Admin";


        public const string DefaultDescription = "";
        public const string DefaultEventDescription = "";

        public static class Email
        {
            public const string Admin = "admin@minecraftcubed.net";
            public const string PaypalEmail = "sponsor@minecraftcubed.net";
            public const string PaypalEmailDev = "test@minecraftcubed.net";
        }

        public static class Cache
        {
            public const string User = "User:";
            public const string ManagedServers = "ManagedServers:";
            public const string ManagedServersListItems = "ManagedServersListItems:";
            public const string ManagedEvents = "ManagedEvents:";

            public const string CountryList = "CountryList:";
            public const string TimeZoneList = "TimeZoneList:";
            public const string HostList = "HostList:";
            public const string Staff = "Staff:";

            public const string ServerIdFromIdentifier = "ServerIdFromIdentifier:";
            public const string IdentifierFromServerId = "IdentifierFromServerId:";
            public const string OrganicPingListResults = "OrganicPingListResults:";
            public const string ServerCount = "ServerCount:";
            public const string EventIdFromIdentifier = "EventIdFromIdentifier:";
            public const string IdentifierFromEventId = "IdentifierFromEventId:";
            public const string IsServerManager = "IsServerManager:";
            public const string IsServerAdmin = "IsServerAdmin:";
            public const string IsAnyServerManager = "IsAnyServerManager:";
            public const string IsAnyServerAdmin = "IsAnyServerAdmin:";
            public const string BuiltServer = "BuiltServer:";
            public const string MatchingServers = "MatchingServers:";
            public const string EventDateAttendees = "EventDateAttendees:";
            public const string MatchingEventDates = "MatchingEventDates:";
            public const string ActiveEventDates = "ActiveEventDates:";
            public const string LatestEventDates = "LatestEventDates:";
            public const string LatestEventDatesWithDupes = "LatestEventDatesWithDupes:";
            public const string EventDate = "EventDate:";
            public const string PageServer = "PageServer:";
            public const string ForgotEmailLimit = "ForgotEmailLimit:";
            public const string LoginLimit = "LoginLimit:";
            public const string PageData = "PageData:";
            public const string ChatLineData = "ChatLineData:";
            public const string FaceData = "FaceData";

            public const string ConnectionInfosNames = "ConnectionInfosNames";

            public static class Filters
            {
                public const string ServerTypes = "ServerTypes";
                public const string Gameplay = "Gameplay";
                public const string MiniGames = "MiniGames";
                public const string Plugins = "Plugins";
                public const string Versions = "Versions";
                public const string Countries = "Countries";
                public const string Languages = "Languages";
                public const string Modpacks = "Modpacks";
                public const string EventTags = "EventTags";
                public const string EventCountries = "EventCountries";
                public const string EventLanguages = "EventLanguages";
                public const string Hosts = "Hosts";
            }
        }

        public static class EventBanner
        {
            public const int Width = 272;
            public const int Height = 190;
        }

        public static class ServerBanner
        {
            public const int Width = 573;
            public const int Height = 73;
        }

        public static class Logo
        {
            public const int Width = 720;
            public const int Height = 230;
        }

        public static class FeaturedBanner
        {
            public const int Width = 573;
            public const int Height = 248;
        }

        public static class TipLogo
        {
            public const int Width = 270;
            public const int Height = 151;
        }

        public static class MiniGameLogo
        {
            public const int Width = 270;
            public const int Height = 151;
        }
        public static class ModpackLogo
        {
            public const int Width = 270;
            public const int Height = 151;
        }

        public static class DefaultImageBackgroundColor
        {
            public const int R = 250;
            public const int G = 250;
            public const int B = 250;
        }

        public const int ImagePadding = 10;
    }
}
