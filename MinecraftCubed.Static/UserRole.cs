﻿namespace MinecraftCubed.Static
{
    public enum UserRole
    {
        SysAdmin = 10,
        Editor = 20,
        User = 30,
        Unverified = 40,
    }
}