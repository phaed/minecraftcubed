﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using MinecraftCubed.Common;
using MinecraftCubed.Service.Services.Interfaces;
using MinecraftCubed.Static;
using MinecraftCubed.Web.DependencyResolution;
using Nito.AsyncEx;
using StructureMap;

namespace Pinger
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IoC.Initialize();
         
            try
            {
                var service = ObjectFactory.GetInstance<IServerService>();
                var servers = service.GetServerConnectInfos();

                Console.WriteLine("Pinging " + servers.Count + " servers");

                Parallel.ForEach(servers, x => Pinger.PingServer(x, Timeout.FifteenSeconds));
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                Console.WriteLine(ex.Message);
            }
        }
    }
}
