﻿using System;
using System.Linq;
using MinecraftCubed.Domain.Context;
using MinecraftCubed.Domain.Core;
using MinecraftCubed.Domain.Models;
using MinecraftCubed.Domain.Repositories;
using MinecraftCubed.Minecraft;
using MinecraftCubed.Minecraft.Models;
using MinecraftCubed.Static;

namespace Pinger
{
    public class Pinger
    {
        public static QueryResponse PingServer(ServerConnectInfo info, Timeout timeout)
        {
            using (var factory = new DatabaseFactory())
            {
                using (var unit = new UnitOfWork(factory))
                {
                    var context = factory.Get();

                    try
                    {
                        // ping]

                        var resp = StatusQuery.Ping(info.Address, info.Port);

                        if (resp != null)
                        {
                         
                            if (info.ServerId == 0)
                            {
                                return resp;
                            }
                            
                            resp.ServerId = info.ServerId;
                            
                            // get latest ping for the hour

                            var latest = context.ServerPings.Where(x => x.ServerId == info.ServerId)
                                                            .Where(x => x.CreationTime.Year == DateTime.UtcNow.Year && x.CreationTime.Month == DateTime.UtcNow.Month && x.CreationTime.Day == DateTime.UtcNow.Day && x.CreationTime.Hour == DateTime.UtcNow.Hour)
                                                            .OrderByDescending(x => x.CreationTime)
                                                            .FirstOrDefault();

                            if (latest == null)
                            {
                                // if no ping exists for the hour create a new one

                                context.ServerPings.Add(new ServerPing
                                {
                                    CreationTime = DateTime.UtcNow,
                                    IsOnline = true,
                                    PlayersOnline = resp.PlayersOnline,
                                    ServerId = info.ServerId,
                                });
                            }
                            else
                            {
                                // if there is already one for the hour update it

                                latest.PlayersOnline = Math.Max(resp.PlayersOnline, latest.PlayersOnline);
                                latest.IsOnline = true;
                            }

                            unit.Commit();
                            Console.WriteLine("[" + info.Address + "] Ping done.");

                            return resp;
                        }

                        // fail

                        context.ServerPings.Add(new ServerPing
                        {
                            CreationTime = DateTime.UtcNow,
                            IsOnline = false,
                            PlayersOnline = 0,
                            ServerId = info.ServerId,
                        });

                        unit.Commit();
                        Console.WriteLine("[" + info.Address + "] Offline.");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("attempt failed"))
                        {
                            Console.WriteLine("[" + info.Address + "] Failed.");
                        }
                        else
                        {
                            Console.WriteLine("[" + info.Address + "] " + ex.Message);

                        }
                    }
                }
            }
            return new QueryResponse
            {
                Success = false
            };
        }
    }
}
