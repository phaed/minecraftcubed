﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace MinecraftCubed.Common
{
    public static class Utils
    {
        public static int ParseInt(string str, int defaultVal = 1)
        {
            int num;

            if (Int32.TryParse(str, out num))
            {
                return num;
            }

            return defaultVal;
        }

        public static bool IsNullOrEmpty<T>(Object obj)
        {
            if (obj == null)
            {
                return true;
            }

            var collection = obj as ICollection<T>;

            if (collection != null)
            {
                return collection.Count == 0;
            }

            return false;
        }

        public static bool IsValidImageMimeType(HttpPostedFileBase file)
        {
            if (file == null)
            {
                return false;
            }

            return file.ContentType == "image/png" || file.ContentType == "image/jpeg" || file.ContentType == "image/gif";
        }

        public static byte[] ConvertImageToByteArray(HttpPostedFileBase file)
        {
            using (var inputStream = file.InputStream) 
            {
                var memoryStream = inputStream as MemoryStream;

                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    inputStream.CopyTo(memoryStream);
                }

                return memoryStream.ToArray();
            }
        }

        public static UrlHelper GetUrlHelper()
        {
            var requestContext = new System.Web.Routing.RequestContext(
                new HttpContextWrapper(HttpContext.Current),
                new System.Web.Routing.RouteData());

            return new UrlHelper(requestContext);
        }

        public static string GetHash(string payload, string ssoSecret)
        {
            var encoding = new UTF8Encoding();
            var keyBytes = encoding.GetBytes(ssoSecret);

            var hasher = new System.Security.Cryptography.HMACSHA256(keyBytes);

            var bytes = encoding.GetBytes(payload);
            var hash = hasher.ComputeHash(bytes);

            return hash.Aggregate(string.Empty, (current, x) => current + String.Format("{0:x2}", x));
        }

        public static string RemoveDiacritics(string input)
        {
            var stFormD = input.Normalize(NormalizationForm.FormD);
            var len = stFormD.Length;
            var sb = new StringBuilder();
            for (var i = 0; i < len; i++)
            {
                var uc = CharUnicodeInfo.GetUnicodeCategory(stFormD[i]);

                switch (uc)
                {
                    case UnicodeCategory.NonSpacingMark:
                    case UnicodeCategory.SpacingCombiningMark:
                    case UnicodeCategory.EnclosingMark:
                        //do nothing
                        break;
                    default:
                        sb.Append(stFormD[i]);
                        break;
                }
            }
            return (sb.ToString().Normalize(NormalizationForm.FormC));
        }

        public static string[] RandomizeStrings(string[] arr)
        {
            var random = new Random();

            var list = arr.Select(s => new KeyValuePair<int, string>(random.Next(), s)).ToList();
            // Add all strings from array
            // Add new random int each time
            // Sort the list by the random number
            var sorted = from item in list
                         orderby item.Key
                         select item;
            // Allocate new string array
            var result = new string[arr.Length];
            // Copy values to array
            var index = 0;
            foreach (var pair in sorted)
            {
                result[index] = pair.Value;
                index++;
            }
            // Return copied array
            return result;
        }
    }
}
