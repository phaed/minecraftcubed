﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;

namespace MinecraftCubed.Common.Helpers
{
    public static class DateHelper
    {
        public static string GetDayName(int day)
        {
            if (DateTimeFormatInfo.CurrentInfo != null) 
                return DateTimeFormatInfo.CurrentInfo.DayNames[day];

            return string.Empty;
        }

        public static DateTimeOffset ConvertUtcToTimeZone(this DateTime dateTime, string timeZoneId)
        {
            dateTime = DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);
            var toUtcOffset = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId).GetUtcOffset(dateTime);
            var convertedTime = DateTime.SpecifyKind(dateTime.Add(toUtcOffset), DateTimeKind.Unspecified);
            return new DateTimeOffset(convertedTime, toUtcOffset);
        }

        public static DateTime ConvertTimeZoneToUtc(this DateTime dateTime, string timeZoneId)
        {
            var tz = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            return TimeZoneInfo.ConvertTimeToUtc(dateTime, tz);  
        }

        public static DateTime RoundToNearestHour(this DateTime dateTime)
        {
            dateTime += TimeSpan.FromMinutes(30);

            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, 0, 0, dateTime.Kind);
        }

        public static string GetBiddingPeriodMonth()
        {
            return DateTime.UtcNow.AddMonths(1).ToString("MMMM");
        }

        public static string GetActivePeriodMonth()
        {
            return DateTime.UtcNow.ToString("MMMM");
        }  
        
        public static string GetBiddingPeriodDateString()
        {
            return DateTime.UtcNow.AddMonths(1).ToString("MMMM yyyy");
        }

        public static string GetActivePeriodDateString()
        {
            return DateTime.UtcNow.ToString("MMMM yyyy");
        }

        public static DateTime GetFirstOfNextMonth()
        {
            var nextMonth = DateTime.UtcNow.AddMonths(1);
            return new DateTime(nextMonth.Year, nextMonth.Month, 1, 0, 0, 0, 0);
        }
    }
}
