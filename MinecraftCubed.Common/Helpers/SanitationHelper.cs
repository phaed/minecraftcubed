﻿using System.Collections.Generic;
using Html;

namespace MinecraftCubed.Common.Helpers
{
    public static class SanitationHelper
    {
        public static string CleanHtml(string html, bool allowImages = false, bool allowFontSizing = false)
        {
            var tags = new List<string>
            {
                "p",
                "ul",
                "li",
                "ol",
                "br",
                "span",
                "blockquote",
                "table",
                "tr",
                "td",
                "tbody",
                "thead",
                "th",
                "b",
                "strong",
                "i",
                "em",
            };
            
            var props = new List<string>
            {
                "color",
                "font-weight",
                "float",
                "display",
                "line-height",
                "text-align",
                "max-width",
                "max-height",
                "width",
                "height",
                "margin",
                "margin-top",
                "margin-bottom",
                "margin-right",
                "margin-left",
                "padding",
                "padding-top",
                "padding-bottom",
                "padding-right",
                "padding-left",
                "cellpadding",
                "cellspacing",
                "border",
                "style",
                "border-top",
                "border-bottom",
                "border-right",
                "border-left",
                "vertical-align",
            };

            if (allowFontSizing)
            {
                props.Add("font-style");
                props.Add("font-size");
                props.Add("font-weight");
                props.Add("font");
            }

            if (allowImages)
            {
                tags.Add("img");
            }

            return new HtmlSanitizer
            {
                AllowedTags = tags,
                AllowedCssProperties = props
            }.Sanitize(html);
        }
    }
}
