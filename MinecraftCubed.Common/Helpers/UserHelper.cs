﻿using System;
using System.Web;
using MinecraftCubed.Static;

namespace MinecraftCubed.Common.Helpers
{
    public static class UserHelper
    {
        public static string GetUserIdentity()
        {
            if (HttpContext.Current == null)
            {
                return null;
            }

            if (HttpContext.Current.Request.IsAuthenticated)
            {
                var username = HttpContext.Current.User.Identity.Name;

                if (username != null)
                {
                    return username;
                }
            }

            return null;
        }

        public static string GetIp()
        {
            var ip = HttpContext.Current.Request.ServerVariables["CF-Connecting-IP"];

            if (!string.IsNullOrEmpty(ip))
            {
                return ip;
            }

            ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            if (!string.IsNullOrEmpty(ip))
            {
                return ip;
            }

            return null;
        }
    }
}