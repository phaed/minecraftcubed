﻿using System;

namespace MinecraftCubed.Common.Helpers
{
    public static class ColorHelper
    {
        public static String ToHex(this System.Drawing.Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }

        private static String ToRGB(this System.Drawing.Color c)
        {
            return "rgb(" + c.R.ToString() + "," + c.G.ToString() + "," + c.B.ToString() + ")";
        }

        public static string Opacity(string hex, int opacity)
        {
            var rgb = SoundInTheory.DynamicImage.Color.FromHtml(hex);

            var color = System.Drawing.Color.FromArgb(rgb.R, rgb.G, rgb.B);

            var h = color.GetHue();
            var s = color.GetSaturation();
            var l = color.GetBrightness();

            return "hsla(" + h + ", " + String.Format("{0:0%}", s) + ", " + String.Format("{0:0%}", l) + ", " + String.Format("{0:00.0}", opacity/10.0) + ")";
        }

        public static string Darken(string hex, int strength, int opacity = 0)
        {
            var rgb = SoundInTheory.DynamicImage.Color.FromHtml(hex);

            var color = System.Drawing.Color.FromArgb(rgb.R, rgb.G, rgb.B);

            var h = color.GetHue();
            var s = color.GetSaturation();
            var l = color.GetBrightness();
            
            var per = (float) (strength/100.0);

            var small = (per * l);
            var big = per;

            l -= (big + small) / 2;
            
            l = Math.Max(0, l);

            if (opacity > 0)
            {
                return "hsla(" + h + ", " + String.Format("{0:0%}", s) + ", " + String.Format("{0:0%}", l) + ", " + String.Format("{0:00.0}", opacity/10.0) + ")";
            }

            return "hsl(" + h + ", " + String.Format("{0:0%}", s) + ", " + String.Format("{0:0%}", l) + ")";
        }   
    }
}
