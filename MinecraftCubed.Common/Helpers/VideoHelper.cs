﻿using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace MinecraftCubed.Common.Helpers
{
    public static class VideoHelper
    {
        private static readonly Regex VimeoVideoRegex = new Regex(@"vimeo\.com/(?:.*#|.*/videos/)?([0-9]+)", RegexOptions.IgnoreCase | RegexOptions.Multiline);
        private static readonly Regex YoutubeVideoRegex = new Regex(@"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)", RegexOptions.IgnoreCase);

        public static bool IsVideoUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return false;
            }

            var youtubeMatch = YoutubeVideoRegex.Match(url);
            var vimeoMatch = VimeoVideoRegex.Match(url);

            if (youtubeMatch.Success)
            {
                return true;
            }

            if (vimeoMatch.Success)
            {
                return true;
            }

            return false;
        }

        public static string ToSecure(string url)
        {
            if (IsVideoUrl(url))
            {
                return url.Replace("http:", "https:");
            }

            return url;
        }

        public static string GenerateEmbedUrl(string url)
        {
            var id = ExtractVideoId(url);

            if (id.Length > 0)
            {
                return "https://www.youtube.com/embed/" + id + "?controls=2&showinfo=0&autohide=1";
            }

            return string.Empty;
        }

        public static string ExtractVideoId(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return string.Empty;
            }

            var youtubeMatch = YoutubeVideoRegex.Match(url);
            var vimeoMatch = VimeoVideoRegex.Match(url);

            if (youtubeMatch.Success)
            {
                return youtubeMatch.Groups[1].Value;
            }

            if (vimeoMatch.Success)
            {
                return vimeoMatch.Groups[1].Value;
            }

            return string.Empty;
        }

        public static string GetThumbnailUrl(string url)
        {
            var youtubeMatch = YoutubeVideoRegex.Match(url);
            var vimeoMatch = VimeoVideoRegex.Match(url);

            if (youtubeMatch.Success)
            {
                var id = youtubeMatch.Groups[1].Value;
                return "http://img.youtube.com/vi/" + id + "/0.jpg";
            }

            if (vimeoMatch.Success)
            {
                var id = vimeoMatch.Groups[1].Value;

                try
                {
                    var doc = new XmlDocument();
                    doc.Load("http://vimeo.com/api/v2/video/" + id + ".xml");
                    var root = doc.DocumentElement;
                    if (root != null)
                    {
                        var selectSingleNode = root.FirstChild.SelectSingleNode("thumbnail_medium");
                        if (selectSingleNode != null)
                        {
                            var vimeoThumb = selectSingleNode.ChildNodes[0].Value;
                            var imageURL = vimeoThumb;
                            return imageURL;
                        }
                    }
                }
                catch { }
            }

            return Settings.Website + "content/images/video.jpg";
        }
    }
}
