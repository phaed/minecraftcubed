﻿
namespace MinecraftCubed.Common.Helpers
{
    public class CurrencyHelper
    {
        public static string ToCurrency(object amount)
        {
            return "$" + string.Format("{0:N2}", amount);
        }

        public static string ToCurrencyShort(object amount)
        {
            return "$" + string.Format("{0:N0}", amount).Replace(".00", "");
        }

        public static string ToCurrencyRounded(object amount)
        {
            return "$" + string.Format("{0:N0}", amount);
        }
    }
}
