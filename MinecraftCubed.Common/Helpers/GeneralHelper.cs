﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Routing;

namespace MinecraftCubed.Common.Helpers
{
    public static class GeneralHelper
    {
        public static string ReferringUrl
        {
            get
            {
                if (HttpContext.Current.Request.UrlReferrer != null)
                {
                    if (HttpContext.Current.Request.UrlReferrer.AbsoluteUri !=
                        HttpContext.Current.Request.Url.AbsoluteUri)
                    {
                        return HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                    }
                }

                return VirtualPathUtility.ToAbsolute("~/");
            }
        }

        public static RouteValueDictionary ToProperHtmlAttributes(object htmlAttributes)
        {
            var result = new RouteValueDictionary();
            if (htmlAttributes != null)
            {
                foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(htmlAttributes))
                {
                    result.Add(property.Name.Replace('_', '-'), property.GetValue(htmlAttributes));
                }
            }
            return result;
        }

        public static string PullSubdomain()
        {
            var host = HttpContext.Current.Request.Headers["HOST"];

            if (host.Split('.').Length > 1)
            {
                var index = host.IndexOf(".", StringComparison.Ordinal);

                return host.Substring(0, index);
            }

            return null;
        }

        public static bool IsUrlIfExists(this string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return true;
            }

            Uri result;
            return Uri.TryCreate(url, UriKind.Absolute, out result) && result.Scheme == Uri.UriSchemeHttp;
        }

        public static bool IsUrl(this string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return false;
            }

            Uri result;
            return Uri.TryCreate(url, UriKind.Absolute, out result) && result.Scheme == Uri.UriSchemeHttp;
        }

        public static int GetRandomRange(int min, int max)
        {
            var r = new Random();
            return r.Next(min, max);
        }

        public static bool RollDice(int odds)
        {
            var r = new Random();
            return r.Next(1, odds) == 1;
        }

        public static string FormatVersion(string ver)
        {
            if (ver == null)
            {
                return null;
            }

            ver = ver.Replace("BungeeCord ", "");
                
            if (ver.Length == 2 && !ver.Contains("."))
            {
                return ver[0] + "." + ver[1];
            }

            if (ver.Length == 3 && !ver.Contains("."))
            {
                return ver[0] + "." + ver[1] + "." + ver[2];
            }

            if (ver.Length == 4 && !ver.Contains("."))
            {
                return ver[0] + "." + ver[1] + "." + ver[2] + ver[3];
            }

            return ver;
        }
    }
}
