﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AviFile;
using ImageResizer;
using MinecraftCubed.Common.Extensions;
using MinecraftCubed.Common.Models;
using MinecraftCubed.Static;

namespace MinecraftCubed.Common.Helpers
{
    public static class ImageHelper
    {
        private static readonly Dictionary<Guid, ImageFormat> GUIDToImageFormatMap = new Dictionary<Guid, ImageFormat>()
        {
            {ImageFormat.Bmp.Guid, ImageFormat.Bmp},
            {ImageFormat.Gif.Guid, ImageFormat.Png},
            {ImageFormat.Icon.Guid, ImageFormat.Png},
            {ImageFormat.Jpeg.Guid, ImageFormat.Jpeg},
            {ImageFormat.Png.Guid, ImageFormat.Png}
        };

        public static async Task<MemoryStream> DownloadImage(string url)
        {
            try
            {
                using (var client = new WebClient())
                {
                    var bytes = await client.DownloadDataTaskAsync(url).ConfigureAwait(false);

                    return new MemoryStream(bytes);
                }
            }
            catch (Exception ex)
            {
                var inner = ex.InnerException;
            }

            return null;
        }

        public static MemoryStream ConvertToJpeg(Stream image)
        {
            if (image == null)
            {
                return null;
            }

            using (var src = Image.FromStream(image))
            {
                // otherwise just convert to jpeg

                using (var dst = new Bitmap(src.Width, src.Height))
                {
                    using (var g = Graphics.FromImage(dst))
                    {
                        g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        g.SmoothingMode = SmoothingMode.HighQuality;
                        g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        g.CompositingQuality = CompositingQuality.HighQuality;
                        g.Clear(Color.White);
                        g.DrawImage(src, 0, 0, src.Width, src.Height);
                    }
                    using (var memStream = new MemoryStream())
                    {
                        dst.Save(memStream, ImageFormat.Jpeg);
                        return new MemoryStream(memStream.ToArray());
                    }
                }
            }
        }

        public static byte[] ToBytes(Stream image)
        {
            if (image == null)
            {
                return null;
            }

            using (var memoryStream = new MemoryStream())
            {
                image.CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }

        public static string GetFormat(Stream stream)
        {
            using (var src = Image.FromStream(stream))
            {
                return src.RawFormat.ToString().ToLower();
            }
        }

        // link to modes http://imageresizing.net/docs/basics

        public static MemoryStream ResizeImage(Stream stream, int width, int height, string format = "jpg", string mode = "crop", bool trim = false, string anchor = "topcenter", string scale = "both", bool transparent = false)
        {
            stream.Seek(0, SeekOrigin.Begin);

            using (var bmp = new Bitmap(stream))
            {
                return ResizeImage(bmp, width, height, format, mode, trim, anchor, scale, transparent);
            }
        }

        public static MemoryStream ResizeImage(Bitmap bmp, int width, int height, string format = "jpg", string mode = "crop", bool trim = false, string anchor = "topcenter", string scale = "both", bool transparent = false)
        {
            if (trim)
            {
                bmp = CropWhiteSpace(bmp);
            }

            var color = string.Empty;

            if (!transparent)
            {
                color = ";bgcolor=" + GetPaddingColor(bmp).ToHex().Replace("#", string.Empty); 
            }

            var resized = new MemoryStream();
            new ImageJob(bmp, resized, new Instructions("scale=" + scale + ";anchor=" + anchor + ";mode=" + mode + ";format=" + format + ";w=" + width + ";h=" + height + color)).Build();
            return resized;
        }

        public static Stream ShrinkIfTooBig(Stream stream, int width, int height, string format, string mode = "crop", bool trim = false, bool transparent = false, string anchor = "topcenter", string scale = "both")
        {
            // resize

            using (var src = Image.FromStream(stream))
            {
                if (src.Width > width || src.Height > height)
                {
                    return ResizeImage(stream, width, height, format, mode, trim, anchor, scale, transparent);
                }
            }

            return stream;
        }

        private static int CalculateResizedHeight(int srcWidth, int srcHeight, int width)
        {
            var wr = width / (double)srcWidth;
            var h = wr * srcHeight;
            return (int)h;
        }

        public static Stream AutoSize(Stream stream, int width, int height)
        {
            using (var src = Image.FromStream(stream))
            {
                var tooBig = src.Width > width || src.Height > height;
                var tooSmall = !tooBig && (src.Width < width || src.Height < height);
                var tooTall = CalculateResizedHeight(src.Width, src.Height, width) > height;
                var refRatio = height / (double)width;
                var ratio = src.Height / (double)src.Width;
                var wideRatio = ratio < refRatio;
                var anchor = "middlecenter";

                if (tooSmall)
                {
                    return ResizeImage(stream, width, height, "png", "pad", false, anchor, "canvas");
                }

                if (wideRatio && tooTall)
                {
                    anchor = "topcenter";
                }

                if (tooBig)
                {
                    var trans = HasTransparency(stream);
                    var flat = HasFlatBackgroundColor(stream);
                    var letterbox = HasLetterbox(stream);

                    var trimmedWidth = width - (Constant.ImagePadding * 2);
                    var trimmedHeight = height - (Constant.ImagePadding * 2);

                    if (trans)
                    {
                        var def = Color.FromArgb(Constant.DefaultImageBackgroundColor.R,
                                                Constant.DefaultImageBackgroundColor.G,
                                                Constant.DefaultImageBackgroundColor.B);

                        var resized = ResizeImage(stream, trimmedWidth, trimmedHeight, "png", "pad", true, anchor, "down", true);

                        return AddPadding(resized, width, height, def);
                    }

                    if (flat)
                    {
                        var resized = ResizeImage(stream, trimmedWidth, trimmedHeight, "png", "pad", true, anchor, "down");

                        using (var bmp = new Bitmap(stream))
                        {
                            return AddPadding(resized, width, height, GetPaddingColor(bmp));
                        }
                    }

                    if (letterbox)
                    {
                        return ResizeImage(stream, width, height, "png", "crop", true, "middlecenter", "down");
                    }

                    return ResizeImage(stream, width, height, "png", "crop");
                }
            }

            return stream;
        }

        public static Color GetPaddingColor(Bitmap image)
        {
            var colors = new List<Color>();

            for (var x = 0; x < image.Width; x++)
            {
                colors.Add(image.GetPixel(x, 1));
                colors.Add(image.GetPixel(x, image.Height - 2));
            }

            for (var y = 0; y < image.Height; y++)
            {
                colors.Add(image.GetPixel(1, y));
                colors.Add(image.GetPixel(image.Width - 2, y));
            }

            var most = colors.GroupBy(i => i).OrderByDescending(grp => grp.Count()).Select(grp => grp.Key).First();

            if (most.A < 64)
            {
                return Color.FromArgb(Constant.DefaultImageBackgroundColor.R,
                                    Constant.DefaultImageBackgroundColor.G,
                                    Constant.DefaultImageBackgroundColor.B);
            }

            return Color.FromArgb(most.A, most.R, most.G, most.B);
        }

        public static Stream AddPadding(Stream source, int width, int height, Color color)
        {
            using (var newImage = new Bitmap(width, height, PixelFormat.Format32bppArgb))
            {
                using (var g = Graphics.FromImage(newImage))
                {
                    using (var src = Image.FromStream(source))
                    {
                        g.CompositingMode = CompositingMode.SourceOver;
                        g.Clear(color);
                        var x = (width - src.Width) / 2;
                        var y = (height - src.Height) / 2;
                        g.DrawImage(src, x, y);

                        using (var memStream = new MemoryStream())
                        {
                            newImage.Save(memStream, ImageFormat.Png);
                            return new MemoryStream(memStream.ToArray());
                        }
                    }
                }
            }
        }

        public static bool HasTransparentEdge(Stream stream)
        {
            using (var src = Image.FromStream(stream))
            {
                using (var image = new Bitmap(src))
                {
                    if (image.GetPixel(0, 0).A < 64)
                    {
                        return true;
                    }
                    if (image.GetPixel(image.Width - 1, 0).A < 64)
                    {
                        return true;
                    }
                    if (image.GetPixel(0, image.Height - 1).A < 64)
                    {
                        return true;
                    }
                    if (image.GetPixel(image.Width - 1, image.Height - 1).A < 64)
                    {
                        return true;
                    }

                    return false;
                }
            }
        }

        public static bool HasTransparency(Stream stream)
        {
            using (var src = Image.FromStream(stream))
            {
                using (var image = new Bitmap(src))
                {
                    var colors = new List<float>();

                    for (var x = 0; x < image.Width; x++)
                    {
                        colors.Add(image.GetPixel(x, 1).A);
                        colors.Add(image.GetPixel(x, image.Height - 2).A);
                    }

                    for (var y = 0; y < image.Height; y++)
                    {
                        colors.Add(image.GetPixel(1, y).A);
                        colors.Add(image.GetPixel(image.Width - 2, y).A);
                    }

                    var trans = colors.Count(x => x < 1);

                    return trans > 100;
                }
            }
        }

        private static bool HasFlatBackgroundColor(Stream stream)
        {
            using (var src = Image.FromStream(stream))
            {
                using (var image = new Bitmap(src))
                {
                    var colors = new List<float>();

                    for (var x = 0; x < image.Width; x++)
                    {
                        colors.Add(image.GetPixel(x, 1).GetHue());
                        colors.Add(image.GetPixel(x, 5).GetHue());
                        colors.Add(image.GetPixel(x, 10).GetHue());
                        colors.Add(image.GetPixel(x, image.Height - 12).GetHue());
                        colors.Add(image.GetPixel(x, image.Height - 7).GetHue());
                        colors.Add(image.GetPixel(x, image.Height - 2).GetHue());
                    }

                    for (var y = 0; y < image.Height; y++)
                    {
                        colors.Add(image.GetPixel(1, y).GetHue());
                        colors.Add(image.GetPixel(5, y).GetHue());
                        colors.Add(image.GetPixel(10, y).GetHue());
                        colors.Add(image.GetPixel(image.Width - 12, y).GetHue());
                        colors.Add(image.GetPixel(image.Width - 7, y).GetHue());
                        colors.Add(image.GetPixel(image.Width - 2, y).GetHue());
                    }

                    var most = colors.GroupBy(i => i).OrderByDescending(grp => grp.Count()).Select(grp => grp.Key).First();
                    var exist = colors.Count(x => x.Equals(most));
                    double ratio = exist / (colors.Count * 1.0);

                    return ratio > 0.8;
                }
            }
        }

        private static bool HasLetterbox(Stream stream)
        {
            using (var src = Image.FromStream(stream))
            {
                using (var image = new Bitmap(src))
                {
                    var colors = new List<float>();

                    for (var x = 0; x < image.Width; x++)
                    {
                        colors.Add(image.GetPixel(x, 1).GetHue());
                        colors.Add(image.GetPixel(x, 5).GetHue());
                        colors.Add(image.GetPixel(x, 10).GetHue());
                        colors.Add(image.GetPixel(x, image.Height - 12).GetHue());
                        colors.Add(image.GetPixel(x, image.Height - 7).GetHue());
                        colors.Add(image.GetPixel(x, image.Height - 2).GetHue());
                    }

                    var most = colors.GroupBy(i => i).OrderByDescending(grp => grp.Count()).Select(grp => grp.Key).First();
                    var exist = colors.Count(x => x.Equals(most));
                    double ratio = exist / (colors.Count * 1.0);

                    return ratio > 0.8;
                }
            }
        }

        public static bool HasTransparentBottom(Stream stream)
        {
            using (var src = Image.FromStream(stream))
            {
                var transparent = 0;
                var opaque = 0;

                using (var image = new Bitmap(src))
                {
                    for (var width = 0; width < image.Width; width = width + 10)
                    {
                        if (width < image.Width)
                        {
                            var result = image.GetPixel(width, image.Height - 30).A < 5;

                            if (result)
                                transparent++;
                            else
                                opaque++;
                        }
                    }
                }

                if (opaque == 0)
                {
                    return true;
                }

                return transparent > opaque;
            }
        }


        public static async Task<bool> ImageTooSmall(string url, int width, int height)
        {
            var stream = await DownloadImage(url).ConfigureAwait(false);

            if (stream == null)
            {
                return false;
            }

            using (var src = Image.FromStream(stream))
            {
                var tooBig = src.Width > width || src.Height > height;
                var tooSmall = !tooBig && (src.Width < width || src.Height < height);

                return tooSmall;
            }
        }
        public static Bitmap CropWhiteSpace(Bitmap bmp)
        {
            int w = bmp.Width;
            int h = bmp.Height;
            int white = 0xffffff;

            Func<int, bool> allWhiteRow = r =>
            {
                for (int i = 0; i < w; ++i)
                    if ((bmp.GetPixel(i, r).ToArgb() & white) != white)
                        return false;
                return true;
            };

            Func<int, bool> allWhiteColumn = c =>
            {
                for (int i = 0; i < h; ++i)
                    if ((bmp.GetPixel(c, i).ToArgb() & white) != white)
                        return false;
                return true;
            };

            int topmost = 0;
            for (int row = 0; row < h; ++row)
            {
                if (!allWhiteRow(row))
                    break;
                topmost = row;
            }

            int bottommost = 0;
            for (int row = h - 1; row >= 0; --row)
            {
                if (!allWhiteRow(row))
                    break;
                bottommost = row;
            }

            int leftmost = 0, rightmost = 0;
            for (int col = 0; col < w; ++col)
            {
                if (!allWhiteColumn(col))
                    break;
                leftmost = col;
            }

            for (int col = w - 1; col >= 0; --col)
            {
                if (!allWhiteColumn(col))
                    break;
                rightmost = col;
            }

            if (rightmost == 0) rightmost = w; // As reached left
            if (bottommost == 0) bottommost = h; // As reached top.

            int croppedWidth = rightmost - leftmost;
            int croppedHeight = bottommost - topmost;

            if (croppedWidth == 0) // No border on left or right
            {
                leftmost = 0;
                croppedWidth = w;
            }

            if (croppedHeight == 0) // No border on top or bottom
            {
                topmost = 0;
                croppedHeight = h;
            }

            try
            {
                var target = new Bitmap(croppedWidth, croppedHeight);
                using (Graphics g = Graphics.FromImage(target))
                {
                    g.DrawImage(bmp,
                      new RectangleF(0, 0, croppedWidth, croppedHeight),
                      new RectangleF(leftmost, topmost, croppedWidth, croppedHeight),
                      GraphicsUnit.Pixel);
                }
                return target;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Values are topmost={0} btm={1} left={2} right={3} croppedWidth={4} croppedHeight={5}", topmost, bottommost, leftmost, rightmost, croppedWidth, croppedHeight), ex);
            }
        }


        public static async Task<LogoData> GetLogoData(string url, string identifier = null)
        {
            if (string.IsNullOrEmpty(url))
            {
                return null;
            }

            var stream = await DownloadImage(url).ConfigureAwait(false);

            if (stream == null)
            {
                return null;
            }

            return new LogoData
            {
                ObjectIdentifier = identifier,
                Stream = stream
            };
        }

        public static async Task<bool> IsUrlImage(string url)
        {
            try
            {
                var request = WebRequest.Create(url);
                request.Timeout = 5000;
                using (var response = await request.GetResponseAsync())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        if (!response.ContentType.Contains("text/html"))
                        {
                            if (responseStream != null)
                            {
                                using (var br = new BinaryReader(responseStream))
                                {
                                    // e.g. test for a JPEG header here
                                    var soi = br.ReadUInt16(); // Start of Image (SOI) marker (FFD8)
                                    var jfif = br.ReadUInt16(); // JFIF marker (FFE0)
                                    return soi == 0xd8ff && jfif == 0xe0ff;
                                }
                            }
                        }
                    }
                }
            }
            catch (WebException)
            {
            }
            return false;
        }

        public static Stream GetRandomAnimatedGifFrame(Stream stream)
        {
            using (var img = Image.FromStream(stream))
            {
                if (img.RawFormat.Guid == ImageFormat.Gif.Guid)
                {
                    //Get the frame count
                    var dimension = new FrameDimension(img.FrameDimensionsList[0]);
                    var frameCount = img.GetFrameCount(dimension);

                    if (frameCount > 1)
                    {
                        var frame = new Random().Next(frameCount);

                        img.SelectActiveFrame(dimension, frame);

                        var ms = new MemoryStream();
                        img.Save(ms, ImageFormat.Png);
                        return ms;
                    }
                }
            }

            return null;
        }

        public static bool IsAnimatedGif(Stream stream)
        {
            using (var img = Image.FromStream(stream))
            {
                if (img.RawFormat.Guid == ImageFormat.Gif.Guid)
                {
                    //Get the frame count
                    var dimension = new FrameDimension(img.FrameDimensionsList[0]);
                    var frameCount = img.GetFrameCount(dimension);

                    return frameCount > 1;
                }
            }

            return false;
        }

        public static bool ConvertToVideo(Stream stream)
        {
            try
            {

                Bitmap cover = null;
                var bmps = new List<Bitmap>();

                using (var img = Image.FromStream(stream))
                {
                    //Check the image format to determine what
                    //format the image will be saved to the 
                    //memory stream in
                    var imageGuid = img.RawFormat.Guid;

                    var imageFormat = (from pair in GUIDToImageFormatMap where imageGuid == pair.Key select pair.Value).FirstOrDefault();

                    if (imageFormat == null)
                    {
                        throw new NoNullAllowedException("Unable to determine image format");
                    }

                    //Get the frame count
                    var dimension = new FrameDimension(img.FrameDimensionsList[0]);
                    var frameCount = img.GetFrameCount(dimension);

                    //Step through each frame
                    for (var i = 0; i < frameCount; i++)
                    {
                        //Set the active frame of the image and then 
                        //write the bytes to the tmpFrames array
                        img.SelectActiveFrame(dimension, i);

                        using (var ms = new MemoryStream())
                        {
                            img.Save(ms, imageFormat);

                            using (var bmp = new Bitmap(ms))
                            {
                                if (cover == null)
                                {
                                    cover = (Bitmap)bmp.Clone();
                                    continue;
                                }

                                bmps.Add((Bitmap)bmp.Clone());
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception)
            {
                // bad
            }

            return false;
        }



        private static Bitmap ToBitmap(byte[] byteArrayIn)
        {
            var ms = new MemoryStream(byteArrayIn);
            var returnImage = Image.FromStream(ms);
            var bitmap = new Bitmap(returnImage);

            return bitmap;
        }

        private static Bitmap ConvertBytesToImage(byte[] imageBytes)
        {
            if (imageBytes == null || imageBytes.Length == 0)
            {
                return null;
            }

            try
            {
                //Read bytes into a MemoryStream
                using (var ms = new MemoryStream(imageBytes))
                {
                    //Recreate the frame from the MemoryStream
                    using (var bmp = new Bitmap(ms))
                    {
                        return (Bitmap)bmp.Clone();
                    }
                }
            }
            catch (Exception)
            {
            }

            return null;
        }
    }
}