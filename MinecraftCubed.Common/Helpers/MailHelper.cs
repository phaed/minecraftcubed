﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using MinecraftCubed.Static;
using MinecraftCubed.Common.Lang;

namespace MinecraftCubed.Common.Helpers
{
    public static class MailHelper
    {
        public static void SendEmail(string email, string subject, string body)
        {
            var client = new AmazonSimpleEmailServiceClient(Settings.AWSAccessKey, Settings.AWSSecret, Amazon.RegionEndpoint.USEast1);

            var destination = new Destination
            {
                ToAddresses = new List<string> { email }
            };

            var sub = new Content(subject);
            
            var bod = new Body
            {
                Html = new Content(body)
            };
            
            var message = new Message(sub, bod);

            var sendEmailRequest = new SendEmailRequest(Constant.Email.Admin, destination, message);

            // Send the email.
            try
            {
                client.SendEmail(sendEmailRequest);
            }
            catch (Exception ex)
            {
                Trace.TraceError("AWS API Email failed:  " + email + " - " + sub, ex);
            }
        }

        public static void SendForgotPassEmail(string email, string body)
        {
            var sub = "MinecraftCubed Password Reset";
            
            SendEmail(email, sub, body);
        }
    }
}
