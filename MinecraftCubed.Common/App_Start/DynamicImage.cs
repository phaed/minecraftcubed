using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using MinecraftCubed.Common;
using SoundInTheory.DynamicImage;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(DynamicImage), "PreStart")]

namespace MinecraftCubed.Common
{
	public static class DynamicImage
	{
		public static void PreStart()
		{
			DynamicModuleUtility.RegisterModule(typeof(DynamicImageModule));
		}
	}
}