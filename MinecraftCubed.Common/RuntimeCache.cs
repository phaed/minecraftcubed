﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using MinecraftCubed.Common.Helpers;
using MinecraftCubed.Static;

namespace MinecraftCubed.Common
{
    public class RuntimeCache
    {
        private static readonly Object SyncRoot = new Object();

        public static T GetOrStore<T>(String key, T newItem, TimeSpan expiredIn)
        {
            return GetOrStore(key, () => newItem, expiredIn);
        }

        public static T GetOrStore<T>(String key, Func<T> itemGenerator, TimeSpan expiredIn)
        {
            var item = Get<T>(key);
            if (!EqualityComparer<T>.Default.Equals(item, default(T)))
                return item;

            lock (SyncRoot)
            {
                item = Get<T>(key);
                if (!EqualityComparer<T>.Default.Equals(item, default(T)))
                    return item;

                item = itemGenerator.Invoke();
                if (!EqualityComparer<T>.Default.Equals(item, default(T)))
                    Store(key, item, expiredIn);
                return item;
            }
        }

        public static T Get<T>(string key)
        {
            try
            {
                return (T) HttpRuntime.Cache.Get(key);
            }
            catch
            {
                return default(T);
            }
        }

        public static void Store<T>(string key, T objectToCache, TimeSpan expiredIn)
        {
            HttpRuntime.Cache.Insert(key, objectToCache, null, Cache.NoAbsoluteExpiration, expiredIn);
        }

        public static void Remove(string key)
        {
            HttpRuntime.Cache.Remove(key);

            var username = UserHelper.GetUserIdentity();

            if (username != null)
            {
                HttpRuntime.Cache.Remove(username + "_" + key);
            }
        }
    }
}
