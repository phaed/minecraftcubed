﻿using System.Net;
using System.Text;
using System.Threading.Tasks;
using MinecraftCubed.Common.Extensions;

namespace MinecraftCubed.Common
{
    public static class SafeBrowsing
    {
        const string TestUrl = "https://sb-ssl.google.com/safebrowsing/api/lookup?client=api&apikey={0}&appver=1.0&pver=3.0&url={1}";

        public static async Task<bool> IsSafe(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return true;
            }

            try
            {
                using (var client = new WebClient())
                {
                    var bytes = await client.DownloadDataTaskAsync(string.Format(TestUrl, Settings.SafeBrowsingAPIKey, WebUtility.UrlEncode(url)));
                    var str = Encoding.UTF8.GetString(bytes);

                    var bad = str.Contains("phishing") || str.Contains("malware");

                    return !bad;
                }
            }
            catch { }

            return true;
        }
    }
}
