﻿using System.IO;

namespace MinecraftCubed.Common.Models
{
    public class LogoData
    {
        public string ObjectIdentifier { get; set; }
        public Stream Stream {get; set; }
    }
}
