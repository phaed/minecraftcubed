﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftCubed.Common.Extensions
{
    public static class StreamExtensions
    {
        public static MemoryStream ToMemoryStream(this Stream stream)
        {
            var ms = new MemoryStream();
            stream.CopyTo(ms);
            return ms;
        }
    }
}
