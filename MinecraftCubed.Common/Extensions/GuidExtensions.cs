﻿using System;

namespace MinecraftCubed.Common.Extensions
{
    public static class GuidExtensions
    {
        public static bool IsGuidString(string id)
        {
            return id != null && (id.Length == 32 || id.Replace("-", string.Empty).Length == 32);
        }

        public static string Packed(this Guid guid)
        {
            return guid.ToString("N");
        }

        public static bool IsNull(this Guid guid)
        {
            return guid.Packed() == Guid.Empty.Packed();
        }

        public static string Pack(this string guidString)
        {
            return guidString.Replace("-", string.Empty);
        }

        public static Guid ToGuid(this string guidString)
        {
            if (string.IsNullOrEmpty(guidString))
            {
                return Guid.Empty;
            }

            if (guidString.Length != 32 && guidString.Length != 36)
            {
                return Guid.Empty;
            }

            return new Guid(guidString);
        }

        public static string ExractGuidFromUrl(string url)
        {
            if (url == null || url.Length < 32)
            {
                return null;
            }

            return url.Substring(url.Length - 32);
        }
    }
}
