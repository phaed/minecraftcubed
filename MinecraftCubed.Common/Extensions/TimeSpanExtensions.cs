﻿using System;
using System.Globalization;

namespace MinecraftCubed.Common.Extensions
{
    public static class TimeSpanExtensions
    {
        public static DateTime ToDateTime(this TimeSpan t)
        {
            return (new DateTime()).Add(t);
        }

        public static string ToTimeString(this TimeSpan t)
        {
            return t.ToDateTime().ToString("h:mm tt", DateTimeFormatInfo.InvariantInfo);
        }
    }
}