﻿using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MinecraftCubed.Common.Extensions
{
    public static class ResourcesExtensions
    {
        public static IHtmlString Resource(this HtmlHelper htmlHelper, string message, params object[] args)
        {
            var parameters = args.Select(htmlHelper.Encode).ToArray();
            return new HtmlString(string.Format(message, parameters));
        }
    }
}