﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MinecraftCubed.Common.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> OrderByIf<T, TKey>(this IEnumerable<T> source, bool condition, string order, Func<T, TKey> keySelector)
        {
            if (condition)
            {
                if (order == "desc")
                {
                    return source.OrderByDescending(keySelector).AsEnumerable();
                }

                return source.OrderBy(keySelector).AsEnumerable();
            }

            return source;
        }
    }
}