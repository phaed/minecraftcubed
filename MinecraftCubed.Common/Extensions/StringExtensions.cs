﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using MinecraftCubed.Common.Helpers;

namespace MinecraftCubed.Common.Extensions
{
    public static class StringExtensions
    {
        public static string Capitalize(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            var words = input.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var result = words.Select(w => w[0].ToString(CultureInfo.InvariantCulture).ToUpper() + w.ToLower().Remove(0, 1)).Aggregate((s, w) => s + ' ' + w);
            return result.Trim();
        }

        public static bool IsLowerCase(this string text)
        {
            return !string.IsNullOrEmpty(text) && text.All(c => !char.IsLetter(c) || char.IsLower(c));
        }

        public static string ToIdentifier(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }

            var arr = Utils.RemoveDiacritics(str.Trim()).ToCharArray();

            arr = Array.FindAll(arr, (c => (char.IsLetterOrDigit(c) || char.IsWhiteSpace(c) || c == '-' || c == '.')));
            var newStr = new string(arr);
            
            return newStr.Replace(@"\s+", " ").Replace(" -", "-").Replace("- ", "-").Replace(' ', '-').Replace('.', '-').ToLower();
        }

        public static string ToTag(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            var arr = Utils.RemoveDiacritics(input.Trim()).ToCharArray();

            arr = Array.FindAll(arr, (c => (char.IsLetterOrDigit(c) || char.IsWhiteSpace(c) || c == '-' || c == '.')));
            var newStr = new string(arr);
            newStr = newStr.TrimEnd('.', '-', ' ').TrimStart('.', '-', ' ');

            Regex.Replace(newStr, @"\s+", " ");
            
            var words = newStr.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var result = words.Select(w =>
            {
                if (w.IsLowerCase() && w.Length> 3)
                {
                    return w[0].ToString(CultureInfo.InvariantCulture).ToUpper() + w.ToLower().Remove(0, 1);
                }

                return w;

            }).Aggregate((s, w) => s + ' ' + w);

            return result.Trim();
        }

        public static string BreakCache(this string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return url;
            }

            if (url.Contains("http"))
            {
                return url + "?c=" + EncryptionHelper.GenerateConfirmationCode(10);
            }

            return url;
        }
    }
}