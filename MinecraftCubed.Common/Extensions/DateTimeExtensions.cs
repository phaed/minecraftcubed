﻿using System;
using System.Globalization;

namespace MinecraftCubed.Common.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime RoundUpMinutes(this DateTime d)
        {
            var rounded = new DateTime(d.Year, d.Month, d.Day, d.Hour, d.Minute, 0);

            var rnum = Math.Ceiling(d.Minute / 10.0) * 10;
            var offset = rnum - d.Minute;

            return rounded.AddMinutes(offset);
        }

        public static string DayOfWeekLocalized(this DateTime d)
        {
            if (DateTimeFormatInfo.CurrentInfo != null)
            {
                return DateTimeFormatInfo.CurrentInfo.GetDayName(d.DayOfWeek);
            }
            return d.DayOfWeek.ToString();
        }

        public static string ToISOString(this DateTime d)
        {
            return d.ToString("yyyy-MM-ddTHH:mm:ssZ");
        }
    }
}