﻿using MinecraftCubed.Common.Helpers;
using Omu.ValueInjecter;

namespace MinecraftCubed.Common.Extensions
{
    public static class ObjectExtensions
    {
        public static string ToJson(this object obj)
        {
            return JsonHelper.Serialize(obj);
        }
    }
}
