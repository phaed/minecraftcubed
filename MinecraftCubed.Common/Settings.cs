﻿using System;
using System.Configuration;
using System.Web;
using MinecraftCubed.Static;

namespace MinecraftCubed.Common
{
    public class Settings
    {
        public static bool RedisCachingDisabled
        {
            get { return Convert.ToBoolean(ConfigurationManager.AppSettings["RedisCachingDisabled"]); }
        }        
        
        public static string AzureConnectionString
        {
            get
            {
                string connectionString;

                if (Settings.InDevelopment)
                {
                    connectionString = "UseDevelopmentStorage=true";
                }
                else
                {
                    var account = ConfigurationManager.AppSettings["StorageAccountName"];
                    var key = ConfigurationManager.AppSettings["StorageAccountAccessKey"];
                    connectionString = String.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}", account, key);
                }

                return connectionString;
            }
        }

        public static bool PersistLogin
        {
            get { return Convert.ToBoolean(ConfigurationManager.AppSettings["PersistLogin"]); }
        }

        public static bool Profiling
        {
            get { return Convert.ToBoolean(ConfigurationManager.AppSettings["Profiling"]); }
        }

        public static bool InDevelopment
        {
            get { return Convert.ToBoolean(ConfigurationManager.AppSettings["InDevelopment"]); }
        }

        public static bool RequiresSecure
        {
            get { return Convert.ToBoolean(ConfigurationManager.AppSettings["RequiresSecure"]); }
        }

        public static bool DisableAnimatedGifs
        {
            get { return Convert.ToBoolean(ConfigurationManager.AppSettings["DisableAnimatedGifs"]); }
        }

        public static bool RePullAllLogos
        {
            get { return Convert.ToBoolean(ConfigurationManager.AppSettings["RePullAllLogos"]); }
        }
        public static bool GiveFreeMonthSponsorship
        {
            get { return Convert.ToBoolean(ConfigurationManager.AppSettings["GiveFreeMonthSponsorship"]); }
        }

        public static int FreeSponsorshipMaxServerCount
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["FreeSponsorshipMaxServerCount"]); }
        }

        public static int FreeMonthBidMin
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["FreeMonthBidMin"]); }
        }

        public static int FreeMonthBidMax
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["FreeMonthBidMax"]); }
        }

        public static int FreeMonthBidOdds
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["FreeMonthBidOdds"]); }
        }

        public static int ActivityBufferDays
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["ActivityBufferDays"]); }
        }

        public static int CookieExpireDays
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["CookieExpireDays"]); }
        }

        public static int MaxTagSize
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["MaxTagSize"]); }
        }

        public static int MaxEventDuration
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["MaxEventDuration"]); }
        }

        public static int MinimumBidForFeatured
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["MinimumBidForFeatured"]); }
        }

        public static int NonFeaturedMaxEventDates
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["NonFeaturedMaxEventDates"]); }
        }

        public static int FeaturedMaxEventDates
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["FeaturedMaxEventDates"]); }
        }

        public static int EventDateDeletePointOfNoReturnHours
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["EventDateDeletePointOfNoReturnHours"]); }
        }

        public static int EventDateMustBeDaysInTheFuture
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["EventDateMustBeDaysInTheFuture"]); }
        }

        public static string MailChimpAPIKey
        {
            get { return ConfigurationManager.AppSettings["MailChimpAPIKey"]; }
        }

        public static string SafeBrowsingAPIKey
        {
            get { return ConfigurationManager.AppSettings["SafeBrowsingAPIKey"]; }
        }

        public static string PaypalEmail
        {
            get { return InDevelopment ? Constant.Email.PaypalEmailDev : Constant.Email.PaypalEmail; }
        }

        public static string AzureStorageUrl
        {
            get { return InDevelopment ? Constant.AzureStorageLocalUrl : Constant.AzureStorageUrl; }
        }

        public static string Website
        {
            get { return InDevelopment ? Constant.WebsiteDev : Constant.Website; }
        }

        public static string PaypalEndpoint
        {
            get { return InDevelopment ? Constant.PaypalEndpointDev : Constant.PaypalEndpoint; }
        }

        public static string AWSAccessKey
        {
            get { return InDevelopment ? ConfigurationManager.AppSettings["DEV_AWS_ACCESS_KEY_ID"] : ConfigurationManager.AppSettings["AWS_ACCESS_KEY_ID"]; }
        }

        public static string AWSSecret
        {
            get { return InDevelopment ? ConfigurationManager.AppSettings["DEV_AWS_SECRET_KEY"] : ConfigurationManager.AppSettings["AWS_SECRET_KEY"]; }
        }

        public static string FBAppId
        {
            get { return ConfigurationManager.AppSettings["FBAppId"]; }
        }

        public static string FBAppSecret
        {
            get { return ConfigurationManager.AppSettings["FBAppSecret"]; }
        }

        public static string IsMobileDevice
        {
            get { return HttpContext.Current.Request.Browser.IsMobileDevice ? "true" : "false"; }
        }

        public static string WebRoot
        {
            get
            {
                var url = "http://" + HttpContext.Current.Request.Url.Host;

                if (HttpContext.Current.Request.ApplicationPath != "/")
                {
                    url += HttpContext.Current.Request.ApplicationPath;
                }

                if (HttpContext.Current.Request.Url.Port != 80)
                {
                    url += ":" + HttpContext.Current.Request.Url.Port;
                }

                return url;
            }
        }
    }
}